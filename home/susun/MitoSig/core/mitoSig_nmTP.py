import os,subprocess
import shlex


#----------------Where To Run the Programm, the template hhmBase,query profile and the result stored-------
modelPath ='/export2/susun/MitoSig/modelpath/'     #path to models
dataPath = '/export2/susun/MitoSig/datapath/'    #path to query files
inputPath = '/export2/susun/MitoSig/morfsite_lite/inputs/'
outputPath = '/export2/susun/MitoSig/morfsite_lite/outputs/' #path to outputs
#--------------------------------------command line options-------------------------------------------------
import sys, getopt

queryfile=''
outputfile=''

try:
    myopts, args = getopt.getopt(sys.argv[1:],"q:o:")
    #print myopts
except getopt.GetoptError as e:
    print (str(e))
    print("Usage: %s -q queryfile -o outputfile" % sys.argv[0])
    sys.exit(2)

for o, a in myopts:
    if o == '-q':
        queryfile=a
    elif o == '-o':
        outputfile=a
    else:
        print("Usage: %s -q query -o output" % sys.argv[0])

#-----------------------------------------------------------------------------------------------------
def parseFasta(infile, second = 'OFF'):
    inf = open(infile,'r')
    lines = inf.readlines()
    inf.close()
    ID = []
    site = []
    seqs = {}
    idSeqs = {}
    n = -1
    for i in lines:
        if '>' in i:
            if len(shlex.split(i))>1:
                ID.append(shlex.split(i)[0][1:])
                if second == 'ON':
                    site.append(i.split(' ')[1])
            else:
                ID.append(i.strip()[1:])
            n = n+1
            seqs[str(n)] = ''
        else:
            seqs[str(n)] = seqs[str(n)]+i.rstrip()
    for j in range(n+1):
       idSeqs[ID[j]] = seqs[str(j)]
       if second == 'ON':
           pep = seqs[str(j)][:int(site[j])]
           nonSi = seqs[str(j)][int(site[j]):]
           idSeqs[ID[j]] = [seqs[str(j)],site[j],pep,nonSi]
    return idSeqs


def parseHMM2searchOutput(outputfile,sigModel,loc):
    infile = open(outputfile,'r')
    lines = infile.readlines()
    infile.close()
    sw = 0
    newlist = []
    for i in lines:
        if i == 'Alignments of top-scoring domains:\n':
            sw = 1
        if i == 'Histogram of all scores:\n':
            sw = 0
        if sw == 1 :
            newlist.append(i)
    if len(newlist)>3:
        motifs = {}
        titles = []
        l = len(newlist)-1
        for i in range(1,l,5):
            j = newlist[i]
            title = shlex.split(j)[0][:-1]
            if not title in titles:
                titles.append(title)
                motifs[title]=[]
            #m = newlist[i+3]
            #mst = shlex.split(m)[1]
            #msto = shlex.split(m)[3]
            #motif = shlex.split(m)[2]
            #e = int(shlex.split(j)[-1])
            mst = int(shlex.split(j)[6])-1
            msto = int(shlex.split(j)[8][:-1])
            motifs[title].append([mst,msto,sigModel,loc])
    else:
        motifs = 'NULL'
    return motifs



def nonMitoSig(queryfile):
    subprocess.call('hmmsearch -Z 1000 -E 40 '+modelPath+'beta_298.hmm '+inputPath+queryfile+' >tmpoutput',shell=True)
    motifB = parseHMM2searchOutput('tmpoutput','betaSig','Mitochondria outer membrane')
    subprocess.call('hmmsearch -Z 1000 -E 2.3 '+modelPath+'mdm_35.hmm '+inputPath+queryfile+' >tmpoutput',shell=True)
    motifBm = parseHMM2searchOutput('tmpoutput','betaSig','Mitochondira outer membrane')
    subprocess.call('hmmsearch -Z 1000 -E 0.42 '+modelPath+'carrier_4475.hmm '+inputPath+queryfile+' >tmpoutput',shell=True)
    motifC = parseHMM2searchOutput('tmpoutput','carrierSig','Mitochondria inner membrane')
    subprocess.call('hmmsearch -Z 1000 -E 2.8 '+modelPath+'cys9_840.hmm '+inputPath+queryfile+' >tmpoutput',shell=True)
    motifC9 = parseHMM2searchOutput('tmpoutput','cysSig','Mitochondria inter membrane')
    subprocess.call('hmmsearch -Z 1000 -E 1.1 '+modelPath+'cys-st_259.hmm '+inputPath+queryfile+' >tmpoutput',shell=True)
    motifCs = parseHMM2searchOutput('tmpoutput','cysSig','Mitochondria inter membrane')
    return motifB,motifBm,motifC,motifC9,motifCs



def writeFresult(queryfile,outputfile):
    motifs = {}
    idSeq = parseFasta(inputPath+queryfile)
    motifB,motifBm,motifC,motifC9,motifCs = nonMitoSig(queryfile)
    for i in idSeq.keys():
        motifs[i]=[]
        if motifB!='NULL' and i in motifB.keys():
            motifs[i]=motifs[i]+motifB[i]
        if motifBm!='NULL' and i in motifBm.keys():
            motifs[i]=motifs[i]+motifBm[i]
        if motifC!='NULL' and i in motifC.keys():
            motifs[i]=motifs[i]+motifC[i]
        if motifC9!='NULL' and i in motifC9.keys():
            motifs[i]=motifs[i]+motifC9[i]
        if motifCs!='NULL' and i in motifCs.keys():
            motifs[i]=motifs[i]+motifCs[i]
    outfile = open(outputPath + outputfile,'w')
    outfile.write('\n\n{:*^100}'.format('mitoSig v1.1 prediction results')+'\n' )
    outfile.write('Number of query sequences: '+str(len(motifs))+'\n')
    outfile.write('Using NON-mTP mitochondiral signal models\n\n')
    outfile.write('{0:50}{1:20}{2:20}{3:20}'.format('NAME','motifSite','sigType','loc')+'\n')
    outfile.write('{:-^100}'.format('')+'\n')
    for j in motifs.keys():
        if motifs[j]==[]:
            outfile.write('{0:50}{1:20}{2:20}{3:20}'.format(j[:40],'-','-','-')+'\n')
        else:
            tmp = motifs[j]
            outfile.write('{0:50}{1:20}{2:20}{3:20}'.format(j[:40],str(tmp[0][0])+'-'+str(tmp[0][1]),tmp[0][2],tmp[0][3])+'\n')
            if len(tmp)>0:
                for m in motifs[j][1:]:
                    outfile.write('{0:50}{1:20}{2:20}{3:20}'.format('',str(m[0])+'-'+str(m[1]),m[2],m[3])+'\n')
    outfile.close()

       




writeFresult(queryfile,outputfile) 
