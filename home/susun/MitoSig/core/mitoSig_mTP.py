import os,subprocess
import re
import operator
import time
from random import sample
from orthologsLocal_N import *
from ToptiQuery import *
os.environ["http_proxy"]="http://proxy01.biochem.mpg.de:3128"
os.environ['HHLIB']= '/export2/susun/hhsuit/lib/hh/'
os.environ['PATH'] += os.pathsep +'/export2/susun/hhsuit/lib/hh/bin:/export2/susun/hhsuit/lib/hh/scripts'
os.environ['PERL5LIB'] = '/export2/susun/hhsuit/lib/hh/scripts'
#----------------Where To Run the Programm, the template hhmBase,query profile and the result stored-------
modelPath ='/export2/susun/MitoSig/modelpath/'     #path to models
dataPath = '/export2/susun/MitoSig/datapath/'    #path to query files
inputPath = '/export2/susun/MitoSig/morfsite_lite/inputs/'
outputPath = '/export2/susun/MitoSig/morfsite_lite/outputs/' #path to outputs
#--------------------------------------command line options-------------------------------------------------
import sys, getopt

queryFasta=''
output=''

###############################
# o == option
# a == argument passed to the o
###############################
# Cache an error with try..except 
# Note: options is the string of option letters that the script wants to recognize, with 
# options that require an argument followed by a colon (':') i.e. -i fileName
#
try:
    myopts, args = getopt.getopt(sys.argv[1:],"q:o:")
    #print myopts
except getopt.GetoptError as e:
    print (str(e))
    print("Usage: %s -q query -o output" % sys.argv[0])
    sys.exit(2)

for o, a in myopts:
    if o == '-q':
        queryFasta=a
    elif o == '-o':
        output=a   
    else:
        print("Usage: %s -q query -o output" % sys.argv[0])


#print queryFasta,output,cle,stop
# Display input and output file name passed as the args
#print ("Input file : %s and output file: %s" +queryFasta+','+templateFasta+','+output)
#------------------------Job ID-------------------------------------------------------------------
def getJobID(infile):             #read a fasta file with one sequence, get only ID
    inf = open(inputPath+infile,'r')
    lines = inf.readlines()
    inf.close()
    lines=[x for x in lines if x.strip()]
    i=lines[0].strip()
    try:
        if len(i.split(' '))>1:
            tmp = i.split(' ')[0].replace('>','')
        else:
            tmp = i.replace('>','')
    except:
        text_content='Please enter protein sequence in fasta format:)'
    ID = re.sub("[^a-zA-Z\d:]+", "", tmp)
    return ID
ID = getJobID(queryFasta)
#----------------------------------------------------------------------------------------------------
def parseSinFasta(infile):             #read a fasta file with one sequence
    inf = open(infile,'r')
    lines = inf.readlines()
    inf.close()
    lines=[x for x in lines if x.strip()]
    for i in lines:
        if '>' in i:
            if len(i.split(' '))>1:
                iD = i.split(' ')[0]
            else:
                iD = i.rstrip('\n')
            seq =''
        else:
            seq = seq+i.rstrip('\n')
    return iD,seq
 
def removeFiles(path,pattern):
    for f in os.listdir(path):
        if pattern in f:
            os.remove(path+f)



def scoring(queryFasta,dataPath,output,cle,stop):
    print ("scoring...")
    #iD,seq = parseSinFasta(dataPath+queryFasta)
    outFile = oneExe(queryFasta,ID)
    tmp = outFile.split('.')
    print (tmp[0])
    subprocess.call('mafft --auto --quiet '+outFile+' >'+tmp[0],shell=True)
    subprocess.call('reformat.pl fas a2m '+tmp[0]+' '+tmp[0]+'.a2m',shell=True)
    scoredFrag = mainSearch(tmp[0]+'.a2m',qPath=dataPath+ID,opath = outputPath, db=modelPath+"mTP.hhm",ST=cle,STO=stop,iD = ID) 
    #removeFiles(dataPath,'.hhm')
    #removeFiles(dataPath,'.a2m')
    sortedID = sorted(scoredFrag.items(),key=operator.itemgetter(1))
    #sortedID.reverse()
    file = open(outputPath+output,'w')
    for frag,score in sortedID:
        file.write('{0:10} ==> {1:10f}\n'.format(frag, score))    
    return sortedID

sortedID = scoring(queryFasta,dataPath,output,cle=0,stop=100)

