Query         5_Mustela
Match_columns 6
No_of_seqs    4 out of 20
Neff          1.4 
Searched_HMMs 436
Date          Wed Sep  6 16:08:07 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_15A21_15Q21_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P11177_30_MM_ref_sig5_130       39.0    0.14 0.00032   13.0   0.0    6    1-6      16-21  (35)
  2 P10818_26_IM_ref_sig5_130       36.3    0.16 0.00037   12.7   0.0    6    1-6      12-17  (31)
  3 P13182_24_IM_ref_sig5_130       22.4    0.41 0.00094   11.6   0.0    6    1-6      10-15  (29)
  4 Q95108_59_Mito_ref_sig5_130      4.5       4  0.0091   10.2   0.0    3    4-6       7-9   (64)
  5 Q9ZP06_22_MM_ref_sig5_130        4.4     4.1  0.0094    8.8   0.0    4    3-6      17-20  (27)
  6 Q9SVM8_34_Mito_ref_sig5_130      4.2     4.2  0.0097    9.4   0.0    4    2-5       9-12  (39)
  7 P16622_31_IM_ref_sig5_130        4.1     4.4    0.01    9.3   0.0    4    3-6      12-15  (36)
  8 Q9NVH6_15_MM_ref_sig5_130        4.0     4.5    0.01    8.2   0.0    4    3-6      16-19  (20)
  9 Q99757_59_Mito_ref_sig5_130      3.8     4.9   0.011   10.0   0.0    3    4-6       7-9   (64)
 10 O82662_26_Mito_ref_sig5_130      3.7     5.1   0.012    8.9   0.0    3    2-4       3-5   (31)
 11 P36526_16_Mito_ref_sig5_130      3.4     5.7   0.013    7.9   0.0    4    3-6      16-19  (21)
 12 Q07511_25_Mito_ref_sig5_130      3.1     6.1   0.014    8.5   0.0    1    2-2      20-20  (30)
 13 P38447_48_Mito_ref_sig5_130      3.0     6.5   0.015    9.3   0.0    1    3-3      36-36  (53)
 14 P35434_22_Mito_IM_ref_sig5_130   2.9     6.6   0.015    8.3   0.0    3    3-5       6-8   (27)
 15 Q02380_46_IM_ref_sig5_130        2.8       7   0.016    9.3   0.0    1    3-3      32-32  (51)
 16 P83373_19_MM_ref_sig5_130        2.8       7   0.016    8.0   0.0    1    5-5      16-16  (24)
 17 P82928_72_Mito_ref_sig5_130      2.6     7.6   0.017    9.8   0.0    1    3-3      16-16  (77)
 18 Q5BJX1_13_Mito_ref_sig5_130      2.6     7.8   0.018    7.4   0.0    2    3-4      11-12  (18)
 19 Q02376_27_IM_ref_sig5_130        2.3     8.8    0.02    8.2   0.0    1    4-4       7-7   (32)
 20 Q5JRX3_15_MM_ref_sig5_130        2.2     9.2   0.021    7.2   0.0    1    4-4      13-13  (20)

No 1  
>P11177_30_MM_ref_sig5_130
Probab=38.98  E-value=0.14  Score=13.04  Aligned_cols=6  Identities=83%  Similarity=1.342  Sum_probs=2.9

Q 5_Mustela         1 SGLLRR    6 (6)
Q 2_Macaca          1 SGLLKR    6 (6)
Q 18_Ceratotheri    1 SGFLRR    6 (6)
Q 4_Pongo           1 SRLLKR    6 (6)
Q Consensus         1 sgll~r    6 (6)
                      |+++||
T Consensus        16 s~~lrR   21 (35)
T signal           16 SGLLKR   21 (35)
T 143              20 SGLPRR   25 (39)
T 144              16 SVFLKR   21 (35)
T 145              16 AVVLQR   21 (35)
T 147              16 SVVLRR   21 (35)
T 135              25 RLQLRS   30 (44)
T 141              30 RLQQRR   35 (49)
T 142              15 LVLGRR   20 (34)
T 149              15 SALQRR   20 (34)
T 150              15 SAVRRR   20 (34)
Confidence            345554


No 2  
>P10818_26_IM_ref_sig5_130
Probab=36.33  E-value=0.16  Score=12.69  Aligned_cols=6  Identities=83%  Similarity=1.138  Sum_probs=3.4

Q 5_Mustela         1 SGLLRR    6 (6)
Q 2_Macaca          1 SGLLKR    6 (6)
Q 18_Ceratotheri    1 SGFLRR    6 (6)
Q 4_Pongo           1 SRLLKR    6 (6)
Q Consensus         1 sgll~r    6 (6)
                      ||||.|
T Consensus        12 SgLLgR   17 (31)
T signal           12 SGLLGR   17 (31)
T 53                9 SGLLLR   14 (28)
T 56                9 FGLPVR   14 (28)
T 64                9 SRLLGR   14 (28)
T 36               11 SLLLGG   16 (30)
T 44                9 SRLLGR   14 (28)
T 55                9 SRLLNW   14 (28)
T 6                11 LRHLGR   16 (30)
T 7                10 SGLLGR   15 (29)
T 43                7 SGILRR   12 (25)
Confidence            566654


No 3  
>P13182_24_IM_ref_sig5_130
Probab=22.44  E-value=0.41  Score=11.56  Aligned_cols=6  Identities=67%  Similarity=0.861  Sum_probs=3.0

Q 5_Mustela         1 SGLLRR    6 (6)
Q 2_Macaca          1 SGLLKR    6 (6)
Q 18_Ceratotheri    1 SGFLRR    6 (6)
Q 4_Pongo           1 SRLLKR    6 (6)
Q Consensus         1 sgll~r    6 (6)
                      ||||-|
T Consensus        10 s~lLgr   15 (29)
T signal           10 FGLLGR   15 (29)
T 11               12 SRPLGR   17 (31)
T 16               10 SRLLGR   15 (29)
T 19               10 SGLLSR   15 (29)
T 26               10 SRLLGR   15 (29)
T 30               10 FGLPVR   15 (29)
T 33               10 SGLLLR   15 (29)
Confidence            456543


No 4  
>Q95108_59_Mito_ref_sig5_130
Probab=4.48  E-value=4  Score=10.20  Aligned_cols=3  Identities=100%  Similarity=1.486  Sum_probs=1.4

Q 5_Mustela         4 LRR    6 (6)
Q 2_Macaca          4 LKR    6 (6)
Q 18_Ceratotheri    4 LRR    6 (6)
Q 4_Pongo           4 LKR    6 (6)
Q Consensus         4 l~r    6 (6)
                      |||
T Consensus         7 LRR    9 (64)
T signal            7 LRR    9 (64)
T 82                7 LRR    9 (65)
T 80                7 LRR    9 (62)
T 79                7 LRR    9 (66)
T 58                7 LRR    9 (47)
T 77                7 LKR    9 (67)
T 78                7 LRR    9 (70)
T 76                6 LRP    8 (68)
Confidence            444


No 5  
>Q9ZP06_22_MM_ref_sig5_130
Probab=4.36  E-value=4.1  Score=8.78  Aligned_cols=4  Identities=50%  Similarity=1.165  Sum_probs=1.7

Q 5_Mustela         3 LLRR    6 (6)
Q 2_Macaca          3 LLKR    6 (6)
Q 18_Ceratotheri    3 FLRR    6 (6)
Q 4_Pongo           3 LLKR    6 (6)
Q Consensus         3 ll~r    6 (6)
                      +|||
T Consensus        17 vlRR   20 (27)
T signal           17 VIRR   20 (27)
T 93               17 VSRR   20 (27)
T 95               17 LLRR   20 (27)
T 97               18 LLTR   21 (28)
T 91               21 LLRR   24 (31)
T 94               17 V-RR   19 (26)
T 98               16 ILRR   19 (26)
Confidence            3444


No 6  
>Q9SVM8_34_Mito_ref_sig5_130
Probab=4.24  E-value=4.2  Score=9.43  Aligned_cols=4  Identities=100%  Similarity=1.605  Sum_probs=1.9

Q 5_Mustela         2 GLLR    5 (6)
Q 2_Macaca          2 GLLK    5 (6)
Q 18_Ceratotheri    2 GFLR    5 (6)
Q 4_Pongo           2 RLLK    5 (6)
Q Consensus         2 gll~    5 (6)
                      +|||
T Consensus         9 ~lLR   12 (39)
T signal            9 GLLR   12 (39)
T 10                9 SLVR   12 (40)
T 8                 9 NLLR   12 (40)
T 9                 9 NVLR   12 (40)
T 11                9 NILR   12 (40)
T 4                 9 NLLR   12 (44)
T 5                 9 SLLR   12 (41)
T 6                 9 SLLR   12 (41)
T 7                 9 GLLR   12 (41)
T 3                 9 GLLR   12 (49)
Confidence            3454


No 7  
>P16622_31_IM_ref_sig5_130
Probab=4.10  E-value=4.4  Score=9.29  Aligned_cols=4  Identities=75%  Similarity=1.281  Sum_probs=1.8

Q 5_Mustela         3 LLRR    6 (6)
Q 2_Macaca          3 LLKR    6 (6)
Q 18_Ceratotheri    3 FLRR    6 (6)
Q 4_Pongo           3 LLKR    6 (6)
Q Consensus         3 ll~r    6 (6)
                      ||||
T Consensus        12 flrr   15 (36)
T signal           12 FLRR   15 (36)
Confidence            4544


No 8  
>Q9NVH6_15_MM_ref_sig5_130
Probab=4.00  E-value=4.5  Score=8.22  Aligned_cols=4  Identities=50%  Similarity=0.808  Sum_probs=1.9

Q 5_Mustela         3 LLRR    6 (6)
Q 2_Macaca          3 LLKR    6 (6)
Q 18_Ceratotheri    3 FLRR    6 (6)
Q 4_Pongo           3 LLKR    6 (6)
Q Consensus         3 ll~r    6 (6)
                      ||||
T Consensus        16 Llk~   19 (20)
T signal           16 LLKG   19 (20)
T 63               16 LLKG   19 (20)
T 65               16 FMKG   19 (20)
T 106              16 LLKG   19 (20)
T 109              16 LLRR   19 (20)
T 99               16 LMKR   19 (20)
T 104              16 LLKR   19 (20)
T 96               16 LLNR   19 (19)
T 95               17 LLKS   20 (21)
Confidence            4553


No 9  
>Q99757_59_Mito_ref_sig5_130
Probab=3.76  E-value=4.9  Score=9.96  Aligned_cols=3  Identities=100%  Similarity=1.486  Sum_probs=1.3

Q 5_Mustela         4 LRR    6 (6)
Q 2_Macaca          4 LKR    6 (6)
Q 18_Ceratotheri    4 LRR    6 (6)
Q 4_Pongo           4 LKR    6 (6)
Q Consensus         4 l~r    6 (6)
                      |||
T Consensus         7 LRR    9 (64)
T signal            7 LRR    9 (64)
T 80                7 LRR    9 (62)
T 82                7 LRR    9 (64)
T 81                7 LRR    9 (65)
T 88                7 LRR    9 (64)
T 78                7 LRR    9 (66)
T 61                7 LRR    9 (47)
T 65                7 LRR    9 (43)
T 77                7 LRR    9 (70)
T 76                6 LRP    8 (68)
Confidence            444


No 10 
>O82662_26_Mito_ref_sig5_130
Probab=3.66  E-value=5.1  Score=8.88  Aligned_cols=3  Identities=67%  Similarity=1.376  Sum_probs=1.3

Q 5_Mustela         2 GLL    4 (6)
Q 2_Macaca          2 GLL    4 (6)
Q 18_Ceratotheri    2 GFL    4 (6)
Q 4_Pongo           2 RLL    4 (6)
Q Consensus         2 gll    4 (6)
                      |+|
T Consensus         3 gll    5 (31)
T signal            3 GLV    5 (31)
T 160               3 GSL    5 (31)
T 165               3 GLL    5 (31)
T 166               3 GLL    5 (31)
T 170               3 GML    5 (31)
T 171               3 GLV    5 (31)
T 157               1 -LL    2 (28)
Confidence            444


No 11 
>P36526_16_Mito_ref_sig5_130
Probab=3.35  E-value=5.7  Score=7.93  Aligned_cols=4  Identities=50%  Similarity=0.609  Sum_probs=1.7

Q 5_Mustela         3 LLRR    6 (6)
Q 2_Macaca          3 LLKR    6 (6)
Q 18_Ceratotheri    3 FLRR    6 (6)
Q 4_Pongo           3 LLKR    6 (6)
Q Consensus         3 ll~r    6 (6)
                      +|+|
T Consensus        16 ~LtR   19 (21)
T signal           16 ALTR   19 (21)
T 11               16 LLTR   19 (21)
T 13               16 QLTR   19 (21)
T 14               16 QLTR   19 (21)
T 18               15 LLTR   18 (20)
T 20               16 NLLR   19 (21)
T 5                16 NLRR   19 (21)
T 7                16 SLRR   19 (21)
T 8                16 NLRR   19 (21)
T 9                12 LLKR   15 (17)
Confidence            3444


No 12 
>Q07511_25_Mito_ref_sig5_130
Probab=3.15  E-value=6.1  Score=8.49  Aligned_cols=1  Identities=0%  Similarity=-1.459  Sum_probs=0.3

Q 5_Mustela         2 G    2 (6)
Q 2_Macaca          2 G    2 (6)
Q 18_Ceratotheri    2 G    2 (6)
Q 4_Pongo           2 R    2 (6)
Q Consensus         2 g    2 (6)
                      +
T Consensus        20 ~   20 (30)
T signal           20 L   20 (30)
T 64               22 T   22 (32)
T 65               22 G   22 (32)
T 60               26 S   26 (36)
T 61               26 G   26 (36)
T 62               20 S   20 (30)
T 56               21 G   21 (33)
T 58               21 H   21 (33)
Confidence            2


No 13 
>P38447_48_Mito_ref_sig5_130
Probab=2.97  E-value=6.5  Score=9.32  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.4

Q 5_Mustela         3 L    3 (6)
Q 2_Macaca          3 L    3 (6)
Q 18_Ceratotheri    3 F    3 (6)
Q 4_Pongo           3 L    3 (6)
Q Consensus         3 l    3 (6)
                      |
T Consensus        36 L   36 (53)
T signal           36 L   36 (53)
T 102              35 L   35 (46)
T 113              31 L   31 (48)
T 111              34 L   34 (50)
T 63               32 L   32 (41)
T 93               34 L   34 (44)
T 97               35 L   35 (51)
T 103              38 F   38 (49)
T 96               33 V   33 (38)
T 101              34 L   34 (44)
Confidence            3


No 14 
>P35434_22_Mito_IM_ref_sig5_130
Probab=2.94  E-value=6.6  Score=8.29  Aligned_cols=3  Identities=100%  Similarity=1.409  Sum_probs=1.2

Q 5_Mustela         3 LLR    5 (6)
Q 2_Macaca          3 LLK    5 (6)
Q 18_Ceratotheri    3 FLR    5 (6)
Q 4_Pongo           3 LLK    5 (6)
Q Consensus         3 ll~    5 (6)
                      +|+
T Consensus         6 ~L~    8 (27)
T signal            6 LLR    8 (27)
T 160               6 LLR    8 (27)
T 152               6 VFR    8 (27)
T 140               5 VLR    7 (25)
T 143               5 ALL    7 (33)
T 132               6 LLL    8 (34)
T 137               6 LLA    8 (29)
T 121               6 FLL    8 (25)
T 127               6 FL-    7 (24)
T 120               6 ALF    8 (26)
Confidence            443


No 15 
>Q02380_46_IM_ref_sig5_130
Probab=2.82  E-value=7  Score=9.27  Aligned_cols=1  Identities=0%  Similarity=0.666  Sum_probs=0.3

Q 5_Mustela         3 L    3 (6)
Q 2_Macaca          3 L    3 (6)
Q 18_Ceratotheri    3 F    3 (6)
Q 4_Pongo           3 L    3 (6)
Q Consensus         3 l    3 (6)
                      |
T Consensus        32 f   32 (51)
T signal           32 F   32 (51)
T 55               32 F   32 (51)
T 59               32 L   32 (51)
T 51               36 L   36 (57)
T 94               32 L   32 (42)
T 49               35 -   34 (52)
T 46               31 L   31 (51)
T 42               42 I   42 (60)
T 24               29 F   29 (44)
T 40               29 V   29 (48)
Confidence            2


No 16 
>P83373_19_MM_ref_sig5_130
Probab=2.80  E-value=7  Score=8.04  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.3

Q 5_Mustela         5 R    5 (6)
Q 2_Macaca          5 K    5 (6)
Q 18_Ceratotheri    5 R    5 (6)
Q 4_Pongo           5 K    5 (6)
Q Consensus         5 ~    5 (6)
                      |
T Consensus        16 R   16 (24)
T signal           16 R   16 (24)
T 147              19 R   19 (27)
T 151              19 R   19 (27)
T 153              22 R   22 (30)
T 150              19 R   19 (27)
T 141              15 R   15 (22)
T 146              15 R   15 (22)
T 149              15 A   15 (22)
T 152              18 R   18 (25)
Confidence            2


No 17 
>P82928_72_Mito_ref_sig5_130
Probab=2.63  E-value=7.6  Score=9.81  Aligned_cols=1  Identities=0%  Similarity=0.666  Sum_probs=0.3

Q 5_Mustela         3 L    3 (6)
Q 2_Macaca          3 L    3 (6)
Q 18_Ceratotheri    3 F    3 (6)
Q 4_Pongo           3 L    3 (6)
Q Consensus         3 l    3 (6)
                      |
T Consensus        16 f   16 (77)
T signal           16 F   16 (77)
T 46               16 F   16 (76)
T 56               16 F   16 (76)
T 52               16 F   16 (76)
T 54               16 F   16 (75)
T 61               16 F   16 (77)
T 45               16 F   16 (75)
T 26                1 -    0 (30)
T 24                1 -    0 (25)
Confidence            3


No 18 
>Q5BJX1_13_Mito_ref_sig5_130
Probab=2.58  E-value=7.8  Score=7.42  Aligned_cols=2  Identities=50%  Similarity=0.966  Sum_probs=0.7

Q 5_Mustela         3 LL.    4 (6)
Q 2_Macaca          3 LL.    4 (6)
Q 18_Ceratotheri    3 FL.    4 (6)
Q 4_Pongo           3 LL.    4 (6)
Q Consensus         3 ll.    4 (6)
                      |+ 
T Consensus        11 lv.   12 (18)
T signal           11 LV.   12 (18)
T 12               11 LR.   12 (18)
T 22               11 LV.   12 (18)
T 17               11 VL.   12 (18)
T 26               11 LVl   13 (19)
T 15               20 LL.   21 (27)
T 61               11 FL.   12 (17)
T 62               11 FT.   12 (17)
Confidence            33 


No 19 
>Q02376_27_IM_ref_sig5_130
Probab=2.32  E-value=8.8  Score=8.25  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.3

Q 5_Mustela         4 L    4 (6)
Q 2_Macaca          4 L    4 (6)
Q 18_Ceratotheri    4 L    4 (6)
Q 4_Pongo           4 L    4 (6)
Q Consensus         4 l    4 (6)
                      |
T Consensus         7 l    7 (32)
T signal            7 L    7 (32)
T 10                7 L    7 (33)
T 11                7 L    7 (32)
T 17                7 L    7 (32)
T 30                7 L    7 (32)
T 13                7 L    7 (32)
T 12                7 L    7 (32)
T 8                 8 L    8 (33)
Confidence            2


No 20 
>Q5JRX3_15_MM_ref_sig5_130
Probab=2.25  E-value=9.2  Score=7.16  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.3

Q 5_Mustela         4 L    4 (6)
Q 2_Macaca          4 L    4 (6)
Q 18_Ceratotheri    4 L    4 (6)
Q 4_Pongo           4 L    4 (6)
Q Consensus         4 l    4 (6)
                      +
T Consensus        13 l   13 (20)
T signal           13 L   13 (20)
T 140              15 S   15 (19)
T 151              12 L   12 (18)
T 159              13 L   13 (19)
T 164              13 -   12 (17)
T 112              12 L   12 (18)
T 143              17 T   17 (24)
T 146              13 I   13 (19)
Confidence            3


Done!
