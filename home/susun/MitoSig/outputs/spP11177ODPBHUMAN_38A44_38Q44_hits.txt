Query         5_Mustela
Match_columns 6
No_of_seqs    2 out of 20
Neff          1.1 
Searched_HMMs 436
Date          Wed Sep  6 16:08:42 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_38A44_38Q44_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P13196_56_MM_ref_sig5_130        1.1      22   0.051    4.8   0.0    1    5-5       1-1   (61)
  2 P39697_29_Mito_ref_sig5_130      1.0      22   0.051    7.4   0.0    2    3-4      13-14  (34)
  3 P09622_35_MM_ref_sig5_130        1.0      23   0.054    3.7   0.0    1    5-5       1-1   (40)
  4 P99028_13_IM_ref_sig5_130        1.0      24   0.054    6.4   0.0    3    3-5       7-9   (18)
  5 P17694_33_IM_ref_sig5_130        0.9      25   0.058    3.7   0.0    1    5-5       1-1   (38)
  6 Q02372_28_IM_ref_sig5_130        0.9      27   0.061    3.6   0.0    1    5-5       1-1   (33)
  7 Q9BYV1_41_Mito_ref_sig5_130      0.9      28   0.064    3.7   0.0    1    5-5       1-1   (46)
  8 P21953_50_MM_ref_sig5_130        0.9      28   0.064    3.8   0.0    1    5-5       1-1   (55)
  9 P18155_35_Mito_ref_sig5_130      0.9      28   0.065    3.8   0.0    1    5-5       1-1   (40)
 10 Q0P5L8_21_Mito_Nu_ref_sig5_130   0.8      29   0.067    3.5   0.0    1    5-5       1-1   (26)
 11 Q874C1_38_IM_ref_sig5_130        0.8      29   0.067    7.3   0.0    3    3-5      36-38  (43)
 12 P15650_30_MM_ref_sig5_130        0.8      29   0.067    3.7   0.0    1    5-5       1-1   (35)
 13 P41367_25_MM_ref_sig5_130        0.8      29   0.067    3.6   0.0    1    5-5       1-1   (30)
 14 A4FUC0_29_Mito_ref_sig5_130      0.8      29   0.067    3.3   0.0    1    5-5       1-1   (34)
 15 P53193_10_MM_ref_sig5_130        0.8      30   0.069    5.9   0.0    2    2-3       6-7   (15)
 16 P08503_25_MM_ref_sig5_130        0.8      30    0.07    3.6   0.0    1    5-5       1-1   (30)
 17 Q9Y2D0_33_Mito_ref_sig5_130      0.8      31   0.072    4.9   0.0    1    5-5       1-1   (38)
 18 P05626_35_Mito_IM_ref_sig5_130   0.8      31   0.072    7.1   0.0    2    3-4      22-23  (40)
 19 P08559_29_MM_ref_sig5_130        0.8      32   0.073    3.7   0.0    1    5-5       1-1   (34)
 20 Q6UB35_31_Mito_ref_sig5_130      0.8      32   0.074    3.8   0.0    1    5-5       1-1   (36)

No 1  
>P13196_56_MM_ref_sig5_130
Probab=1.05  E-value=22  Score=4.79  Aligned_cols=1  Identities=100%  Similarity=1.431  Sum_probs=0.2

Q 5_Mustela         5 M    5 (6)
Q 12_Sorex          5 M    5 (6)
Q Consensus         5 m    5 (6)
                      |
T Consensus         1 m    1 (61)
T signal            1 M    1 (61)
T 72_Branchiosto    1 F    1 (61)
T 63_Columba        1 M    1 (61)
T 10_Canis          1 M    1 (61)
T 74_Schistosoma    1 -    0 (59)
T 72_Branchiosto    1 -    0 (59)
T 63_Columba        1 -    0 (59)
T 62_Danio          1 -    0 (59)
T 57_Saimiri        1 -    0 (59)
T 55_Taeniopygia    1 -    0 (59)
Confidence            2


No 2  
>P39697_29_Mito_ref_sig5_130
Probab=1.05  E-value=22  Score=7.36  Aligned_cols=2  Identities=100%  Similarity=1.548  Sum_probs=1.0

Q 5_Mustela         3 QG    4 (6)
Q 12_Sorex          3 QG    4 (6)
Q Consensus         3 qg    4 (6)
                      ||
T Consensus        13 qg   14 (34)
T signal           13 QG   14 (34)
T 4                13 QG   14 (33)
Confidence            44


No 3  
>P09622_35_MM_ref_sig5_130
Probab=1.00  E-value=23  Score=3.72  Aligned_cols=1  Identities=100%  Similarity=1.431  Sum_probs=0.2

Q 5_Mustela         5 M    5 (6)
Q 12_Sorex          5 M    5 (6)
Q Consensus         5 m    5 (6)
                      |
T Consensus         1 M    1 (40)
T signal            1 M    1 (40)
T 181_Paramecium    1 M    1 (38)
T 142_Physcomitr    1 M    1 (38)
T 97_Anopheles      1 L    1 (38)
T 92_Brugia         1 M    1 (38)
T 77_Tursiops       1 S    1 (38)
T 74_Ciona          1 M    1 (38)
T 62_Meleagris      1 M    1 (38)
T 17_Ceratotheri    1 M    1 (38)
Confidence            1


No 4  
>P99028_13_IM_ref_sig5_130
Probab=1.00  E-value=24  Score=6.36  Aligned_cols=3  Identities=33%  Similarity=0.523  Sum_probs=1.4

Q 5_Mustela         3 QGM    5 (6)
Q 12_Sorex          3 QGM    5 (6)
Q Consensus         3 qgm    5 (6)
                      |+|
T Consensus         7 ~~M    9 (18)
T signal            7 RKM    9 (18)
T 23                7 QKM    9 (18)
T 42                7 RNM    9 (18)
T 3                 7 QRM    9 (18)
T 14                7 RKM    9 (18)
T 24                7 LEM    9 (18)
T 59                7 K-M    8 (17)
T 62                7 QGM    9 (18)
Confidence            444


No 5  
>P17694_33_IM_ref_sig5_130
Probab=0.95  E-value=25  Score=3.75  Aligned_cols=1  Identities=100%  Similarity=1.431  Sum_probs=0.2

Q 5_Mustela         5 M    5 (6)
Q 12_Sorex          5 M    5 (6)
Q Consensus         5 m    5 (6)
                      |
T Consensus         1 M    1 (38)
T signal            1 M    1 (38)
T 203_Boea          1 M    1 (37)
T 189_Naegleria     1 M    1 (37)
T 167_Loa           1 M    1 (37)
T 125_Talaromyce    1 M    1 (37)
T 84_Andalucia      1 M    1 (37)
Confidence            2


No 6  
>Q02372_28_IM_ref_sig5_130
Probab=0.90  E-value=27  Score=3.55  Aligned_cols=1  Identities=100%  Similarity=1.431  Sum_probs=0.2

Q 5_Mustela         5 M    5 (6)
Q 12_Sorex          5 M    5 (6)
Q Consensus         5 m    5 (6)
                      |
T Consensus         1 M    1 (33)
T signal            1 M    1 (33)
T 89_Brugia         1 M    1 (31)
T 78_Nasonia        1 M    1 (31)
T 75_Bombyx         1 M    1 (31)
T 74_Branchiosto    1 M    1 (31)
T 72_Tursiops       1 M    1 (31)
T 62_Anas           1 A    1 (31)
T 57_Falco          1 M    1 (31)
T 53_Oryzias        1 M    1 (31)
T 29_Capra          1 I    1 (31)
Confidence            2


No 7  
>Q9BYV1_41_Mito_ref_sig5_130
Probab=0.86  E-value=28  Score=3.75  Aligned_cols=1  Identities=100%  Similarity=1.431  Sum_probs=0.2

Q 5_Mustela         5 M    5 (6)
Q 12_Sorex          5 M    5 (6)
Q Consensus         5 m    5 (6)
                      |
T Consensus         1 M    1 (46)
T signal            1 M    1 (46)
T 122_Ixodes        1 M    1 (43)
T 118_Zea           1 M    1 (43)
T 94_Bombus         1 M    1 (43)
T 91_Nasonia        1 M    1 (43)
T 78_Musca          1 M    1 (43)
T 74_Strongyloce    1 M    1 (43)
T 64_Meleagris      1 M    1 (43)
T 58_Ficedula       1 M    1 (43)
Confidence            2


No 8  
>P21953_50_MM_ref_sig5_130
Probab=0.86  E-value=28  Score=3.77  Aligned_cols=1  Identities=100%  Similarity=1.431  Sum_probs=0.2

Q 5_Mustela         5 M    5 (6)
Q 12_Sorex          5 M    5 (6)
Q Consensus         5 m    5 (6)
                      |
T Consensus         1 M    1 (55)
T signal            1 M    1 (55)
T 145_Nectria       1 M    1 (53)
T 134_Cyanidiosc    1 M    1 (53)
T 123_Trichinell    1 K    1 (53)
T 111_Phytophtho    1 M    1 (53)
T 110_Toxoplasma    1 M    1 (53)
T 105_Glycine       1 M    1 (53)
T 104_Plasmodium    1 M    1 (53)
T 78_Loa            1 M    1 (53)
T 76_Trichoplax     1 G    1 (53)
Confidence            1


No 9  
>P18155_35_Mito_ref_sig5_130
Probab=0.85  E-value=28  Score=3.77  Aligned_cols=1  Identities=100%  Similarity=1.431  Sum_probs=0.2

Q 5_Mustela         5 M    5 (6)
Q 12_Sorex          5 M    5 (6)
Q Consensus         5 m    5 (6)
                      |
T Consensus         1 M    1 (40)
T signal            1 M    1 (40)
T 95_Emiliania      1 M    1 (39)
T 93_Volvox         1 M    1 (39)
T 89_Ornithorhyn    1 H    1 (39)
T 77_Pediculus      1 M    1 (39)
T 75_Trichoplax     1 M    1 (39)
T 71_Saccoglossu    1 M    1 (39)
T 67_Monodelphis    1 M    1 (39)
T 41_Capra          1 M    1 (39)
Confidence            2


No 10 
>Q0P5L8_21_Mito_Nu_ref_sig5_130
Probab=0.83  E-value=29  Score=3.52  Aligned_cols=1  Identities=100%  Similarity=1.431  Sum_probs=0.3

Q 5_Mustela         5 M    5 (6)
Q 12_Sorex          5 M    5 (6)
Q Consensus         5 m    5 (6)
                      |
T Consensus         1 M    1 (26)
T signal            1 M    1 (26)
T 60                1 M    1 (26)
T 108_Galdieria     1 M    1 (23)
T 104_Cyanidiosc    1 M    1 (23)
T 102_Acanthamoe    1 M    1 (23)
T 78_Musca          1 M    1 (23)
T 76_Aplysia        1 M    1 (23)
T 74_Branchiosto    1 M    1 (23)
T 57_Loxodonta      1 M    1 (23)
T 6_Orcinus         1 M    1 (23)
Confidence            2


No 11 
>Q874C1_38_IM_ref_sig5_130
Probab=0.83  E-value=29  Score=7.33  Aligned_cols=3  Identities=33%  Similarity=0.689  Sum_probs=1.3

Q 5_Mustela         3 QGM    5 (6)
Q 12_Sorex          3 QGM    5 (6)
Q Consensus         3 qgm    5 (6)
                      +||
T Consensus        36 RgM   38 (43)
T signal           36 RSM   38 (43)
T 176              36 RGM   38 (43)
T 175              35 RGM   37 (42)
T 177              36 RGM   38 (43)
T 173              38 RGM   40 (45)
T 170              32 RSM   34 (39)
Confidence            344


No 12 
>P15650_30_MM_ref_sig5_130
Probab=0.83  E-value=29  Score=3.70  Aligned_cols=1  Identities=100%  Similarity=1.431  Sum_probs=0.3

Q 5_Mustela         5 M    5 (6)
Q 12_Sorex          5 M    5 (6)
Q Consensus         5 m    5 (6)
                      |
T Consensus         1 M    1 (35)
T signal            1 M    1 (35)
T 78_Saimiri        1 M    1 (34)
T 74_Tursiops       1 A    1 (34)
T 67_Xiphophorus    1 R    1 (34)
T 57_Meleagris      1 M    1 (34)
T 48_Anas           1 M    1 (34)
T 22_Ceratotheri    1 M    1 (34)
T 5_Cricetulus      1 M    1 (34)
Confidence            2


No 13 
>P41367_25_MM_ref_sig5_130
Probab=0.83  E-value=29  Score=3.62  Aligned_cols=1  Identities=100%  Similarity=1.431  Sum_probs=0.2

Q 5_Mustela         5 M    5 (6)
Q 12_Sorex          5 M    5 (6)
Q Consensus         5 m    5 (6)
                      |
T Consensus         1 M    1 (30)
T signal            1 M    1 (30)
T 102_Strongyloc    1 M    1 (28)
T 100_Leishmania    1 M    1 (28)
T 89_Caenorhabdi    1 M    1 (28)
T 88_Haplochromi    1 M    1 (28)
T 79_Cavia          1 M    1 (28)
T 76_Tribolium      1 M    1 (28)
T 71_Pediculus      1 M    1 (28)
T 64_Sarcophilus    1 T    1 (28)
T 41_Ficedula       1 M    1 (28)
Confidence            2


No 14 
>A4FUC0_29_Mito_ref_sig5_130
Probab=0.82  E-value=29  Score=3.34  Aligned_cols=1  Identities=100%  Similarity=1.431  Sum_probs=0.2

Q 5_Mustela         5 M    5 (6)
Q 12_Sorex          5 M    5 (6)
Q Consensus         5 m    5 (6)
                      |
T Consensus         1 M    1 (34)
T signal            1 M    1 (34)
T 83_Ceratitis      1 M    1 (34)
T 70_Xiphophorus    1 M    1 (34)
T 62_Anolis         1 M    1 (34)
T 61_Geospiza       1 W    1 (34)
T 60_Anas           1 C    1 (34)
T 59_Zonotrichia    1 R    1 (34)
T 57_Columba        1 P    1 (34)
T 55_Falco          1 W    1 (34)
T 43_Dasypus        1 M    1 (34)
Confidence            2


No 15 
>P53193_10_MM_ref_sig5_130
Probab=0.81  E-value=30  Score=5.88  Aligned_cols=2  Identities=50%  Similarity=0.086  Sum_probs=0.9

Q 5_Mustela         2 NQ    3 (6)
Q 12_Sorex          2 NQ    3 (6)
Q Consensus         2 nq    3 (6)
                      ||
T Consensus         6 ~q    7 (15)
T signal            6 VQ    7 (15)
T 63                6 RQ    7 (15)
T 62                2 NQ    3 (11)
Confidence            44


No 16 
>P08503_25_MM_ref_sig5_130
Probab=0.80  E-value=30  Score=3.63  Aligned_cols=1  Identities=100%  Similarity=1.431  Sum_probs=0.2

Q 5_Mustela         5 M    5 (6)
Q 12_Sorex          5 M    5 (6)
Q Consensus         5 m    5 (6)
                      |
T Consensus         1 M    1 (30)
T signal            1 M    1 (30)
T 96_Nematostell    1 M    1 (28)
T 51_Xenopus        1 M    1 (28)
T 47_Ornithorhyn    1 M    1 (28)
T 46_Geospiza       1 M    1 (28)
Confidence            2


No 17 
>Q9Y2D0_33_Mito_ref_sig5_130
Probab=0.78  E-value=31  Score=4.86  Aligned_cols=1  Identities=100%  Similarity=1.431  Sum_probs=0.3

Q 5_Mustela         5 M    5 (6)
Q 12_Sorex          5 M    5 (6)
Q Consensus         5 m    5 (6)
                      |
T Consensus         1 M    1 (38)
T signal            1 M    1 (38)
T 57_Xenopus        1 M    1 (38)
T 54_Chrysemys      1 M    1 (38)
T 49_Meleagris      1 M    1 (38)
T 42_Dasypus        1 M    1 (38)
T 41_Camelus        1 M    1 (38)
T 37_Ailuropoda     1 M    1 (38)
T 22_Myotis         1 M    1 (38)
T 14                1 L    1 (32)
Confidence            2


No 18 
>P05626_35_Mito_IM_ref_sig5_130
Probab=0.78  E-value=31  Score=7.13  Aligned_cols=2  Identities=100%  Similarity=1.548  Sum_probs=0.8

Q 5_Mustela         3 QG    4 (6)
Q 12_Sorex          3 QG    4 (6)
Q Consensus         3 qg    4 (6)
                      ||
T Consensus        22 ~G   23 (40)
T signal           22 QG   23 (40)
T 47               18 QG   19 (34)
T 43               18 LG   19 (32)
T 45               18 LG   19 (32)
T 48               18 VG   19 (32)
T 49               18 LA   19 (32)
Confidence            34


No 19 
>P08559_29_MM_ref_sig5_130
Probab=0.77  E-value=32  Score=3.66  Aligned_cols=1  Identities=100%  Similarity=1.431  Sum_probs=0.2

Q 5_Mustela         5 M    5 (6)
Q 12_Sorex          5 M    5 (6)
Q Consensus         5 m    5 (6)
                      |
T Consensus         1 M    1 (34)
T signal            1 M    1 (34)
T 85_Trichinella    1 M    1 (33)
T 81_Apis           1 M    1 (33)
T 76_Trichoplax     1 M    1 (33)
T 55_Pelodiscus     1 M    1 (33)
T 44_Capra          1 V    1 (33)
T 39_Ailuropoda     1 M    1 (33)
Confidence            2


No 20 
>Q6UB35_31_Mito_ref_sig5_130
Probab=0.76  E-value=32  Score=3.82  Aligned_cols=1  Identities=100%  Similarity=1.431  Sum_probs=0.3

Q 5_Mustela         5 M    5 (6)
Q 12_Sorex          5 M    5 (6)
Q Consensus         5 m    5 (6)
                      |
T Consensus         1 M    1 (36)
T signal            1 M    1 (36)
T 44_Alligator      1 M    1 (35)
T 38_Ictidomys      1 M    1 (35)
T 25_Myotis         1 M    1 (35)
T 19_Tupaia         1 M    1 (35)
T 39_Condylura      1 M    1 (35)
Confidence            2


Done!
