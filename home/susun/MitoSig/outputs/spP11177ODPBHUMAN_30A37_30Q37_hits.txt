Query         5_Mustela
Match_columns 7
No_of_seqs    3 out of 20
Neff          1.2 
Searched_HMMs 436
Date          Wed Sep  6 16:08:30 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_30A37_30Q37_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P17505_17_MM_ref_sig5_130        0.8      32   0.073    6.3   0.0    1    4-4      21-21  (22)
  2 Q16595_41_Cy_Mito_ref_sig5_130   0.8      32   0.073    7.5   0.0    1    3-3      44-44  (46)
  3 O14561_68_Mito_ref_sig5_130      0.7      33   0.076    8.1   0.0    1    5-5      10-10  (73)
  4 Q8N3J5_29_MM_ref_sig5_130        0.7      34   0.078    5.3   0.0    1    3-3      34-34  (34)
  5 Q8BXN7_29_MM_ref_sig5_130        0.7      34   0.078    5.0   0.0    1    3-3      34-34  (34)
  6 P12695_28_MM_ref_sig5_130        0.7      36   0.082    6.9   0.0    1    4-4      24-24  (33)
  7 P24310_21_IM_ref_sig5_130        0.6      40   0.091    6.5   0.0    1    5-5      11-11  (26)
  8 P12687_27_Mito_ref_sig5_130      0.6      40   0.093    6.8   0.0    1    6-6      26-26  (32)
  9 P25708_20_IM_ref_sig5_130        0.6      42   0.097    6.3   0.0    1    5-5      18-18  (25)
 10 Q6UB35_31_Mito_ref_sig5_130      0.6      43   0.099    3.7   0.0    1    1-1       1-1   (36)
 11 Q04728_8_MM_ref_sig5_130         0.6      43   0.099    5.5   0.0    2    1-2       8-9   (13)
 12 P52505_68_Mito_ref_sig5_130      0.6      44     0.1    7.7   0.0    1    5-5      10-10  (73)
 13 P10355_8_IM_ref_sig5_130         0.5      49    0.11    5.3   0.0    1    4-4       5-5   (13)
 14 Q02372_28_IM_ref_sig5_130        0.5      50    0.11    3.1   0.0    1    3-3      33-33  (33)
 15 P07257_16_IM_ref_sig5_130        0.5      52    0.12    5.8   0.0    1    3-3      21-21  (21)
 16 P26269_27_MM_ref_sig5_130        0.5      53    0.12    6.4   0.0    1    5-5      24-24  (32)
 17 Q9NPH0_32_Mito_ref_sig5_130      0.5      53    0.12    3.5   0.0    1    1-1       1-1   (37)
 18 Q06645_61_MitoM_ref_sig5_130     0.4      58    0.13    6.5   0.0    1    3-3      66-66  (66)
 19 P0CS90_23_MM_Nu_ref_sig5_130     0.4      59    0.14    5.9   0.0    1    3-3      28-28  (28)
 20 P56522_34_MM_ref_sig5_130        0.4      61    0.14    2.8   0.0    1    1-1       1-1   (39)

No 1  
>P17505_17_MM_ref_sig5_130
Probab=0.77  E-value=32  Score=6.34  Aligned_cols=1  Identities=100%  Similarity=0.833  Sum_probs=0.3

Q 5_Mustela         4 T    4 (7)
Q 9_Ailuropoda      4 T    4 (7)
Q 15_Ictidomys      4 T    4 (7)
Q Consensus         4 t    4 (7)
                      +
T Consensus        21 a   21 (22)
T signal           21 T   21 (22)
T 148              21 A   21 (22)
T 156              21 S   21 (22)
T 157              16 A   16 (17)
T 115              19 A   19 (20)
T 55               28 T   28 (29)
T 10               27 A   27 (28)
T 26               16 A   16 (17)
T 120              19 T   19 (20)
T 125              19 S   19 (20)
Confidence            3


No 2  
>Q16595_41_Cy_Mito_ref_sig5_130
Probab=0.77  E-value=32  Score=7.51  Aligned_cols=1  Identities=0%  Similarity=0.003  Sum_probs=0.2

Q 5_Mustela         3 V    3 (7)
Q 9_Ailuropoda      3 V    3 (7)
Q 15_Ictidomys      3 M    3 (7)
Q Consensus         3 ~    3 (7)
                      +
T Consensus        44 ~   44 (46)
T signal           44 T   44 (46)
T 159              47 T   47 (49)
T 157              45 I   45 (46)
T 156              45 A   45 (46)
T 153              44 -   43 (43)
T 145              45 I   45 (46)
T 142              43 V   43 (44)
T 147              45 -   44 (44)
T 141              38 I   38 (38)
Confidence            2


No 3  
>O14561_68_Mito_ref_sig5_130
Probab=0.74  E-value=33  Score=8.10  Aligned_cols=1  Identities=100%  Similarity=1.132  Sum_probs=0.3

Q 5_Mustela         5 V    5 (7)
Q 9_Ailuropoda      5 V    5 (7)
Q 15_Ictidomys      5 V    5 (7)
Q Consensus         5 v    5 (7)
                      |
T Consensus        10 V   10 (73)
T signal           10 V   10 (73)
T 82               10 V   10 (72)
T 80               10 V   10 (76)
T 78               10 V   10 (66)
T 79               10 V   10 (65)
T 76               10 V   10 (67)
T 73               10 V   10 (70)
T 65               10 V   10 (73)
T 77               13 L   13 (94)
T 72               10 V   10 (73)
Confidence            3


No 4  
>Q8N3J5_29_MM_ref_sig5_130
Probab=0.73  E-value=34  Score=5.30  Aligned_cols=1  Identities=100%  Similarity=1.132  Sum_probs=0.0

Q 5_Mustela         3 V    3 (7)
Q 9_Ailuropoda      3 V    3 (7)
Q 15_Ictidomys      3 M    3 (7)
Q Consensus         3 ~    3 (7)
                      +
T Consensus        34 ~   34 (34)
T signal           34 V   34 (34)
T 76_Salpingoeca   34 -   33 (33)
T 74_Hydra         34 -   33 (33)
T 73_Nematostell   34 -   33 (33)
T 70_Strongyloce   34 -   33 (33)
T 69_Ixodes        34 -   33 (33)
T 68_Saccoglossu   34 -   33 (33)
T cl|CABBABABA|1   34 -   33 (33)
T cl|FABBABABA|1   34 -   33 (33)
T cl|QABBABABA|2   34 -   33 (33)
Confidence            0


No 5  
>Q8BXN7_29_MM_ref_sig5_130
Probab=0.72  E-value=34  Score=4.99  Aligned_cols=1  Identities=0%  Similarity=0.036  Sum_probs=0.0

Q 5_Mustela         3 V    3 (7)
Q 9_Ailuropoda      3 V    3 (7)
Q 15_Ictidomys      3 M    3 (7)
Q Consensus         3 ~    3 (7)
                      +
T Consensus        34 ~   34 (34)
T signal           34 A   34 (34)
T 79_Trypanosoma   34 -   33 (33)
T 77_Coprinopsis   34 -   33 (33)
T 73_Nematostell   34 -   33 (33)
T 69_Ixodes        34 -   33 (33)
T 68_Saccoglossu   34 -   33 (33)
T 67_Sorex         34 -   33 (33)
T cl|KABBABABA|1   34 -   33 (33)
Confidence            0


No 6  
>P12695_28_MM_ref_sig5_130
Probab=0.69  E-value=36  Score=6.94  Aligned_cols=1  Identities=0%  Similarity=0.003  Sum_probs=0.2

Q 5_Mustela         4 T    4 (7)
Q 9_Ailuropoda      4 T    4 (7)
Q 15_Ictidomys      4 T    4 (7)
Q Consensus         4 t    4 (7)
                      .
T Consensus        24 q   24 (33)
T signal           24 Q   24 (33)
T 9                22 Q   22 (32)
T 61               27 Q   27 (36)
T 72               25 T   25 (34)
T 81               25 Q   25 (34)
T 38               21 Q   21 (30)
T 84               21 Q   21 (30)
T 87               27 Q   27 (37)
Confidence            1


No 7  
>P24310_21_IM_ref_sig5_130
Probab=0.63  E-value=40  Score=6.51  Aligned_cols=1  Identities=0%  Similarity=1.032  Sum_probs=0.3

Q 5_Mustela         5 V    5 (7)
Q 9_Ailuropoda      5 V    5 (7)
Q 15_Ictidomys      5 V    5 (7)
Q Consensus         5 v    5 (7)
                      +
T Consensus        11 i   11 (26)
T signal           11 I   11 (26)
T 6                11 V   11 (26)
T 8                11 V   11 (26)
T 10               11 A   11 (26)
T 11               11 T   11 (26)
T 13               11 I   11 (26)
T 15               11 V   11 (26)
T 18               11 V   11 (26)
T 7                12 L   12 (27)
T 2                 9 I    9 (23)
Confidence            2


No 8  
>P12687_27_Mito_ref_sig5_130
Probab=0.62  E-value=40  Score=6.77  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.3

Q 5_Mustela         6 R    6 (7)
Q 9_Ailuropoda      6 R    6 (7)
Q 15_Ictidomys      6 R    6 (7)
Q Consensus         6 r    6 (7)
                      |
T Consensus        26 R   26 (32)
T signal           26 R   26 (32)
T 93               25 R   25 (31)
T 84               19 R   19 (25)
Confidence            3


No 9  
>P25708_20_IM_ref_sig5_130
Probab=0.60  E-value=42  Score=6.30  Aligned_cols=1  Identities=100%  Similarity=1.132  Sum_probs=0.4

Q 5_Mustela         5 V    5 (7)
Q 9_Ailuropoda      5 V    5 (7)
Q 15_Ictidomys      5 V    5 (7)
Q Consensus         5 v    5 (7)
                      |
T Consensus        18 v   18 (25)
T signal           18 V   18 (25)
T 96               19 A   19 (27)
T 103              13 V   13 (20)
T 135              15 A   15 (27)
T 3                18 V   18 (25)
Confidence            3


No 10 
>Q6UB35_31_Mito_ref_sig5_130
Probab=0.59  E-value=43  Score=3.72  Aligned_cols=1  Identities=0%  Similarity=0.932  Sum_probs=0.0

Q 5_Mustela         1 L    1 (7)
Q 9_Ailuropoda      1 L    1 (7)
Q 15_Ictidomys      1 L    1 (7)
Q Consensus         1 l    1 (7)
                      .
T Consensus         1 M    1 (36)
T signal            1 M    1 (36)
T 44_Alligator      1 M    1 (35)
T 38_Ictidomys      1 M    1 (35)
T 25_Myotis         1 M    1 (35)
T 19_Tupaia         1 M    1 (35)
T 39_Condylura      1 M    1 (35)
Confidence            0


No 11 
>Q04728_8_MM_ref_sig5_130
Probab=0.58  E-value=43  Score=5.47  Aligned_cols=2  Identities=100%  Similarity=1.115  Sum_probs=0.9

Q 5_Mustela         1 LQ    2 (7)
Q 9_Ailuropoda      1 LQ    2 (7)
Q 15_Ictidomys      1 LQ    2 (7)
Q Consensus         1 lq    2 (7)
                      ||
T Consensus         8 lq    9 (13)
T signal            8 LQ    9 (13)
T 90                8 LQ    9 (13)
T 88                8 LQ    9 (13)
Confidence            34


No 12 
>P52505_68_Mito_ref_sig5_130
Probab=0.58  E-value=44  Score=7.72  Aligned_cols=1  Identities=100%  Similarity=1.132  Sum_probs=0.3

Q 5_Mustela         5 V    5 (7)
Q 9_Ailuropoda      5 V    5 (7)
Q 15_Ictidomys      5 V    5 (7)
Q Consensus         5 v    5 (7)
                      |
T Consensus        10 V   10 (73)
T signal           10 V   10 (73)
T 100              10 V   10 (58)
T 29               10 V   10 (65)
T 55               10 V   10 (66)
T 9                10 V   10 (70)
T 59               10 V   10 (67)
T 17               10 V   10 (73)
T 18               10 A   10 (42)
T 88               13 L   13 (94)
T 19               10 V   10 (73)
Confidence            2


No 13 
>P10355_8_IM_ref_sig5_130
Probab=0.52  E-value=49  Score=5.35  Aligned_cols=1  Identities=100%  Similarity=0.833  Sum_probs=0.3

Q 5_Mustela         4 T    4 (7)
Q 9_Ailuropoda      4 T    4 (7)
Q 15_Ictidomys      4 T    4 (7)
Q Consensus         4 t    4 (7)
                      |
T Consensus         5 t    5 (13)
T signal            5 T    5 (13)
Confidence            2


No 14 
>Q02372_28_IM_ref_sig5_130
Probab=0.51  E-value=50  Score=3.14  Aligned_cols=1  Identities=0%  Similarity=0.003  Sum_probs=0.0

Q 5_Mustela         3 V    3 (7)
Q 9_Ailuropoda      3 V    3 (7)
Q 15_Ictidomys      3 M    3 (7)
Q Consensus         3 ~    3 (7)
                      -
T Consensus        33 ~   33 (33)
T signal           33 T   33 (33)
T 89_Brugia        32 -   31 (31)
T 78_Nasonia       32 -   31 (31)
T 75_Bombyx        32 -   31 (31)
T 74_Branchiosto   32 -   31 (31)
T 72_Tursiops      32 -   31 (31)
T 62_Anas          32 -   31 (31)
T 57_Falco         32 -   31 (31)
T 53_Oryzias       32 -   31 (31)
T 29_Capra         32 -   31 (31)
Confidence            0


No 15 
>P07257_16_IM_ref_sig5_130
Probab=0.49  E-value=52  Score=5.78  Aligned_cols=1  Identities=0%  Similarity=0.036  Sum_probs=0.0

Q 5_Mustela         3 V    3 (7)
Q 9_Ailuropoda      3 V    3 (7)
Q 15_Ictidomys      3 M    3 (7)
Q Consensus         3 ~    3 (7)
                      -
T Consensus        21 a   21 (21)
T signal           21 A   21 (21)
T 20               18 A   18 (18)
T 34               18 S   18 (18)
T 14               19 T   19 (19)
T 15               20 A   20 (20)
T 23               22 A   22 (22)
T 19               20 A   20 (20)
T 40               22 A   22 (22)
T 42               20 A   20 (20)
Confidence            0


No 16 
>P26269_27_MM_ref_sig5_130
Probab=0.48  E-value=53  Score=6.44  Aligned_cols=1  Identities=100%  Similarity=1.132  Sum_probs=0.3

Q 5_Mustela         5 V    5 (7)
Q 9_Ailuropoda      5 V    5 (7)
Q 15_Ictidomys      5 V    5 (7)
Q Consensus         5 v    5 (7)
                      |
T Consensus        24 v   24 (32)
T signal           24 V   24 (32)
Confidence            3


No 17 
>Q9NPH0_32_Mito_ref_sig5_130
Probab=0.48  E-value=53  Score=3.51  Aligned_cols=1  Identities=0%  Similarity=0.932  Sum_probs=0.0

Q 5_Mustela         1 L    1 (7)
Q 9_Ailuropoda      1 L    1 (7)
Q 15_Ictidomys      1 L    1 (7)
Q Consensus         1 l    1 (7)
                      .
T Consensus         1 M    1 (37)
T signal            1 M    1 (37)
T 92_Coccidioide    1 M    1 (36)
T 77_Strongyloce    1 M    1 (36)
T 76_Ciona          1 M    1 (36)
T 74_Latimeria      1 V    1 (36)
T 72_Branchiosto    1 F    1 (36)
T 60_Ficedula       1 M    1 (36)
T 49_Condylura      1 M    1 (36)
Confidence            0


No 18 
>Q06645_61_MitoM_ref_sig5_130
Probab=0.44  E-value=58  Score=6.50  Aligned_cols=1  Identities=0%  Similarity=0.036  Sum_probs=0.0

Q 5_Mustela         3 V    3 (7)
Q 9_Ailuropoda      3 V    3 (7)
Q 15_Ictidomys      3 M    3 (7)
Q Consensus         3 ~    3 (7)
                      .
T Consensus        66 a   66 (66)
T signal           66 A   66 (66)
T 59_Zonotrichia   66 T   66 (66)
T 54_Latimeria     66 I   66 (66)
T 52_Anolis        66 A   66 (66)
T 39_Heterocepha   66 D   66 (66)
T cl|GABBABABA|1   66 L   66 (66)
T cl|KABBABABA|1   66 A   66 (66)
T cl|LABBABABA|3   66 R   66 (66)
T cl|RABBABABA|7   66 V   66 (66)
T 1                68 A   68 (68)
Confidence            0


No 19 
>P0CS90_23_MM_Nu_ref_sig5_130
Probab=0.44  E-value=59  Score=5.95  Aligned_cols=1  Identities=100%  Similarity=1.132  Sum_probs=0.0

Q 5_Mustela         3 V    3 (7)
Q 9_Ailuropoda      3 V    3 (7)
Q 15_Ictidomys      3 M    3 (7)
Q Consensus         3 ~    3 (7)
                      |
T Consensus        28 V   28 (28)
T signal           28 V   28 (28)
T 55               29 I   29 (29)
T 65               29 V   29 (29)
T 54               28 V   28 (28)
T 57               26 V   26 (26)
T 59               26 V   26 (26)
T 62               24 V   24 (24)
T 56               24 V   24 (24)
T 60               23 V   23 (23)
Confidence            0


No 20 
>P56522_34_MM_ref_sig5_130
Probab=0.42  E-value=61  Score=2.78  Aligned_cols=1  Identities=0%  Similarity=0.932  Sum_probs=0.0

Q 5_Mustela         1 L    1 (7)
Q 9_Ailuropoda      1 L    1 (7)
Q 15_Ictidomys      1 L    1 (7)
Q Consensus         1 l    1 (7)
                      .
T Consensus         1 M    1 (39)
T signal            1 M    1 (39)
T 138_Tuber         1 M    1 (37)
T 98_Oryza          1 M    1 (37)
T 88_Apis           1 M    1 (37)
T 81_Amphimedon     1 M    1 (37)
T 79_Trichoplax     1 M    1 (37)
T 61_Meleagris      1 M    1 (37)
Confidence            0


Done!
