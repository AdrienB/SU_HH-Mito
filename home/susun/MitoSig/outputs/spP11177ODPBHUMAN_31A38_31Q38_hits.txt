Query         5_Mustela
Match_columns 7
No_of_seqs    3 out of 20
Neff          1.2 
Searched_HMMs 436
Date          Wed Sep  6 16:08:32 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_31A38_31Q38_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 Q6UB35_31_Mito_ref_sig5_130      0.6      42   0.097    3.7   0.0    1    1-1       1-1   (36)
  2 P07919_13_IM_ref_sig5_130        0.6      43   0.098    5.8   0.0    1    5-5       4-4   (18)
  3 Q02380_46_IM_ref_sig5_130        0.6      45     0.1    7.3   0.0    2    4-5      44-45  (51)
  4 Q8N3J5_29_MM_ref_sig5_130        0.6      45     0.1    5.0   0.0    1    3-3      34-34  (34)
  5 P00424_20_IM_ref_sig5_130        0.6      46    0.11    6.3   0.0    1    5-5      19-19  (25)
  6 P43265_42_IM_ref_sig5_130        0.5      47    0.11    7.1   0.0    1    2-2      30-30  (47)
  7 Q8BXN7_29_MM_ref_sig5_130        0.5      49    0.11    4.6   0.0    1    3-3      34-34  (34)
  8 P34942_23_MM_ref_sig5_130        0.5      49    0.11    3.4   0.0    1    1-1       1-1   (28)
  9 P05630_22_Mito_IM_ref_sig5_130   0.5      51    0.12    6.3   0.0    1    5-5      17-17  (27)
 10 P56522_34_MM_ref_sig5_130        0.5      54    0.12    2.9   0.0    1    1-1       1-1   (39)
 11 P25708_20_IM_ref_sig5_130        0.5      54    0.12    6.0   0.0    1    4-4      18-18  (25)
 12 P00447_26_MM_ref_sig5_130        0.5      54    0.12    6.3   0.0    1    3-3      31-31  (31)
 13 P10175_24_IM_ref_sig5_130        0.5      54    0.12    6.2   0.0    1    3-3      29-29  (29)
 14 P04037_25_IM_ref_sig5_130        0.5      55    0.13    6.2   0.0    1    5-5       5-5   (30)
 15 Q9NPH0_32_Mito_ref_sig5_130      0.5      55    0.13    3.5   0.0    1    1-1       1-1   (37)
 16 P22068_44_Mito_IM_ref_sig5_130   0.5      56    0.13    6.9   0.0    1    3-3      49-49  (49)
 17 P19955_8_Mito_ref_sig5_130       0.4      60    0.14    5.1   0.0    2    4-5       6-7   (13)
 18 P16221_24_IM_ref_sig5_130        0.4      60    0.14    6.0   0.0    1    3-3      29-29  (29)
 19 P07756_38_Mito_Nu_ref_sig5_130   0.4      61    0.14    4.4   0.0    1    1-1       1-1   (43)
 20 P13184_23_IM_ref_sig5_130        0.4      61    0.14    6.0   0.0    1    3-3      28-28  (28)

No 1  
>Q6UB35_31_Mito_ref_sig5_130
Probab=0.59  E-value=42  Score=3.74  Aligned_cols=1  Identities=0%  Similarity=-0.330  Sum_probs=0.0

Q 5_Mustela         1 Q    1 (7)
Q 9_Ailuropoda      1 Q    1 (7)
Q 15_Ictidomys      1 Q    1 (7)
Q Consensus         1 q    1 (7)
                      =
T Consensus         1 M    1 (36)
T signal            1 M    1 (36)
T 44_Alligator      1 M    1 (35)
T 38_Ictidomys      1 M    1 (35)
T 25_Myotis         1 M    1 (35)
T 19_Tupaia         1 M    1 (35)
T 39_Condylura      1 M    1 (35)
Confidence            0


No 2  
>P07919_13_IM_ref_sig5_130
Probab=0.59  E-value=43  Score=5.84  Aligned_cols=1  Identities=0%  Similarity=0.135  Sum_probs=0.3

Q 5_Mustela         5 R    5 (7)
Q 9_Ailuropoda      5 R    5 (7)
Q 15_Ictidomys      5 R    5 (7)
Q Consensus         5 r    5 (7)
                      +
T Consensus         4 ~    4 (18)
T signal            4 E    4 (18)
T 41                4 E    4 (18)
T 13                4 E    4 (18)
T 23                4 E    4 (18)
T 58                4 M    4 (17)
T 61                4 E    4 (18)
T 18                4 R    4 (18)
T 22                4 R    4 (18)
T 59                4 Q    4 (18)
T 79                4 R    4 (15)
Confidence            3


No 3  
>Q02380_46_IM_ref_sig5_130
Probab=0.57  E-value=45  Score=7.27  Aligned_cols=2  Identities=100%  Similarity=1.348  Sum_probs=0.8

Q 5_Mustela         4 VR    5 (7)
Q 9_Ailuropoda      4 VR    5 (7)
Q 15_Ictidomys      4 VR    5 (7)
Q Consensus         4 vr    5 (7)
                      ||
T Consensus        44 VR   45 (51)
T signal           44 VR   45 (51)
T 55               44 VR   45 (51)
T 59               44 VR   45 (51)
T 51               50 VR   51 (57)
T 94               43 --   42 (42)
T 49               45 VR   46 (52)
T 46               45 VR   46 (51)
T 42               53 VR   54 (60)
T 24               41 VR   42 (44)
T 40               42 VR   43 (48)
Confidence            33


No 4  
>Q8N3J5_29_MM_ref_sig5_130
Probab=0.56  E-value=45  Score=4.99  Aligned_cols=1  Identities=0%  Similarity=0.003  Sum_probs=0.0

Q 5_Mustela         3 T    3 (7)
Q 9_Ailuropoda      3 T    3 (7)
Q 15_Ictidomys      3 T    3 (7)
Q Consensus         3 t    3 (7)
                      .
T Consensus        34 ~   34 (34)
T signal           34 V   34 (34)
T 76_Salpingoeca   34 -   33 (33)
T 74_Hydra         34 -   33 (33)
T 73_Nematostell   34 -   33 (33)
T 70_Strongyloce   34 -   33 (33)
T 69_Ixodes        34 -   33 (33)
T 68_Saccoglossu   34 -   33 (33)
T cl|CABBABABA|1   34 -   33 (33)
T cl|FABBABABA|1   34 -   33 (33)
T cl|QABBABABA|2   34 -   33 (33)
Confidence            0


No 5  
>P00424_20_IM_ref_sig5_130
Probab=0.55  E-value=46  Score=6.28  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.3

Q 5_Mustela         5 R    5 (7)
Q 9_Ailuropoda      5 R    5 (7)
Q 15_Ictidomys      5 R    5 (7)
Q Consensus         5 r    5 (7)
                      |
T Consensus        19 R   19 (25)
T signal           19 R   19 (25)
T 30               16 R   16 (22)
T 32               16 R   16 (22)
T 34               16 R   16 (22)
T 35               16 R   16 (22)
T 31               15 R   15 (21)
T 33               15 R   15 (21)
T 28               16 R   16 (20)
T 29               12 R   12 (18)
T 27               16 R   16 (22)
Confidence            3


No 6  
>P43265_42_IM_ref_sig5_130
Probab=0.54  E-value=47  Score=7.09  Aligned_cols=1  Identities=100%  Similarity=1.132  Sum_probs=0.2

Q 5_Mustela         2 V    2 (7)
Q 9_Ailuropoda      2 V    2 (7)
Q 15_Ictidomys      2 M    2 (7)
Q Consensus         2 ~    2 (7)
                      +
T Consensus        30 v   30 (47)
T signal           30 V   30 (47)
Confidence            2


No 7  
>Q8BXN7_29_MM_ref_sig5_130
Probab=0.52  E-value=49  Score=4.62  Aligned_cols=1  Identities=0%  Similarity=0.202  Sum_probs=0.0

Q 5_Mustela         3 T    3 (7)
Q 9_Ailuropoda      3 T    3 (7)
Q 15_Ictidomys      3 T    3 (7)
Q Consensus         3 t    3 (7)
                      -
T Consensus        34 ~   34 (34)
T signal           34 A   34 (34)
T 79_Trypanosoma   34 -   33 (33)
T 77_Coprinopsis   34 -   33 (33)
T 73_Nematostell   34 -   33 (33)
T 69_Ixodes        34 -   33 (33)
T 68_Saccoglossu   34 -   33 (33)
T 67_Sorex         34 -   33 (33)
T cl|KABBABABA|1   34 -   33 (33)
Confidence            0


No 8  
>P34942_23_MM_ref_sig5_130
Probab=0.52  E-value=49  Score=3.36  Aligned_cols=1  Identities=0%  Similarity=-0.330  Sum_probs=0.0

Q 5_Mustela         1 Q    1 (7)
Q 9_Ailuropoda      1 Q    1 (7)
Q 15_Ictidomys      1 Q    1 (7)
Q Consensus         1 q    1 (7)
                      .
T Consensus         1 M    1 (28)
T signal            1 M    1 (28)
T 78_Musca          1 M    1 (39)
T 74_Strongyloce    1 M    1 (39)
T 62_Alligator      1 M    1 (39)
T 60_Xenopus        1 M    1 (39)
T 56_Anas           1 M    1 (39)
T 52_Meleagris      1 M    1 (39)
T 40_Saimiri        1 M    1 (39)
Confidence            0


No 9  
>P05630_22_Mito_IM_ref_sig5_130
Probab=0.50  E-value=51  Score=6.26  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.2

Q 5_Mustela         5 R    5 (7)
Q 9_Ailuropoda      5 R    5 (7)
Q 15_Ictidomys      5 R    5 (7)
Q Consensus         5 r    5 (7)
                      |
T Consensus        17 ~   17 (27)
T signal           17 R   17 (27)
T 133              17 L   17 (27)
T 148              17 L   17 (27)
T 167              17 R   17 (27)
T 155              17 R   17 (27)
T 147              17 R   17 (27)
T 140              17 R   17 (26)
T 142              16 R   16 (32)
T 121              17 L   17 (34)
T 134              17 -   16 (29)
Confidence            2


No 10 
>P56522_34_MM_ref_sig5_130
Probab=0.48  E-value=54  Score=2.91  Aligned_cols=1  Identities=0%  Similarity=-0.330  Sum_probs=0.0

Q 5_Mustela         1 Q    1 (7)
Q 9_Ailuropoda      1 Q    1 (7)
Q 15_Ictidomys      1 Q    1 (7)
Q Consensus         1 q    1 (7)
                      =
T Consensus         1 M    1 (39)
T signal            1 M    1 (39)
T 138_Tuber         1 M    1 (37)
T 98_Oryza          1 M    1 (37)
T 88_Apis           1 M    1 (37)
T 81_Amphimedon     1 M    1 (37)
T 79_Trichoplax     1 M    1 (37)
T 61_Meleagris      1 M    1 (37)
Confidence            0


No 11 
>P25708_20_IM_ref_sig5_130
Probab=0.48  E-value=54  Score=6.00  Aligned_cols=1  Identities=100%  Similarity=1.132  Sum_probs=0.3

Q 5_Mustela         4 V    4 (7)
Q 9_Ailuropoda      4 V    4 (7)
Q 15_Ictidomys      4 V    4 (7)
Q Consensus         4 v    4 (7)
                      |
T Consensus        18 v   18 (25)
T signal           18 V   18 (25)
T 96               19 A   19 (27)
T 103              13 V   13 (20)
T 135              15 A   15 (27)
T 3                18 V   18 (25)
Confidence            3


No 12 
>P00447_26_MM_ref_sig5_130
Probab=0.47  E-value=54  Score=6.33  Aligned_cols=1  Identities=0%  Similarity=0.036  Sum_probs=0.0

Q 5_Mustela         3 T    3 (7)
Q 9_Ailuropoda      3 T    3 (7)
Q 15_Ictidomys      3 T    3 (7)
Q Consensus         3 t    3 (7)
                      +
T Consensus        31 P   31 (31)
T signal           31 P   31 (31)
T 183              26 P   26 (26)
T 185              29 P   29 (29)
T 184              32 P   32 (32)
T 176              25 P   25 (25)
T 181              26 P   26 (26)
Confidence            0


No 13 
>P10175_24_IM_ref_sig5_130
Probab=0.47  E-value=54  Score=6.21  Aligned_cols=1  Identities=0%  Similarity=0.036  Sum_probs=0.0

Q 5_Mustela         3 T    3 (7)
Q 9_Ailuropoda      3 T    3 (7)
Q 15_Ictidomys      3 T    3 (7)
Q Consensus         3 t    3 (7)
                      +
T Consensus        29 P   29 (29)
T signal           29 P   29 (29)
T 4                29 P   29 (29)
T 15               29 P   29 (29)
T 7                29 V   29 (29)
T 6                29 P   29 (29)
T 9                29 P   29 (29)
T 28               29 P   29 (29)
T 3                23 P   23 (23)
T 30               27 P   27 (27)
Confidence            0


No 14 
>P04037_25_IM_ref_sig5_130
Probab=0.46  E-value=55  Score=6.18  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.3

Q 5_Mustela         5 R    5 (7)
Q 9_Ailuropoda      5 R    5 (7)
Q 15_Ictidomys      5 R    5 (7)
Q Consensus         5 r    5 (7)
                      |
T Consensus         5 R    5 (30)
T signal            5 R    5 (30)
T 50                5 R    5 (31)
T 44                5 R    5 (29)
T 51                5 R    5 (28)
T 46                2 R    2 (36)
T 45                5 R    5 (30)
T 53                4 R    4 (29)
T 49                5 R    5 (32)
T 43                3 R    3 (27)
Confidence            2


No 15 
>Q9NPH0_32_Mito_ref_sig5_130
Probab=0.46  E-value=55  Score=3.47  Aligned_cols=1  Identities=0%  Similarity=-0.330  Sum_probs=0.0

Q 5_Mustela         1 Q    1 (7)
Q 9_Ailuropoda      1 Q    1 (7)
Q 15_Ictidomys      1 Q    1 (7)
Q Consensus         1 q    1 (7)
                      =
T Consensus         1 M    1 (37)
T signal            1 M    1 (37)
T 92_Coccidioide    1 M    1 (36)
T 77_Strongyloce    1 M    1 (36)
T 76_Ciona          1 M    1 (36)
T 74_Latimeria      1 V    1 (36)
T 72_Branchiosto    1 F    1 (36)
T 60_Ficedula       1 M    1 (36)
T 49_Condylura      1 M    1 (36)
Confidence            0


No 16 
>P22068_44_Mito_IM_ref_sig5_130
Probab=0.46  E-value=56  Score=6.92  Aligned_cols=1  Identities=0%  Similarity=0.202  Sum_probs=0.0

Q 5_Mustela         3 T    3 (7)
Q 9_Ailuropoda      3 T    3 (7)
Q 15_Ictidomys      3 T    3 (7)
Q Consensus         3 t    3 (7)
                      .
T Consensus        49 a   49 (49)
T signal           49 A   49 (49)
Confidence            0


No 17 
>P19955_8_Mito_ref_sig5_130
Probab=0.43  E-value=60  Score=5.12  Aligned_cols=2  Identities=50%  Similarity=1.298  Sum_probs=0.8

Q 5_Mustela         4 VR    5 (7)
Q 9_Ailuropoda      4 VR    5 (7)
Q 15_Ictidomys      4 VR    5 (7)
Q Consensus         4 vr    5 (7)
                      +|
T Consensus         6 ir    7 (13)
T signal            6 IR    7 (13)
Confidence            44


No 18 
>P16221_24_IM_ref_sig5_130
Probab=0.43  E-value=60  Score=6.05  Aligned_cols=1  Identities=0%  Similarity=0.036  Sum_probs=0.0

Q 5_Mustela         3 T    3 (7)
Q 9_Ailuropoda      3 T    3 (7)
Q 15_Ictidomys      3 T    3 (7)
Q Consensus         3 t    3 (7)
                      +
T Consensus        29 P   29 (29)
T signal           29 P   29 (29)
T 24               29 V   29 (29)
T 10               29 P   29 (29)
T 2                29 P   29 (29)
T 18               29 P   29 (29)
T 5                29 P   29 (29)
T 8                29 P   29 (29)
T 29               29 P   29 (29)
T 3                28 P   28 (28)
T 32               27 P   27 (27)
Confidence            0


No 19 
>P07756_38_Mito_Nu_ref_sig5_130
Probab=0.42  E-value=61  Score=4.38  Aligned_cols=1  Identities=0%  Similarity=-0.330  Sum_probs=0.0

Q 5_Mustela         1 Q    1 (7)
Q 9_Ailuropoda      1 Q    1 (7)
Q 15_Ictidomys      1 Q    1 (7)
Q Consensus         1 q    1 (7)
                      =
T Consensus         1 M    1 (43)
T signal            1 M    1 (43)
T 79_Emiliania      1 M    1 (42)
T 77_Thalassiosi    1 M    1 (42)
T 60_Anolis         1 M    1 (42)
T 48_Alligator      1 M    1 (42)
T 44_Papio          1 M    1 (42)
T 28_Ovis           1 M    1 (42)
T 18_Pongo          1 M    1 (42)
T cl|HABBABABA|1    1 M    1 (52)
T 72_Branchiosto    1 M    1 (41)
Confidence            0


No 20 
>P13184_23_IM_ref_sig5_130
Probab=0.42  E-value=61  Score=6.02  Aligned_cols=1  Identities=0%  Similarity=0.003  Sum_probs=0.0

Q 5_Mustela         3 T    3 (7)
Q 9_Ailuropoda      3 T    3 (7)
Q 15_Ictidomys      3 T    3 (7)
Q Consensus         3 t    3 (7)
                      -
T Consensus        28 V   28 (28)
T signal           28 V   28 (28)
T 63               28 V   28 (28)
T 28               28 V   28 (28)
T 15               28 V   28 (28)
T 25               28 V   28 (28)
T 16               28 V   28 (28)
T 19               28 V   28 (28)
T 12               28 V   28 (28)
T 14               28 V   28 (28)
Confidence            0


Done!
