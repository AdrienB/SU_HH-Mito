Query         5_Mustela
Match_columns 6
No_of_seqs    1 out of 20
Neff          1.0 
Searched_HMMs 436
Date          Wed Sep  6 16:08:48 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_42A48_42Q48_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P08559_29_MM_ref_sig5_130        0.7      36   0.083    3.5   0.0    1    2-2      34-34  (34)
  2 Q3ZBF3_26_Mito_ref_sig5_130      0.7      36   0.083    3.5   0.0    1    2-2      31-31  (31)
  3 P05165_52_MM_ref_sig5_130        0.7      36   0.084    3.7   0.0    1    2-2      57-57  (57)
  4 P34942_23_MM_ref_sig5_130        0.7      36   0.084    3.5   0.0    1    2-2      28-28  (28)
  5 P11310_25_MM_ref_sig5_130        0.7      37   0.086    3.5   0.0    1    2-2      30-30  (30)
  6 Q9NPH0_32_Mito_ref_sig5_130      0.6      38   0.088    3.6   0.0    1    2-2      37-37  (37)
  7 O35854_27_Mito_ref_sig5_130      0.6      42   0.095    4.3   0.0    1    2-2      32-32  (32)
  8 P02721_32_Mito_IM_ref_sig5_130   0.6      42   0.095    6.6   0.0    1    4-4      35-35  (37)
  9 P51557_62_Mito_ref_sig5_130      0.6      42   0.096    5.5   0.0    1    1-1       1-1   (67)
 10 Q9Y2D0_33_Mito_ref_sig5_130      0.6      42   0.097    4.5   0.0    1    2-2      38-38  (38)
 11 Q04467_39_Mito_ref_sig5_130      0.6      42   0.097    3.5   0.0    1    2-2      44-44  (44)
 12 P99028_13_IM_ref_sig5_130        0.6      43   0.099    5.7   0.0    1    2-2       5-5   (18)
 13 P22033_32_MM_ref_sig5_130        0.6      44     0.1    3.6   0.0    1    2-2      37-37  (37)
 14 P08680_49_MM_ref_sig5_130        0.6      44     0.1    4.0   0.0    1    1-1       1-1   (54)
 15 O00142_33_Mito_ref_sig5_130      0.6      45     0.1    3.7   0.0    1    2-2      38-38  (38)
 16 P22570_32_MM_ref_sig5_130        0.6      45     0.1    3.6   0.0    1    2-2      37-37  (37)
 17 P22557_49_MM_ref_sig5_130        0.6      46     0.1    4.0   0.0    1    1-1       1-1   (54)
 18 P38646_46_Mito_Nu_ref_sig5_130   0.5      47    0.11    3.8   0.0    1    2-2      51-51  (51)
 19 P13264_16_Cy_Mito_ref_sig5_130   0.5      47    0.11    3.3   0.0    1    2-2      21-21  (21)
 20 Q9H300_52_IM_Nu_ref_sig5_130     0.5      47    0.11    7.1   0.0    3    3-5      27-29  (57)

No 1  
>P08559_29_MM_ref_sig5_130
Probab=0.68  E-value=36  Score=3.52  Aligned_cols=1  Identities=0%  Similarity=-0.097  Sum_probs=0.0

Q 5_Mustela         2 D    2 (6)
Q Consensus         2 d    2 (6)
                      .
T Consensus        34 ~   34 (34)
T signal           34 A   34 (34)
T 85_Trichinella   34 -   33 (33)
T 81_Apis          34 -   33 (33)
T 76_Trichoplax    34 -   33 (33)
T 55_Pelodiscus    34 -   33 (33)
T 44_Capra         34 -   33 (33)
T 39_Ailuropoda    34 -   33 (33)
Confidence            0


No 2  
>Q3ZBF3_26_Mito_ref_sig5_130
Probab=0.68  E-value=36  Score=3.54  Aligned_cols=1  Identities=0%  Similarity=-0.230  Sum_probs=0.0

Q 5_Mustela         2 D    2 (6)
Q Consensus         2 d    2 (6)
                      .
T Consensus        31 ~   31 (31)
T signal           31 P   31 (31)
T 96_Brugia        31 -   30 (30)
T 87_Branchiosto   31 -   30 (30)
T 86_Oryzias       31 -   30 (30)
T 84_Pediculus     31 -   30 (30)
T 70_Anas          31 -   30 (30)
T 69_Strongyloce   31 -   30 (30)
T 63_Zonotrichia   31 -   30 (30)
T 55_Alligator     31 -   30 (30)
T 44_Myotis        31 -   30 (30)
Confidence            0


No 3  
>P05165_52_MM_ref_sig5_130
Probab=0.68  E-value=36  Score=3.73  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.0

Q 5_Mustela         2 D    2 (6)
Q Consensus         2 d    2 (6)
                      |
T Consensus        57 ~   57 (57)
T signal           57 D   57 (57)
T 91_Xenopus       55 -   54 (54)
T 88_Taeniopygia   55 -   54 (54)
T 68_Strongyloce   55 -   54 (54)
T 59_Acanthamoeb   55 -   54 (54)
T 52_Trichoplax    55 -   54 (54)
T 42_Condylura     55 -   54 (54)
T 6_Columba        55 -   54 (54)
T 1_Mustela        55 -   54 (54)
Confidence            0


No 4  
>P34942_23_MM_ref_sig5_130
Probab=0.68  E-value=36  Score=3.46  Aligned_cols=1  Identities=0%  Similarity=-0.230  Sum_probs=0.0

Q 5_Mustela         2 D    2 (6)
Q Consensus         2 d    2 (6)
                      .
T Consensus        28 ~   28 (28)
T signal           28 P   28 (28)
T 78_Musca         40 -   39 (39)
T 74_Strongyloce   40 -   39 (39)
T 62_Alligator     40 -   39 (39)
T 60_Xenopus       40 -   39 (39)
T 56_Anas          40 -   39 (39)
T 52_Meleagris     40 -   39 (39)
T 40_Saimiri       40 -   39 (39)
Confidence            0


No 5  
>P11310_25_MM_ref_sig5_130
Probab=0.66  E-value=37  Score=3.50  Aligned_cols=1  Identities=0%  Similarity=0.302  Sum_probs=0.0

Q 5_Mustela         2 D    2 (6)
Q Consensus         2 d    2 (6)
                      .
T Consensus        30 ~   30 (30)
T signal           30 Q   30 (30)
T 81_Nematostell   29 -   28 (28)
T 80_Nasonia       29 -   28 (28)
T 70_Bombyx        29 -   28 (28)
T 64_Branchiosto   29 -   28 (28)
T 63_Aplysia       29 -   28 (28)
T 61_Saccoglossu   29 -   28 (28)
T 50_Columba       29 -   28 (28)
Confidence            0


No 6  
>Q9NPH0_32_Mito_ref_sig5_130
Probab=0.65  E-value=38  Score=3.63  Aligned_cols=1  Identities=0%  Similarity=-0.097  Sum_probs=0.0

Q 5_Mustela         2 D    2 (6)
Q Consensus         2 d    2 (6)
                      .
T Consensus        37 ~   37 (37)
T signal           37 A   37 (37)
T 92_Coccidioide   37 -   36 (36)
T 77_Strongyloce   37 -   36 (36)
T 76_Ciona         37 -   36 (36)
T 74_Latimeria     37 -   36 (36)
T 72_Branchiosto   37 -   36 (36)
T 60_Ficedula      37 -   36 (36)
T 49_Condylura     37 -   36 (36)
Confidence            0


No 7  
>O35854_27_Mito_ref_sig5_130
Probab=0.60  E-value=42  Score=4.25  Aligned_cols=1  Identities=0%  Similarity=-1.493  Sum_probs=0.0

Q 5_Mustela         2 D    2 (6)
Q Consensus         2 d    2 (6)
                      +
T Consensus        32 ~   32 (32)
T signal           32 F   32 (32)
T 55_Tetrapisisp   32 K   32 (32)
T 51_Micromonas    32 D   32 (32)
T 50_Laccaria      32 A   32 (32)
T 47_Acanthamoeb   32 G   32 (32)
T 46_Dictyosteli   32 D   32 (32)
T 44_Xenopus       32 A   32 (32)
T 42_Alligator     32 Q   32 (32)
T 41_Capra         32 A   32 (32)
T 38_Saimiri       32 A   32 (32)
Confidence            0


No 8  
>P02721_32_Mito_IM_ref_sig5_130
Probab=0.60  E-value=42  Score=6.61  Aligned_cols=1  Identities=100%  Similarity=1.199  Sum_probs=0.3

Q 5_Mustela         4 E    4 (6)
Q Consensus         4 e    4 (6)
                      |
T Consensus        35 e   35 (37)
T signal           35 E   35 (37)
T 20               38 D   38 (40)
T 52               37 E   37 (39)
T 22               35 K   35 (37)
T 2                37 E   37 (39)
T 18               34 -   33 (33)
T 23               28 -   27 (27)
T 84               28 -   27 (27)
T 87               28 A   28 (28)
Confidence            3


No 9  
>P51557_62_Mito_ref_sig5_130
Probab=0.60  E-value=42  Score=5.49  Aligned_cols=1  Identities=100%  Similarity=1.431  Sum_probs=0.1

Q 5_Mustela         1 M    1 (6)
Q Consensus         1 m    1 (6)
                      |
T Consensus         1 m    1 (67)
T signal            1 M    1 (67)
T 64_Maylandia      1 M    1 (67)
T 49_Columba        1 M    1 (67)
T 77_Phytophthor    1 M    1 (65)
T 76_Amphimedon     1 M    1 (65)
T cl|CABBABABA|1    1 -    0 (60)
T cl|QABBABABA|1    1 -    0 (60)
T 44_Ictidomys      1 -    0 (59)
Confidence            1


No 10 
>Q9Y2D0_33_Mito_ref_sig5_130
Probab=0.59  E-value=42  Score=4.53  Aligned_cols=1  Identities=0%  Similarity=0.003  Sum_probs=0.0

Q 5_Mustela         2 D    2 (6)
Q Consensus         2 d    2 (6)
                      .
T Consensus        38 ~   38 (38)
T signal           38 T   38 (38)
T 57_Xenopus       38 I   38 (38)
T 54_Chrysemys     38 C   38 (38)
T 49_Meleagris     38 K   38 (38)
T 42_Dasypus       38 Y   38 (38)
T 41_Camelus       38 C   38 (38)
T 37_Ailuropoda    38 L   38 (38)
T 22_Myotis        38 M   38 (38)
T 14               33 -   32 (32)
Confidence            0


No 11 
>Q04467_39_Mito_ref_sig5_130
Probab=0.59  E-value=42  Score=3.47  Aligned_cols=1  Identities=0%  Similarity=-1.259  Sum_probs=0.0

Q 5_Mustela         2 D    2 (6)
Q Consensus         2 d    2 (6)
                      -
T Consensus        44 ~   44 (44)
T signal           44 I   44 (44)
T 92_Chlorella     41 -   40 (40)
T 86_Trichoplax    41 -   40 (40)
T 76_Bombyx        41 -   40 (40)
T 69_Meleagris     41 -   40 (40)
T 65_Latimeria     41 -   40 (40)
T 38_Sarcophilus   41 -   40 (40)
Confidence            0


No 12 
>P99028_13_IM_ref_sig5_130
Probab=0.58  E-value=43  Score=5.70  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.2

Q 5_Mustela         2 D    2 (6)
Q Consensus         2 d    2 (6)
                      |
T Consensus         5 d    5 (18)
T signal            5 D    5 (18)
T 23                5 D    5 (18)
T 42                5 D    5 (18)
T 3                 5 D    5 (18)
T 14                5 D    5 (18)
T 24                5 D    5 (18)
T 59                5 E    5 (17)
T 62                5 E    5 (18)
Confidence            2


No 13 
>P22033_32_MM_ref_sig5_130
Probab=0.58  E-value=44  Score=3.64  Aligned_cols=1  Identities=0%  Similarity=0.302  Sum_probs=0.0

Q 5_Mustela         2 D    2 (6)
Q Consensus         2 d    2 (6)
                      .
T Consensus        37 ~   37 (37)
T signal           37 Q   37 (37)
T 99_Phytophthor   37 -   36 (36)
T 97_Salpingoeca   37 -   36 (36)
T 83_Caenorhabdi   37 -   36 (36)
T 75_Branchiosto   37 -   36 (36)
T 65_Xenopus       37 -   36 (36)
Confidence            0


No 14 
>P08680_49_MM_ref_sig5_130
Probab=0.57  E-value=44  Score=4.00  Aligned_cols=1  Identities=100%  Similarity=1.431  Sum_probs=0.1

Q 5_Mustela         1 M    1 (6)
Q Consensus         1 m    1 (6)
                      |
T Consensus         1 M    1 (54)
T signal            1 M    1 (54)
T 113_Candida       1 M    1 (53)
T 98_Phytophthor    1 M    1 (53)
T 87_Puccinia       1 M    1 (53)
T 73_Nematostell    1 H    1 (53)
T 72_Bombus         1 M    1 (53)
T 71_Aplysia        1 M    1 (53)
T 68_Megachile      1 M    1 (53)
T 57_Saccoglossu    1 M    1 (53)
T 46_Xenopus        1 M    1 (53)
Confidence            0


No 15 
>O00142_33_Mito_ref_sig5_130
Probab=0.57  E-value=45  Score=3.65  Aligned_cols=1  Identities=0%  Similarity=-0.097  Sum_probs=0.0

Q 5_Mustela         2 D    2 (6)
Q Consensus         2 d    2 (6)
                      .
T Consensus        38 ~   38 (38)
T signal           38 A   38 (38)
T 92_Pediculus     35 -   34 (34)
T 83_Nematostell   35 -   34 (34)
T 79_Bombus        35 -   34 (34)
T 75_Bombyx        35 -   34 (34)
T 57_Xiphophorus   35 -   34 (34)
T 45_Anolis        35 -   34 (34)
T 40_Monodelphis   35 -   34 (34)
Confidence            0


No 16 
>P22570_32_MM_ref_sig5_130
Probab=0.56  E-value=45  Score=3.61  Aligned_cols=1  Identities=0%  Similarity=0.169  Sum_probs=0.0

Q 5_Mustela         2 D    2 (6)
Q Consensus         2 d    2 (6)
                      .
T Consensus        37 ~   37 (37)
T signal           37 K   37 (37)
T 90_Drosophila    30 -   29 (29)
T 84_Megachile     30 -   29 (29)
T 73_Nematostell   30 -   29 (29)
T 49_Columba       30 -   29 (29)
T 38_Myotis        30 -   29 (29)
Confidence            0


No 17 
>P22557_49_MM_ref_sig5_130
Probab=0.56  E-value=46  Score=4.00  Aligned_cols=1  Identities=100%  Similarity=1.431  Sum_probs=0.1

Q 5_Mustela         1 M    1 (6)
Q Consensus         1 m    1 (6)
                      |
T Consensus         1 M    1 (54)
T signal            1 M    1 (54)
T 115_Kazachstan    1 M    1 (53)
T 106_Phytophtho    1 M    1 (53)
T 91_Dictyosteli    1 M    1 (53)
T 88_Puccinia       1 M    1 (53)
T 73_Aplysia        1 M    1 (53)
T 69_Bombus         1 M    1 (53)
T 68_Ceratitis      1 M    1 (53)
Confidence            0


No 18 
>P38646_46_Mito_Nu_ref_sig5_130
Probab=0.54  E-value=47  Score=3.78  Aligned_cols=1  Identities=0%  Similarity=-1.259  Sum_probs=0.0

Q 5_Mustela         2 D    2 (6)
Q Consensus         2 d    2 (6)
                      -
T Consensus        51 ~   51 (51)
T signal           51 I   51 (51)
T 80_Apis          51 -   50 (50)
T 179_Acyrthosip   51 -   50 (50)
T 105_Ornithorhy   51 -   50 (50)
T 90_Saccoglossu   51 -   50 (50)
T 98_Trichinella   51 -   50 (50)
T 77_Ixodes        51 -   50 (50)
T 82_Aplysia       51 -   50 (50)
T 46_Pelodiscus    50 -   49 (49)
Confidence            0


No 19 
>P13264_16_Cy_Mito_ref_sig5_130
Probab=0.54  E-value=47  Score=3.31  Aligned_cols=1  Identities=0%  Similarity=-0.961  Sum_probs=0.0

Q 5_Mustela         2 D    2 (6)
Q Consensus         2 d    2 (6)
                      -
T Consensus        21 ~   21 (21)
T signal           21 V   21 (21)
T 49_Falco         21 -   20 (20)
T 55_Xenopus       21 -   20 (20)
T cl|XABBABABA|1   21 -   20 (20)
T 44_Melopsittac   21 -   20 (20)
Confidence            0


No 20 
>Q9H300_52_IM_Nu_ref_sig5_130
Probab=0.54  E-value=47  Score=7.11  Aligned_cols=3  Identities=100%  Similarity=1.243  Sum_probs=1.1

Q 5_Mustela         3 EEL    5 (6)
Q Consensus         3 eel    5 (6)
                      |||
T Consensus        27 eel   29 (57)
T signal           27 EEL   29 (57)
T 109              26 PEL   28 (55)
T 119              27 EEL   29 (57)
T 120              26 REL   28 (55)
T 129              24 RSF   26 (53)
T 130              27 EEL   29 (57)
T 136              27 EEH   29 (57)
T 117              26 REL   28 (56)
T 116               2 EQT    4 (32)
Confidence            343


Done!
