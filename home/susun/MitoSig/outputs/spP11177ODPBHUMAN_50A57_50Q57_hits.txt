Query         5_Mustela
Match_columns 7
No_of_seqs    2 out of 20
Neff          1.1 
Searched_HMMs 436
Date          Wed Sep  6 16:09:01 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_50A57_50Q57_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 Q9SVM8_34_Mito_ref_sig5_130      0.6      38   0.088    7.1   0.0    1    4-4      38-38  (39)
  2 P36527_26_Mito_ref_sig5_130      0.6      39   0.089    6.8   0.0    3    3-5      22-24  (31)
  3 Q8HXG5_29_IM_ref_sig5_130        0.6      39   0.089    6.9   0.0    1    6-6       6-6   (34)
  4 P25285_22_Mito_ref_sig5_130      0.6      41   0.093    6.5   0.0    1    6-6       5-5   (27)
  5 P41563_27_Mito_ref_sig5_130      0.6      43   0.099    6.5   0.0    1    6-6      14-14  (32)
  6 P15150_24_MitoM_ref_sig5_130     0.5      47    0.11    4.6   0.0    1    3-3      29-29  (29)
  7 Q91YT0_20_IM_ref_sig5_130        0.5      47    0.11    6.3   0.0    1    7-7       9-9   (25)
  8 P13619_42_Mito_IM_ref_sig5_130   0.5      47    0.11    4.7   0.0    1    1-1       1-1   (47)
  9 P43165_34_Mito_ref_sig5_130      0.5      48    0.11    6.7   0.0    1    6-6       7-7   (39)
 10 P36520_57_Mito_ref_sig5_130      0.5      48    0.11    7.4   0.0    1    3-3      62-62  (62)
 11 O13401_34_MM_ref_sig5_130        0.5      49    0.11    6.8   0.0    1    3-3      10-10  (39)
 12 P34942_23_MM_ref_sig5_130        0.5      50    0.11    3.3   0.0    1    3-3      28-28  (28)
 13 Q93170_22_Mito_ref_sig5_130      0.5      51    0.12    6.3   0.0    1    4-4      17-17  (27)
 14 P07271_45_Nu_IM_ref_sig5_130     0.5      53    0.12    7.0   0.0    1    5-5      26-26  (50)
 15 P37298_31_IM_ref_sig5_130        0.5      55    0.13    6.5   0.0    1    5-5      10-10  (36)
 16 P22027_25_Mito_IM_ref_sig5_130   0.5      56    0.13    6.0   0.0    1    6-6       4-4   (30)
 17 P25712_34_IM_ref_sig5_130        0.4      58    0.13    6.5   0.0    1    5-5       6-6   (39)
 18 P08461_77_MM_ref_sig5_130        0.4      59    0.13    7.6   0.0    1    1-1       1-1   (82)
 19 Q9NUB1_37_MM_ref_sig5_130        0.4      62    0.14    6.6   0.0    1    6-6      14-14  (42)
 20 P23934_28_IM_ref_sig5_130        0.4      62    0.14    6.2   0.0    1    6-6      11-11  (33)

No 1  
>Q9SVM8_34_Mito_ref_sig5_130
Probab=0.65  E-value=38  Score=7.10  Aligned_cols=1  Identities=100%  Similarity=2.327  Sum_probs=0.3

Q 5_Mustela         4 F    4 (7)
Q 11_Cavia          4 F    4 (7)
Q Consensus         4 f    4 (7)
                      |
T Consensus        38 F   38 (39)
T signal           38 F   38 (39)
T 10               39 F   39 (40)
T 8                39 F   39 (40)
T 9                39 F   39 (40)
T 11               39 F   39 (40)
T 4                43 F   43 (44)
T 5                40 F   40 (41)
T 6                40 F   40 (41)
T 7                40 F   40 (41)
T 3                48 F   48 (49)
Confidence            3


No 2  
>P36527_26_Mito_ref_sig5_130
Probab=0.64  E-value=39  Score=6.80  Aligned_cols=3  Identities=67%  Similarity=1.464  Sum_probs=1.3

Q 5_Mustela         3 VFL    5 (7)
Q 11_Cavia          3 VFL    5 (7)
Q Consensus         3 vfl    5 (7)
                      ||+
T Consensus        22 vfi   24 (31)
T signal           22 VFI   24 (31)
Confidence            443


No 3  
>Q8HXG5_29_IM_ref_sig5_130
Probab=0.64  E-value=39  Score=6.91  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.4

Q 5_Mustela         6 L    6 (7)
Q 11_Cavia          6 L    6 (7)
Q Consensus         6 l    6 (7)
                      |
T Consensus         6 L    6 (34)
T signal            6 L    6 (34)
T 47                6 L    6 (34)
T 52                6 L    6 (34)
T 29                6 L    6 (35)
T 41                6 L    6 (33)
T 45                6 L    6 (35)
T 46                6 L    6 (35)
T 51                6 L    6 (35)
T 53                6 A    6 (35)
T 21                4 L    4 (28)
Confidence            3


No 4  
>P25285_22_Mito_ref_sig5_130
Probab=0.62  E-value=41  Score=6.46  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.3

Q 5_Mustela         6 L    6 (7)
Q 11_Cavia          6 L    6 (7)
Q Consensus         6 l    6 (7)
                      |
T Consensus         5 l    5 (27)
T signal            5 L    5 (27)
T 107               5 L    5 (27)
T 127               5 L    5 (27)
T 128               5 L    5 (27)
T 134               5 L    5 (27)
T 138               5 V    5 (27)
T 145               5 L    5 (27)
T 146               5 L    5 (27)
T 147               5 L    5 (27)
T 152               5 L    5 (27)
Confidence            2


No 5  
>P41563_27_Mito_ref_sig5_130
Probab=0.59  E-value=43  Score=6.50  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.3

Q 5_Mustela         6 L    6 (7)
Q 11_Cavia          6 L    6 (7)
Q Consensus         6 l    6 (7)
                      +
T Consensus        14 ~   14 (32)
T signal           14 L   14 (32)
T 20               14 L   14 (30)
T 42               14 V   14 (32)
T 45               14 V   14 (31)
T 46               13 M   13 (30)
T 61               14 A   14 (31)
T 76               14 M   14 (30)
T 84               14 V   14 (31)
T 94               14 I   14 (31)
T 114              14 L   14 (31)
Confidence            2


No 6  
>P15150_24_MitoM_ref_sig5_130
Probab=0.54  E-value=47  Score=4.59  Aligned_cols=1  Identities=0%  Similarity=0.036  Sum_probs=0.0

Q 5_Mustela         3 V    3 (7)
Q 11_Cavia          3 V    3 (7)
Q Consensus         3 v    3 (7)
                      +
T Consensus        29 ~   29 (29)
T signal           29 A   29 (29)
T 53_Capra         29 V   29 (29)
T 51_Pundamilia    29 G   29 (29)
T 47_Xiphophorus   29 S   29 (29)
T 37_Sarcophilus   29 A   29 (29)
T 29_Mus           29 T   29 (29)
T 28_Ochotona      29 V   29 (29)
T cl|CABBABABA|1   29 I   29 (29)
Confidence            0


No 7  
>Q91YT0_20_IM_ref_sig5_130
Probab=0.54  E-value=47  Score=6.26  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.3

Q 5_Mustela         7 G    7 (7)
Q 11_Cavia          7 G    7 (7)
Q Consensus         7 g    7 (7)
                      |
T Consensus         9 G    9 (25)
T signal            9 G    9 (25)
T 132              11 G   11 (29)
T 93                9 G    9 (25)
Confidence            2


No 8  
>P13619_42_Mito_IM_ref_sig5_130
Probab=0.54  E-value=47  Score=4.73  Aligned_cols=1  Identities=0%  Similarity=-0.661  Sum_probs=0.0

Q 5_Mustela         1 E    1 (7)
Q 11_Cavia          1 E    1 (7)
Q Consensus         1 e    1 (7)
                      =
T Consensus         1 M    1 (47)
T signal            1 M    1 (47)
T 104_Caenorhabd    1 M    1 (45)
T 102_Salpingoec    1 M    1 (45)
T 85_Nasonia        1 M    1 (45)
T 79_Ixodes         1 M    1 (45)
T 71_Ficedula       1 G    1 (45)
T 61_Anas           1 K    1 (45)
T 60_Zonotrichia    1 P    1 (45)
T 53_Pelodiscus     1 M    1 (45)
T 47_Sarcophilus    1 M    1 (45)
Confidence            0


No 9  
>P43165_34_Mito_ref_sig5_130
Probab=0.53  E-value=48  Score=6.67  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.3

Q 5_Mustela         6 L    6 (7)
Q 11_Cavia          6 L    6 (7)
Q Consensus         6 l    6 (7)
                      |
T Consensus         7 l    7 (39)
T signal            7 L    7 (39)
T 16                2 L    2 (39)
T 35                2 L    2 (40)
T 23                7 L    7 (45)
T 28                7 W    7 (44)
T 6                 7 L    7 (45)
T 15                7 L    7 (45)
T 24                7 L    7 (45)
T 26                7 S    7 (45)
T 29                7 L    7 (45)
Confidence            2


No 10 
>P36520_57_Mito_ref_sig5_130
Probab=0.53  E-value=48  Score=7.43  Aligned_cols=1  Identities=0%  Similarity=-1.093  Sum_probs=0.0

Q 5_Mustela         3 V    3 (7)
Q 11_Cavia          3 V    3 (7)
Q Consensus         3 v    3 (7)
                      -
T Consensus        62 g   62 (62)
T signal           62 G   62 (62)
Confidence            0


No 11 
>O13401_34_MM_ref_sig5_130
Probab=0.52  E-value=49  Score=6.81  Aligned_cols=1  Identities=100%  Similarity=1.132  Sum_probs=0.3

Q 5_Mustela         3 V    3 (7)
Q 11_Cavia          3 V    3 (7)
Q Consensus         3 v    3 (7)
                      |
T Consensus        10 v   10 (39)
T signal           10 V   10 (39)
Confidence            3


No 12 
>P34942_23_MM_ref_sig5_130
Probab=0.51  E-value=50  Score=3.33  Aligned_cols=1  Identities=0%  Similarity=-0.595  Sum_probs=0.0

Q 5_Mustela         3 V    3 (7)
Q 11_Cavia          3 V    3 (7)
Q Consensus         3 v    3 (7)
                      -
T Consensus        28 ~   28 (28)
T signal           28 P   28 (28)
T 78_Musca         40 -   39 (39)
T 74_Strongyloce   40 -   39 (39)
T 62_Alligator     40 -   39 (39)
T 60_Xenopus       40 -   39 (39)
T 56_Anas          40 -   39 (39)
T 52_Meleagris     40 -   39 (39)
T 40_Saimiri       40 -   39 (39)
Confidence            0


No 13 
>Q93170_22_Mito_ref_sig5_130
Probab=0.50  E-value=51  Score=6.27  Aligned_cols=1  Identities=100%  Similarity=2.327  Sum_probs=0.2

Q 5_Mustela         4 F    4 (7)
Q 11_Cavia          4 F    4 (7)
Q Consensus         4 f    4 (7)
                      |
T Consensus        17 f   17 (27)
T signal           17 F   17 (27)
Confidence            2


No 14 
>P07271_45_Nu_IM_ref_sig5_130
Probab=0.48  E-value=53  Score=7.01  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.3

Q 5_Mustela         5 L    5 (7)
Q 11_Cavia          5 L    5 (7)
Q Consensus         5 l    5 (7)
                      |
T Consensus        26 l   26 (50)
T signal           26 L   26 (50)
Confidence            2


No 15 
>P37298_31_IM_ref_sig5_130
Probab=0.47  E-value=55  Score=6.54  Aligned_cols=1  Identities=0%  Similarity=0.932  Sum_probs=0.3

Q 5_Mustela         5 L    5 (7)
Q 11_Cavia          5 L    5 (7)
Q Consensus         5 l    5 (7)
                      +
T Consensus        10 ~   10 (36)
T signal           10 M   10 (36)
T 12                1 -    0 (25)
T 15                1 L    1 (27)
T 11                1 -    0 (25)
T 16                4 I    4 (30)
Confidence            2


No 16 
>P22027_25_Mito_IM_ref_sig5_130
Probab=0.46  E-value=56  Score=5.98  Aligned_cols=1  Identities=0%  Similarity=0.666  Sum_probs=0.4

Q 5_Mustela         6 L    6 (7)
Q 11_Cavia          6 L    6 (7)
Q Consensus         6 l    6 (7)
                      +
T Consensus         4 ~    4 (30)
T signal            4 F    4 (30)
T 24                1 -    0 (24)
T 25                1 -    0 (25)
T 26                1 -    0 (25)
T 32                1 -    0 (25)
T 35                1 -    0 (25)
T 39                1 -    0 (25)
T 40                1 -    0 (25)
T 41                1 -    0 (25)
Confidence            3


No 17 
>P25712_34_IM_ref_sig5_130
Probab=0.44  E-value=58  Score=6.45  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.3

Q 5_Mustela         5 L    5 (7)
Q 11_Cavia          5 L    5 (7)
Q Consensus         5 l    5 (7)
                      |
T Consensus         6 L    6 (39)
T signal            6 L    6 (39)
T 33                6 V    6 (39)
T 60                1 -    0 (32)
T 63                1 -    0 (28)
T 59                6 L    6 (40)
T 35                6 L    6 (39)
T 5                 6 L    6 (39)
T 1                 6 F    6 (37)
T 62                6 L    6 (37)
T 10                4 L    4 (35)
Confidence            2


No 18 
>P08461_77_MM_ref_sig5_130
Probab=0.44  E-value=59  Score=7.56  Aligned_cols=1  Identities=0%  Similarity=-0.661  Sum_probs=0.0

Q 5_Mustela         1 E    1 (7)
Q 11_Cavia          1 E    1 (7)
Q Consensus         1 e    1 (7)
                      -
T Consensus         1 M    1 (82)
T signal            1 M    1 (82)
T 153               1 M    1 (91)
T 154               1 M    1 (92)
T 156               1 M    1 (91)
T 161               1 M    1 (91)
T 166               1 M    1 (91)
T 145               1 M    1 (87)
Confidence            0


No 19 
>Q9NUB1_37_MM_ref_sig5_130
Probab=0.42  E-value=62  Score=6.56  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.2

Q 5_Mustela         6 L    6 (7)
Q 11_Cavia          6 L    6 (7)
Q Consensus         6 l    6 (7)
                      |
T Consensus        14 l   14 (42)
T signal           14 L   14 (42)
T 89               14 L   14 (42)
T 91               14 L   14 (42)
T 95               14 L   14 (33)
T 98               14 V   14 (37)
T 103              14 L   14 (41)
T 107              14 L   14 (39)
T 110              14 L   14 (42)
T 93               14 R   14 (29)
T 105              14 L   14 (38)
Confidence            2


No 20 
>P23934_28_IM_ref_sig5_130
Probab=0.42  E-value=62  Score=6.22  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.2

Q 5_Mustela         6 L    6 (7)
Q 11_Cavia          6 L    6 (7)
Q Consensus         6 l    6 (7)
                      |
T Consensus        11 L   11 (33)
T signal           11 L   11 (33)
T 44               16 L   16 (38)
T 64               13 L   13 (35)
T 67               11 L   11 (33)
T 89               11 L   11 (33)
T 63               11 V   11 (35)
T 84               11 L   11 (33)
T 25               11 L   11 (26)
T 101              11 L   11 (25)
Confidence            2


Done!
