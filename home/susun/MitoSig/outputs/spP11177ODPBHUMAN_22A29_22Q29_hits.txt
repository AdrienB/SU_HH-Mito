Query         5_Mustela
Match_columns 7
No_of_seqs    4 out of 20
Neff          1.3 
Searched_HMMs 436
Date          Wed Sep  6 16:08:19 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_22A29_22Q29_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P11177_30_MM_ref_sig5_130       95.4 8.5E-06 1.9E-08   25.0   0.0    7    1-7      23-29  (35)
  2 P19234_31_IM_ref_sig5_130        8.7     1.7  0.0039   10.7   0.0    6    1-6      24-29  (36)
  3 P39112_41_MM_ref_sig5_130        6.7     2.4  0.0055   10.8   0.0    6    1-6      16-21  (46)
  4 Q9UJ68_23_Mito_Cy_Nu_ref_sig5_   4.6     3.8  0.0087    9.3   0.0    6    1-6      14-19  (28)
  5 Q3SZC1_27_MM_ref_sig5_130        1.5      15   0.035    7.8   0.0    3    4-6      22-24  (32)
  6 P40185_17_MM_ref_sig5_130        1.4      16   0.036    7.4   0.0    4    3-6       9-12  (22)
  7 P36516_59_Mito_ref_sig5_130      1.3      18    0.04    8.8   0.0    2    2-3      22-23  (64)
  8 Q7Z6M4_42_Mito_ref_sig5_130      1.3      18   0.041    8.2   0.0    1    2-2      12-12  (47)
  9 Q3ZBF3_26_Mito_ref_sig5_130      1.2      19   0.043    4.4   0.0    1    3-3      31-31  (31)
 10 P22354_18_Mito_ref_sig5_130      1.2      19   0.044    7.2   0.0    2    1-2      11-12  (23)
 11 P53219_38_Mito_ref_sig5_130      1.2      20   0.045    8.1   0.0    2    1-2      39-40  (43)
 12 P22778_45_Mito_IM_ref_sig5_130   1.1      22   0.051    8.1   0.0    1    6-6      49-49  (50)
 13 P31039_44_IM_ref_sig5_130        1.0      23   0.054    4.8   0.0    1    3-3      49-49  (49)
 14 P23965_28_MM_ref_sig5_130        1.0      25   0.056    7.3   0.0    1    5-5      21-21  (33)
 15 P25348_71_Mito_ref_sig5_130      0.9      25   0.058    8.6   0.0    1    4-4      24-24  (76)
 16 Q09544_18_Mito_IM_ref_sig5_130   0.9      26    0.06    6.8   0.0    1    6-6      22-22  (23)
 17 Q9Y6E7_28_MM_ref_sig5_130        0.9      27   0.062    7.2   0.0    2    3-4      16-17  (33)
 18 P16221_24_IM_ref_sig5_130        0.8      29   0.066    6.9   0.0    1    3-3      18-18  (29)
 19 P32089_13_IM_ref_sig5_130        0.8      29   0.066    6.4   0.0    1    6-6      15-15  (18)
 20 Q2M2T7_35_Mito_ref_sig5_130      0.8      30    0.07    7.3   0.0    1    2-2      29-29  (40)

No 1  
>P11177_30_MM_ref_sig5_130
Probab=95.37  E-value=8.5e-06  Score=25.00  Aligned_cols=7  Identities=86%  Similarity=1.251  Sum_probs=6.5

Q 5_Mustela         1 FHRTAPA    7 (7)
Q 2_Macaca          1 FHWTAPT    7 (7)
Q 1_Homo            1 FHWTAPA    7 (7)
Q 15_Ictidomys      1 FHRTTPA    7 (7)
Q Consensus         1 fh~tapa    7 (7)
                      |||++||
T Consensus        23 FHrs~PA   29 (35)
T signal           23 FHWTAPA   29 (35)
T 143              27 FHRSPPA   33 (39)
T 144              23 FHRSTPA   29 (35)
T 145              23 FHRTAPV   29 (35)
T 147              23 FHRTPSA   29 (35)
T 135              32 IHGSAPA   38 (44)
T 141              37 FRLSAPA   43 (49)
T 142              22 FHGSGSA   28 (34)
T 149              22 FHRSVPA   28 (34)
T 150              22 FHRTVPA   28 (34)
Confidence            8999996


No 2  
>P19234_31_IM_ref_sig5_130
Probab=8.65  E-value=1.7  Score=10.66  Aligned_cols=6  Identities=50%  Similarity=0.767  Sum_probs=3.4

Q 5_Mustela         1 FHRTAP    6 (7)
Q 2_Macaca          1 FHWTAP    6 (7)
Q 1_Homo            1 FHWTAP    6 (7)
Q 15_Ictidomys      1 FHRTTP    6 (7)
Q Consensus         1 fh~tap    6 (7)
                      +|.|+.
T Consensus        24 LHkTa~   29 (36)
T signal           24 LHKTAV   29 (36)
T 55               24 LHKTAV   29 (36)
T 99               19 LHKTAV   24 (31)
T 2                20 LHKTPA   25 (32)
T 13               20 LHKTVA   25 (32)
T 25               20 LHRSAP   25 (32)
T 102              25 LHGTAE   30 (36)
T 104              19 LHETPA   24 (31)
T 1                17 LHQSAV   22 (28)
T 33               18 LHTTTA   23 (29)
Confidence            366654


No 3  
>P39112_41_MM_ref_sig5_130
Probab=6.66  E-value=2.4  Score=10.83  Aligned_cols=6  Identities=50%  Similarity=1.060  Sum_probs=3.1

Q 5_Mustela         1 FHRTAP    6 (7)
Q 2_Macaca          1 FHWTAP    6 (7)
Q 1_Homo            1 FHWTAP    6 (7)
Q 15_Ictidomys      1 FHRTTP    6 (7)
Q Consensus         1 fh~tap    6 (7)
                      ||.-+|
T Consensus        16 fhsytp   21 (46)
T signal           16 FHSYTP   21 (46)
Confidence            555554


No 4  
>Q9UJ68_23_Mito_Cy_Nu_ref_sig5_130
Probab=4.62  E-value=3.8  Score=9.32  Aligned_cols=6  Identities=33%  Similarity=0.656  Sum_probs=3.0

Q 5_Mustela         1 FHRTAP    6 (7)
Q 2_Macaca          1 FHWTAP    6 (7)
Q 1_Homo            1 FHWTAP    6 (7)
Q 15_Ictidomys      1 FHRTTP    6 (7)
Q Consensus         1 fh~tap    6 (7)
                      ||..-|
T Consensus        14 ~~~~~p   19 (28)
T signal           14 LHSLFP   19 (28)
T 102              12 FHSLFP   17 (26)
T 105              12 LRGLLP   17 (26)
T 110              12 LSGPMP   17 (26)
T 111              12 FHSLFP   17 (26)
T 108              14 SS--IP   17 (26)
T 113              14 GF--LL   17 (26)
T 76               10 FLRTLH   15 (26)
Confidence            455444


No 5  
>Q3SZC1_27_MM_ref_sig5_130
Probab=1.46  E-value=15  Score=7.83  Aligned_cols=3  Identities=33%  Similarity=1.132  Sum_probs=1.1

Q 5_Mustela         4 TAP    6 (7)
Q 2_Macaca          4 TAP    6 (7)
Q 1_Homo            4 TAP    6 (7)
Q 15_Ictidomys      4 TTP    6 (7)
Q Consensus         4 tap    6 (7)
                      +.|
T Consensus        22 ~sP   24 (32)
T signal           22 SSP   24 (32)
T 132              22 PAP   24 (32)
T 133              22 TSS   24 (32)
T 108              22 TSP   24 (31)
T 111              24 TSP   26 (33)
T 98               24 SSP   26 (33)
T 106              24 ATP   26 (33)
T 114              21 TSP   23 (30)
T 121              22 TS-   23 (31)
T 97               26 TCP   28 (36)
Confidence            333


No 6  
>P40185_17_MM_ref_sig5_130
Probab=1.43  E-value=16  Score=7.35  Aligned_cols=4  Identities=100%  Similarity=1.431  Sum_probs=2.0

Q 5_Mustela         3 RTA.P    6 (7)
Q 2_Macaca          3 WTA.P    6 (7)
Q 1_Homo            3 WTA.P    6 (7)
Q 15_Ictidomys      3 RTT.P    6 (7)
Q Consensus         3 ~ta.p    6 (7)
                      +++ |
T Consensus         9 rs~.P   12 (22)
T signal            9 RTA.P   12 (22)
T 114               9 KT-.P   11 (20)
T 115               8 KS-.P   10 (19)
T 117               8 RT-.P   10 (19)
T 118               9 KA-.P   11 (20)
T 119               9 RSRqP   13 (22)
T 116               9 RS-.P   11 (20)
Confidence            445 5


No 7  
>P36516_59_Mito_ref_sig5_130
Probab=1.29  E-value=18  Score=8.82  Aligned_cols=2  Identities=50%  Similarity=0.734  Sum_probs=1.0

Q 5_Mustela         2 HR    3 (7)
Q 2_Macaca          2 HW    3 (7)
Q 1_Homo            2 HW    3 (7)
Q 15_Ictidomys      2 HR    3 (7)
Q Consensus         2 h~    3 (7)
                      ||
T Consensus        22 hw   23 (64)
T signal           22 HW   23 (64)
Confidence            44


No 8  
>Q7Z6M4_42_Mito_ref_sig5_130
Probab=1.28  E-value=18  Score=8.22  Aligned_cols=1  Identities=100%  Similarity=1.996  Sum_probs=0.3

Q 5_Mustela         2 H    2 (7)
Q 2_Macaca          2 H    2 (7)
Q 1_Homo            2 H    2 (7)
Q 15_Ictidomys      2 H    2 (7)
Q Consensus         2 h    2 (7)
                      |
T Consensus        12 h   12 (47)
T signal           12 H   12 (47)
T 36               12 H   12 (46)
T 42               12 H   12 (47)
T 38               12 H   12 (47)
T 33                5 H    5 (40)
T 39               12 R   12 (43)
T 34               10 P   10 (43)
T 30                7 R    7 (43)
T 31                5 P    5 (32)
T 29               20 P   20 (44)
Confidence            2


No 9  
>Q3ZBF3_26_Mito_ref_sig5_130
Probab=1.23  E-value=19  Score=4.39  Aligned_cols=1  Identities=0%  Similarity=-0.296  Sum_probs=0.0

Q 5_Mustela         3 R    3 (7)
Q 2_Macaca          3 W    3 (7)
Q 1_Homo            3 W    3 (7)
Q 15_Ictidomys      3 R    3 (7)
Q Consensus         3 ~    3 (7)
                      -
T Consensus        31 ~   31 (31)
T signal           31 P   31 (31)
T 96_Brugia        31 -   30 (30)
T 87_Branchiosto   31 -   30 (30)
T 86_Oryzias       31 -   30 (30)
T 84_Pediculus     31 -   30 (30)
T 70_Anas          31 -   30 (30)
T 69_Strongyloce   31 -   30 (30)
T 63_Zonotrichia   31 -   30 (30)
T 55_Alligator     31 -   30 (30)
T 44_Myotis        31 -   30 (30)
Confidence            0


No 10 
>P22354_18_Mito_ref_sig5_130
Probab=1.20  E-value=19  Score=7.20  Aligned_cols=2  Identities=100%  Similarity=2.161  Sum_probs=1.1

Q 5_Mustela         1 FH    2 (7)
Q 2_Macaca          1 FH    2 (7)
Q 1_Homo            1 FH    2 (7)
Q 15_Ictidomys      1 FH    2 (7)
Q Consensus         1 fh    2 (7)
                      ||
T Consensus        11 fh   12 (23)
T signal           11 FH   12 (23)
Confidence            55


No 11 
>P53219_38_Mito_ref_sig5_130
Probab=1.16  E-value=20  Score=8.08  Aligned_cols=2  Identities=100%  Similarity=2.161  Sum_probs=0.8

Q 5_Mustela         1 FH    2 (7)
Q 2_Macaca          1 FH    2 (7)
Q 1_Homo            1 FH    2 (7)
Q 15_Ictidomys      1 FH    2 (7)
Q Consensus         1 fh    2 (7)
                      ||
T Consensus        39 fh   40 (43)
T signal           39 FH   40 (43)
Confidence            34


No 12 
>P22778_45_Mito_IM_ref_sig5_130
Probab=1.05  E-value=22  Score=8.09  Aligned_cols=1  Identities=0%  Similarity=0.136  Sum_probs=0.2

Q 5_Mustela         6 P    6 (7)
Q 2_Macaca          6 P    6 (7)
Q 1_Homo            6 P    6 (7)
Q 15_Ictidomys      6 P    6 (7)
Q Consensus         6 p    6 (7)
                      |
T Consensus        49 s   49 (50)
T signal           49 S   49 (50)
T 194              53 S   53 (53)
T 196              51 S   51 (51)
T 197              44 P   44 (45)
T 185              51 S   51 (51)
T 191              49 T   49 (49)
T 192              47 P   47 (47)
T 195              47 P   47 (47)
T 189              42 S   42 (43)
T 193              49 T   49 (50)
Confidence            2


No 13 
>P31039_44_IM_ref_sig5_130
Probab=1.01  E-value=23  Score=4.84  Aligned_cols=1  Identities=0%  Similarity=-0.064  Sum_probs=0.0

Q 5_Mustela         3 R    3 (7)
Q 2_Macaca          3 W    3 (7)
Q 1_Homo            3 W    3 (7)
Q 15_Ictidomys      3 R    3 (7)
Q Consensus         3 ~    3 (7)
                      -
T Consensus        49 ~   49 (49)
T signal           49 S   49 (49)
T 83_Strongyloce   46 -   45 (45)
T 39_Sarcophilus   46 -   45 (45)
T 21_Mustela       46 -   45 (45)
T 94_Branchiosto   46 -   45 (45)
T 109_Pediculus    46 -   45 (45)
T 93_Sorex         46 -   45 (45)
T 33_Ictidomys     46 -   45 (45)
T 69_Tursiops      46 -   45 (45)
T 27_Saimiri       46 -   45 (45)
Confidence            0


No 14 
>P23965_28_MM_ref_sig5_130
Probab=0.96  E-value=25  Score=7.34  Aligned_cols=1  Identities=0%  Similarity=0.169  Sum_probs=0.3

Q 5_Mustela         5 A    5 (7)
Q 2_Macaca          5 A    5 (7)
Q 1_Homo            5 A    5 (7)
Q 15_Ictidomys      5 T    5 (7)
Q Consensus         5 a    5 (7)
                      .
T Consensus        21 ~   21 (33)
T signal           21 G   21 (33)
T 94               28 A   28 (43)
T 93               30 E   30 (46)
T 72               30 E   30 (49)
T 79               30 G   30 (46)
T 63               24 -   23 (34)
T 78               25 A   25 (37)
T 39               28 E   28 (44)
Confidence            3


No 15 
>P25348_71_Mito_ref_sig5_130
Probab=0.94  E-value=25  Score=8.58  Aligned_cols=1  Identities=0%  Similarity=-0.429  Sum_probs=0.4

Q 5_Mustela         4 T    4 (7)
Q 2_Macaca          4 T    4 (7)
Q 1_Homo            4 T    4 (7)
Q 15_Ictidomys      4 T    4 (7)
Q Consensus         4 t    4 (7)
                      +
T Consensus        24 ~   24 (76)
T signal           24 L   24 (76)
T 21               10 V   10 (63)
T 19                9 T    9 (57)
T 20                9 S    9 (52)
T 15               10 S   10 (44)
T 17                1 -    0 (34)
T 12                1 -    0 (27)
T 14                1 -    0 (38)
T 9                 1 -    0 (49)
T 13                1 -    0 (36)
Confidence            3


No 16 
>Q09544_18_Mito_IM_ref_sig5_130
Probab=0.91  E-value=26  Score=6.84  Aligned_cols=1  Identities=100%  Similarity=2.527  Sum_probs=0.4

Q 5_Mustela         6 P    6 (7)
Q 2_Macaca          6 P    6 (7)
Q 1_Homo            6 P    6 (7)
Q 15_Ictidomys      6 P    6 (7)
Q Consensus         6 p    6 (7)
                      |
T Consensus        22 p   22 (23)
T signal           22 P   22 (23)
Confidence            3


No 17 
>Q9Y6E7_28_MM_ref_sig5_130
Probab=0.89  E-value=27  Score=7.21  Aligned_cols=2  Identities=0%  Similarity=-0.362  Sum_probs=0.8

Q 5_Mustela         3 RT    4 (7)
Q 2_Macaca          3 WT    4 (7)
Q 1_Homo            3 WT    4 (7)
Q 15_Ictidomys      3 RT    4 (7)
Q Consensus         3 ~t    4 (7)
                      |.
T Consensus        16 W~   17 (33)
T signal           16 WI   17 (33)
T 116              16 WV   17 (33)
T 117              14 LM   15 (31)
T 119              13 WI   14 (30)
T 120              17 WV   18 (34)
T 121              16 WL   17 (33)
T 135              16 WI   17 (33)
T 140              16 WM   17 (33)
T 112              16 WT   17 (34)
Confidence            43


No 18 
>P16221_24_IM_ref_sig5_130
Probab=0.84  E-value=29  Score=6.95  Aligned_cols=1  Identities=0%  Similarity=-0.064  Sum_probs=0.3

Q 5_Mustela         3 R    3 (7)
Q 2_Macaca          3 W    3 (7)
Q 1_Homo            3 W    3 (7)
Q 15_Ictidomys      3 R    3 (7)
Q Consensus         3 ~    3 (7)
                      |
T Consensus        18 ~   18 (29)
T signal           18 T   18 (29)
T 24               18 T   18 (29)
T 10               18 R   18 (29)
T 2                18 W   18 (29)
T 18               18 W   18 (29)
T 5                18 R   18 (29)
T 8                18 R   18 (29)
T 29               18 R   18 (29)
T 3                17 Q   17 (28)
T 32               16 R   16 (27)
Confidence            3


No 19 
>P32089_13_IM_ref_sig5_130
Probab=0.84  E-value=29  Score=6.37  Aligned_cols=1  Identities=100%  Similarity=2.527  Sum_probs=0.3

Q 5_Mustela         6 P    6 (7)
Q 2_Macaca          6 P    6 (7)
Q 1_Homo            6 P    6 (7)
Q 15_Ictidomys      6 P    6 (7)
Q Consensus         6 p    6 (7)
                      |
T Consensus        15 p   15 (18)
T signal           15 P   15 (18)
T 136              13 P   13 (16)
T 126              15 P   15 (18)
T 127              15 P   15 (18)
Confidence            3


No 20 
>Q2M2T7_35_Mito_ref_sig5_130
Probab=0.80  E-value=30  Score=7.27  Aligned_cols=1  Identities=100%  Similarity=1.996  Sum_probs=0.4

Q 5_Mustela         2 H    2 (7)
Q 2_Macaca          2 H    2 (7)
Q 1_Homo            2 H    2 (7)
Q 15_Ictidomys      2 H    2 (7)
Q Consensus         2 h    2 (7)
                      |
T Consensus        29 H   29 (40)
T signal           29 H   29 (40)
T 46               18 H   18 (29)
T 50               28 H   28 (39)
T 51               28 H   28 (39)
T 54               29 H   29 (40)
T 43               31 Q   31 (42)
T 44               30 H   30 (41)
T 34               32 H   32 (43)
T 41               32 Q   32 (43)
T 31               25 H   25 (36)
Confidence            3


Done!
