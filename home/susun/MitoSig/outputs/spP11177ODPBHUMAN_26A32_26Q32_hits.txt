Query         5_Mustela
Match_columns 6
No_of_seqs    4 out of 20
Neff          1.3 
Searched_HMMs 436
Date          Wed Sep  6 16:08:24 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_26A32_26Q32_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P11177_30_MM_ref_sig5_130       87.2  0.0017 3.9E-06   18.1   0.0    6    1-6      27-32  (35)
  2 P35434_22_Mito_IM_ref_sig5_130   1.5      15   0.033    7.4   0.0    1    2-2       3-3   (27)
  3 Q0P5L8_21_Mito_Nu_ref_sig5_130   1.2      18   0.042    4.0   0.0    1    2-2      26-26  (26)
  4 Q09544_18_Mito_IM_ref_sig5_130   1.2      20   0.045    6.9   0.0    1    2-2      22-22  (23)
  5 P09380_17_Mito_MM_Nu_ref_sig5_   1.1      21   0.048    6.7   0.0    2    5-6       7-8   (22)
  6 Q96CB9_25_Mito_ref_sig5_130      1.1      22    0.05    3.6   0.0    1    1-1       1-1   (30)
  7 Q02218_40_MM_ref_sig5_130        1.0      25   0.057    5.7   0.0    1    2-2      45-45  (45)
  8 Q7M0E7_30_Mito_ref_sig5_130      0.9      25   0.058    7.0   0.0    1    5-5      32-32  (35)
  9 P53590_38_Mito_ref_sig5_130      0.9      26   0.059    7.5   0.0    1    2-2      43-43  (43)
 10 P25285_22_Mito_ref_sig5_130      0.9      28   0.065    6.6   0.0    1    4-4      15-15  (27)
 11 Q9UII2_25_Mito_ref_sig5_130      0.8      31    0.07    6.7   0.0    1    5-5      20-20  (30)
 12 Q95108_59_Mito_ref_sig5_130      0.8      32   0.072    7.6   0.0    1    2-2      64-64  (64)
 13 Q04728_8_MM_ref_sig5_130         0.8      32   0.074    5.6   0.0    3    4-6       7-9   (13)
 14 P25846_21_Mito_ref_sig5_130      0.7      33   0.075    6.5   0.0    3    2-4       8-10  (26)
 15 P07251_35_IM_ref_sig5_130        0.7      33   0.076    7.1   0.0    1    2-2      24-24  (40)
 16 Q99757_59_Mito_ref_sig5_130      0.7      33   0.077    7.6   0.0    1    2-2      64-64  (64)
 17 O00142_33_Mito_ref_sig5_130      0.7      37   0.085    3.9   0.0    1    2-2      38-38  (38)
 18 P01096_25_Mito_ref_sig5_130      0.7      38   0.087    6.5   0.0    1    5-5      20-20  (30)
 19 Q99797_35_MM_ref_sig5_130        0.6      39    0.09    6.8   0.0    1    4-4      16-16  (40)
 20 P46367_24_MM_ref_sig5_130        0.6      40   0.091    6.4   0.0    1    5-5      19-19  (29)

No 1  
>P11177_30_MM_ref_sig5_130
Probab=87.16  E-value=0.0017  Score=18.07  Aligned_cols=6  Identities=100%  Similarity=1.193  Sum_probs=5.0

Q 5_Mustela         1 APAALQ    6 (6)
Q 2_Macaca          1 APTALQ    6 (6)
Q 15_Ictidomys      1 TPAALQ    6 (6)
Q 11_Cavia          1 APATLQ    6 (6)
Q Consensus         1 apaalq    6 (6)
                      +|||+|
T Consensus        27 ~PAAvQ   32 (35)
T signal           27 APAALQ   32 (35)
T 143              31 PPAAIQ   36 (39)
T 144              27 TPAALQ   32 (35)
T 145              27 APVSLQ   32 (35)
T 147              27 PSAAVQ   32 (35)
T 135              36 APAAVQ   41 (44)
T 141              41 APAAIQ   46 (49)
T 142              26 GSAAVQ   31 (34)
T 149              26 VPAAVQ   31 (34)
T 150              26 VPAAVQ   31 (34)
Confidence            589988


No 2  
>P35434_22_Mito_IM_ref_sig5_130
Probab=1.52  E-value=15  Score=7.44  Aligned_cols=1  Identities=100%  Similarity=2.527  Sum_probs=0.3

Q 5_Mustela         2 P    2 (6)
Q 2_Macaca          2 P    2 (6)
Q 15_Ictidomys      2 P    2 (6)
Q 11_Cavia          2 P    2 (6)
Q Consensus         2 p    2 (6)
                      |
T Consensus         3 p    3 (27)
T signal            3 P    3 (27)
T 160               3 P    3 (27)
T 152               3 P    3 (27)
T 140               3 S    3 (25)
T 143               3 P    3 (33)
T 132               3 P    3 (34)
T 137               3 P    3 (29)
T 121               3 A    3 (25)
T 127               3 A    3 (24)
T 120               3 S    3 (26)
Confidence            3


No 3  
>Q0P5L8_21_Mito_Nu_ref_sig5_130
Probab=1.24  E-value=18  Score=3.96  Aligned_cols=1  Identities=0%  Similarity=-0.064  Sum_probs=0.0

Q 5_Mustela         2 P    2 (6)
Q 2_Macaca          2 P    2 (6)
Q 15_Ictidomys      2 P    2 (6)
Q 11_Cavia          2 P    2 (6)
Q Consensus         2 p    2 (6)
                      -
T Consensus        26 ~   26 (26)
T signal           26 Q   26 (26)
T 60               27 -   26 (26)
T 108_Galdieria    24 -   23 (23)
T 104_Cyanidiosc   24 -   23 (23)
T 102_Acanthamoe   24 -   23 (23)
T 78_Musca         24 -   23 (23)
T 76_Aplysia       24 -   23 (23)
T 74_Branchiosto   24 -   23 (23)
T 57_Loxodonta     24 -   23 (23)
T 6_Orcinus        24 -   23 (23)
Confidence            0


No 4  
>Q09544_18_Mito_IM_ref_sig5_130
Probab=1.18  E-value=20  Score=6.93  Aligned_cols=1  Identities=100%  Similarity=2.527  Sum_probs=0.4

Q 5_Mustela         2 P    2 (6)
Q 2_Macaca          2 P    2 (6)
Q 15_Ictidomys      2 P    2 (6)
Q 11_Cavia          2 P    2 (6)
Q Consensus         2 p    2 (6)
                      |
T Consensus        22 p   22 (23)
T signal           22 P   22 (23)
Confidence            3


No 5  
>P09380_17_Mito_MM_Nu_ref_sig5_130
Probab=1.10  E-value=21  Score=6.71  Aligned_cols=2  Identities=100%  Similarity=1.115  Sum_probs=0.7

Q 5_Mustela         5 LQ    6 (6)
Q 2_Macaca          5 LQ    6 (6)
Q 15_Ictidomys      5 LQ    6 (6)
Q 11_Cavia          5 LQ    6 (6)
Q Consensus         5 lq    6 (6)
                      +|
T Consensus         7 ~Q    8 (22)
T signal            7 LQ    8 (22)
T 26                7 LQ    8 (20)
T 18                7 AQ    8 (20)
T 21                7 AQ    8 (20)
T 22                7 LQ    8 (20)
T 23                7 WQ    8 (21)
T 27                7 VQ    8 (21)
T 30                7 FQ    8 (21)
T 24                7 AQ    8 (20)
T 17                6 LQ    7 (18)
Confidence            33


No 6  
>Q96CB9_25_Mito_ref_sig5_130
Probab=1.08  E-value=22  Score=3.62  Aligned_cols=1  Identities=0%  Similarity=-0.230  Sum_probs=0.0

Q 5_Mustela         1 A    1 (6)
Q 2_Macaca          1 A    1 (6)
Q 15_Ictidomys      1 T    1 (6)
Q 11_Cavia          1 A    1 (6)
Q Consensus         1 a    1 (6)
                      -
T Consensus         1 M    1 (30)
T signal            1 M    1 (30)
T 100_Pediculus     1 M    1 (30)
T 96_Nematostell    1 M    1 (30)
T 80_Bombus         1 M    1 (30)
T 78_Aedes          1 M    1 (30)
T 63_Columba        1 M    1 (30)
Confidence            0


No 7  
>Q02218_40_MM_ref_sig5_130
Probab=0.96  E-value=25  Score=5.75  Aligned_cols=1  Identities=0%  Similarity=0.102  Sum_probs=0.0

Q 5_Mustela         2 P    2 (6)
Q 2_Macaca          2 P    2 (6)
Q 15_Ictidomys      2 P    2 (6)
Q 11_Cavia          2 P    2 (6)
Q Consensus         2 p    2 (6)
                      -
T Consensus        45 ~   45 (45)
T signal           45 A   45 (45)
T 53_Papio         45 A   45 (45)
T 54_Pelodiscus    45 A   45 (45)
T 67_Gorilla       45 M   45 (45)
T 61_Mesocricetu   45 G   45 (45)
T 65_Takifugu      45 -   44 (44)
T 67_Gorilla       45 -   44 (44)
T 70_Oryzias       45 -   44 (44)
T 61_Mesocricetu   45 -   44 (44)
T 1                45 M   45 (45)
Confidence            0


No 8  
>Q7M0E7_30_Mito_ref_sig5_130
Probab=0.94  E-value=25  Score=6.98  Aligned_cols=1  Identities=0%  Similarity=0.932  Sum_probs=0.2

Q 5_Mustela         5 L    5 (6)
Q 2_Macaca          5 L    5 (6)
Q 15_Ictidomys      5 L    5 (6)
Q 11_Cavia          5 L    5 (6)
Q Consensus         5 l    5 (6)
                      +
T Consensus        32 I   32 (35)
T signal           32 I   32 (35)
T 53               32 I   32 (35)
T 33               31 I   31 (34)
T 49               31 I   31 (34)
T 47               32 I   32 (35)
T 31               30 I   30 (33)
T 38               30 I   30 (33)
T 34               33 I   33 (36)
T 27               27 I   27 (30)
T 30               27 I   27 (30)
Confidence            2


No 9  
>P53590_38_Mito_ref_sig5_130
Probab=0.93  E-value=26  Score=7.47  Aligned_cols=1  Identities=0%  Similarity=-0.164  Sum_probs=0.0

Q 5_Mustela         2 P    2 (6)
Q 2_Macaca          2 P    2 (6)
Q 15_Ictidomys      2 P    2 (6)
Q 11_Cavia          2 P    2 (6)
Q Consensus         2 p    2 (6)
                      -
T Consensus        43 E   43 (43)
T signal           43 E   43 (43)
T 34               41 E   41 (41)
T 62               41 E   41 (41)
T 65               41 E   41 (41)
T 74               43 E   43 (43)
T 84               41 E   41 (41)
T 92               41 E   41 (41)
T 57               31 E   31 (31)
T 44               40 E   40 (40)
T 24               27 E   27 (27)
Confidence            0


No 10 
>P25285_22_Mito_ref_sig5_130
Probab=0.85  E-value=28  Score=6.65  Aligned_cols=1  Identities=100%  Similarity=0.800  Sum_probs=0.3

Q 5_Mustela         4 A    4 (6)
Q 2_Macaca          4 A    4 (6)
Q 15_Ictidomys      4 A    4 (6)
Q 11_Cavia          4 T    4 (6)
Q Consensus         4 a    4 (6)
                      +
T Consensus        15 ~   15 (27)
T signal           15 A   15 (27)
T 107              15 A   15 (27)
T 127              15 A   15 (27)
T 128              15 V   15 (27)
T 134              15 V   15 (27)
T 138              15 T   15 (27)
T 145              15 T   15 (27)
T 146              15 T   15 (27)
T 147              15 D   15 (27)
T 152              15 V   15 (27)
Confidence            2


No 11 
>Q9UII2_25_Mito_ref_sig5_130
Probab=0.79  E-value=31  Score=6.72  Aligned_cols=1  Identities=0%  Similarity=0.932  Sum_probs=0.2

Q 5_Mustela         5 L    5 (6)
Q 2_Macaca          5 L    5 (6)
Q 15_Ictidomys      5 L    5 (6)
Q 11_Cavia          5 L    5 (6)
Q Consensus         5 l    5 (6)
                      +
T Consensus        20 m   20 (30)
T signal           20 M   20 (30)
T 61               20 L   20 (29)
T 41               20 I   20 (30)
T 43               20 L   20 (30)
T 48               20 V   20 (30)
T 24               20 M   20 (29)
T 57               17 M   17 (26)
T 22               20 M   20 (30)
T 72               20 L   20 (30)
Confidence            2


No 12 
>Q95108_59_Mito_ref_sig5_130
Probab=0.77  E-value=32  Score=7.65  Aligned_cols=1  Identities=0%  Similarity=-0.861  Sum_probs=0.0

Q 5_Mustela         2 P    2 (6)
Q 2_Macaca          2 P    2 (6)
Q 15_Ictidomys      2 P    2 (6)
Q 11_Cavia          2 P    2 (6)
Q Consensus         2 p    2 (6)
                      =
T Consensus        64 i   64 (64)
T signal           64 I   64 (64)
T 82               65 I   65 (65)
T 80               62 V   62 (62)
T 79               66 I   66 (66)
T 58               47 V   47 (47)
T 77               67 V   67 (67)
T 78               70 V   70 (70)
T 76               68 V   68 (68)
Confidence            0


No 13 
>Q04728_8_MM_ref_sig5_130
Probab=0.76  E-value=32  Score=5.58  Aligned_cols=3  Identities=67%  Similarity=0.612  Sum_probs=1.3

Q 5_Mustela         4 ALQ    6 (6)
Q 2_Macaca          4 ALQ    6 (6)
Q 15_Ictidomys      4 ALQ    6 (6)
Q 11_Cavia          4 TLQ    6 (6)
Q Consensus         4 alq    6 (6)
                      -||
T Consensus         7 llq    9 (13)
T signal            7 LLQ    9 (13)
T 90                7 LLQ    9 (13)
T 88                7 LLQ    9 (13)
Confidence            344


No 14 
>P25846_21_Mito_ref_sig5_130
Probab=0.75  E-value=33  Score=6.51  Aligned_cols=3  Identities=67%  Similarity=1.176  Sum_probs=1.3

Q 5_Mustela         2 PAA    4 (6)
Q 2_Macaca          2 PTA    4 (6)
Q 15_Ictidomys      2 PAA    4 (6)
Q 11_Cavia          2 PAT    4 (6)
Q Consensus         2 paa    4 (6)
                      |+|
T Consensus         8 pta   10 (26)
T signal            8 PTA   10 (26)
Confidence            444


No 15 
>P07251_35_IM_ref_sig5_130
Probab=0.74  E-value=33  Score=7.06  Aligned_cols=1  Identities=100%  Similarity=2.527  Sum_probs=0.3

Q 5_Mustela         2 P    2 (6)
Q 2_Macaca          2 P    2 (6)
Q 15_Ictidomys      2 P    2 (6)
Q 11_Cavia          2 P    2 (6)
Q Consensus         2 p    2 (6)
                      |
T Consensus        24 P   24 (40)
T signal           24 P   24 (40)
T 174              30 P   30 (44)
T 178              29 P   29 (43)
T 183              29 P   29 (43)
T 186              27 P   27 (41)
T 185              24 P   24 (39)
T 172              27 P   27 (40)
T 173              27 P   27 (39)
T 175              30 P   30 (41)
T 176              27 P   27 (40)
Confidence            3


No 16 
>Q99757_59_Mito_ref_sig5_130
Probab=0.74  E-value=33  Score=7.60  Aligned_cols=1  Identities=0%  Similarity=-0.861  Sum_probs=0.0

Q 5_Mustela         2 P    2 (6)
Q 2_Macaca          2 P    2 (6)
Q 15_Ictidomys      2 P    2 (6)
Q 11_Cavia          2 P    2 (6)
Q Consensus         2 p    2 (6)
                      =
T Consensus        64 i   64 (64)
T signal           64 I   64 (64)
T 80               62 V   62 (62)
T 82               64 I   64 (64)
T 81               65 I   65 (65)
T 88               64 I   64 (64)
T 78               66 I   66 (66)
T 61               47 V   47 (47)
T 65               43 V   43 (43)
T 77               70 V   70 (70)
T 76               68 V   68 (68)
Confidence            0


No 17 
>O00142_33_Mito_ref_sig5_130
Probab=0.67  E-value=37  Score=3.89  Aligned_cols=1  Identities=0%  Similarity=0.102  Sum_probs=0.0

Q 5_Mustela         2 P    2 (6)
Q 2_Macaca          2 P    2 (6)
Q 15_Ictidomys      2 P    2 (6)
Q 11_Cavia          2 P    2 (6)
Q Consensus         2 p    2 (6)
                      -
T Consensus        38 ~   38 (38)
T signal           38 A   38 (38)
T 92_Pediculus     35 -   34 (34)
T 83_Nematostell   35 -   34 (34)
T 79_Bombus        35 -   34 (34)
T 75_Bombyx        35 -   34 (34)
T 57_Xiphophorus   35 -   34 (34)
T 45_Anolis        35 -   34 (34)
T 40_Monodelphis   35 -   34 (34)
Confidence            0


No 18 
>P01096_25_Mito_ref_sig5_130
Probab=0.66  E-value=38  Score=6.47  Aligned_cols=1  Identities=0%  Similarity=0.932  Sum_probs=0.2

Q 5_Mustela         5 L    5 (6)
Q 2_Macaca          5 L    5 (6)
Q 15_Ictidomys      5 L    5 (6)
Q 11_Cavia          5 L    5 (6)
Q Consensus         5 l    5 (6)
                      +
T Consensus        20 m   20 (30)
T signal           20 M   20 (30)
T 26               20 M   20 (30)
T 48               20 L   20 (30)
T 60               20 L   20 (30)
T 69               20 M   20 (30)
T 38               20 L   20 (30)
T 42               20 V   20 (30)
T 40               20 V   20 (30)
T 49               20 V   20 (30)
T 23               20 M   20 (29)
Confidence            2


No 19 
>Q99797_35_MM_ref_sig5_130
Probab=0.64  E-value=39  Score=6.84  Aligned_cols=1  Identities=100%  Similarity=0.800  Sum_probs=0.3

Q 5_Mustela         4 A..    4 (6)
Q 2_Macaca          4 A..    4 (6)
Q 15_Ictidomys      4 A..    4 (6)
Q 11_Cavia          4 T..    4 (6)
Q Consensus         4 a..    4 (6)
                      +  
T Consensus        16 a..   16 (40)
T signal           16 A..   16 (40)
T 102              16 A..   16 (40)
T 164              16 A..   16 (40)
T 180              16 A..   16 (40)
T 183              16 A..   16 (40)
T 122              17 L..   17 (39)
T 158              21 L..   21 (37)
T 168              16 A..   16 (40)
T 174              16 A..   16 (40)
T 155              16 Pat   18 (41)
Confidence            2  


No 20 
>P46367_24_MM_ref_sig5_130
Probab=0.63  E-value=40  Score=6.42  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.3

Q 5_Mustela         5 L    5 (6)
Q 2_Macaca          5 L    5 (6)
Q 15_Ictidomys      5 L    5 (6)
Q 11_Cavia          5 L    5 (6)
Q Consensus         5 l    5 (6)
                      +
T Consensus        19 ~   19 (29)
T signal           19 L   19 (29)
T 98               20 Y   20 (30)
T 105              20 I   20 (30)
T 100              27 L   27 (37)
Confidence            3


Done!
