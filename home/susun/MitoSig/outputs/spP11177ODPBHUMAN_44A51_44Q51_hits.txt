Query         5_Mustela
Match_columns 7
No_of_seqs    1 out of 20
Neff          1.0 
Searched_HMMs 436
Date          Wed Sep  6 16:08:52 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_44A51_44Q51_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P36526_16_Mito_ref_sig5_130      0.2 1.4E+02    0.33    4.5   0.0    3    3-5      17-19  (21)
  2 Q04467_39_Mito_ref_sig5_130      0.2 1.4E+02    0.33    2.4   0.0    1    5-5      43-43  (44)
  3 P51557_62_Mito_ref_sig5_130      0.2 1.4E+02    0.33    4.2   0.0    6    2-7      34-39  (67)
  4 P05165_52_MM_ref_sig5_130        0.2 1.4E+02    0.33    2.5   0.0    1    6-6      57-57  (57)
  5 P11325_9_MM_ref_sig5_130         0.2 1.5E+02    0.34    4.1   0.0    3    3-5       2-4   (14)
  6 P17764_30_Mito_ref_sig5_130      0.2 1.5E+02    0.34    5.1   0.0    3    3-5      26-28  (35)
  7 P28834_11_Mito_ref_sig5_130      0.2 1.5E+02    0.35    4.2   0.0    3    3-5       2-4   (16)
  8 P83565_46_Mito_ref_sig5_130      0.2 1.5E+02    0.35    3.0   0.0    5    1-5      47-51  (51)
  9 P83484_51_Mito_IM_ref_sig5_130   0.2 1.5E+02    0.35    4.3   0.0    3    3-5      45-47  (56)
 10 P06576_47_Mito_IM_ref_sig5_130   0.2 1.5E+02    0.35    5.5   0.0    2    5-6      44-45  (52)
 11 P07926_68_MitoM_ref_sig5_130     0.2 1.6E+02    0.36    5.8   0.0    2    5-6      68-69  (73)
 12 P10719_46_Mito_IM_ref_sig5_130   0.2 1.6E+02    0.36    5.4   0.0    2    5-6      44-45  (51)
 13 Q9UGC7_26_Mito_ref_sig5_130      0.2 1.6E+02    0.36    1.5   0.0    7    1-7      24-30  (31)
 14 P13619_42_Mito_IM_ref_sig5_130   0.2 1.6E+02    0.37    3.3   0.0    3    3-5       2-4   (47)
 15 P00829_48_Mito_IM_ref_sig5_130   0.2 1.6E+02    0.37    5.4   0.0    2    5-6      44-45  (53)
 16 P10612_39_MitoM_ref_sig5_130     0.2 1.6E+02    0.37    3.2   0.0    3    3-5       2-4   (44)
 17 Q0P5L8_21_Mito_Nu_ref_sig5_130   0.2 1.6E+02    0.38    1.9   0.0    1    3-3      24-24  (26)
 18 P36528_19_Mito_ref_sig5_130      0.2 1.7E+02    0.39    4.5   0.0    3    3-5       7-9   (24)
 19 P13196_56_MM_ref_sig5_130        0.1 1.7E+02    0.39    2.8   0.0    5    3-7      11-15  (61)
 20 P26969_86_Mito_ref_sig5_130      0.1 1.7E+02    0.39    6.0   0.0    3    3-5       1-3   (91)

No 1  
>P36526_16_Mito_ref_sig5_130
Probab=0.18  E-value=1.4e+02  Score=4.48  Aligned_cols=3  Identities=67%  Similarity=0.955  Sum_probs=0.0

Q 5_Mustela         3 LER    5 (7)
Q Consensus         3 ler    5 (7)
                      |.|
T Consensus        17 LtR   19 (21)
T signal           17 LTR   19 (21)
T 11               17 LTR   19 (21)
T 13               17 LTR   19 (21)
T 14               17 LTR   19 (21)
T 18               16 LTR   18 (20)
T 20               17 LLR   19 (21)
T 5                17 LRR   19 (21)
T 7                17 LRR   19 (21)
T 8                17 LRR   19 (21)
T 9                13 LKR   15 (17)


No 2  
>Q04467_39_Mito_ref_sig5_130
Probab=0.18  E-value=1.4e+02  Score=2.37  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.0

Q 5_Mustela         5 R    5 (7)
Q Consensus         5 r    5 (7)
                      |
T Consensus        43 ~   43 (44)
T signal           43 R   43 (44)
T 92_Chlorella     41 -   40 (40)
T 86_Trichoplax    41 -   40 (40)
T 76_Bombyx        41 -   40 (40)
T 69_Meleagris     41 -   40 (40)
T 65_Latimeria     41 -   40 (40)
T 38_Sarcophilus   41 -   40 (40)


No 3  
>P51557_62_Mito_ref_sig5_130
Probab=0.18  E-value=1.4e+02  Score=4.18  Aligned_cols=6  Identities=33%  Similarity=0.368  Sum_probs=0.0

Q 5_Mustela         2 ELERDE    7 (7)
Q Consensus         2 elerde    7 (7)
                      ++.++.
T Consensus        34 ~~~~~~   39 (67)
T signal           34 ELNWRA   39 (67)
T 64_Maylandia     34 SKLKML   39 (67)
T 49_Columba       34 ISQELS   39 (67)
T 77_Phytophthor   34 VEDAAA   39 (65)
T 76_Amphimedon    34 TGTRSN   39 (65)
T cl|CABBABABA|1   33 VLYEAE   38 (60)
T cl|QABBABABA|1   33 FRLEVV   38 (60)
T 44_Ictidomys     33 GDKVLS   38 (59)


No 4  
>P05165_52_MM_ref_sig5_130
Probab=0.18  E-value=1.4e+02  Score=2.46  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.0

Q 5_Mustela         6 D    6 (7)
Q Consensus         6 d    6 (7)
                      |
T Consensus        57 ~   57 (57)
T signal           57 D   57 (57)
T 91_Xenopus       55 -   54 (54)
T 88_Taeniopygia   55 -   54 (54)
T 68_Strongyloce   55 -   54 (54)
T 59_Acanthamoeb   55 -   54 (54)
T 52_Trichoplax    55 -   54 (54)
T 42_Condylura     55 -   54 (54)
T 6_Columba        55 -   54 (54)
T 1_Mustela        55 -   54 (54)


No 5  
>P11325_9_MM_ref_sig5_130
Probab=0.18  E-value=1.5e+02  Score=4.14  Aligned_cols=3  Identities=67%  Similarity=0.988  Sum_probs=0.0

Q 5_Mustela         3 LER    5 (7)
Q Consensus         3 ler    5 (7)
                      |.|
T Consensus         2 lsr    4 (14)
T signal            2 LSR    4 (14)


No 6  
>P17764_30_Mito_ref_sig5_130
Probab=0.17  E-value=1.5e+02  Score=5.07  Aligned_cols=3  Identities=67%  Similarity=0.877  Sum_probs=0.0

Q 5_Mustela         3 LER    5 (7)
Q Consensus         3 ler    5 (7)
                      .||
T Consensus        26 veR   28 (35)
T signal           26 LGR   28 (35)
T 144              29 MKR   31 (38)
T 149              29 MER   31 (38)
T 152              34 VER   36 (43)
T 154              29 VER   31 (38)
T 157              17 AER   19 (26)
T 161              29 VER   31 (38)
T 153              17 VKR   19 (26)
T 134              29 VHR   31 (38)
T 159              29 VER   31 (38)


No 7  
>P28834_11_Mito_ref_sig5_130
Probab=0.17  E-value=1.5e+02  Score=4.24  Aligned_cols=3  Identities=67%  Similarity=1.066  Sum_probs=0.0

Q 5_Mustela         3 LER    5 (7)
Q Consensus         3 ler    5 (7)
                      |.|
T Consensus         2 lnr    4 (16)
T signal            2 LNR    4 (16)


No 8  
>P83565_46_Mito_ref_sig5_130
Probab=0.17  E-value=1.5e+02  Score=3.01  Aligned_cols=5  Identities=40%  Similarity=0.215  Sum_probs=0.0

Q 5_Mustela         1 EELER    5 (7)
Q Consensus         1 eeler    5 (7)
                      .+..|
T Consensus        47 ~~~~~   51 (51)
T signal           47 AEPLR   51 (51)
T 76_Brugia        47 AKLR-   50 (50)
T 72_Bombyx        47 KKID-   50 (50)
T 65_Ficedula      47 PGGP-   50 (50)
T 58_Gallus        47 EANS-   50 (50)
T 44_Pantholops    47 PTSS-   50 (50)
T 39_Tursiops      47 RDAH-   50 (50)
T 18_Ictidomys     47 DRVE-   50 (50)


No 9  
>P83484_51_Mito_IM_ref_sig5_130
Probab=0.17  E-value=1.5e+02  Score=4.30  Aligned_cols=3  Identities=67%  Similarity=0.877  Sum_probs=0.0

Q 5_Mustela         3 LER    5 (7)
Q Consensus         3 ler    5 (7)
                      |.|
T Consensus        45 ~~~   47 (56)
T signal           45 LGR   47 (56)
T 34_Volvox        39 ---   38 (38)
T 134_Guillardia   39 ---   38 (38)
T 152_Trichinell   39 ---   38 (38)
T 159_Ixodes       39 ---   38 (38)
T 41_Galdieria     39 ---   38 (38)
T 112_Tribolium    39 ---   38 (38)
T 117_Ceratitis    39 ---   38 (38)
T 111_Musca        39 ---   38 (38)
T 39_Selaginella   39 ---   38 (38)


No 10 
>P06576_47_Mito_IM_ref_sig5_130
Probab=0.17  E-value=1.5e+02  Score=5.50  Aligned_cols=2  Identities=100%  Similarity=1.564  Sum_probs=0.0

Q 5_Mustela         5 RD    6 (7)
Q Consensus         5 rd    6 (7)
                      ||
T Consensus        44 Rd   45 (52)
T signal           44 RD   45 (52)
T 130              44 GD   45 (52)
T 148              46 RD   47 (53)
T 147              51 RD   52 (59)
T 112              37 RS   38 (44)
T 131              37 RG   38 (44)
T 135              36 RD   37 (43)
T 151              41 RD   42 (48)
T 143              49 RD   50 (57)
T 144              44 RN   45 (52)


No 11 
>P07926_68_MitoM_ref_sig5_130
Probab=0.17  E-value=1.6e+02  Score=5.85  Aligned_cols=2  Identities=100%  Similarity=1.564  Sum_probs=0.0

Q 5_Mustela         5 RD    6 (7)
Q Consensus         5 rd    6 (7)
                      ||
T Consensus        68 RD   69 (73)
T signal           68 RD   69 (73)
T 10               77 RD   78 (82)
T 14               56 --   55 (55)
T 22               38 --   37 (37)
T 9                62 CD   63 (67)
T 5                69 RD   70 (74)
T 6                63 RD   64 (68)
T 2                79 RD   80 (84)
T 3                79 RD   80 (84)
T 53               35 KD   36 (40)


No 12 
>P10719_46_Mito_IM_ref_sig5_130
Probab=0.17  E-value=1.6e+02  Score=5.44  Aligned_cols=2  Identities=100%  Similarity=1.564  Sum_probs=0.0

Q 5_Mustela         5 RD    6 (7)
Q Consensus         5 rd    6 (7)
                      ||
T Consensus        44 Rd   45 (51)
T signal           44 RD   45 (51)
T 130              44 GD   45 (50)
T 148              46 RD   47 (52)
T 110              37 RS   38 (44)
T 131              36 RD   37 (43)
T 135              37 RG   38 (44)
T 151              41 RD   42 (48)
T 149              51 RD   52 (58)
T 144              49 RD   50 (56)
T 145              44 RN   45 (51)


No 13 
>Q9UGC7_26_Mito_ref_sig5_130
Probab=0.16  E-value=1.6e+02  Score=1.49  Aligned_cols=7  Identities=14%  Similarity=0.202  Sum_probs=0.0

Q 5_Mustela         1 EELERDE    7 (7)
Q Consensus         1 eelerde    7 (7)
                      .......
T Consensus        24 ~~~~~~~   30 (31)
T signal           24 RPLSSGS   30 (31)
T 119_Populus      24 FRQTPSR   30 (30)
T 107_Phytophtho   24 SHNDGSF   30 (30)
T 101_Setaria      24 ACEEKDM   30 (30)
T 82_Anopheles     24 QISDDKI   30 (30)
T 69_Chrysemys     24 QQYAAYK   30 (30)
T 55_Geospiza      24 ELKQQIV   30 (30)


No 14 
>P13619_42_Mito_IM_ref_sig5_130
Probab=0.16  E-value=1.6e+02  Score=3.29  Aligned_cols=3  Identities=67%  Similarity=0.988  Sum_probs=0.0

Q 5_Mustela         3 LER    5 (7)
Q Consensus         3 ler    5 (7)
                      |-|
T Consensus         2 lSR    4 (47)
T signal            2 LSR    4 (47)
T 104_Caenorhabd    2 SLS    4 (45)
T 102_Salpingoec    2 MAL    4 (45)
T 85_Nasonia        2 LSR    4 (45)
T 79_Ixodes         2 LRV    4 (45)
T 71_Ficedula       2 RGG    4 (45)
T 61_Anas           2 RSS    4 (45)
T 60_Zonotrichia    2 VAV    4 (45)
T 53_Pelodiscus     2 HRT    4 (45)
T 47_Sarcophilus    2 VTS    4 (45)


No 15 
>P00829_48_Mito_IM_ref_sig5_130
Probab=0.16  E-value=1.6e+02  Score=5.44  Aligned_cols=2  Identities=100%  Similarity=1.564  Sum_probs=0.0

Q 5_Mustela         5 RD    6 (7)
Q Consensus         5 rd    6 (7)
                      ||
T Consensus        44 Rd   45 (53)
T signal           44 RD   45 (53)
T 125              44 GD   45 (52)
T 111              37 RS   38 (45)
T 131              37 RG   38 (45)
T 136              36 RD   37 (44)
T 152              41 RD   42 (49)
T 149              46 RD   47 (54)
T 142              49 RD   50 (58)
T 151              51 RD   52 (60)
T 147              44 RN   45 (53)


No 16 
>P10612_39_MitoM_ref_sig5_130
Probab=0.16  E-value=1.6e+02  Score=3.22  Aligned_cols=3  Identities=67%  Similarity=0.966  Sum_probs=0.0

Q 5_Mustela         3 LER    5 (7)
Q Consensus         3 ler    5 (7)
                      |.+
T Consensus         2 l~~    4 (44)
T signal            2 LAR    4 (44)
T 73_Anas           2 RRG    4 (43)
T 71_Monodelphis    2 VPT    4 (43)
T 67_Ornithorhyn    2 LEG    4 (43)
T 65_Anolis         2 LEA    4 (43)
T 63_Danio          2 ARW    4 (43)
T 52_Xenopus        2 LLL    4 (43)
T 50_Tupaia         2 ALN    4 (43)
T 48_Melopsittac    2 PIP    4 (43)
T 47_Falco          2 LAR    4 (43)


No 17 
>Q0P5L8_21_Mito_Nu_ref_sig5_130
Probab=0.16  E-value=1.6e+02  Score=1.91  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.0

Q 5_Mustela         3 L    3 (7)
Q Consensus         3 l    3 (7)
                      |
T Consensus        24 ~   24 (26)
T signal           24 L   24 (26)
T 60               27 -   26 (26)
T 108_Galdieria    24 -   23 (23)
T 104_Cyanidiosc   24 -   23 (23)
T 102_Acanthamoe   24 -   23 (23)
T 78_Musca         24 -   23 (23)
T 76_Aplysia       24 -   23 (23)
T 74_Branchiosto   24 -   23 (23)
T 57_Loxodonta     24 -   23 (23)
T 6_Orcinus        24 -   23 (23)


No 18 
>P36528_19_Mito_ref_sig5_130
Probab=0.15  E-value=1.7e+02  Score=4.53  Aligned_cols=3  Identities=67%  Similarity=1.099  Sum_probs=0.0

Q 5_Mustela         3 LER    5 (7)
Q Consensus         3 ler    5 (7)
                      |.|
T Consensus         7 lkr    9 (24)
T signal            7 LKR    9 (24)


No 19 
>P13196_56_MM_ref_sig5_130
Probab=0.15  E-value=1.7e+02  Score=2.76  Aligned_cols=5  Identities=40%  Similarity=0.368  Sum_probs=0.0

Q 5_Mustela         3 LERDE    7 (7)
Q Consensus         3 lerde    7 (7)
                      +.+.+
T Consensus        11 ~~~~~   15 (61)
T signal           11 LSRVP   15 (61)
T 72_Branchiosto   11 DKFLK   15 (61)
T 63_Columba       11 PTSDA   15 (61)
T 10_Canis         11 GRLRS   15 (61)
T 74_Schistosoma   10 CPFTV   14 (59)
T 72_Branchiosto   10 VETIV   14 (59)
T 63_Columba       10 DVQEM   14 (59)
T 62_Danio         10 VPAGH   14 (59)
T 57_Saimiri       10 AKVQQ   14 (59)
T 55_Taeniopygia   10 VAQQN   14 (59)


No 20 
>P26969_86_Mito_ref_sig5_130
Probab=0.15  E-value=1.7e+02  Score=6.03  Aligned_cols=3  Identities=67%  Similarity=1.232  Sum_probs=0.0

Q 5_Mustela         3 LER    5 (7)
Q Consensus         3 ler    5 (7)
                      +||
T Consensus         1 MER    3 (91)
T signal            1 MER    3 (91)
T 198               1 MER    3 (64)
T 196               1 MER    3 (92)
T 193               1 MER    3 (93)
T 195               1 MER    3 (83)
T 194               1 MER    3 (77)
T 191               1 MER    3 (69)
T 188               1 MER    3 (73)
T 189               1 MER    3 (79)
T 185               1 MER    3 (43)


Done!
