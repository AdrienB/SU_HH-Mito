Query         5_Mustela
Match_columns 7
No_of_seqs    1 out of 20
Neff          1.0 
Searched_HMMs 436
Date          Wed Sep  6 16:09:12 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_57A64_57Q64_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P09440_34_Mito_ref_sig5_130      0.3      90    0.21    6.0   0.0    1    5-5      34-34  (39)
  2 P49448_53_MM_ref_sig5_130        0.3      91    0.21    5.0   0.0    1    3-3      58-58  (58)
  3 Q9W6G7_44_Mito_ref_sig5_130      0.3      93    0.21    6.2   0.0    1    6-6      26-26  (49)
  4 Q99K67_32_Mito_ref_sig5_130      0.3      93    0.21    5.7   0.0    2    2-3      32-33  (37)
  5 Q9Y2D0_33_Mito_ref_sig5_130      0.3      93    0.21    3.8   0.0    1    3-3      38-38  (38)
  6 P81140_13_MM_ref_sig5_130        0.3      96    0.22    4.9   0.0    1    6-6      17-17  (18)
  7 P17694_33_IM_ref_sig5_130        0.3   1E+02    0.23    2.5   0.0    1    5-5      38-38  (38)
  8 P09380_17_Mito_MM_Nu_ref_sig5_   0.3   1E+02    0.24    5.1   0.0    3    3-5       6-8   (22)
  9 P36527_26_Mito_ref_sig5_130      0.2 1.1E+02    0.24    5.5   0.0    3    3-5       2-4   (31)
 10 Q25417_9_Mito_ref_sig5_130       0.2 1.1E+02    0.25    4.5   0.0    3    2-4      10-12  (14)
 11 P22570_32_MM_ref_sig5_130        0.2 1.1E+02    0.25    2.8   0.0    2    5-6      31-32  (37)
 12 P40360_13_Mito_ref_sig5_130      0.2 1.1E+02    0.26    4.7   0.0    2    6-7      12-13  (18)
 13 P22315_53_IM_ref_sig5_130        0.2 1.1E+02    0.26    6.1   0.0    4    2-5      47-50  (58)
 14 P17614_54_Mito_IM_ref_sig5_130   0.2 1.1E+02    0.26    4.8   0.0    6    2-7      50-55  (59)
 15 P14519_29_Mito_MM_Nu_IM_ref_si   0.2 1.2E+02    0.27    2.8   0.0    5    1-5      29-33  (34)
 16 Q95108_59_Mito_ref_sig5_130      0.2 1.2E+02    0.27    6.1   0.0    3    3-5       1-3   (64)
 17 P34897_29_Mito_MM_Nu_IM_ref_si   0.2 1.2E+02    0.27    3.2   0.0    5    1-5      29-33  (34)
 18 P30048_62_Mito_ref_sig5_130      0.2 1.2E+02    0.27    3.8   0.0    3    3-5      65-67  (67)
 19 P28834_11_Mito_ref_sig5_130      0.2 1.2E+02    0.28    4.6   0.0    2    4-5      15-16  (16)
 20 P17695_35_Cy_Mito_ref_sig5_130   0.2 1.2E+02    0.28    5.6   0.0    3    3-5      36-38  (40)

No 1  
>P09440_34_Mito_ref_sig5_130
Probab=0.29  E-value=90  Score=6.00  Aligned_cols=1  Identities=100%  Similarity=0.900  Sum_probs=0.4

Q 5_Mustela         5 Q    5 (7)
Q Consensus         5 q    5 (7)
                      |
T Consensus        34 q   34 (39)
T signal           34 Q   34 (39)
Confidence            3


No 2  
>P49448_53_MM_ref_sig5_130
Probab=0.29  E-value=91  Score=5.01  Aligned_cols=1  Identities=0%  Similarity=0.036  Sum_probs=0.0

Q 5_Mustela         3 V    3 (7)
Q Consensus         3 v    3 (7)
                      .
T Consensus        58 ~   58 (58)
T signal           58 A   58 (58)
T 17_Sorghum       58 -   57 (57)
T 13_Coccomyxa     58 -   57 (57)
T 5_Ciona          58 -   57 (57)
T cl|CABBABABA|1   58 -   57 (57)
T cl|DABBABABA|1   58 -   57 (57)
T cl|LABBABABA|1   58 -   57 (57)
T cl|RABBABABA|1   58 -   57 (57)
T cl|TABBABABA|1   58 -   57 (57)
T cl|VABBABABA|1   58 -   57 (57)
Confidence            0


No 3  
>Q9W6G7_44_Mito_ref_sig5_130
Probab=0.28  E-value=93  Score=6.23  Aligned_cols=1  Identities=100%  Similarity=2.593  Sum_probs=0.3

Q 5_Mustela         6 Y    6 (7)
Q Consensus         6 y    6 (7)
                      |
T Consensus        26 y   26 (49)
T signal           26 Y   26 (49)
Confidence            2


No 4  
>Q99K67_32_Mito_ref_sig5_130
Probab=0.28  E-value=93  Score=5.68  Aligned_cols=2  Identities=50%  Similarity=1.016  Sum_probs=0.8

Q 5_Mustela         2 EV    3 (7)
Q Consensus         2 ev    3 (7)
                      +|
T Consensus        32 Dv   33 (37)
T signal           32 DV   33 (37)
T 74               34 DV   35 (39)
T 70               32 DI   33 (37)
T 111              30 DI   31 (35)
T 99               39 DV   40 (44)
T 103              36 DV   37 (41)
T 127              35 DI   36 (40)
T 57               38 DQ   39 (43)
T 85               24 ES   25 (29)
Confidence            33


No 5  
>Q9Y2D0_33_Mito_ref_sig5_130
Probab=0.28  E-value=93  Score=3.83  Aligned_cols=1  Identities=0%  Similarity=0.003  Sum_probs=0.0

Q 5_Mustela         3 V    3 (7)
Q Consensus         3 v    3 (7)
                      -
T Consensus        38 ~   38 (38)
T signal           38 T   38 (38)
T 57_Xenopus       38 I   38 (38)
T 54_Chrysemys     38 C   38 (38)
T 49_Meleagris     38 K   38 (38)
T 42_Dasypus       38 Y   38 (38)
T 41_Camelus       38 C   38 (38)
T 37_Ailuropoda    38 L   38 (38)
T 22_Myotis        38 M   38 (38)
T 14               33 -   32 (32)
Confidence            0


No 6  
>P81140_13_MM_ref_sig5_130
Probab=0.28  E-value=96  Score=4.91  Aligned_cols=1  Identities=0%  Similarity=1.696  Sum_probs=0.2

Q 5_Mustela         6 Y    6 (7)
Q Consensus         6 y    6 (7)
                      +
T Consensus        17 F   17 (18)
T signal           17 F   17 (18)
T 122              17 F   17 (18)
T 123              17 F   17 (18)
T 127              17 F   17 (18)
T 129              18 F   18 (19)
T 130              17 F   17 (18)
T 145              17 F   17 (18)
T 131              15 F   15 (16)
T 113              13 F   13 (14)
Confidence            2


No 7  
>P17694_33_IM_ref_sig5_130
Probab=0.26  E-value=1e+02  Score=2.52  Aligned_cols=1  Identities=100%  Similarity=0.900  Sum_probs=0.0

Q 5_Mustela         5 Q    5 (7)
Q Consensus         5 q    5 (7)
                      |
T Consensus        38 ~   38 (38)
T signal           38 Q   38 (38)
T 203_Boea         38 -   37 (37)
T 189_Naegleria    38 -   37 (37)
T 167_Loa          38 -   37 (37)
T 125_Talaromyce   38 -   37 (37)
T 84_Andalucia     38 -   37 (37)


No 8  
>P09380_17_Mito_MM_Nu_ref_sig5_130
Probab=0.26  E-value=1e+02  Score=5.08  Aligned_cols=3  Identities=33%  Similarity=0.180  Sum_probs=0.0

Q 5_Mustela         3 VAQ    5 (7)
Q Consensus         3 vaq    5 (7)
                      ++|
T Consensus         6 ~~Q    8 (22)
T signal            6 ALQ    8 (22)
T 26                6 VLQ    8 (20)
T 18                6 SAQ    8 (20)
T 21                6 SAQ    8 (20)
T 22                6 SLQ    8 (20)
T 23                6 AWQ    8 (21)
T 27                6 AVQ    8 (21)
T 30                6 VFQ    8 (21)
T 24                6 VAQ    8 (20)
T 17                5 VLQ    7 (18)


No 9  
>P36527_26_Mito_ref_sig5_130
Probab=0.25  E-value=1.1e+02  Score=5.49  Aligned_cols=3  Identities=67%  Similarity=0.767  Sum_probs=0.0

Q 5_Mustela         3 VAQ    5 (7)
Q Consensus         3 vaq    5 (7)
                      +||
T Consensus         2 laq    4 (31)
T signal            2 LAQ    4 (31)


No 10 
>Q25417_9_Mito_ref_sig5_130
Probab=0.24  E-value=1.1e+02  Score=4.52  Aligned_cols=3  Identities=67%  Similarity=0.844  Sum_probs=0.0

Q 5_Mustela         2 EVA    4 (7)
Q Consensus         2 eva    4 (7)
                      |.|
T Consensus        10 ema   12 (14)
T signal           10 EMA   12 (14)


No 11 
>P22570_32_MM_ref_sig5_130
Probab=0.24  E-value=1.1e+02  Score=2.83  Aligned_cols=2  Identities=0%  Similarity=1.049  Sum_probs=0.0

Q 5_Mustela         5 QY    6 (7)
Q Consensus         5 qy    6 (7)
                      |+
T Consensus        31 ~~   32 (37)
T signal           31 HF   32 (37)
T 90_Drosophila    30 --   29 (29)
T 84_Megachile     30 --   29 (29)
T 73_Nematostell   30 --   29 (29)
T 49_Columba       30 --   29 (29)
T 38_Myotis        30 --   29 (29)


No 12 
>P40360_13_Mito_ref_sig5_130
Probab=0.24  E-value=1.1e+02  Score=4.71  Aligned_cols=2  Identities=100%  Similarity=2.079  Sum_probs=0.0

Q 5_Mustela         6 YD    7 (7)
Q Consensus         6 yd    7 (7)
                      ||
T Consensus        12 y~   13 (18)
T signal           12 YD   13 (18)
T 54               12 YE   13 (18)
T 53               12 YD   13 (18)
T 52               12 YQ   13 (17)
T 50               12 YE   13 (17)
T 51               12 VE   13 (18)
T 49               13 LN   14 (19)


No 13 
>P22315_53_IM_ref_sig5_130
Probab=0.24  E-value=1.1e+02  Score=6.12  Aligned_cols=4  Identities=25%  Similarity=0.268  Sum_probs=0.0

Q 5_Mustela         2 EVAQ.    5 (7)
Q Consensus         2 evaq.    5 (7)
                      |-+| 
T Consensus        47 EtAq.   50 (58)
T signal           47 EKVH.   50 (58)
T 170              48 ETAQ.   51 (59)
T 162              21 ETAQ.   24 (32)
T 165              25 ETAQ.   28 (36)
T 155              29 ETAR.   32 (40)
T 141              45 E--P.   46 (54)
T 142              45 ----.   44 (44)
T 148              27 ATAQp   31 (39)
T 137              38 ESAK.   41 (41)


No 14 
>P17614_54_Mito_IM_ref_sig5_130
Probab=0.23  E-value=1.1e+02  Score=4.78  Aligned_cols=6  Identities=33%  Similarity=0.601  Sum_probs=0.0

Q 5_Mustela         2 EVAQYD    7 (7)
Q Consensus         2 evaqyd    7 (7)
                      ..++|.
T Consensus        50 ~~a~~a   55 (59)
T signal           50 RAVQYA   55 (59)
T 74_Ficedula      51 ETRLVL   56 (58)
T cl|SABBABABA|1   51 PTKAET   56 (59)
T cl|MIBBABABA|1   51 GDFVTD   56 (59)
T cl|NOBBABABA|1   51 MQTRGF   56 (59)
T cl|SOBBABABA|1   51 GKVVAV   56 (59)


No 15 
>P14519_29_Mito_MM_Nu_IM_ref_sig5_130
Probab=0.23  E-value=1.2e+02  Score=2.76  Aligned_cols=5  Identities=40%  Similarity=0.375  Sum_probs=0.0

Q 5_Mustela         1 EEVAQ    5 (7)
Q Consensus         1 eevaq    5 (7)
                      .+.++
T Consensus        29 ~~~~~   33 (34)
T signal           29 GKAAQ   33 (34)
T 94_Galdieria     29 VDSGT   33 (33)
T 92_Populus       29 MSSLP   33 (33)
T 89_Capsella      29 PLLED   33 (33)
T 71_Capsaspora    29 VTVVA   33 (33)
T 65_Ciona         29 TGRES   33 (33)
T 63_Takifugu      29 LQQEK   33 (33)
T 56_Maylandia     29 LFDRT   33 (33)
T 52_Columba       29 PLCHL   33 (33)
T 29_Sus           29 HSEAA   33 (33)


No 16 
>Q95108_59_Mito_ref_sig5_130
Probab=0.23  E-value=1.2e+02  Score=6.12  Aligned_cols=3  Identities=67%  Similarity=0.745  Sum_probs=0.0

Q 5_Mustela         3 VAQ    5 (7)
Q Consensus         3 vaq    5 (7)
                      .||
T Consensus         1 MAQ    3 (64)
T signal            1 MAQ    3 (64)
T 82                1 MAQ    3 (65)
T 80                1 MAQ    3 (62)
T 79                1 MAQ    3 (66)
T 58                1 MAQ    3 (47)
T 77                1 MAQ    3 (67)
T 78                1 MAQ    3 (70)
T 76                1 MAQ    3 (68)


No 17 
>P34897_29_Mito_MM_Nu_IM_ref_sig5_130
Probab=0.22  E-value=1.2e+02  Score=3.18  Aligned_cols=5  Identities=40%  Similarity=0.421  Sum_probs=0.0

Q 5_Mustela         1 EEVAQ    5 (7)
Q Consensus         1 eevaq    5 (7)
                      .+.++
T Consensus        29 ~~~~~   33 (34)
T signal           29 SNAAQ   33 (34)
T 86_Glycine       29 SSSLS   33 (33)
T 87_Trichoplax    29 EKKRQ   33 (33)
T 76_Aplysia       29 GQEGI   33 (33)
T 74_Ciona         29 TGRES   33 (33)
T 60_Maylandia     29 LFDRT   33 (33)
T 58_Pseudopodoc   29 AAAAA   33 (33)


No 18 
>P30048_62_Mito_ref_sig5_130
Probab=0.22  E-value=1.2e+02  Score=3.79  Aligned_cols=3  Identities=67%  Similarity=0.745  Sum_probs=0.0

Q 5_Mustela         3 VAQ    5 (7)
Q Consensus         3 vaq    5 (7)
                      +.|
T Consensus        65 ~~~   67 (67)
T signal           65 VTQ   67 (67)
T 79_Drosophila    66 FR-   67 (67)
T 76_Meleagris     66 SD-   67 (67)
T 70_Strongyloce   66 TS-   67 (67)
T 69_Metaseiulus   66 GA-   67 (67)
T 63_Columba       66 VS-   67 (67)
T 57_Falco         66 KV-   67 (67)
T 44_Camelus       66 LD-   67 (67)


No 19 
>P28834_11_Mito_ref_sig5_130
Probab=0.22  E-value=1.2e+02  Score=4.56  Aligned_cols=2  Identities=100%  Similarity=0.850  Sum_probs=0.0

Q 5_Mustela         4 AQ    5 (7)
Q Consensus         4 aq    5 (7)
                      ||
T Consensus        15 aq   16 (16)
T signal           15 AQ   16 (16)


No 20 
>P17695_35_Cy_Mito_ref_sig5_130
Probab=0.22  E-value=1.2e+02  Score=5.63  Aligned_cols=3  Identities=67%  Similarity=0.800  Sum_probs=0.0

Q 5_Mustela         3 VAQ    5 (7)
Q Consensus         3 vaq    5 (7)
                      |.|
T Consensus        36 VSQ   38 (40)
T signal           36 VSQ   38 (40)
T 42               29 VSQ   31 (33)
T 41               27 VSQ   29 (31)
T 40               29 VSQ   31 (33)


Done!
