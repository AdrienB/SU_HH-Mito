Query         5_Mustela
Match_columns 6
No_of_seqs    1 out of 20
Neff          1.0 
Searched_HMMs 436
Date          Wed Sep  6 16:10:02 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_91A97_91Q97_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 Q04467_39_Mito_ref_sig5_130      0.4      58    0.13    3.2   0.0    1    1-1       1-1   (44)
  2 P14066_25_IM_ref_sig5_130        0.4      60    0.14    6.0   0.0    1    3-3      16-16  (30)
  3 P0C2B9_31_Mito_ref_sig5_130      0.4      63    0.15    6.0   0.0    1    1-1       1-1   (36)
  4 Q01992_33_MM_ref_sig5_130        0.4      67    0.15    6.2   0.0    1    2-2      38-38  (38)
  5 P86924_17_Mito_ref_sig5_130      0.4      69    0.16    5.1   0.0    1    3-3      20-20  (22)
  6 P00430_16_IM_ref_sig5_130        0.4      70    0.16    5.3   0.0    1    1-1       9-9   (21)
  7 P00366_57_MM_ref_sig5_130        0.4      72    0.17    4.4   0.0    1    2-2      62-62  (62)
  8 P21953_50_MM_ref_sig5_130        0.4      75    0.17    2.8   0.0    1    1-1      54-54  (55)
  9 O46419_25_Mito_ref_sig5_130      0.3      75    0.17    5.6   0.0    1    3-3      23-23  (30)
 10 P09457_17_Mito_IM_ref_sig5_130   0.3      77    0.18    5.3   0.0    1    3-3      17-17  (22)
 11 P05165_52_MM_ref_sig5_130        0.3      77    0.18    3.0   0.0    1    2-2      57-57  (57)
 12 P12234_49_IM_ref_sig5_130        0.3      78    0.18    4.0   0.0    1    1-1       1-1   (54)
 13 P53875_73_Mito_ref_sig5_130      0.3      80    0.18    6.6   0.0    1    3-3      17-17  (78)
 14 P36967_16_Mito_ref_sig5_130      0.3      80    0.18    5.2   0.0    2    2-3       4-5   (21)
 15 P22068_44_Mito_IM_ref_sig5_130   0.3      82    0.19    6.2   0.0    1    3-3       9-9   (49)
 16 P26440_29_MM_ref_sig5_130        0.3      83    0.19    5.7   0.0    1    3-3      23-23  (34)
 17 P36521_31_Mito_ref_sig5_130      0.3      84    0.19    5.8   0.0    1    3-3      18-18  (36)
 18 P36528_19_Mito_ref_sig5_130      0.3      84    0.19    5.3   0.0    1    3-3      10-10  (24)
 19 Q25423_17_IM_ref_sig5_130        0.3      86     0.2    5.2   0.0    2    1-2      17-18  (22)
 20 P35571_42_Mito_ref_sig5_130      0.3      87     0.2    4.0   0.0    1    2-2      47-47  (47)

No 1  
>Q04467_39_Mito_ref_sig5_130
Probab=0.45  E-value=58  Score=3.18  Aligned_cols=1  Identities=0%  Similarity=0.534  Sum_probs=0.0

Q 5_Mustela         1 F    1 (6)
Q Consensus         1 f    1 (6)
                      .
T Consensus         1 M    1 (44)
T signal            1 M    1 (44)
T 92_Chlorella      1 M    1 (40)
T 86_Trichoplax     1 M    1 (40)
T 76_Bombyx         1 M    1 (40)
T 69_Meleagris      1 F    1 (40)
T 65_Latimeria      1 A    1 (40)
T 38_Sarcophilus    1 M    1 (40)
Confidence            0


No 2  
>P14066_25_IM_ref_sig5_130
Probab=0.43  E-value=60  Score=5.98  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.3

Q 5_Mustela         3 G    3 (6)
Q Consensus         3 g    3 (6)
                      |
T Consensus        16 g   16 (30)
T signal           16 G   16 (30)
Confidence            3


No 3  
>P0C2B9_31_Mito_ref_sig5_130
Probab=0.41  E-value=63  Score=6.00  Aligned_cols=1  Identities=0%  Similarity=0.534  Sum_probs=0.0

Q 5_Mustela         1 F    1 (6)
Q Consensus         1 f    1 (6)
                      .
T Consensus         1 ~    1 (36)
T signal            1 M    1 (36)
T 31                1 V    1 (36)
T 49                1 A    1 (36)
T 26                1 G    1 (36)
T 35                1 A    1 (36)
T 10                1 -    0 (40)
T 24                1 M    1 (42)
T 19                1 M    1 (41)
T 8                 1 M    1 (40)
T 14                1 M    1 (42)
Confidence            0


No 4  
>Q01992_33_MM_ref_sig5_130
Probab=0.39  E-value=67  Score=6.15  Aligned_cols=1  Identities=0%  Similarity=-1.193  Sum_probs=0.0

Q 5_Mustela         2 A    2 (6)
Q Consensus         2 a    2 (6)
                      -
T Consensus        38 W   38 (38)
T signal           38 W   38 (38)
T 180              38 W   38 (38)
T 130              37 W   37 (37)
T 102              36 W   36 (36)
T 165              36 W   36 (36)
T 168              36 W   36 (36)
T 182              36 W   36 (36)
T 186              36 W   36 (36)
T 164              36 W   36 (36)
T 169              36 W   36 (36)
Confidence            0


No 5  
>P86924_17_Mito_ref_sig5_130
Probab=0.38  E-value=69  Score=5.07  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.3

Q 5_Mustela         3 G    3 (6)
Q Consensus         3 g    3 (6)
                      |
T Consensus        20 G   20 (22)
T signal           20 G   20 (22)
T 4_Naegleria      20 G   20 (22)
T 2_Leishmania     20 G   20 (22)
T cl|CABBABABA|1   20 P   20 (22)
T cl|FABBABABA|1   20 G   20 (22)
Confidence            3


No 6  
>P00430_16_IM_ref_sig5_130
Probab=0.37  E-value=70  Score=5.33  Aligned_cols=1  Identities=100%  Similarity=2.327  Sum_probs=0.2

Q 5_Mustela         1 F    1 (6)
Q Consensus         1 f    1 (6)
                      |
T Consensus         9 F    9 (21)
T signal            9 F    9 (21)
T 13                9 F    9 (21)
T 14                9 F    9 (21)
T 15                9 F    9 (21)
T 16                9 F    9 (21)
T 24                9 F    9 (21)
T 25                9 F    9 (21)
T 35                9 F    9 (21)
T 44                9 F    9 (21)
T 6                 6 F    6 (16)
Confidence            2


No 7  
>P00366_57_MM_ref_sig5_130
Probab=0.36  E-value=72  Score=4.38  Aligned_cols=1  Identities=0%  Similarity=-0.097  Sum_probs=0.0

Q 5_Mustela         2 A    2 (6)
Q Consensus         2 a    2 (6)
                      -
T Consensus        62 ~   62 (62)
T signal           62 D   62 (62)
T 114_Galdieria    44 -   43 (43)
T 110_Naegleria    44 -   43 (43)
T 104_Ichthyopht   44 -   43 (43)
T 102_Guillardia   44 -   43 (43)
T 93_Amphimedon    44 -   43 (43)
T 92_Schistosoma   44 -   43 (43)
T 89_Culex         44 -   43 (43)
T 69_Gallus        44 -   43 (43)
T 24_Saimiri       44 -   43 (43)
Confidence            0


No 8  
>P21953_50_MM_ref_sig5_130
Probab=0.35  E-value=75  Score=2.82  Aligned_cols=1  Identities=100%  Similarity=2.327  Sum_probs=0.2

Q 5_Mustela         1 F    1 (6)
Q Consensus         1 f    1 (6)
                      |
T Consensus        54 ~   54 (55)
T signal           54 F   54 (55)
T 145_Nectria      54 -   53 (53)
T 134_Cyanidiosc   54 -   53 (53)
T 123_Trichinell   54 -   53 (53)
T 111_Phytophtho   54 -   53 (53)
T 110_Toxoplasma   54 -   53 (53)
T 105_Glycine      54 -   53 (53)
T 104_Plasmodium   54 -   53 (53)
T 78_Loa           54 -   53 (53)
T 76_Trichoplax    54 -   53 (53)
Confidence            2


No 9  
>O46419_25_Mito_ref_sig5_130
Probab=0.35  E-value=75  Score=5.59  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.4

Q 5_Mustela         3 G    3 (6)
Q Consensus         3 g    3 (6)
                      |
T Consensus        23 g   23 (30)
T signal           23 G   23 (30)
T 118              23 G   23 (29)
T 119              23 G   23 (29)
T 120              23 G   23 (29)
T 122              23 G   23 (29)
T 123              23 G   23 (29)
T 131              23 G   23 (29)
T 133              23 A   23 (29)
T 107              23 G   23 (30)
T 117              22 C   22 (28)
Confidence            3


No 10 
>P09457_17_Mito_IM_ref_sig5_130
Probab=0.34  E-value=77  Score=5.28  Aligned_cols=1  Identities=0%  Similarity=0.169  Sum_probs=0.3

Q 5_Mustela         3 G    3 (6)
Q Consensus         3 g    3 (6)
                      |
T Consensus        17 g   17 (22)
T signal           17 A   17 (22)
T 184              17 G   17 (18)
T 180              17 G   17 (18)
T 181              17 G   17 (18)
T 176              17 -   16 (16)
T 175              17 -   16 (16)
T 170              17 -   16 (16)
T 186              17 G   17 (18)
T 177              17 -   16 (16)
T 185              17 -   16 (16)
Confidence            3


No 11 
>P05165_52_MM_ref_sig5_130
Probab=0.34  E-value=77  Score=3.00  Aligned_cols=1  Identities=0%  Similarity=-0.097  Sum_probs=0.0

Q 5_Mustela         2 A    2 (6)
Q Consensus         2 a    2 (6)
                      -
T Consensus        57 ~   57 (57)
T signal           57 D   57 (57)
T 91_Xenopus       55 -   54 (54)
T 88_Taeniopygia   55 -   54 (54)
T 68_Strongyloce   55 -   54 (54)
T 59_Acanthamoeb   55 -   54 (54)
T 52_Trichoplax    55 -   54 (54)
T 42_Condylura     55 -   54 (54)
T 6_Columba        55 -   54 (54)
T 1_Mustela        55 -   54 (54)
Confidence            0


No 12 
>P12234_49_IM_ref_sig5_130
Probab=0.34  E-value=78  Score=4.01  Aligned_cols=1  Identities=0%  Similarity=0.534  Sum_probs=0.0

Q 5_Mustela         1 F    1 (6)
Q Consensus         1 f    1 (6)
                      .
T Consensus         1 M    1 (54)
T signal            1 M    1 (54)
T 124_Salpingoec    1 M    1 (41)
T 115_Sorghum       1 M    1 (41)
T 97_Branchiosto    1 L    1 (41)
T 80_Trichoplax     1 M    1 (41)
T 66_Ciona          1 M    1 (41)
T 62_Geospiza       1 M    1 (41)
T 58_Anas           1 K    1 (41)
T 57_Falco          1 M    1 (41)
T 54_Ficedula       1 P    1 (41)
Confidence            0


No 13 
>P53875_73_Mito_ref_sig5_130
Probab=0.33  E-value=80  Score=6.64  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.2

Q 5_Mustela         3 G    3 (6)
Q Consensus         3 g    3 (6)
                      |
T Consensus        17 G   17 (78)
T signal           17 G   17 (78)
T 27               10 G   10 (71)
T 144              10 G   10 (71)
T 63               21 Q   21 (81)
T 139              16 G   16 (77)
T 23                8 G    8 (68)
T 12                8 A    8 (67)
T 52                9 Q    9 (69)
T 95               10 G   10 (70)
T 25                1 -    0 (60)
Confidence            2


No 14 
>P36967_16_Mito_ref_sig5_130
Probab=0.33  E-value=80  Score=5.20  Aligned_cols=2  Identities=100%  Similarity=1.498  Sum_probs=0.8

Q 5_Mustela         2 AG    3 (6)
Q Consensus         2 ag    3 (6)
                      ||
T Consensus         4 ag    5 (21)
T signal            4 AG    5 (21)
Confidence            34


No 15 
>P22068_44_Mito_IM_ref_sig5_130
Probab=0.32  E-value=82  Score=6.18  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.3

Q 5_Mustela         3 G    3 (6)
Q Consensus         3 g    3 (6)
                      |
T Consensus         9 g    9 (49)
T signal            9 G    9 (49)
Confidence            2


No 16 
>P26440_29_MM_ref_sig5_130
Probab=0.32  E-value=83  Score=5.71  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.3

Q 5_Mustela         3 G    3 (6)
Q Consensus         3 g    3 (6)
                      |
T Consensus        23 ~   23 (34)
T signal           23 G   23 (34)
T 107              26 G   26 (37)
T 130              26 G   26 (37)
T 132              23 G   23 (34)
T 136              26 G   26 (37)
T 138              23 V   23 (34)
T 150              26 S   26 (37)
T 140              26 N   26 (37)
T 144              23 R   23 (34)
T 139              26 S   26 (37)
Confidence            3


No 17 
>P36521_31_Mito_ref_sig5_130
Probab=0.31  E-value=84  Score=5.79  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.2

Q 5_Mustela         3 G    3 (6)
Q Consensus         3 g    3 (6)
                      |
T Consensus        18 g   18 (36)
T signal           18 G   18 (36)
Confidence            2


No 18 
>P36528_19_Mito_ref_sig5_130
Probab=0.31  E-value=84  Score=5.31  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.3

Q 5_Mustela         3 G    3 (6)
Q Consensus         3 g    3 (6)
                      |
T Consensus        10 g   10 (24)
T signal           10 G   10 (24)
Confidence            3


No 19 
>Q25423_17_IM_ref_sig5_130
Probab=0.31  E-value=86  Score=5.18  Aligned_cols=2  Identities=100%  Similarity=1.564  Sum_probs=0.8

Q 5_Mustela         1 FA    2 (6)
Q Consensus         1 fa    2 (6)
                      ||
T Consensus        17 fa   18 (22)
T signal           17 FA   18 (22)
T cl|CABBABABA|1   20 FA   21 (25)
Confidence            33


No 20 
>P35571_42_Mito_ref_sig5_130
Probab=0.30  E-value=87  Score=3.98  Aligned_cols=1  Identities=0%  Similarity=0.368  Sum_probs=0.0

Q 5_Mustela         2 A    2 (6)
Q Consensus         2 a    2 (6)
                      .
T Consensus        47 ~   47 (47)
T signal           47 S   47 (47)
T 85_Nematostell   47 -   46 (46)
T 97_Amphimedon    47 -   46 (46)
T 64_Oreochromis   47 -   46 (46)
T 74_Pongo         47 -   46 (46)
T 158_Laccaria     47 -   46 (46)
T 73_Ciona         47 -   46 (46)
T 81_Drosophila    47 -   46 (46)
T 112_Aplysia      47 -   46 (46)
T 58_Columba       47 -   46 (46)
Confidence            0


Done!
