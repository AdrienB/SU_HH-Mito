Query         5_Mustela
Match_columns 7
No_of_seqs    1 out of 20
Neff          1.0 
Searched_HMMs 436
Date          Wed Sep  6 16:09:09 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_55A62_55Q62_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 Q07021_73_MM_Nu_Cm_Cy_ref_sig5   0.3      83    0.19    6.9   0.0    1    2-2      13-13  (78)
  2 P09380_17_Mito_MM_Nu_ref_sig5_   0.3      84    0.19    5.3   0.0    1    6-6       7-7   (22)
  3 P22315_53_IM_ref_sig5_130        0.3      85     0.2    6.5   0.0    1    1-1       1-1   (58)
  4 P09622_35_MM_ref_sig5_130        0.3      86     0.2    2.6   0.0    1    3-3      40-40  (40)
  5 Q25417_9_Mito_ref_sig5_130       0.3      90    0.21    4.7   0.0    1    5-5      11-11  (14)
  6 Q95108_59_Mito_ref_sig5_130      0.3      91    0.21    6.5   0.0    1    6-6       2-2   (64)
  7 P36527_26_Mito_ref_sig5_130      0.3      95    0.22    5.6   0.0    1    6-6       3-3   (31)
  8 P12063_32_MM_ref_sig5_130        0.3      95    0.22    5.9   0.0    1    5-5      15-15  (37)
  9 P38646_46_Mito_Nu_ref_sig5_130   0.3      95    0.22    3.2   0.0    1    3-3      51-51  (51)
 10 Q99757_59_Mito_ref_sig5_130      0.3      96    0.22    6.4   0.0    1    6-6       2-2   (64)
 11 P10817_9_IM_ref_sig5_130         0.3      98    0.22    4.6   0.0    1    5-5       9-9   (14)
 12 Q99797_35_MM_ref_sig5_130        0.3      98    0.23    5.9   0.0    1    3-3      40-40  (40)
 13 P37298_31_IM_ref_sig5_130        0.3   1E+02    0.23    5.8   0.0    7    1-7      11-17  (36)
 14 P07756_38_Mito_Nu_ref_sig5_130   0.3   1E+02    0.23    3.8   0.0    6    1-6      38-43  (43)
 15 P56522_34_MM_ref_sig5_130        0.3   1E+02    0.23    2.2   0.0    3    1-3      36-38  (39)
 16 P28834_11_Mito_ref_sig5_130      0.3   1E+02    0.24    4.8   0.0    2    6-7      15-16  (16)
 17 P86926_44_Mito_ref_sig5_130      0.3   1E+02    0.24    5.6   0.0    6    1-6      34-39  (49)
 18 P83372_28_MM_ref_sig5_130        0.2 1.1E+02    0.25    5.6   0.0    7    1-7      10-16  (33)
 19 P15538_24_MitoM_ref_sig5_130     0.2 1.1E+02    0.25    5.4   0.0    6    1-6      24-29  (29)
 20 P34897_29_Mito_MM_Nu_IM_ref_si   0.2 1.1E+02    0.25    3.3   0.0    5    3-7      29-33  (34)

No 1  
>Q07021_73_MM_Nu_Cm_Cy_ref_sig5_130
Probab=0.32  E-value=83  Score=6.91  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.2

Q 5_Mustela         2 G    2 (7)
Q Consensus         2 g    2 (7)
                      |
T Consensus        13 g   13 (78)
T signal           13 G   13 (78)
T 52                6 R    6 (78)
T 36                1 -    0 (31)
T 26                9 G    9 (85)
T 48               10 C   10 (80)
T 30                7 L    7 (68)
T 19                1 -    0 (49)
T 16                1 -    0 (49)
T 21                1 -    0 (45)
T 13                1 -    0 (47)
Confidence            1


No 2  
>P09380_17_Mito_MM_Nu_ref_sig5_130
Probab=0.31  E-value=84  Score=5.33  Aligned_cols=1  Identities=0%  Similarity=-0.396  Sum_probs=0.4

Q 5_Mustela         6 A    6 (7)
Q Consensus         6 a    6 (7)
                      +
T Consensus         7 ~    7 (22)
T signal            7 L    7 (22)
T 26                7 L    7 (20)
T 18                7 A    7 (20)
T 21                7 A    7 (20)
T 22                7 L    7 (20)
T 23                7 W    7 (21)
T 27                7 V    7 (21)
T 30                7 F    7 (21)
T 24                7 A    7 (20)
T 17                6 L    6 (18)
Confidence            3


No 3  
>P22315_53_IM_ref_sig5_130
Probab=0.31  E-value=85  Score=6.51  Aligned_cols=1  Identities=0%  Similarity=0.932  Sum_probs=0.0

Q 5_Mustela         1 L    1 (7)
Q Consensus         1 l    1 (7)
                      +
T Consensus         1 m    1 (58)
T signal            1 M    1 (58)
T 170               1 M    1 (59)
T 162               1 -    0 (32)
T 165               1 -    0 (36)
T 155               1 -    0 (40)
T 141               1 -    0 (54)
T 142               1 M    1 (44)
T 148               1 -    0 (39)
T 137               1 -    0 (41)
Confidence            0


No 4  
>P09622_35_MM_ref_sig5_130
Probab=0.30  E-value=86  Score=2.60  Aligned_cols=1  Identities=0%  Similarity=-0.894  Sum_probs=0.0

Q 5_Mustela         3 E    3 (7)
Q Consensus         3 e    3 (7)
                      -
T Consensus        40 ~   40 (40)
T signal           40 I   40 (40)
T 181_Paramecium   39 -   38 (38)
T 142_Physcomitr   39 -   38 (38)
T 97_Anopheles     39 -   38 (38)
T 92_Brugia        39 -   38 (38)
T 77_Tursiops      39 -   38 (38)
T 74_Ciona         39 -   38 (38)
T 62_Meleagris     39 -   38 (38)
T 17_Ceratotheri   39 -   38 (38)
Confidence            0


No 5  
>Q25417_9_Mito_ref_sig5_130
Probab=0.29  E-value=90  Score=4.75  Aligned_cols=1  Identities=0%  Similarity=0.534  Sum_probs=0.3

Q 5_Mustela         5 V    5 (7)
Q Consensus         5 v    5 (7)
                      .
T Consensus        11 m   11 (14)
T signal           11 M   11 (14)
Confidence            3


No 6  
>Q95108_59_Mito_ref_sig5_130
Probab=0.29  E-value=91  Score=6.48  Aligned_cols=1  Identities=100%  Similarity=0.800  Sum_probs=0.3

Q 5_Mustela         6 A    6 (7)
Q Consensus         6 a    6 (7)
                      |
T Consensus         2 A    2 (64)
T signal            2 A    2 (64)
T 82                2 A    2 (65)
T 80                2 A    2 (62)
T 79                2 A    2 (66)
T 58                2 A    2 (47)
T 77                2 A    2 (67)
T 78                2 A    2 (70)
T 76                2 A    2 (68)
Confidence            2


No 7  
>P36527_26_Mito_ref_sig5_130
Probab=0.28  E-value=95  Score=5.64  Aligned_cols=1  Identities=100%  Similarity=0.800  Sum_probs=0.3

Q 5_Mustela         6 A    6 (7)
Q Consensus         6 a    6 (7)
                      |
T Consensus         3 a    3 (31)
T signal            3 A    3 (31)
Confidence            2


No 8  
>P12063_32_MM_ref_sig5_130
Probab=0.28  E-value=95  Score=5.86  Aligned_cols=1  Identities=0%  Similarity=1.032  Sum_probs=0.2

Q 5_Mustela         5 V    5 (7)
Q Consensus         5 v    5 (7)
                      |
T Consensus        15 i   15 (37)
T signal           15 I   15 (37)
T 193              15 V   15 (37)
Confidence            2


No 9  
>P38646_46_Mito_Nu_ref_sig5_130
Probab=0.28  E-value=95  Score=3.18  Aligned_cols=1  Identities=0%  Similarity=-0.894  Sum_probs=0.0

Q 5_Mustela         3 E    3 (7)
Q Consensus         3 e    3 (7)
                      -
T Consensus        51 ~   51 (51)
T signal           51 I   51 (51)
T 80_Apis          51 -   50 (50)
T 179_Acyrthosip   51 -   50 (50)
T 105_Ornithorhy   51 -   50 (50)
T 90_Saccoglossu   51 -   50 (50)
T 98_Trichinella   51 -   50 (50)
T 77_Ixodes        51 -   50 (50)
T 82_Aplysia       51 -   50 (50)
T 46_Pelodiscus    50 -   49 (49)
Confidence            0


No 10 
>Q99757_59_Mito_ref_sig5_130
Probab=0.28  E-value=96  Score=6.43  Aligned_cols=1  Identities=100%  Similarity=0.800  Sum_probs=0.3

Q 5_Mustela         6 A    6 (7)
Q Consensus         6 a    6 (7)
                      |
T Consensus         2 A    2 (64)
T signal            2 A    2 (64)
T 80                2 A    2 (62)
T 82                2 A    2 (64)
T 81                2 A    2 (65)
T 88                2 A    2 (64)
T 78                2 A    2 (66)
T 61                2 A    2 (47)
T 65                2 A    2 (43)
T 77                2 A    2 (70)
T 76                2 A    2 (68)
Confidence            2


No 11 
>P10817_9_IM_ref_sig5_130
Probab=0.27  E-value=98  Score=4.63  Aligned_cols=1  Identities=0%  Similarity=0.534  Sum_probs=0.3

Q 5_Mustela         5 V    5 (7)
Q Consensus         5 v    5 (7)
                      .
T Consensus         9 l    9 (14)
T signal            9 M    9 (14)
T 14                9 L    9 (14)
T 18                9 L    9 (14)
T 22                9 L    9 (14)
T 23                9 L    9 (14)
T 46                9 F    9 (14)
T 4                 9 L    9 (14)
Confidence            2


No 12 
>Q99797_35_MM_ref_sig5_130
Probab=0.27  E-value=98  Score=5.88  Aligned_cols=1  Identities=0%  Similarity=-1.423  Sum_probs=0.0

Q 5_Mustela         3 E    3 (7)
Q Consensus         3 e    3 (7)
                      =
T Consensus        40 W   40 (40)
T signal           40 W   40 (40)
T 102              40 W   40 (40)
T 164              40 W   40 (40)
T 180              40 W   40 (40)
T 183              40 W   40 (40)
T 122              39 W   39 (39)
T 158              37 W   37 (37)
T 168              40 W   40 (40)
T 174              40 W   40 (40)
T 155              41 W   41 (41)
Confidence            0


No 13 
>P37298_31_IM_ref_sig5_130
Probab=0.26  E-value=1e+02  Score=5.75  Aligned_cols=7  Identities=14%  Similarity=0.387  Sum_probs=0.0

Q 5_Mustela         1 LGEEVAQ    7 (7)
Q Consensus         1 lgeevaq    7 (7)
                      +|...+|
T Consensus        11 ~~rr~~~   17 (36)
T signal           11 TGRRIFH   17 (36)
T 12                1 -PRTIFR    6 (25)
T 15                2 LGNRFAQ    8 (27)
T 11                1 -ARSAAM    6 (25)
T 16                5 SVQRSLQ   11 (30)


No 14 
>P07756_38_Mito_Nu_ref_sig5_130
Probab=0.26  E-value=1e+02  Score=3.76  Aligned_cols=6  Identities=33%  Similarity=-0.075  Sum_probs=0.0

Q 5_Mustela         1 LGEEVA    6 (7)
Q Consensus         1 lgeeva    6 (7)
                      +....|
T Consensus        38 ~~~~~~   43 (43)
T signal           38 LLSVKA   43 (43)
T 79_Emiliania     38 RGVSF-   42 (42)
T 77_Thalassiosi   38 DGEVV-   42 (42)
T 60_Anolis        38 DGTKM-   42 (42)
T 48_Alligator     38 LVLED-   42 (42)
T 44_Papio         38 HQKWK-   42 (42)
T 28_Ovis          38 SSKES-   42 (42)
T 18_Pongo         38 FSRPG-   42 (42)
T cl|HABBABABA|1   50 TRF---   52 (52)
T 72_Branchiosto   37 VRRKD-   41 (41)


No 15 
>P56522_34_MM_ref_sig5_130
Probab=0.26  E-value=1e+02  Score=2.23  Aligned_cols=3  Identities=33%  Similarity=0.147  Sum_probs=0.0

Q 5_Mustela         1 LGE    3 (7)
Q Consensus         1 lge    3 (7)
                      ..|
T Consensus        36 ~~~   38 (39)
T signal           36 TQE   38 (39)
T 138_Tuber        36 YG-   37 (37)
T 98_Oryza         36 PL-   37 (37)
T 88_Apis          36 QQ-   37 (37)
T 81_Amphimedon    36 GS-   37 (37)
T 79_Trichoplax    36 YT-   37 (37)
T 61_Meleagris     36 KN-   37 (37)


No 16 
>P28834_11_Mito_ref_sig5_130
Probab=0.26  E-value=1e+02  Score=4.75  Aligned_cols=2  Identities=100%  Similarity=0.850  Sum_probs=0.0

Q 5_Mustela         6 AQ    7 (7)
Q Consensus         6 aq    7 (7)
                      ||
T Consensus        15 aq   16 (16)
T signal           15 AQ   16 (16)


No 17 
>P86926_44_Mito_ref_sig5_130
Probab=0.26  E-value=1e+02  Score=5.57  Aligned_cols=6  Identities=0%  Similarity=-0.429  Sum_probs=0.0

Q 5_Mustela         1 LGEEVA    6 (7)
Q Consensus         1 lgeeva    6 (7)
                      .+++++
T Consensus        34 ~~~~~~   39 (49)
T signal           34 GSGVFL   39 (49)
T cl|FABBABABA|1   34 LEAETV   39 (49)
T 4                34 SQEWVA   39 (49)
T 1                34 HGANFS   39 (49)
T 2                34 SNKHWD   39 (49)
T 5                34 AQEWVA   39 (48)


No 18 
>P83372_28_MM_ref_sig5_130
Probab=0.24  E-value=1.1e+02  Score=5.55  Aligned_cols=7  Identities=29%  Similarity=0.411  Sum_probs=0.0

Q 5_Mustela         1 LGEEVAQ    7 (7)
Q Consensus         1 lgeevaq    7 (7)
                      |-..++|
T Consensus        10 LRSr~~Q   16 (33)
T signal           10 LRSRLGQ   16 (33)
T 167              13 LRSHVGQ   19 (36)
T 175              13 LRSRAVQ   19 (36)
T 176              13 LRSRVGQ   19 (36)
T 177              13 LRSRVGQ   19 (36)
T 178              13 LRSRLAQ   19 (36)
T 168              13 LRSRMAQ   19 (36)


No 19 
>P15538_24_MitoM_ref_sig5_130
Probab=0.24  E-value=1.1e+02  Score=5.39  Aligned_cols=6  Identities=50%  Similarity=0.745  Sum_probs=0.0

Q 5_Mustela         1 LGEEVA    6 (7)
Q Consensus         1 lgeeva    6 (7)
                      ||...|
T Consensus        24 lgtraa   29 (29)
T signal           24 LGTRAA   29 (29)


No 20 
>P34897_29_Mito_MM_Nu_IM_ref_sig5_130
Probab=0.24  E-value=1.1e+02  Score=3.29  Aligned_cols=5  Identities=40%  Similarity=0.421  Sum_probs=0.0

Q 5_Mustela         3 EEVAQ    7 (7)
Q Consensus         3 eevaq    7 (7)
                      .+.++
T Consensus        29 ~~~~~   33 (34)
T signal           29 SNAAQ   33 (34)
T 86_Glycine       29 SSSLS   33 (33)
T 87_Trichoplax    29 EKKRQ   33 (33)
T 76_Aplysia       29 GQEGI   33 (33)
T 74_Ciona         29 TGRES   33 (33)
T 60_Maylandia     29 LFDRT   33 (33)
T 58_Pseudopodoc   29 AAAAA   33 (33)


Done!
