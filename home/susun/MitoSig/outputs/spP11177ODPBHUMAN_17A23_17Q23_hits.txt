Query         5_Mustela
Match_columns 6
No_of_seqs    3 out of 20
Neff          1.2 
Searched_HMMs 436
Date          Wed Sep  6 16:08:10 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_17A23_17Q23_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P11177_30_MM_ref_sig5_130       73.3   0.013   3E-05   15.8   0.0    6    1-6      18-23  (35)
  2 Q9H300_52_IM_Nu_ref_sig5_130    39.1    0.14 0.00031   14.3   0.0    6    1-6      38-43  (57)
  3 P43265_42_IM_ref_sig5_130        7.7       2  0.0045   10.7   0.0    4    3-6      14-17  (47)
  4 Q02380_46_IM_ref_sig5_130        7.6       2  0.0046   10.8   0.0    1    4-4      35-35  (51)
  5 P40360_13_Mito_ref_sig5_130      6.6     2.4  0.0055    8.7   0.0    5    2-6       6-10  (18)
  6 P35434_22_Mito_IM_ref_sig5_130   6.4     2.5  0.0057    9.4   0.0    2    2-3       7-8   (27)
  7 P20069_32_MM_ref_sig5_130        6.1     2.7  0.0061    9.9   0.0    3    4-6      30-32  (37)
  8 Q9NVH6_15_MM_ref_sig5_130        5.9     2.8  0.0064    8.8   0.0    3    2-4      17-19  (20)
  9 Q42777_22_MM_ref_sig5_130        5.5     3.1   0.007    9.3   0.0    3    2-4       7-9   (27)
 10 P21801_20_IM_ref_sig5_130        5.4     3.1  0.0071    9.1   0.0    3    2-4       6-8   (25)
 11 Q64591_34_Mito_ref_sig5_130      5.0     3.5   0.008    9.6   0.0    3    4-6      22-24  (39)
 12 Q40089_21_Mito_IM_ref_sig5_130   4.8     3.6  0.0082    8.9   0.0    4    3-6      18-21  (26)
 13 P00366_57_MM_ref_sig5_130        4.7     3.7  0.0085    7.5   0.0    1    4-4      51-51  (62)
 14 Q91YT0_20_IM_ref_sig5_130        4.6     3.9  0.0089    8.8   0.0    1    4-4       5-5   (25)
 15 P53077_37_Mito_ref_sig5_130      4.5     3.9  0.0089    9.7   0.0    5    2-6      11-15  (42)
 16 P16622_31_IM_ref_sig5_130        4.5     3.9   0.009    9.5   0.0    3    2-4      13-15  (36)
 17 P12007_30_MM_ref_sig5_130        4.5       4  0.0091    9.4   0.0    1    2-2      35-35  (35)
 18 P80404_28_MM_ref_sig5_130        4.1     4.4    0.01    9.1   0.0    4    2-5       6-9   (33)
 19 P12686_37_Mito_ref_sig5_130      4.0     4.5    0.01    9.6   0.0    3    4-6      35-37  (42)
 20 Q0QF01_42_IM_ref_sig5_130        4.0     4.6    0.01    9.5   0.0    1    3-3      12-12  (47)

No 1  
>P11177_30_MM_ref_sig5_130
Probab=73.31  E-value=0.013  Score=15.78  Aligned_cols=6  Identities=83%  Similarity=1.503  Sum_probs=4.2

Q 5_Mustela         1 LLRRRF    6 (6)
Q 2_Macaca          1 LLKRRF    6 (6)
Q 18_Ceratotheri    1 FLRRRF    6 (6)
Q Consensus         1 ~lrrrf    6 (6)
                      ++||+|
T Consensus        18 ~lrR~F   23 (35)
T signal           18 LLKRRF   23 (35)
T 143              22 LPRRGF   27 (39)
T 144              18 FLKRSF   23 (35)
T 145              18 VLQREF   23 (35)
T 147              18 VLRREF   23 (35)
T 135              27 QLRSGI   32 (44)
T 141              32 QQRRGF   37 (49)
T 142              17 LGRRRF   22 (34)
T 149              17 LQRREF   22 (34)
T 150              17 VRRRHF   22 (34)
Confidence            467777


No 2  
>Q9H300_52_IM_Nu_ref_sig5_130
Probab=39.08  E-value=0.14  Score=14.32  Aligned_cols=6  Identities=83%  Similarity=1.298  Sum_probs=3.6

Q 5_Mustela         1 LLRRRF    6 (6)
Q 2_Macaca          1 LLKRRF    6 (6)
Q 18_Ceratotheri    1 FLRRRF    6 (6)
Q Consensus         1 ~lrrrf    6 (6)
                      ||-|||
T Consensus        38 llgrRF   43 (57)
T signal           38 LLGRRF   43 (57)
T 109              36 LLGRRL   41 (55)
T 119              38 LSGRRF   43 (57)
T 120              36 LLGRRF   41 (55)
T 129              34 LLGRRF   39 (53)
T 130              38 LFGRRF   43 (57)
T 136              38 IFPCRF   43 (57)
T 117              37 LLSPRF   42 (56)
T 116              13 FSLPRF   18 (32)
Confidence            456676


No 3  
>P43265_42_IM_ref_sig5_130
Probab=7.71  E-value=2  Score=10.74  Aligned_cols=4  Identities=75%  Similarity=1.099  Sum_probs=2.0

Q 5_Mustela         3 RRRF    6 (6)
Q 2_Macaca          3 KRRF    6 (6)
Q 18_Ceratotheri    3 RRRF    6 (6)
Q Consensus         3 rrrf    6 (6)
                      -|||
T Consensus        14 frrf   17 (47)
T signal           14 FRRF   17 (47)
Confidence            3554


No 4  
>Q02380_46_IM_ref_sig5_130
Probab=7.62  E-value=2  Score=10.80  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.3

Q 5_Mustela         4 R    4 (6)
Q 2_Macaca          4 R    4 (6)
Q 18_Ceratotheri    4 R    4 (6)
Q Consensus         4 r    4 (6)
                      |
T Consensus        35 r   35 (51)
T signal           35 R   35 (51)
T 55               35 R   35 (51)
T 59               35 R   35 (51)
T 51               39 R   39 (57)
T 94               35 R   35 (42)
T 49               36 S   36 (52)
T 46               34 R   34 (51)
T 42               44 P   44 (60)
T 24               32 L   32 (44)
T 40               32 K   32 (48)
Confidence            3


No 5  
>P40360_13_Mito_ref_sig5_130
Probab=6.59  E-value=2.4  Score=8.70  Aligned_cols=5  Identities=0%  Similarity=0.295  Sum_probs=2.2

Q 5_Mustela         2 LR.RRF    6 (6)
Q 2_Macaca          2 LK.RRF    6 (6)
Q 18_Ceratotheri    2 LR.RRF    6 (6)
Q Consensus         2 lr.rrf    6 (6)
                      |. +||
T Consensus         6 l~.~~~   10 (18)
T signal            6 FA.HEL   10 (18)
T 54                6 FS.NGL   10 (18)
T 53                6 LN.RGF   10 (18)
T 52                6 LG.QRL   10 (17)
T 50                6 FK.HGF   10 (17)
T 51                6 LS.GGL   10 (18)
T 49                6 LSgARF   11 (19)
Confidence            44 443


No 6  
>P35434_22_Mito_IM_ref_sig5_130
Probab=6.43  E-value=2.5  Score=9.41  Aligned_cols=2  Identities=100%  Similarity=1.448  Sum_probs=0.7

Q 5_Mustela         2 LR    3 (6)
Q 2_Macaca          2 LK    3 (6)
Q 18_Ceratotheri    2 LR    3 (6)
Q Consensus         2 lr    3 (6)
                      |+
T Consensus         7 L~    8 (27)
T signal            7 LR    8 (27)
T 160               7 LR    8 (27)
T 152               7 FR    8 (27)
T 140               6 LR    7 (25)
T 143               6 LL    7 (33)
T 132               7 LL    8 (34)
T 137               7 LA    8 (29)
T 121               7 LL    8 (25)
T 127               7 L-    7 (24)
T 120               7 LF    8 (26)
Confidence            33


No 7  
>P20069_32_MM_ref_sig5_130
Probab=6.09  E-value=2.7  Score=9.87  Aligned_cols=3  Identities=100%  Similarity=1.818  Sum_probs=1.1

Q 5_Mustela         4 RRF    6 (6)
Q 2_Macaca          4 RRF    6 (6)
Q 18_Ceratotheri    4 RRF    6 (6)
Q Consensus         4 rrf    6 (6)
                      |+|
T Consensus        30 RqF   32 (37)
T signal           30 RRF   32 (37)
T 143              30 RQF   32 (37)
T 58               31 RRF   33 (37)
T 118              34 RQF   36 (40)
T 119              32 RRF   34 (38)
T 123              31 RQF   33 (37)
T 127              32 RGF   34 (38)
T 129              32 RQF   34 (38)
T 134              31 RQF   33 (37)
T 121              21 RRF   23 (27)
Confidence            333


No 8  
>Q9NVH6_15_MM_ref_sig5_130
Probab=5.90  E-value=2.8  Score=8.77  Aligned_cols=3  Identities=33%  Similarity=0.634  Sum_probs=1.3

Q 5_Mustela         2 LRR    4 (6)
Q 2_Macaca          2 LKR    4 (6)
Q 18_Ceratotheri    2 LRR    4 (6)
Q Consensus         2 lrr    4 (6)
                      ||+
T Consensus        17 lk~   19 (20)
T signal           17 LKG   19 (20)
T 63               17 LKG   19 (20)
T 65               17 MKG   19 (20)
T 106              17 LKG   19 (20)
T 109              17 LRR   19 (20)
T 99               17 MKR   19 (20)
T 104              17 LKR   19 (20)
T 96               17 LNR   19 (19)
T 95               18 LKS   20 (21)
Confidence            444


No 9  
>Q42777_22_MM_ref_sig5_130
Probab=5.50  E-value=3.1  Score=9.27  Aligned_cols=3  Identities=100%  Similarity=1.486  Sum_probs=1.1

Q 5_Mustela         2 LRR......    4 (6)
Q 2_Macaca          2 LKR......    4 (6)
Q 18_Ceratotheri    2 LRR......    4 (6)
Q Consensus         2 lrr......    4 (6)
                      |||      
T Consensus         7 lrr......    9 (27)
T signal            7 LRR......    9 (27)
T 8                 7 LRRkliitr   15 (33)
Confidence            343      


No 10 
>P21801_20_IM_ref_sig5_130
Probab=5.45  E-value=3.1  Score=9.10  Aligned_cols=3  Identities=100%  Similarity=1.486  Sum_probs=1.2

Q 5_Mustela         2 LRR    4 (6)
Q 2_Macaca          2 LKR    4 (6)
Q 18_Ceratotheri    2 LRR    4 (6)
Q Consensus         2 lrr    4 (6)
                      |||
T Consensus         6 lrR    8 (25)
T signal            6 LRR    8 (25)
T 192               2 LRR    4 (23)
T 191               2 IQR    4 (21)
Confidence            443


No 11 
>Q64591_34_Mito_ref_sig5_130
Probab=4.96  E-value=3.5  Score=9.60  Aligned_cols=3  Identities=67%  Similarity=1.464  Sum_probs=1.2

Q 5_Mustela         4 RRF    6 (6)
Q 2_Macaca          4 RRF    6 (6)
Q 18_Ceratotheri    4 RRF    6 (6)
Q Consensus         4 rrf    6 (6)
                      |||
T Consensus        22 rRF   24 (39)
T signal           22 QRF   24 (39)
T 76               22 RRF   24 (39)
T 78               22 RRF   24 (39)
T 52               16 RRF   18 (32)
T 55               20 HKF   22 (36)
T 80               22 RRF   24 (38)
T 31               11 ARF   13 (28)
T 34                9 -EF   10 (25)
T 46               11 RRF   13 (28)
T 85               12 GRF   14 (29)
Confidence            344


No 12 
>Q40089_21_Mito_IM_ref_sig5_130
Probab=4.83  E-value=3.6  Score=8.92  Aligned_cols=4  Identities=75%  Similarity=1.290  Sum_probs=1.6

Q 5_Mustela         3 RRRF    6 (6)
Q 2_Macaca          3 KRRF    6 (6)
Q 18_Ceratotheri    3 RRRF    6 (6)
Q Consensus         3 rrrf    6 (6)
                      +|+|
T Consensus        18 rR~f   21 (26)
T signal           18 RRPF   21 (26)
T 44               18 RRPF   21 (26)
T 43               18 TRSF   21 (26)
T 40               16 TRRF   19 (23)
T 41               16 TRRF   19 (23)
T 3                18 RRSY   21 (24)
T 5                18 RRTY   21 (24)
T 10               18 RRTY   21 (24)
T 11               18 RRGY   21 (24)
Confidence            3443


No 13 
>P00366_57_MM_ref_sig5_130
Probab=4.73  E-value=3.7  Score=7.53  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.2

Q 5_Mustela         4 R    4 (6)
Q 2_Macaca          4 R    4 (6)
Q 18_Ceratotheri    4 R    4 (6)
Q Consensus         4 r    4 (6)
                      |
T Consensus        51 ~   51 (62)
T signal           51 R   51 (62)
T 114_Galdieria    44 -   43 (43)
T 110_Naegleria    44 -   43 (43)
T 104_Ichthyopht   44 -   43 (43)
T 102_Guillardia   44 -   43 (43)
T 93_Amphimedon    44 -   43 (43)
T 92_Schistosoma   44 -   43 (43)
T 89_Culex         44 -   43 (43)
T 69_Gallus        44 -   43 (43)
T 24_Saimiri       44 -   43 (43)
Confidence            2


No 14 
>Q91YT0_20_IM_ref_sig5_130
Probab=4.56  E-value=3.9  Score=8.84  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.4

Q 5_Mustela         4 R    4 (6)
Q 2_Macaca          4 R    4 (6)
Q 18_Ceratotheri    4 R    4 (6)
Q Consensus         4 r    4 (6)
                      |
T Consensus         5 R    5 (25)
T signal            5 R    5 (25)
T 132               5 R    5 (29)
T 93                5 R    5 (25)
Confidence            3


No 15 
>P53077_37_Mito_ref_sig5_130
Probab=4.54  E-value=3.9  Score=9.73  Aligned_cols=5  Identities=60%  Similarity=1.212  Sum_probs=2.1

Q 5_Mustela         2 LRRRF    6 (6)
Q 2_Macaca          2 LKRRF    6 (6)
Q 18_Ceratotheri    2 LRRRF    6 (6)
Q Consensus         2 lrrrf    6 (6)
                      |+|-|
T Consensus        11 lkrsf   15 (42)
T signal           11 LKRSF   15 (42)
Confidence            34433


No 16 
>P16622_31_IM_ref_sig5_130
Probab=4.52  E-value=3.9  Score=9.47  Aligned_cols=3  Identities=100%  Similarity=1.486  Sum_probs=1.2

Q 5_Mustela         2 LRR    4 (6)
Q 2_Macaca          2 LKR    4 (6)
Q 18_Ceratotheri    2 LRR    4 (6)
Q Consensus         2 lrr    4 (6)
                      |||
T Consensus        13 lrr   15 (36)
T signal           13 LRR   15 (36)
Confidence            443


No 17 
>P12007_30_MM_ref_sig5_130
Probab=4.48  E-value=4  Score=9.36  Aligned_cols=1  Identities=0%  Similarity=-0.762  Sum_probs=0.0

Q 5_Mustela         2 L    2 (6)
Q 2_Macaca          2 L    2 (6)
Q 18_Ceratotheri    2 L    2 (6)
Q Consensus         2 l    2 (6)
                      =
T Consensus        35 P   35 (35)
T signal           35 P   35 (35)
T 106              37 P   37 (37)
T 129              34 P   34 (34)
T 134              34 P   34 (34)
T 136              37 P   37 (37)
T 141              37 P   37 (37)
T 122              26 -   25 (25)
Confidence            0


No 18 
>P80404_28_MM_ref_sig5_130
Probab=4.12  E-value=4.4  Score=9.07  Aligned_cols=4  Identities=50%  Similarity=0.800  Sum_probs=1.7

Q 5_Mustela         2 LRRR    5 (6)
Q 2_Macaca          2 LKRR    5 (6)
Q 18_Ceratotheri    2 LRRR    5 (6)
Q Consensus         2 lrrr    5 (6)
                      |.||
T Consensus         6 L~R~    9 (33)
T signal            6 LAQR    9 (33)
T 95                6 LTRR    9 (33)
T 101               6 LAHH    9 (33)
T 103               6 LSRQ    9 (33)
T 105               6 LSRQ    9 (33)
T 89                6 ISRQ    9 (33)
T 92                6 LTRQ    9 (33)
T 94                6 LTRQ    9 (33)
T 98                6 LSRH    9 (27)
Confidence            4444


No 19 
>P12686_37_Mito_ref_sig5_130
Probab=4.03  E-value=4.5  Score=9.55  Aligned_cols=3  Identities=100%  Similarity=1.818  Sum_probs=1.3

Q 5_Mustela         4 RRF    6 (6)
Q 2_Macaca          4 RRF    6 (6)
Q 18_Ceratotheri    4 RRF    6 (6)
Q Consensus         4 rrf    6 (6)
                      |||
T Consensus        35 rrf   37 (42)
T signal           35 RRF   37 (42)
Confidence            444


No 20 
>Q0QF01_42_IM_ref_sig5_130
Probab=3.98  E-value=4.6  Score=9.53  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.3

Q 5_Mustela         3 R    3 (6)
Q 2_Macaca          3 K    3 (6)
Q 18_Ceratotheri    3 R    3 (6)
Q Consensus         3 r    3 (6)
                      +
T Consensus        12 ~   12 (47)
T signal           12 R   12 (47)
T 124              12 A   12 (47)
T 126              12 T   12 (43)
T 136              13 S   13 (50)
T 137              12 S   12 (46)
T 144              12 A   12 (48)
T 145               9 C    9 (47)
T 171               1 -    0 (44)
T 154               1 -    0 (30)
Confidence            3


Done!
