Query         5_Mustela
Match_columns 7
No_of_seqs    7 out of 20
Neff          1.6 
Searched_HMMs 436
Date          Wed Sep  6 16:08:02 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_11A18_11Q18_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P36527_26_Mito_ref_sig5_130     84.2  0.0032 7.3E-06   17.8   0.0    6    1-6      14-19  (31)
  2 Q96252_26_Mito_IM_ref_sig5_130  12.6       1  0.0023   11.0   0.0    6    1-6       2-7   (31)
  3 P13621_23_Mito_IM_ref_sig5_130   2.1     9.8   0.023    8.2   0.0    4    4-7       6-9   (28)
  4 Q5A8K2_8_Mito_Cy_ref_sig5_130    2.0      11   0.024    6.9   0.0    4    2-5       7-10  (13)
  5 P55954_29_IM_ref_sig5_130        1.9      11   0.025    8.4   0.0    3    4-6      28-30  (34)
  6 Q9UQX0_21_MM_ref_sig5_130        1.8      12   0.028    7.9   0.0    3    3-5      15-17  (26)
  7 P21571_32_Mito_IM_ref_sig5_130   1.7      13    0.03    8.3   0.0    1    3-3       5-5   (37)
  8 P22068_44_Mito_IM_ref_sig5_130   1.7      13    0.03    8.7   0.0    3    4-6       7-9   (49)
  9 Q9M7T0_30_MM_ref_sig5_130        1.6      14   0.032    8.2   0.0    1    4-4      11-11  (35)
 10 P83372_28_MM_ref_sig5_130        1.5      14   0.033    8.0   0.0    1    4-4       7-7   (33)
 11 Q7Z6M4_42_Mito_ref_sig5_130      1.5      14   0.033    8.5   0.0    2    3-4       7-8   (47)
 12 P32799_9_IM_ref_sig5_130         1.5      15   0.035    6.6   0.0    3    1-3       2-4   (14)
 13 P31039_44_IM_ref_sig5_130        1.5      15   0.035    5.3   0.0    1    3-3      49-49  (49)
 14 Q9H300_52_IM_Nu_ref_sig5_130     1.4      15   0.035    8.8   0.0    1    3-3      49-49  (57)
 15 P21839_50_MM_ref_sig5_130        1.4      15   0.036    8.7   0.0    2    3-4      50-51  (55)
 16 Q02253_32_Mito_ref_sig5_130      1.4      16   0.037    8.1   0.0    3    3-5      17-19  (37)
 17 Q9BEA2_31_MM_ref_sig5_130        1.3      17   0.038    7.4   0.0    1    5-5      33-33  (36)
 18 P22570_32_MM_ref_sig5_130        1.3      17   0.039    4.8   0.0    1    3-3      37-37  (37)
 19 Q25423_17_IM_ref_sig5_130        1.3      17    0.04    7.2   0.0    3    3-5       3-5   (22)
 20 P14066_25_IM_ref_sig5_130        1.2      18   0.042    7.6   0.0    3    4-6      14-16  (30)

No 1  
>P36527_26_Mito_ref_sig5_130
Probab=84.23  E-value=0.0032  Score=17.79  Aligned_cols=6  Identities=100%  Similarity=1.248  Sum_probs=5.1

Q 5_Mustela         1 LEQVSG    6 (7)
Q 2_Macaca          1 LREVSG    6 (7)
Q 18_Ceratotheri    1 LEQVSG    6 (7)
Q 15_Ictidomys      1 LQQVSG    6 (7)
Q 11_Cavia          1 LRQVSG    6 (7)
Q 17_Tupaia         1 FQQVSG    6 (7)
Q 4_Pongo           1 LREVSR    6 (7)
Q Consensus         1 l~qVSg    6 (7)
                      |+||||
T Consensus        14 leqvsg   19 (31)
T signal           14 LEQVSG   19 (31)
Confidence            689998


No 2  
>Q96252_26_Mito_IM_ref_sig5_130
Probab=12.61  E-value=1  Score=11.00  Aligned_cols=6  Identities=33%  Similarity=0.401  Sum_probs=2.9

Q 5_Mustela         1 LEQV.SG    6 (7)
Q 2_Macaca          1 LREV.SG    6 (7)
Q 18_Ceratotheri    1 LEQV.SG    6 (7)
Q 15_Ictidomys      1 LQQV.SG    6 (7)
Q 11_Cavia          1 LRQV.SG    6 (7)
Q 17_Tupaia         1 FQQV.SG    6 (7)
Q 4_Pongo           1 LREV.SR    6 (7)
Q Consensus         1 l~qV.Sg    6 (7)
                      |+|- |.
T Consensus         2 lRqa.Sr    7 (31)
T signal            2 FKQA.SR    7 (31)
T 164               2 LRQA.TR    7 (32)
T 165               2 FRRA.ST    7 (26)
T 166               2 LRQS.SR    7 (26)
T 163               2 LRRA.TA    7 (27)
T 159               2 FRRAtSS    8 (23)
T 161               2 FRRAtST    8 (23)
T 167               2 LRQT.SR    7 (30)
Confidence            3555 54


No 3  
>P13621_23_Mito_IM_ref_sig5_130
Probab=2.12  E-value=9.8  Score=8.18  Aligned_cols=4  Identities=100%  Similarity=1.348  Sum_probs=1.9

Q 5_Mustela         4 VSGL    7 (7)
Q 2_Macaca          4 VSGL    7 (7)
Q 18_Ceratotheri    4 VSGF    7 (7)
Q 15_Ictidomys      4 VSGL    7 (7)
Q 11_Cavia          4 VSGL    7 (7)
Q 17_Tupaia         4 VSGL    7 (7)
Q 4_Pongo           4 VSRL    7 (7)
Q Consensus         4 VSgl    7 (7)
                      ++||
T Consensus         6 ~~gl    9 (28)
T signal            6 VSGL    9 (28)
T 148               6 ATGM    9 (28)
T 131               2 AYML    5 (24)
T 123               2 GAGL    5 (24)
T 137               1 --SF    2 (21)
T 130               2 AVGL    5 (24)
T 144               2 VARL    5 (24)
T 134               2 AAGF    5 (24)
Confidence            4554


No 4  
>Q5A8K2_8_Mito_Cy_ref_sig5_130
Probab=2.01  E-value=11  Score=6.93  Aligned_cols=4  Identities=25%  Similarity=0.476  Sum_probs=1.8

Q 5_Mustela         2 EQVS    5 (7)
Q 2_Macaca          2 REVS    5 (7)
Q 18_Ceratotheri    2 EQVS    5 (7)
Q 15_Ictidomys      2 QQVS    5 (7)
Q 11_Cavia          2 RQVS    5 (7)
Q 17_Tupaia         2 QQVS    5 (7)
Q 4_Pongo           2 REVS    5 (7)
Q Consensus         2 ~qVS    5 (7)
                      |..|
T Consensus         7 rrms   10 (13)
T signal            7 RRMS   10 (13)
Confidence            4444


No 5  
>P55954_29_IM_ref_sig5_130
Probab=1.94  E-value=11  Score=8.41  Aligned_cols=3  Identities=100%  Similarity=1.354  Sum_probs=1.3

Q 5_Mustela         4 VSG    6 (7)
Q 2_Macaca          4 VSG    6 (7)
Q 18_Ceratotheri    4 VSG    6 (7)
Q 15_Ictidomys      4 VSG    6 (7)
Q 11_Cavia          4 VSG    6 (7)
Q 17_Tupaia         4 VSG    6 (7)
Q 4_Pongo           4 VSR    6 (7)
Q Consensus         4 VSg    6 (7)
                      |||
T Consensus        28 vsg   30 (34)
T signal           28 VSG   30 (34)
Confidence            444


No 6  
>Q9UQX0_21_MM_ref_sig5_130
Probab=1.76  E-value=12  Score=7.86  Aligned_cols=3  Identities=67%  Similarity=0.700  Sum_probs=1.1

Q 5_Mustela         3 QVS    5 (7)
Q 2_Macaca          3 EVS    5 (7)
Q 18_Ceratotheri    3 QVS    5 (7)
Q 15_Ictidomys      3 QVS    5 (7)
Q 11_Cavia          3 QVS    5 (7)
Q 17_Tupaia         3 QVS    5 (7)
Q 4_Pongo           3 EVS    5 (7)
Q Consensus         3 qVS    5 (7)
                      .||
T Consensus        15 nvs   17 (26)
T signal           15 NVS   17 (26)
Confidence            343


No 7  
>P21571_32_Mito_IM_ref_sig5_130
Probab=1.68  E-value=13  Score=8.26  Aligned_cols=1  Identities=0%  Similarity=0.501  Sum_probs=0.3

Q 5_Mustela         3 Q    3 (7)
Q 2_Macaca          3 E    3 (7)
Q 18_Ceratotheri    3 Q    3 (7)
Q 15_Ictidomys      3 Q    3 (7)
Q 11_Cavia          3 Q    3 (7)
Q 17_Tupaia         3 Q    3 (7)
Q 4_Pongo           3 E    3 (7)
Q Consensus         3 q    3 (7)
                      +
T Consensus         5 r    5 (37)
T signal            5 R    5 (37)
T 15                5 R    5 (37)
T 19                5 R    5 (37)
T 51                5 Q    5 (39)
T 14                5 S    5 (40)
T 2                 5 R    5 (39)
T 16                1 -    0 (27)
T 11                4 Q    4 (33)
T 87                5 R    5 (28)
T 88                5 Q    5 (28)
Confidence            2


No 8  
>P22068_44_Mito_IM_ref_sig5_130
Probab=1.67  E-value=13  Score=8.75  Aligned_cols=3  Identities=67%  Similarity=1.176  Sum_probs=1.2

Q 5_Mustela         4 VSG    6 (7)
Q 2_Macaca          4 VSG    6 (7)
Q 18_Ceratotheri    4 VSG    6 (7)
Q 15_Ictidomys      4 VSG    6 (7)
Q 11_Cavia          4 VSG    6 (7)
Q 17_Tupaia         4 VSG    6 (7)
Q 4_Pongo           4 VSR    6 (7)
Q Consensus         4 VSg    6 (7)
                      .||
T Consensus         7 lsg    9 (49)
T signal            7 LSG    9 (49)
Confidence            344


No 9  
>Q9M7T0_30_MM_ref_sig5_130
Probab=1.59  E-value=14  Score=8.17  Aligned_cols=1  Identities=0%  Similarity=0.600  Sum_probs=0.2

Q 5_Mustela         4 V    4 (7)
Q 2_Macaca          4 V    4 (7)
Q 18_Ceratotheri    4 V    4 (7)
Q 15_Ictidomys      4 V    4 (7)
Q 11_Cavia          4 V    4 (7)
Q 17_Tupaia         4 V    4 (7)
Q 4_Pongo           4 V    4 (7)
Q Consensus         4 V    4 (7)
                      .
T Consensus        11 l   11 (35)
T signal           11 L   11 (35)
Confidence            2


No 10 
>P83372_28_MM_ref_sig5_130
Probab=1.55  E-value=14  Score=8.05  Aligned_cols=1  Identities=100%  Similarity=1.132  Sum_probs=0.2

Q 5_Mustela         4 V    4 (7)
Q 2_Macaca          4 V    4 (7)
Q 18_Ceratotheri    4 V    4 (7)
Q 15_Ictidomys      4 V    4 (7)
Q 11_Cavia          4 V    4 (7)
Q 17_Tupaia         4 V    4 (7)
Q 4_Pongo           4 V    4 (7)
Q Consensus         4 V    4 (7)
                      |
T Consensus         7 v    7 (33)
T signal            7 V    7 (33)
T 167               7 V    7 (36)
T 175               7 V    7 (36)
T 176               7 V    7 (36)
T 177               7 L    7 (36)
T 178               7 V    7 (36)
T 168               7 L    7 (36)
Confidence            2


No 11 
>Q7Z6M4_42_Mito_ref_sig5_130
Probab=1.53  E-value=14  Score=8.45  Aligned_cols=2  Identities=100%  Similarity=1.016  Sum_probs=0.9

Q 5_Mustela         3 QV    4 (7)
Q 2_Macaca          3 EV    4 (7)
Q 18_Ceratotheri    3 QV    4 (7)
Q 15_Ictidomys      3 QV    4 (7)
Q 11_Cavia          3 QV    4 (7)
Q 17_Tupaia         3 QV    4 (7)
Q 4_Pongo           3 EV    4 (7)
Q Consensus         3 qV    4 (7)
                      ||
T Consensus         7 qV    8 (47)
T signal            7 QV    8 (47)
T 36                7 RV    8 (46)
T 42                7 QV    8 (47)
T 38                7 QV    8 (47)
T 33                1 -V    1 (40)
T 39                7 QV    8 (43)
T 34                5 GV    6 (43)
T 30                2 TV    3 (43)
T 31                1 -A    1 (32)
T 29                7 QV    8 (44)
Confidence            44


No 12 
>P32799_9_IM_ref_sig5_130
Probab=1.46  E-value=15  Score=6.56  Aligned_cols=3  Identities=33%  Similarity=0.567  Sum_probs=1.3

Q 5_Mustela         1 LEQ    3 (7)
Q 2_Macaca          1 LRE    3 (7)
Q 18_Ceratotheri    1 LEQ    3 (7)
Q 15_Ictidomys      1 LQQ    3 (7)
Q 11_Cavia          1 LRQ    3 (7)
Q 17_Tupaia         1 FQQ    3 (7)
Q 4_Pongo           1 LRE    3 (7)
Q Consensus         1 l~q    3 (7)
                      |+|
T Consensus         2 ~rq    4 (14)
T signal            2 FRQ    4 (14)
T 25                2 LAR    4 (12)
T 27                2 LRT    4 (12)
T 33                2 FRQ    4 (14)
T 34                2 FRQ    4 (13)
T 35                2 YRQ    4 (13)
T 40                2 FKQ    4 (13)
T 24                2 LRQ    4 (13)
T 43                2 LRQ    4 (13)
Confidence            344


No 13 
>P31039_44_IM_ref_sig5_130
Probab=1.46  E-value=15  Score=5.30  Aligned_cols=1  Identities=0%  Similarity=0.069  Sum_probs=0.0

Q 5_Mustela         3 Q    3 (7)
Q 2_Macaca          3 E    3 (7)
Q 18_Ceratotheri    3 Q    3 (7)
Q 15_Ictidomys      3 Q    3 (7)
Q 11_Cavia          3 Q    3 (7)
Q 17_Tupaia         3 Q    3 (7)
Q 4_Pongo           3 E    3 (7)
Q Consensus         3 q    3 (7)
                      .
T Consensus        49 ~   49 (49)
T signal           49 S   49 (49)
T 83_Strongyloce   46 -   45 (45)
T 39_Sarcophilus   46 -   45 (45)
T 21_Mustela       46 -   45 (45)
T 94_Branchiosto   46 -   45 (45)
T 109_Pediculus    46 -   45 (45)
T 93_Sorex         46 -   45 (45)
T 33_Ictidomys     46 -   45 (45)
T 69_Tursiops      46 -   45 (45)
T 27_Saimiri       46 -   45 (45)
Confidence            0


No 14 
>Q9H300_52_IM_Nu_ref_sig5_130
Probab=1.44  E-value=15  Score=8.79  Aligned_cols=1  Identities=100%  Similarity=0.900  Sum_probs=0.3

Q 5_Mustela         3 Q    3 (7)
Q 2_Macaca          3 E    3 (7)
Q 18_Ceratotheri    3 Q    3 (7)
Q 15_Ictidomys      3 Q    3 (7)
Q 11_Cavia          3 Q    3 (7)
Q 17_Tupaia         3 Q    3 (7)
Q 4_Pongo           3 E    3 (7)
Q Consensus         3 q    3 (7)
                      |
T Consensus        49 q   49 (57)
T signal           49 Q   49 (57)
T 109              47 Q   47 (55)
T 119              49 Q   49 (57)
T 120              47 Q   47 (55)
T 129              45 Q   45 (53)
T 130              49 Q   49 (57)
T 136              49 Q   49 (57)
T 117              48 S   48 (56)
T 116              24 S   24 (32)
Confidence            2


No 15 
>P21839_50_MM_ref_sig5_130
Probab=1.44  E-value=15  Score=8.71  Aligned_cols=2  Identities=100%  Similarity=1.016  Sum_probs=0.7

Q 5_Mustela         3 QV    4 (7)
Q 2_Macaca          3 EV    4 (7)
Q 18_Ceratotheri    3 QV    4 (7)
Q 15_Ictidomys      3 QV    4 (7)
Q 11_Cavia          3 QV    4 (7)
Q 17_Tupaia         3 QV    4 (7)
Q 4_Pongo           3 EV    4 (7)
Q Consensus         3 qV    4 (7)
                      ||
T Consensus        50 qV   51 (55)
T signal           50 QV   51 (55)
T 150              48 RV   49 (53)
T 153              50 HV   51 (55)
T 155              50 LV   51 (55)
T 160              50 QV   51 (55)
T 154              53 QV   54 (58)
T 164              50 EV   51 (55)
T 112              53 QV   54 (58)
T 148              53 QV   54 (58)
T 127              50 QV   51 (55)
Confidence            33


No 16 
>Q02253_32_Mito_ref_sig5_130
Probab=1.39  E-value=16  Score=8.09  Aligned_cols=3  Identities=100%  Similarity=0.922  Sum_probs=1.3

Q 5_Mustela         3 QVS    5 (7)
Q 2_Macaca          3 EVS    5 (7)
Q 18_Ceratotheri    3 QVS    5 (7)
Q 15_Ictidomys      3 QVS    5 (7)
Q 11_Cavia          3 QVS    5 (7)
Q 17_Tupaia         3 QVS    5 (7)
Q 4_Pongo           3 EVS    5 (7)
Q Consensus         3 qVS    5 (7)
                      |||
T Consensus        17 qVS   19 (37)
T signal           17 QVS   19 (37)
T 23               17 QVS   19 (37)
T 135              16 QVS   18 (35)
T 138              17 QVS   19 (37)
T 134              14 RVS   16 (33)
Confidence            444


No 17 
>Q9BEA2_31_MM_ref_sig5_130
Probab=1.35  E-value=17  Score=7.37  Aligned_cols=1  Identities=100%  Similarity=0.733  Sum_probs=0.3

Q 5_Mustela         5 S    5 (7)
Q 2_Macaca          5 S    5 (7)
Q 18_Ceratotheri    5 S    5 (7)
Q 15_Ictidomys      5 S    5 (7)
Q 11_Cavia          5 S    5 (7)
Q 17_Tupaia         5 S    5 (7)
Q 4_Pongo           5 S    5 (7)
Q Consensus         5 S    5 (7)
                      |
T Consensus        33 s   33 (36)
T signal           33 S   33 (36)
T 16               33 S   33 (36)
T 27               33 S   33 (36)
T 39_Odobenus      33 G   33 (36)
T 38_Sarcophilus   33 L   33 (36)
T 31_Mesocricetu   33 S   33 (36)
T 29_Rattus        33 S   33 (36)
T 26_Mus           33 A   33 (36)
T 7_Condylura      33 G   33 (36)
T 4                33 Q   33 (36)
Confidence            3


No 18 
>P22570_32_MM_ref_sig5_130
Probab=1.32  E-value=17  Score=4.80  Aligned_cols=1  Identities=0%  Similarity=0.501  Sum_probs=0.0

Q 5_Mustela         3 Q    3 (7)
Q 2_Macaca          3 E    3 (7)
Q 18_Ceratotheri    3 Q    3 (7)
Q 15_Ictidomys      3 Q    3 (7)
Q 11_Cavia          3 Q    3 (7)
Q 17_Tupaia         3 Q    3 (7)
Q 4_Pongo           3 E    3 (7)
Q Consensus         3 q    3 (7)
                      +
T Consensus        37 ~   37 (37)
T signal           37 K   37 (37)
T 90_Drosophila    30 -   29 (29)
T 84_Megachile     30 -   29 (29)
T 73_Nematostell   30 -   29 (29)
T 49_Columba       30 -   29 (29)
T 38_Myotis        30 -   29 (29)
Confidence            0


No 19 
>Q25423_17_IM_ref_sig5_130
Probab=1.31  E-value=17  Score=7.22  Aligned_cols=3  Identities=33%  Similarity=0.612  Sum_probs=1.1

Q 5_Mustela         3 QVS    5 (7)
Q 2_Macaca          3 EVS    5 (7)
Q 18_Ceratotheri    3 QVS    5 (7)
Q 15_Ictidomys      3 QVS    5 (7)
Q 11_Cavia          3 QVS    5 (7)
Q 17_Tupaia         3 QVS    5 (7)
Q 4_Pongo           3 EVS    5 (7)
Q Consensus         3 qVS    5 (7)
                      .||
T Consensus         3 r~s    5 (22)
T signal            3 RLS    5 (22)
T cl|CABBABABA|1    3 RVS    5 (25)
Confidence            343


No 20 
>P14066_25_IM_ref_sig5_130
Probab=1.24  E-value=18  Score=7.60  Aligned_cols=3  Identities=67%  Similarity=1.320  Sum_probs=1.6

Q 5_Mustela         4 VSG    6 (7)
Q 2_Macaca          4 VSG    6 (7)
Q 18_Ceratotheri    4 VSG    6 (7)
Q 15_Ictidomys      4 VSG    6 (7)
Q 11_Cavia          4 VSG    6 (7)
Q 17_Tupaia         4 VSG    6 (7)
Q 4_Pongo           4 VSR    6 (7)
Q Consensus         4 VSg    6 (7)
                      +||
T Consensus        14 isg   16 (30)
T signal           14 ISG   16 (30)
Confidence            455


Done!
