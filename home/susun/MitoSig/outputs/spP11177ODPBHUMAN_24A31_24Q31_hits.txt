Query         5_Mustela
Match_columns 7
No_of_seqs    5 out of 20
Neff          1.4 
Searched_HMMs 436
Date          Wed Sep  6 16:08:22 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_24A31_24Q31_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P11177_30_MM_ref_sig5_130       86.5   0.002 4.6E-06   18.5   0.0    7    1-7      25-31  (35)
  2 P42844_47_IM_ref_sig5_130        9.3     1.5  0.0035   11.6   0.0    6    1-6      33-38  (52)
  3 P35434_22_Mito_IM_ref_sig5_130   1.5      14   0.033    7.7   0.0    2    4-5       3-4   (27)
  4 P11183_39_Mito_ref_sig5_130      1.5      15   0.033    8.4   0.0    1    4-4      36-36  (44)
  5 P22778_45_Mito_IM_ref_sig5_130   1.5      15   0.034    8.6   0.0    1    4-4      49-49  (50)
  6 P54622_22_Mito_ref_sig5_130      1.4      16   0.036    7.7   0.0    1    4-4      19-19  (27)
  7 P32473_33_MM_ref_sig5_130        1.4      16   0.036    8.2   0.0    1    4-4      18-18  (38)
  8 Q0P5L8_21_Mito_Nu_ref_sig5_130   1.3      18   0.041    4.2   0.0    1    3-3      26-26  (26)
  9 Q09544_18_Mito_IM_ref_sig5_130   1.2      18   0.042    7.2   0.0    1    4-4      22-22  (23)
 10 Q3ZBF3_26_Mito_ref_sig5_130      1.1      22    0.05    4.2   0.0    1    3-3      31-31  (31)
 11 Q0QF01_42_IM_ref_sig5_130        1.0      23   0.053    7.9   0.0    2    4-5      23-24  (47)
 12 P10176_25_IM_ref_sig5_130        1.0      25   0.056    7.3   0.0    1    4-4      15-15  (30)
 13 Q9BYV1_41_Mito_ref_sig5_130      0.9      25   0.058    4.1   0.0    1    3-3      46-46  (46)
 14 P42116_26_IM_ref_sig5_130        0.9      26    0.06    6.7   0.0    1    3-3      31-31  (31)
 15 P10175_24_IM_ref_sig5_130        0.9      26    0.06    7.1   0.0    1    4-4      21-21  (29)
 16 P16221_24_IM_ref_sig5_130        0.9      28   0.064    7.0   0.0    1    4-4      21-21  (29)
 17 P80433_25_IM_ref_sig5_130        0.8      30   0.068    7.1   0.0    1    4-4      15-15  (30)
 18 P25285_22_Mito_ref_sig5_130      0.8      30    0.07    6.8   0.0    1    4-4      13-13  (27)
 19 Q9YHT1_44_IM_ref_sig5_130        0.8      31   0.071    7.5   0.0    1    4-4      24-24  (49)
 20 P07251_35_IM_ref_sig5_130        0.8      32   0.074    7.3   0.0    1    4-4      24-24  (40)

No 1  
>P11177_30_MM_ref_sig5_130
Probab=86.47  E-value=0.002  Score=18.51  Aligned_cols=7  Identities=86%  Similarity=0.938  Sum_probs=5.7

Q 5_Mustela         1 RTAPAAL    7 (7)
Q 2_Macaca          1 WTAPTAL    7 (7)
Q 1_Homo            1 WTAPAAL    7 (7)
Q 15_Ictidomys      1 RTTPAAL    7 (7)
Q 11_Cavia          1 RTAPATL    7 (7)
Q Consensus         1 ~tapaal    7 (7)
                      |++|||+
T Consensus        25 rs~PAAv   31 (35)
T signal           25 WTAPAAL   31 (35)
T 143              29 RSPPAAI   35 (39)
T 144              25 RSTPAAL   31 (35)
T 145              25 RTAPVSL   31 (35)
T 147              25 RTPSAAV   31 (35)
T 135              34 GSAPAAV   40 (44)
T 141              39 LSAPAAI   45 (49)
T 142              24 GSGSAAV   30 (34)
T 149              24 RSVPAAV   30 (34)
T 150              24 RTVPAAV   30 (34)
Confidence            6899985


No 2  
>P42844_47_IM_ref_sig5_130
Probab=9.33  E-value=1.5  Score=11.60  Aligned_cols=6  Identities=83%  Similarity=1.021  Sum_probs=2.8

Q 5_Mustela         1 RTAPAA    6 (7)
Q 2_Macaca          1 WTAPTA    6 (7)
Q 1_Homo            1 WTAPAA    6 (7)
Q 15_Ictidomys      1 RTTPAA    6 (7)
Q 11_Cavia          1 RTAPAT    6 (7)
Q Consensus         1 ~tapaa    6 (7)
                      ||-|+|
T Consensus        33 rtlpaa   38 (52)
T signal           33 RTLPAA   38 (52)
Confidence            345554


No 3  
>P35434_22_Mito_IM_ref_sig5_130
Probab=1.53  E-value=14  Score=7.70  Aligned_cols=2  Identities=100%  Similarity=1.663  Sum_probs=0.8

Q 5_Mustela         4 PA    5 (7)
Q 2_Macaca          4 PT    5 (7)
Q 1_Homo            4 PA    5 (7)
Q 15_Ictidomys      4 PA    5 (7)
Q 11_Cavia          4 PA    5 (7)
Q Consensus         4 pa    5 (7)
                      ||
T Consensus         3 pA    4 (27)
T signal            3 PA    4 (27)
T 160               3 PA    4 (27)
T 152               3 PA    4 (27)
T 140               3 S-    3 (25)
T 143               3 P-    3 (33)
T 132               3 PA    4 (34)
T 137               3 PA    4 (29)
T 121               3 AA    4 (25)
T 127               3 AA    4 (24)
T 120               3 SA    4 (26)
Confidence            33


No 4  
>P11183_39_Mito_ref_sig5_130
Probab=1.52  E-value=15  Score=8.41  Aligned_cols=1  Identities=0%  Similarity=0.136  Sum_probs=0.2

Q 5_Mustela         4 P    4 (7)
Q 2_Macaca          4 P    4 (7)
Q 1_Homo            4 P    4 (7)
Q 15_Ictidomys      4 P    4 (7)
Q 11_Cavia          4 P    4 (7)
Q Consensus         4 p    4 (7)
                      |
T Consensus        36 ~   36 (44)
T signal           36 S   36 (44)
T 150              41 A   41 (49)
T 151              43 A   43 (51)
T 125              34 S   34 (42)
T 128              34 P   34 (42)
T 131              34 S   34 (42)
T 97               23 R   23 (31)
T 101              23 P   23 (31)
T 105              23 R   23 (31)
Confidence            2


No 5  
>P22778_45_Mito_IM_ref_sig5_130
Probab=1.50  E-value=15  Score=8.60  Aligned_cols=1  Identities=0%  Similarity=0.136  Sum_probs=0.3

Q 5_Mustela         4 P    4 (7)
Q 2_Macaca          4 P    4 (7)
Q 1_Homo            4 P    4 (7)
Q 15_Ictidomys      4 P    4 (7)
Q 11_Cavia          4 P    4 (7)
Q Consensus         4 p    4 (7)
                      |
T Consensus        49 s   49 (50)
T signal           49 S   49 (50)
T 194              53 S   53 (53)
T 196              51 S   51 (51)
T 197              44 P   44 (45)
T 185              51 S   51 (51)
T 191              49 T   49 (49)
T 192              47 P   47 (47)
T 195              47 P   47 (47)
T 189              42 S   42 (43)
T 193              49 T   49 (50)
Confidence            2


No 6  
>P54622_22_Mito_ref_sig5_130
Probab=1.42  E-value=16  Score=7.66  Aligned_cols=1  Identities=100%  Similarity=2.527  Sum_probs=0.4

Q 5_Mustela         4 P    4 (7)
Q 2_Macaca          4 P    4 (7)
Q 1_Homo            4 P    4 (7)
Q 15_Ictidomys      4 P    4 (7)
Q 11_Cavia          4 P    4 (7)
Q Consensus         4 p    4 (7)
                      |
T Consensus        19 ~   19 (27)
T signal           19 P   19 (27)
T 81               15 T   15 (23)
Confidence            3


No 7  
>P32473_33_MM_ref_sig5_130
Probab=1.41  E-value=16  Score=8.16  Aligned_cols=1  Identities=100%  Similarity=2.527  Sum_probs=0.3

Q 5_Mustela         4 P    4 (7)
Q 2_Macaca          4 P    4 (7)
Q 1_Homo            4 P    4 (7)
Q 15_Ictidomys      4 P    4 (7)
Q 11_Cavia          4 P    4 (7)
Q Consensus         4 p    4 (7)
                      |
T Consensus        18 p   18 (38)
T signal           18 P   18 (38)
Confidence            2


No 8  
>Q0P5L8_21_Mito_Nu_ref_sig5_130
Probab=1.27  E-value=18  Score=4.17  Aligned_cols=1  Identities=0%  Similarity=-0.064  Sum_probs=0.0

Q 5_Mustela         3 A    3 (7)
Q 2_Macaca          3 A    3 (7)
Q 1_Homo            3 A    3 (7)
Q 15_Ictidomys      3 T    3 (7)
Q 11_Cavia          3 A    3 (7)
Q Consensus         3 a    3 (7)
                      -
T Consensus        26 ~   26 (26)
T signal           26 Q   26 (26)
T 60               27 -   26 (26)
T 108_Galdieria    24 -   23 (23)
T 104_Cyanidiosc   24 -   23 (23)
T 102_Acanthamoe   24 -   23 (23)
T 78_Musca         24 -   23 (23)
T 76_Aplysia       24 -   23 (23)
T 74_Branchiosto   24 -   23 (23)
T 57_Loxodonta     24 -   23 (23)
T 6_Orcinus        24 -   23 (23)
Confidence            0


No 9  
>Q09544_18_Mito_IM_ref_sig5_130
Probab=1.24  E-value=18  Score=7.23  Aligned_cols=1  Identities=100%  Similarity=2.527  Sum_probs=0.4

Q 5_Mustela         4 P    4 (7)
Q 2_Macaca          4 P    4 (7)
Q 1_Homo            4 P    4 (7)
Q 15_Ictidomys      4 P    4 (7)
Q 11_Cavia          4 P    4 (7)
Q Consensus         4 p    4 (7)
                      |
T Consensus        22 p   22 (23)
T signal           22 P   22 (23)
Confidence            3


No 10 
>Q3ZBF3_26_Mito_ref_sig5_130
Probab=1.06  E-value=22  Score=4.24  Aligned_cols=1  Identities=0%  Similarity=0.102  Sum_probs=0.0

Q 5_Mustela         3 A    3 (7)
Q 2_Macaca          3 A    3 (7)
Q 1_Homo            3 A    3 (7)
Q 15_Ictidomys      3 T    3 (7)
Q 11_Cavia          3 A    3 (7)
Q Consensus         3 a    3 (7)
                      -
T Consensus        31 ~   31 (31)
T signal           31 P   31 (31)
T 96_Brugia        31 -   30 (30)
T 87_Branchiosto   31 -   30 (30)
T 86_Oryzias       31 -   30 (30)
T 84_Pediculus     31 -   30 (30)
T 70_Anas          31 -   30 (30)
T 69_Strongyloce   31 -   30 (30)
T 63_Zonotrichia   31 -   30 (30)
T 55_Alligator     31 -   30 (30)
T 44_Myotis        31 -   30 (30)
Confidence            0


No 11 
>Q0QF01_42_IM_ref_sig5_130
Probab=1.03  E-value=23  Score=7.87  Aligned_cols=2  Identities=100%  Similarity=1.663  Sum_probs=0.9

Q 5_Mustela         4 PA    5 (7)
Q 2_Macaca          4 PT    5 (7)
Q 1_Homo            4 PA    5 (7)
Q 15_Ictidomys      4 PA    5 (7)
Q 11_Cavia          4 PA    5 (7)
Q Consensus         4 pa    5 (7)
                      ||
T Consensus        23 Pa   24 (47)
T signal           23 PA   24 (47)
T 124              23 PA   24 (47)
T 126              22 PA   23 (43)
T 136              26 TA   27 (50)
T 137              23 PA   24 (46)
T 144              24 PA   25 (48)
T 145              23 FA   24 (47)
T 171              20 PA   21 (44)
T 154               6 PA    7 (30)
Confidence            44


No 12 
>P10176_25_IM_ref_sig5_130
Probab=0.96  E-value=25  Score=7.28  Aligned_cols=1  Identities=0%  Similarity=0.136  Sum_probs=0.4

Q 5_Mustela         4 P    4 (7)
Q 2_Macaca          4 P    4 (7)
Q 1_Homo            4 P    4 (7)
Q 15_Ictidomys      4 P    4 (7)
Q 11_Cavia          4 P    4 (7)
Q Consensus         4 p    4 (7)
                      |
T Consensus        15 ~   15 (30)
T signal           15 S   15 (30)
T 6                15 P   15 (30)
T 18               15 P   15 (30)
T 23               15 P   15 (30)
T 29               15 L   15 (30)
T 38               15 S   15 (30)
T 2                15 P   15 (31)
T 3                15 L   15 (30)
T 5                15 P   15 (30)
T 27               15 P   15 (30)
Confidence            3


No 13 
>Q9BYV1_41_Mito_ref_sig5_130
Probab=0.94  E-value=25  Score=4.06  Aligned_cols=1  Identities=0%  Similarity=0.102  Sum_probs=0.0

Q 5_Mustela         3 A    3 (7)
Q 2_Macaca          3 A    3 (7)
Q 1_Homo            3 A    3 (7)
Q 15_Ictidomys      3 T    3 (7)
Q 11_Cavia          3 A    3 (7)
Q Consensus         3 a    3 (7)
                      -
T Consensus        46 ~   46 (46)
T signal           46 P   46 (46)
T 122_Ixodes       44 -   43 (43)
T 118_Zea          44 -   43 (43)
T 94_Bombus        44 -   43 (43)
T 91_Nasonia       44 -   43 (43)
T 78_Musca         44 -   43 (43)
T 74_Strongyloce   44 -   43 (43)
T 64_Meleagris     44 -   43 (43)
T 58_Ficedula      44 -   43 (43)
Confidence            0


No 14 
>P42116_26_IM_ref_sig5_130
Probab=0.91  E-value=26  Score=6.69  Aligned_cols=1  Identities=0%  Similarity=-0.263  Sum_probs=0.0

Q 5_Mustela         3 A    3 (7)
Q 2_Macaca          3 A    3 (7)
Q 1_Homo            3 A    3 (7)
Q 15_Ictidomys      3 T    3 (7)
Q 11_Cavia          3 A    3 (7)
Q Consensus         3 a    3 (7)
                      -
T Consensus        31 h   31 (31)
T signal           31 H   31 (31)
T 23_Arthroderma   31 H   31 (31)
T 21_Talaromyces   31 H   31 (31)
T 17_Leptosphaer   31 H   31 (31)
T 15_Zymoseptori   31 H   31 (31)
T 13_Aspergillus   31 E   31 (31)
T 8_Sclerotinia    31 H   31 (31)
T 5_Thielavia      31 S   31 (31)
T 3_Magnaporthe    31 H   31 (31)
T cl|DABBABABA|2   31 K   31 (31)
Confidence            0


No 15 
>P10175_24_IM_ref_sig5_130
Probab=0.91  E-value=26  Score=7.07  Aligned_cols=1  Identities=100%  Similarity=2.527  Sum_probs=0.3

Q 5_Mustela         4 P    4 (7)
Q 2_Macaca          4 P    4 (7)
Q 1_Homo            4 P    4 (7)
Q 15_Ictidomys      4 P    4 (7)
Q 11_Cavia          4 P    4 (7)
Q Consensus         4 p    4 (7)
                      |
T Consensus        21 P   21 (29)
T signal           21 P   21 (29)
T 4                21 P   21 (29)
T 15               21 S   21 (29)
T 7                21 P   21 (29)
T 6                21 P   21 (29)
T 9                21 P   21 (29)
T 28               21 P   21 (29)
T 3                15 P   15 (23)
T 30               19 P   19 (27)
Confidence            2


No 16 
>P16221_24_IM_ref_sig5_130
Probab=0.86  E-value=28  Score=6.97  Aligned_cols=1  Identities=100%  Similarity=2.527  Sum_probs=0.3

Q 5_Mustela         4 P    4 (7)
Q 2_Macaca          4 P    4 (7)
Q 1_Homo            4 P    4 (7)
Q 15_Ictidomys      4 P    4 (7)
Q 11_Cavia          4 P    4 (7)
Q Consensus         4 p    4 (7)
                      |
T Consensus        21 P   21 (29)
T signal           21 P   21 (29)
T 24               21 P   21 (29)
T 10               21 P   21 (29)
T 2                21 P   21 (29)
T 18               21 S   21 (29)
T 5                21 P   21 (29)
T 8                21 P   21 (29)
T 29               21 P   21 (29)
T 3                20 P   20 (28)
T 32               19 P   19 (27)
Confidence            2


No 17 
>P80433_25_IM_ref_sig5_130
Probab=0.82  E-value=30  Score=7.07  Aligned_cols=1  Identities=100%  Similarity=2.527  Sum_probs=0.3

Q 5_Mustela         4 P    4 (7)
Q 2_Macaca          4 P    4 (7)
Q 1_Homo            4 P    4 (7)
Q 15_Ictidomys      4 P    4 (7)
Q 11_Cavia          4 P    4 (7)
Q Consensus         4 p    4 (7)
                      |
T Consensus        15 ~   15 (30)
T signal           15 P   15 (30)
T 3                15 L   15 (30)
T 20               15 P   15 (30)
T 22               15 P   15 (30)
T 31               15 S   15 (30)
T 36               15 P   15 (30)
T 5                15 S   15 (30)
T 8                15 S   15 (30)
T 19               15 P   15 (30)
Confidence            3


No 18 
>P25285_22_Mito_ref_sig5_130
Probab=0.80  E-value=30  Score=6.80  Aligned_cols=1  Identities=0%  Similarity=0.102  Sum_probs=0.3

Q 5_Mustela         4 P    4 (7)
Q 2_Macaca          4 P    4 (7)
Q 1_Homo            4 P    4 (7)
Q 15_Ictidomys      4 P    4 (7)
Q 11_Cavia          4 P    4 (7)
Q Consensus         4 p    4 (7)
                      |
T Consensus        13 p   13 (27)
T signal           13 A   13 (27)
T 107              13 P   13 (27)
T 127              13 P   13 (27)
T 128              13 H   13 (27)
T 134              13 P   13 (27)
T 138              13 P   13 (27)
T 145              13 P   13 (27)
T 146              13 P   13 (27)
T 147              13 S   13 (27)
T 152              13 P   13 (27)
Confidence            3


No 19 
>Q9YHT1_44_IM_ref_sig5_130
Probab=0.79  E-value=31  Score=7.52  Aligned_cols=1  Identities=100%  Similarity=2.527  Sum_probs=0.4

Q 5_Mustela         4 P    4 (7)
Q 2_Macaca          4 P    4 (7)
Q 1_Homo            4 P    4 (7)
Q 15_Ictidomys      4 P    4 (7)
Q 11_Cavia          4 P    4 (7)
Q Consensus         4 p    4 (7)
                      |
T Consensus        24 p   24 (49)
T signal           24 P   24 (49)
T 127              18 P   18 (43)
T 141              24 P   24 (47)
T 160              24 P   24 (49)
T 126              22 P   22 (47)
T 129              22 P   22 (45)
T 133              23 P   23 (47)
T 140              26 T   26 (51)
T 163              24 A   24 (48)
T 176              20 V   20 (44)
Confidence            3


No 20 
>P07251_35_IM_ref_sig5_130
Probab=0.76  E-value=32  Score=7.34  Aligned_cols=1  Identities=100%  Similarity=2.527  Sum_probs=0.3

Q 5_Mustela         4 P    4 (7)
Q 2_Macaca          4 P    4 (7)
Q 1_Homo            4 P    4 (7)
Q 15_Ictidomys      4 P    4 (7)
Q 11_Cavia          4 P    4 (7)
Q Consensus         4 p    4 (7)
                      |
T Consensus        24 P   24 (40)
T signal           24 P   24 (40)
T 174              30 P   30 (44)
T 178              29 P   29 (43)
T 183              29 P   29 (43)
T 186              27 P   27 (41)
T 185              24 P   24 (39)
T 172              27 P   27 (40)
T 173              27 P   27 (39)
T 175              30 P   30 (41)
T 176              27 P   27 (40)
Confidence            2


Done!
