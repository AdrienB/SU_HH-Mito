Query         5_Mustela
Match_columns 6
No_of_seqs    1 out of 20
Neff          1.0 
Searched_HMMs 436
Date          Wed Sep  6 16:09:26 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_67A73_67Q73_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P07271_45_Nu_IM_ref_sig5_130     2.2     9.6   0.022    8.9   0.0    2    4-5      43-44  (50)
  2 Q06645_61_MitoM_ref_sig5_130     2.1     9.8   0.023    8.4   0.0    2    4-5      19-20  (66)
  3 P49411_43_Mito_ref_sig5_130      2.1     9.8   0.023    8.8   0.0    2    4-5      41-42  (48)
  4 P34942_23_MM_ref_sig5_130        2.1      10   0.023    4.6   0.0    1    2-2      28-28  (28)
  5 P31023_31_MM_ref_sig5_130        2.1      10   0.023    8.3   0.0    2    4-5      29-30  (36)
  6 P80433_25_IM_ref_sig5_130        2.1      10   0.023    8.1   0.0    3    4-6      10-12  (30)
  7 P40360_13_Mito_ref_sig5_130      2.0      11   0.024    7.2   0.0    4    3-6       7-10  (18)
  8 P41563_27_Mito_ref_sig5_130      2.0      11   0.024    7.9   0.0    1    3-3      11-11  (32)
  9 P24310_21_IM_ref_sig5_130        1.9      11   0.026    7.7   0.0    1    3-3       7-7   (26)
 10 P10176_25_IM_ref_sig5_130        1.9      11   0.026    7.9   0.0    3    4-6      10-12  (30)
 11 Q7M0E7_30_Mito_ref_sig5_130      1.8      12   0.028    7.8   0.0    1    4-4      16-16  (35)
 12 P07263_20_Cy_Mito_ref_sig5_130   1.7      13   0.029    7.5   0.0    2    3-4       3-4   (25)
 13 Q02337_46_MM_ref_sig5_130        1.7      13    0.03    6.1   0.0    1    1-1       1-1   (51)
 14 P40185_17_MM_ref_sig5_130        1.6      14   0.032    7.2   0.0    3    3-5      15-17  (22)
 15 O82662_26_Mito_ref_sig5_130      1.6      14   0.032    7.8   0.0    3    4-6       2-4   (31)
 16 P15690_23_IM_ref_sig5_130        1.5      15   0.035    7.4   0.0    1    4-4       8-8   (28)
 17 P81140_13_MM_ref_sig5_130        1.4      16   0.037    6.7   0.0    2    3-4      13-14  (18)
 18 Q04728_8_MM_ref_sig5_130         1.3      17   0.038    6.3   0.0    2    2-3       3-4   (13)
 19 P12063_32_MM_ref_sig5_130        1.3      17   0.039    7.8   0.0    1    5-5      32-32  (37)
 20 Q3T0L3_8_Mito_ref_sig5_130       1.3      17    0.04    6.2   0.0    4    2-5       9-12  (13)

No 1  
>P07271_45_Nu_IM_ref_sig5_130
Probab=2.17  E-value=9.6  Score=8.94  Aligned_cols=2  Identities=100%  Similarity=1.880  Sum_probs=0.8

Q 5_Mustela         4 RG    5 (6)
Q Consensus         4 rg    5 (6)
                      ||
T Consensus        43 rg   44 (50)
T signal           43 RG   44 (50)
Confidence            44


No 2  
>Q06645_61_MitoM_ref_sig5_130
Probab=2.12  E-value=9.8  Score=8.39  Aligned_cols=2  Identities=100%  Similarity=1.880  Sum_probs=0.7

Q 5_Mustela         4 RG    5 (6)
Q Consensus         4 rg    5 (6)
                      ||
T Consensus        19 rg   20 (66)
T signal           19 RG   20 (66)
T 59_Zonotrichia   19 DG   20 (66)
T 54_Latimeria     19 RV   20 (66)
T 52_Anolis        19 RA   20 (66)
T 39_Heterocepha   19 RG   20 (66)
T cl|GABBABABA|1   19 PV   20 (66)
T cl|KABBABABA|1   19 PS   20 (66)
T cl|LABBABABA|3   19 RG   20 (66)
T cl|RABBABABA|7   19 RS   20 (66)
T 1                19 NG   20 (68)
Confidence            33


No 3  
>P49411_43_Mito_ref_sig5_130
Probab=2.12  E-value=9.8  Score=8.82  Aligned_cols=2  Identities=100%  Similarity=1.880  Sum_probs=0.8

Q 5_Mustela         4 RG    5 (6)
Q Consensus         4 rg    5 (6)
                      ||
T Consensus        41 Rg   42 (48)
T signal           41 RG   42 (48)
T 163              41 RG   42 (48)
T 166              36 RS   37 (43)
T 167              41 RG   42 (48)
T 170              38 RG   39 (45)
T 171              41 RG   42 (48)
T 177              41 RG   42 (48)
T 157              55 RG   56 (62)
Confidence            33


No 4  
>P34942_23_MM_ref_sig5_130
Probab=2.08  E-value=10  Score=4.63  Aligned_cols=1  Identities=0%  Similarity=-0.595  Sum_probs=0.0

Q 5_Mustela         2 V    2 (6)
Q Consensus         2 v    2 (6)
                      .
T Consensus        28 ~   28 (28)
T signal           28 P   28 (28)
T 78_Musca         40 -   39 (39)
T 74_Strongyloce   40 -   39 (39)
T 62_Alligator     40 -   39 (39)
T 60_Xenopus       40 -   39 (39)
T 56_Anas          40 -   39 (39)
T 52_Meleagris     40 -   39 (39)
T 40_Saimiri       40 -   39 (39)
Confidence            0


No 5  
>P31023_31_MM_ref_sig5_130
Probab=2.06  E-value=10  Score=8.34  Aligned_cols=2  Identities=50%  Similarity=0.866  Sum_probs=0.9

Q 5_Mustela         4 RG    5 (6)
Q Consensus         4 rg    5 (6)
                      ||
T Consensus        29 Rg   30 (36)
T signal           29 RA   30 (36)
T 194              31 RG   32 (38)
T 185              29 RG   30 (37)
T 186              38 RG   39 (46)
T 187              28 RG   29 (35)
T 188              37 RG   38 (45)
T 190              37 RG   38 (45)
T 193              28 RG   29 (35)
T 189              34 RG   35 (42)
T 191              33 RG   34 (41)
Confidence            44


No 6  
>P80433_25_IM_ref_sig5_130
Probab=2.05  E-value=10  Score=8.09  Aligned_cols=3  Identities=67%  Similarity=1.010  Sum_probs=1.6

Q 5_Mustela         4 RGL    6 (6)
Q Consensus         4 rgl    6 (6)
                      |||
T Consensus        10 rgL   12 (30)
T signal           10 RSL   12 (30)
T 3                10 RGL   12 (30)
T 20               10 RGL   12 (30)
T 22               10 RGL   12 (30)
T 31               10 RGL   12 (30)
T 36               10 RGL   12 (30)
T 5                10 RGL   12 (30)
T 8                10 RGL   12 (30)
T 19               10 RGL   12 (30)
Confidence            554


No 7  
>P40360_13_Mito_ref_sig5_130
Probab=2.00  E-value=11  Score=7.16  Aligned_cols=4  Identities=25%  Similarity=0.410  Sum_probs=1.9

Q 5_Mustela         3 S.RGL    6 (6)
Q Consensus         3 s.rgl    6 (6)
                      | +|+
T Consensus         7 ~.~~~   10 (18)
T signal            7 A.HEL   10 (18)
T 54                7 S.NGL   10 (18)
T 53                7 N.RGF   10 (18)
T 52                7 G.QRL   10 (17)
T 50                7 K.HGF   10 (17)
T 51                7 S.GGL   10 (18)
T 49                7 SgARF   11 (19)
Confidence            4 553


No 8  
>P41563_27_Mito_ref_sig5_130
Probab=1.99  E-value=11  Score=7.88  Aligned_cols=1  Identities=100%  Similarity=0.733  Sum_probs=0.4

Q 5_Mustela         3 S    3 (6)
Q Consensus         3 s    3 (6)
                      |
T Consensus        11 S   11 (32)
T signal           11 S   11 (32)
T 20               11 S   11 (30)
T 42               11 S   11 (32)
T 45               11 S   11 (31)
T 46               10 S   10 (30)
T 61               11 S   11 (31)
T 76               11 R   11 (30)
T 84               11 S   11 (31)
T 94               11 S   11 (31)
T 114              11 S   11 (31)
Confidence            3


No 9  
>P24310_21_IM_ref_sig5_130
Probab=1.89  E-value=11  Score=7.73  Aligned_cols=1  Identities=100%  Similarity=0.733  Sum_probs=0.3

Q 5_Mustela         3 S    3 (6)
Q Consensus         3 s    3 (6)
                      |
T Consensus         7 s    7 (26)
T signal            7 S    7 (26)
T 6                 7 S    7 (26)
T 8                 7 S    7 (26)
T 10                7 S    7 (26)
T 11                7 S    7 (26)
T 13                7 P    7 (26)
T 15                7 S    7 (26)
T 18                7 S    7 (26)
T 7                 8 G    8 (27)
T 2                 5 S    5 (23)
Confidence            3


No 10 
>P10176_25_IM_ref_sig5_130
Probab=1.86  E-value=11  Score=7.94  Aligned_cols=3  Identities=100%  Similarity=1.697  Sum_probs=1.5

Q 5_Mustela         4 RGL    6 (6)
Q Consensus         4 rgl    6 (6)
                      |||
T Consensus        10 rgL   12 (30)
T signal           10 RGL   12 (30)
T 6                10 RGL   12 (30)
T 18               10 RGL   12 (30)
T 23               10 RSL   12 (30)
T 29               10 RGL   12 (30)
T 38               10 RGL   12 (30)
T 2                10 SGL   12 (31)
T 3                10 RGL   12 (30)
T 5                10 RGL   12 (30)
T 27               10 RGL   12 (30)
Confidence            554


No 11 
>Q7M0E7_30_Mito_ref_sig5_130
Probab=1.76  E-value=12  Score=7.83  Aligned_cols=1  Identities=0%  Similarity=-0.329  Sum_probs=0.3

Q 5_Mustela         4 R    4 (6)
Q Consensus         4 r    4 (6)
                      |
T Consensus        16 r   16 (35)
T signal           16 G   16 (35)
T 53               16 R   16 (35)
T 33               15 Q   15 (34)
T 49               15 R   15 (34)
T 47               16 R   16 (35)
T 31               14 E   14 (33)
T 38               14 T   14 (33)
T 34               17 R   17 (36)
T 27               11 S   11 (30)
T 30               11 S   11 (30)
Confidence            3


No 12 
>P07263_20_Cy_Mito_ref_sig5_130
Probab=1.69  E-value=13  Score=7.55  Aligned_cols=2  Identities=100%  Similarity=1.149  Sum_probs=0.9

Q 5_Mustela         3 SR    4 (6)
Q Consensus         3 sr    4 (6)
                      ||
T Consensus         3 sr    4 (25)
T signal            3 SR    4 (25)
Confidence            44


No 13 
>Q02337_46_MM_ref_sig5_130
Probab=1.68  E-value=13  Score=6.15  Aligned_cols=1  Identities=0%  Similarity=-0.463  Sum_probs=0.0

Q 5_Mustela         1 K    1 (6)
Q Consensus         1 k    1 (6)
                      -
T Consensus         1 M    1 (51)
T signal            1 M    1 (51)
T 76_Pediculus      1 M    1 (50)
T 74_Branchiosto    1 M    1 (50)
T 69_Papio          1 V    1 (50)
T 67_Maylandia      1 M    1 (50)
T 60_Sus            1 M    1 (50)
T 72_Ciona          1 M    1 (50)
T 59_Anolis         1 I    1 (50)
T 51_Canis          1 M    1 (50)
T 38_Nomascus       1 M    1 (50)
Confidence            0


No 14 
>P40185_17_MM_ref_sig5_130
Probab=1.58  E-value=14  Score=7.24  Aligned_cols=3  Identities=67%  Similarity=1.232  Sum_probs=1.3

Q 5_Mustela         3 SRG    5 (6)
Q Consensus         3 srg    5 (6)
                      +|+
T Consensus        15 ~R~   17 (22)
T signal           15 RRG   17 (22)
T 114              13 ART   15 (20)
T 115              12 SRT   14 (19)
T 117              12 ARR   14 (19)
T 118              13 RRS   15 (20)
T 119              15 VRS   17 (22)
T 116              13 ARK   15 (20)
Confidence            344


No 15 
>O82662_26_Mito_ref_sig5_130
Probab=1.57  E-value=14  Score=7.75  Aligned_cols=3  Identities=100%  Similarity=1.697  Sum_probs=1.3

Q 5_Mustela         4 RGL    6 (6)
Q Consensus         4 rgl    6 (6)
                      |||
T Consensus         2 rgl    4 (31)
T signal            2 RGL    4 (31)
T 160               2 RGS    4 (31)
T 165               2 RGL    4 (31)
T 166               2 RGL    4 (31)
T 170               2 RGM    4 (31)
T 171               2 RGL    4 (31)
T 157               1 --L    1 (28)
Confidence            453


No 16 
>P15690_23_IM_ref_sig5_130
Probab=1.47  E-value=15  Score=7.43  Aligned_cols=1  Identities=0%  Similarity=0.899  Sum_probs=0.3

Q 5_Mustela         4 R    4 (6)
Q Consensus         4 r    4 (6)
                      |
T Consensus         8 r    8 (28)
T signal            8 K    8 (28)
T 110               9 R    9 (29)
T 99                9 K    9 (34)
T 102               9 R    9 (38)
T 105               9 R    9 (34)
T 107               9 R    9 (32)
T 116               9 R    9 (29)
T 119               8 R    8 (28)
T 123               9 R    9 (31)
T 128               8 R    8 (28)
Confidence            3


No 17 
>P81140_13_MM_ref_sig5_130
Probab=1.40  E-value=16  Score=6.73  Aligned_cols=2  Identities=100%  Similarity=1.149  Sum_probs=0.9

Q 5_Mustela         3 SR    4 (6)
Q Consensus         3 sr    4 (6)
                      ||
T Consensus        13 sR   14 (18)
T signal           13 SR   14 (18)
T 122              13 SH   14 (18)
T 123              13 SR   14 (18)
T 127              13 PR   14 (18)
T 129              14 SR   15 (19)
T 130              13 SR   14 (18)
T 145              13 SR   14 (18)
T 131              11 SR   12 (16)
T 113               9 PR   10 (14)
Confidence            44


No 18 
>Q04728_8_MM_ref_sig5_130
Probab=1.35  E-value=17  Score=6.28  Aligned_cols=2  Identities=50%  Similarity=0.883  Sum_probs=0.8

Q 5_Mustela         2 VS    3 (6)
Q Consensus         2 vs    3 (6)
                      +|
T Consensus         3 is    4 (13)
T signal            3 IS    4 (13)
T 90                3 IS    4 (13)
T 88                3 IS    4 (13)
Confidence            44


No 19 
>P12063_32_MM_ref_sig5_130
Probab=1.33  E-value=17  Score=7.81  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.3

Q 5_Mustela         5 G    5 (6)
Q Consensus         5 g    5 (6)
                      |
T Consensus        32 g   32 (37)
T signal           32 G   32 (37)
T 193              32 G   32 (37)
Confidence            3


No 20 
>Q3T0L3_8_Mito_ref_sig5_130
Probab=1.31  E-value=17  Score=6.21  Aligned_cols=4  Identities=50%  Similarity=1.041  Sum_probs=1.8

Q 5_Mustela         2 VSRG    5 (6)
Q Consensus         2 vsrg    5 (6)
                      +|-|
T Consensus         9 IsHG   12 (13)
T signal            9 ISHG   12 (13)
T 81                9 ISHG   12 (13)
T 95                9 ISHG   12 (13)
T 83                9 ISHG   12 (13)
T 85                9 ISHG   12 (13)
T 88                9 VSHG   12 (13)
T 89                9 ISHG   12 (13)
T 76                9 ITHG   12 (13)
Confidence            3544


Done!
