Query         5_Mustela
Match_columns 7
No_of_seqs    4 out of 20
Neff          1.3 
Searched_HMMs 436
Date          Wed Sep  6 16:08:08 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_15A22_15Q22_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P11177_30_MM_ref_sig5_130       55.2   0.052 0.00012   14.7   0.0    7    1-7      16-22  (35)
  2 Q0QF01_42_IM_ref_sig5_130       35.6    0.17 0.00039   13.9   0.0    7    1-7       8-14  (47)
  3 P10818_26_IM_ref_sig5_130       24.8    0.34 0.00078   12.3   0.0    6    1-6      12-17  (31)
  4 P13182_24_IM_ref_sig5_130       14.4    0.82  0.0019   11.2   0.0    6    1-6      10-15  (29)
  5 P80971_28_IM_ref_sig5_130       11.5     1.1  0.0026   11.1   0.0    6    2-7      11-16  (33)
  6 P12074_24_IM_ref_sig5_130       10.9     1.2  0.0028   10.7   0.0    6    1-6      10-15  (29)
  7 P37224_31_MM_ref_sig5_130        4.2     4.3  0.0098    9.6   0.0    1    4-4      15-15  (36)
  8 Q42777_22_MM_ref_sig5_130        4.2     4.3  0.0099    9.2   0.0    4    3-6       6-9   (27)
  9 P05630_22_Mito_IM_ref_sig5_130   4.2     4.3  0.0099    9.2   0.0    3    3-5       6-8   (27)
 10 P21801_20_IM_ref_sig5_130        4.2     4.3  0.0099    9.0   0.0    4    3-6       5-8   (25)
 11 P32904_16_Mito_ref_sig5_130      4.0     4.5    0.01    8.6   0.0    4    4-7       4-7   (21)
 12 P52903_26_MM_ref_sig5_130        4.0     4.6   0.011    9.2   0.0    1    3-3      12-12  (31)
 13 Q96252_26_Mito_IM_ref_sig5_130   3.8     4.9   0.011    9.2   0.0    2    3-4       8-9   (31)
 14 P17764_30_Mito_ref_sig5_130      3.7       5   0.011    9.3   0.0    3    3-5      15-17  (35)
 15 P32799_9_IM_ref_sig5_130         3.5     5.3   0.012    7.7   0.0    3    4-6       6-8   (14)
 16 P31039_44_IM_ref_sig5_130        3.5     5.4   0.012    6.3   0.0    1    3-3      49-49  (49)
 17 P36523_28_Mito_ref_sig5_130      3.4     5.5   0.013    9.2   0.0    3    4-6      12-14  (33)
 18 P11325_9_MM_ref_sig5_130         3.4     5.5   0.013    7.8   0.0    4    1-4       7-10  (14)
 19 P26440_29_MM_ref_sig5_130        3.2     5.9   0.013    9.2   0.0    1    3-3      24-24  (34)
 20 Q02380_46_IM_ref_sig5_130        2.9     6.7   0.015    9.7   0.0    1    4-4      33-33  (51)

No 1  
>P11177_30_MM_ref_sig5_130
Probab=55.23  E-value=0.052  Score=14.72  Aligned_cols=7  Identities=86%  Similarity=1.374  Sum_probs=4.0

Q 5_Mustela         1 SGLLRRR    7 (7)
Q 2_Macaca          1 SGLLKRR    7 (7)
Q 18_Ceratotheri    1 SGFLRRR    7 (7)
Q 4_Pongo           1 SRLLKRR    7 (7)
Q Consensus         1 sgll~rr    7 (7)
                      |+++||+
T Consensus        16 s~~lrR~   22 (35)
T signal           16 SGLLKRR   22 (35)
T 143              20 SGLPRRG   26 (39)
T 144              16 SVFLKRS   22 (35)
T 145              16 AVVLQRE   22 (35)
T 147              16 SVVLRRE   22 (35)
T 135              25 RLQLRSG   31 (44)
T 141              30 RLQQRRG   36 (49)
T 142              15 LVLGRRR   21 (34)
T 149              15 SALQRRE   21 (34)
T 150              15 SAVRRRH   21 (34)
Confidence            4566664


No 2  
>Q0QF01_42_IM_ref_sig5_130
Probab=35.65  E-value=0.17  Score=13.90  Aligned_cols=7  Identities=71%  Similarity=0.857  Sum_probs=3.7

Q 5_Mustela         1 SGLLRRR    7 (7)
Q 2_Macaca          1 SGLLKRR    7 (7)
Q 18_Ceratotheri    1 SGFLRRR    7 (7)
Q 4_Pongo           1 SRLLKRR    7 (7)
Q Consensus         1 sgll~rr    7 (7)
                      |++|++|
T Consensus         8 SRlL~~r   14 (47)
T signal            8 SRLLRAR   14 (47)
T 124               8 SRVVAKR   14 (47)
T 126               8 SRVLTKK   14 (43)
T 136               9 SRLLSRA   15 (50)
T 137               8 SRLLSKR   14 (46)
T 144               8 SRGLARR   14 (48)
T 145               5 ATVVCRR   11 (47)
T 171               1 -------    0 (44)
T 154               1 -------    0 (30)
Confidence            4566554


No 3  
>P10818_26_IM_ref_sig5_130
Probab=24.83  E-value=0.34  Score=12.32  Aligned_cols=6  Identities=83%  Similarity=1.138  Sum_probs=3.4

Q 5_Mustela         1 SGLLRR    6 (7)
Q 2_Macaca          1 SGLLKR    6 (7)
Q 18_Ceratotheri    1 SGFLRR    6 (7)
Q 4_Pongo           1 SRLLKR    6 (7)
Q Consensus         1 sgll~r    6 (7)
                      ||||.|
T Consensus        12 SgLLgR   17 (31)
T signal           12 SGLLGR   17 (31)
T 53                9 SGLLLR   14 (28)
T 56                9 FGLPVR   14 (28)
T 64                9 SRLLGR   14 (28)
T 36               11 SLLLGG   16 (30)
T 44                9 SRLLGR   14 (28)
T 55                9 SRLLNW   14 (28)
T 6                11 LRHLGR   16 (30)
T 7                10 SGLLGR   15 (29)
T 43                7 SGILRR   12 (25)
Confidence            566654


No 4  
>P13182_24_IM_ref_sig5_130
Probab=14.40  E-value=0.82  Score=11.19  Aligned_cols=6  Identities=67%  Similarity=0.861  Sum_probs=3.0

Q 5_Mustela         1 SGLLRR    6 (7)
Q 2_Macaca          1 SGLLKR    6 (7)
Q 18_Ceratotheri    1 SGFLRR    6 (7)
Q 4_Pongo           1 SRLLKR    6 (7)
Q Consensus         1 sgll~r    6 (7)
                      ||||-|
T Consensus        10 s~lLgr   15 (29)
T signal           10 FGLLGR   15 (29)
T 11               12 SRPLGR   17 (31)
T 16               10 SRLLGR   15 (29)
T 19               10 SGLLSR   15 (29)
T 26               10 SRLLGR   15 (29)
T 30               10 FGLPVR   15 (29)
T 33               10 SGLLLR   15 (29)
Confidence            455543


No 5  
>P80971_28_IM_ref_sig5_130
Probab=11.51  E-value=1.1  Score=11.10  Aligned_cols=6  Identities=50%  Similarity=0.639  Sum_probs=3.1

Q 5_Mustela         2 GLLRRR    7 (7)
Q 2_Macaca          2 GLLKRR    7 (7)
Q 18_Ceratotheri    2 GFLRRR    7 (7)
Q 4_Pongo           2 RLLKRR    7 (7)
Q Consensus         2 gll~rr    7 (7)
                      +||-||
T Consensus        11 ~LLa~R   16 (33)
T signal           11 SLLAGR   16 (33)
T 94               11 GLLARR   16 (31)
T 96               11 CLLARR   16 (31)
T 93               11 GLLTRR   16 (32)
T 92               11 SLLAGR   16 (32)
Confidence            455554


No 6  
>P12074_24_IM_ref_sig5_130
Probab=10.90  E-value=1.2  Score=10.71  Aligned_cols=6  Identities=67%  Similarity=0.717  Sum_probs=2.9

Q 5_Mustela         1 SGLLRR    6 (7)
Q 2_Macaca          1 SGLLKR    6 (7)
Q 18_Ceratotheri    1 SGFLRR    6 (7)
Q 4_Pongo           1 SRLLKR    6 (7)
Q Consensus         1 sgll~r    6 (7)
                      ||||-|
T Consensus        10 s~lLgR   15 (29)
T signal           10 SRLLGR   15 (29)
T 32               10 SRPLGR   15 (29)
T 37               12 SGLLGR   17 (31)
T 43               10 SRLLGR   15 (29)
T 54               10 SRLLNW   15 (29)
T 44               10 SGLLSR   15 (29)
T 51               10 FGLPVR   15 (29)
T 59               10 SRLLGR   15 (29)
T 60               10 AGLLGR   15 (29)
T 7                 8 LRHLGR   13 (27)
Confidence            455543


No 7  
>P37224_31_MM_ref_sig5_130
Probab=4.20  E-value=4.3  Score=9.55  Aligned_cols=1  Identities=0%  Similarity=0.932  Sum_probs=0.3

Q 5_Mustela         4 L    4 (7)
Q 2_Macaca          4 L    4 (7)
Q 18_Ceratotheri    4 L    4 (7)
Q 4_Pongo           4 L    4 (7)
Q Consensus         4 l    4 (7)
                      +
T Consensus        15 i   15 (36)
T signal           15 I   15 (36)
T 111              12 S   12 (32)
T 122              12 L   12 (36)
T 124              12 I   12 (35)
T 117              15 I   15 (41)
T 123              15 I   15 (40)
T 121              12 M   12 (39)
T 125              15 I   15 (39)
T 116              15 L   15 (39)
T 120              13 -   12 (35)
Confidence            2


No 8  
>Q42777_22_MM_ref_sig5_130
Probab=4.18  E-value=4.3  Score=9.19  Aligned_cols=4  Identities=100%  Similarity=1.448  Sum_probs=2.0

Q 5_Mustela         3 LLRR......    6 (7)
Q 2_Macaca          3 LLKR......    6 (7)
Q 18_Ceratotheri    3 FLRR......    6 (7)
Q 4_Pongo           3 LLKR......    6 (7)
Q Consensus         3 ll~r......    6 (7)
                      ||||      
T Consensus         6 llrr......    9 (27)
T signal            6 LLRR......    9 (27)
T 8                 6 LLRRkliitr   15 (33)
Confidence            4554      


No 9  
>P05630_22_Mito_IM_ref_sig5_130
Probab=4.16  E-value=4.3  Score=9.16  Aligned_cols=3  Identities=100%  Similarity=1.409  Sum_probs=1.1

Q 5_Mustela         3 LLR    5 (7)
Q 2_Macaca          3 LLK    5 (7)
Q 18_Ceratotheri    3 FLR    5 (7)
Q 4_Pongo           3 LLK    5 (7)
Q Consensus         3 ll~    5 (7)
                      +|+
T Consensus         6 llr    8 (27)
T signal            6 LLR    8 (27)
T 133               6 LLR    8 (27)
T 148               6 LLR    8 (27)
T 167               6 LLR    8 (27)
T 155               6 LLR    8 (27)
T 147               6 VFR    8 (27)
T 140               6 LLR    8 (26)
T 142               6 LLR    8 (32)
T 121               6 LLL    8 (34)
T 134               6 LLA    8 (29)
Confidence            333


No 10 
>P21801_20_IM_ref_sig5_130
Probab=4.16  E-value=4.3  Score=9.02  Aligned_cols=4  Identities=100%  Similarity=1.448  Sum_probs=1.7

Q 5_Mustela         3 LLRR    6 (7)
Q 2_Macaca          3 LLKR    6 (7)
Q 18_Ceratotheri    3 FLRR    6 (7)
Q 4_Pongo           3 LLKR    6 (7)
Q Consensus         3 ll~r    6 (7)
                      ||+|
T Consensus         5 LlrR    8 (25)
T signal            5 LLRR    8 (25)
T 192               1 LLRR    4 (23)
T 191               1 LIQR    4 (21)
Confidence            3444


No 11 
>P32904_16_Mito_ref_sig5_130
Probab=4.02  E-value=4.5  Score=8.55  Aligned_cols=4  Identities=50%  Similarity=1.140  Sum_probs=1.6

Q 5_Mustela         4 LRRR    7 (7)
Q 2_Macaca          4 LKRR    7 (7)
Q 18_Ceratotheri    4 LRRR    7 (7)
Q 4_Pongo           4 LKRR    7 (7)
Q Consensus         4 l~rr    7 (7)
                      ..||
T Consensus         4 i~rR    7 (21)
T signal            4 IQRR    7 (21)
T 41                4 IKRR    7 (21)
T 39                4 VSRR    7 (21)
T 20                4 VFRR    7 (21)
T 26                4 ISAR    7 (21)
T 24                4 ISKR    7 (21)
T 25                4 GLKR    7 (21)
T 28                4 VWRR    7 (21)
T 40                2 FGRR    5 (19)
T 9                 1 FSVR    4 (17)
Confidence            3343


No 12 
>P52903_26_MM_ref_sig5_130
Probab=3.97  E-value=4.6  Score=9.22  Aligned_cols=1  Identities=0%  Similarity=0.932  Sum_probs=0.3

Q 5_Mustela         3 L    3 (7)
Q 2_Macaca          3 L    3 (7)
Q 18_Ceratotheri    3 F    3 (7)
Q 4_Pongo           3 L    3 (7)
Q Consensus         3 l    3 (7)
                      |
T Consensus        12 l   12 (31)
T signal           12 I   12 (31)
T 105              15 L   15 (33)
T 113              15 L   15 (35)
T 114              17 F   17 (37)
T 117              16 L   16 (35)
T 104              15 L   15 (34)
T 111              14 L   14 (38)
T 108              12 L   12 (36)
T 116              11 L   11 (35)
T 109              13 I   13 (29)
Confidence            3


No 13 
>Q96252_26_Mito_IM_ref_sig5_130
Probab=3.78  E-value=4.9  Score=9.21  Aligned_cols=2  Identities=100%  Similarity=1.331  Sum_probs=0.8

Q 5_Mustela         3 LL    4 (7)
Q 2_Macaca          3 LL    4 (7)
Q 18_Ceratotheri    3 FL    4 (7)
Q 4_Pongo           3 LL    4 (7)
Q Consensus         3 ll    4 (7)
                      ||
T Consensus         8 Ll    9 (31)
T signal            8 LL    9 (31)
T 164               8 LL    9 (32)
T 165               8 LL    9 (26)
T 166               8 LL    9 (26)
T 163               8 LL    9 (27)
T 159               9 LF   10 (23)
T 161               9 FL   10 (23)
T 167               8 LF    9 (30)
Confidence            33


No 14 
>P17764_30_Mito_ref_sig5_130
Probab=3.72  E-value=5  Score=9.34  Aligned_cols=3  Identities=100%  Similarity=1.409  Sum_probs=1.1

Q 5_Mustela         3 LLR    5 (7)
Q 2_Macaca          3 LLK    5 (7)
Q 18_Ceratotheri    3 FLR    5 (7)
Q 4_Pongo           3 LLK    5 (7)
Q Consensus         3 ll~    5 (7)
                      ||+
T Consensus        15 llr   17 (35)
T signal           15 LLR   17 (35)
T 144              18 LLR   20 (38)
T 149              18 LLQ   20 (38)
T 152              23 LVR   25 (43)
T 154              18 LLR   20 (38)
T 157               6 FSI    8 (26)
T 161              18 LLR   20 (38)
T 153               7 LLR    9 (26)
T 134              18 PLR   20 (38)
T 159              18 LLC   20 (38)
Confidence            333


No 15 
>P32799_9_IM_ref_sig5_130
Probab=3.54  E-value=5.3  Score=7.70  Aligned_cols=3  Identities=33%  Similarity=0.689  Sum_probs=1.3

Q 5_Mustela         4 LRR    6 (7)
Q 2_Macaca          4 LKR    6 (7)
Q 18_Ceratotheri    4 LRR    6 (7)
Q 4_Pongo           4 LKR    6 (7)
Q Consensus         4 l~r    6 (7)
                      ++|
T Consensus         6 ~~R    8 (14)
T signal            6 AKR    8 (14)
T 25                6 AIR    8 (12)
T 27                6 LIR    8 (12)
T 33                6 AKR    8 (14)
T 34                6 LRK    8 (13)
T 35                6 LRR    8 (13)
T 40                6 IRR    8 (13)
T 24                6 IRR    8 (13)
T 43                6 IRR    8 (13)
Confidence            344


No 16 
>P31039_44_IM_ref_sig5_130
Probab=3.50  E-value=5.4  Score=6.29  Aligned_cols=1  Identities=0%  Similarity=-0.695  Sum_probs=0.0

Q 5_Mustela         3 L    3 (7)
Q 2_Macaca          3 L    3 (7)
Q 18_Ceratotheri    3 F    3 (7)
Q 4_Pongo           3 L    3 (7)
Q Consensus         3 l    3 (7)
                      -
T Consensus        49 ~   49 (49)
T signal           49 S   49 (49)
T 83_Strongyloce   46 -   45 (45)
T 39_Sarcophilus   46 -   45 (45)
T 21_Mustela       46 -   45 (45)
T 94_Branchiosto   46 -   45 (45)
T 109_Pediculus    46 -   45 (45)
T 93_Sorex         46 -   45 (45)
T 33_Ictidomys     46 -   45 (45)
T 69_Tursiops      46 -   45 (45)
T 27_Saimiri       46 -   45 (45)
Confidence            0


No 17 
>P36523_28_Mito_ref_sig5_130
Probab=3.42  E-value=5.5  Score=9.23  Aligned_cols=3  Identities=100%  Similarity=1.486  Sum_probs=1.2

Q 5_Mustela         4 LRR    6 (7)
Q 2_Macaca          4 LKR    6 (7)
Q 18_Ceratotheri    4 LRR    6 (7)
Q 4_Pongo           4 LKR    6 (7)
Q Consensus         4 l~r    6 (7)
                      |||
T Consensus        12 lrr   14 (33)
T signal           12 LRR   14 (33)
Confidence            443


No 18 
>P11325_9_MM_ref_sig5_130
Probab=3.41  E-value=5.5  Score=7.78  Aligned_cols=4  Identities=50%  Similarity=0.600  Sum_probs=2.1

Q 5_Mustela         1 SGLL    4 (7)
Q 2_Macaca          1 SGLL    4 (7)
Q 18_Ceratotheri    1 SGFL    4 (7)
Q 4_Pongo           1 SRLL    4 (7)
Q Consensus         1 sgll    4 (7)
                      |+||
T Consensus         7 srfl   10 (14)
T signal            7 SRFL   10 (14)
Confidence            4555


No 19 
>P26440_29_MM_ref_sig5_130
Probab=3.25  E-value=5.9  Score=9.16  Aligned_cols=1  Identities=0%  Similarity=0.666  Sum_probs=0.3

Q 5_Mustela         3 L    3 (7)
Q 2_Macaca          3 L    3 (7)
Q 18_Ceratotheri    3 F    3 (7)
Q 4_Pongo           3 L    3 (7)
Q Consensus         3 l    3 (7)
                      |
T Consensus        24 l   24 (34)
T signal           24 F   24 (34)
T 107              27 L   27 (37)
T 130              27 L   27 (37)
T 132              24 L   24 (34)
T 136              27 L   27 (37)
T 138              24 L   24 (34)
T 150              27 L   27 (37)
T 140              27 L   27 (37)
T 144              24 V   24 (34)
T 139              27 L   27 (37)
Confidence            2


No 20 
>Q02380_46_IM_ref_sig5_130
Probab=2.92  E-value=6.7  Score=9.68  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.3

Q 5_Mustela         4 L    4 (7)
Q 2_Macaca          4 L    4 (7)
Q 18_Ceratotheri    4 L    4 (7)
Q 4_Pongo           4 L    4 (7)
Q Consensus         4 l    4 (7)
                      |
T Consensus        33 l   33 (51)
T signal           33 L   33 (51)
T 55               33 L   33 (51)
T 59               33 L   33 (51)
T 51               37 L   37 (57)
T 94               33 L   33 (42)
T 49               35 -   34 (52)
T 46               32 L   32 (51)
T 42               43 -   42 (60)
T 24               30 P   30 (44)
T 40               30 L   30 (48)
Confidence            3


Done!
