Query         5_Mustela
Match_columns 6
No_of_seqs    3 out of 20
Neff          1.2 
Searched_HMMs 436
Date          Wed Sep  6 16:08:14 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_19A25_19Q25_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P11177_30_MM_ref_sig5_130       93.6 9.2E-05 2.1E-07   21.4   0.0    6    1-6      20-25  (35)
  2 Q9N2I8_21_Mito_ref_sig5_130      5.0     3.5   0.008    9.0   0.0    4    3-6      10-13  (26)
  3 P07257_16_IM_ref_sig5_130        4.9     3.5  0.0081    8.5   0.0    4    2-5      15-18  (21)
  4 P12686_37_Mito_ref_sig5_130      4.9     3.6  0.0082    9.8   0.0    3    2-4      35-37  (42)
  5 Q95108_59_Mito_ref_sig5_130      4.9     3.6  0.0082   10.3   0.0    3    2-4       8-10  (64)
  6 P20069_32_MM_ref_sig5_130        4.6     3.8  0.0088    9.5   0.0    3    2-4      30-32  (37)
  7 P43265_42_IM_ref_sig5_130        4.3     4.2  0.0096    9.8   0.0    3    2-4      15-17  (47)
  8 P22068_44_Mito_IM_ref_sig5_130   4.2     4.3  0.0098    9.9   0.0    3    2-4      11-13  (49)
  9 P25711_33_IM_ref_sig5_130        4.0     4.5    0.01    9.3   0.0    3    2-4      30-32  (38)
 10 Q40089_21_Mito_IM_ref_sig5_130   3.3     5.7   0.013    8.4   0.0    3    2-4      19-21  (26)
 11 P39112_41_MM_ref_sig5_130        3.0     6.4   0.015    9.3   0.0    4    2-5      14-17  (46)
 12 Q9YHT2_38_IM_ref_sig5_130        2.9     6.9   0.016    9.1   0.0    2    2-3      12-13  (43)
 13 Q04467_39_Mito_ref_sig5_130      2.5     8.2   0.019    5.0   0.0    1    3-3      43-43  (44)
 14 P00366_57_MM_ref_sig5_130        2.4     8.5    0.02    6.7   0.0    1    2-2      51-51  (62)
 15 Q02486_26_Mito_Nu_ref_sig5_130   2.2     9.6   0.022    7.8   0.0    4    2-5       9-12  (31)
 16 Q27607_9_Mito_ref_sig5_130       2.1     9.9   0.023    6.9   0.0    3    3-5       2-4   (14)
 17 P32799_9_IM_ref_sig5_130         2.0      10   0.024    6.8   0.0    2    2-3       8-9   (14)
 18 P19234_31_IM_ref_sig5_130        1.9      11   0.026    8.1   0.0    3    3-5      23-25  (36)
 19 P37298_31_IM_ref_sig5_130        1.9      11   0.026    8.2   0.0    2    4-5      24-25  (36)
 20 O35854_27_Mito_ref_sig5_130      1.8      12   0.027    5.5   0.0    1    3-3      25-25  (32)

No 1  
>P11177_30_MM_ref_sig5_130
Probab=93.59  E-value=9.2e-05  Score=21.43  Aligned_cols=6  Identities=67%  Similarity=1.304  Sum_probs=5.1

Q 5_Mustela         1 RRRFHR    6 (6)
Q 2_Macaca          1 KRRFHW    6 (6)
Q 11_Cavia          1 KRRFHR    6 (6)
Q Consensus         1 krrfh~    6 (6)
                      ||+|||
T Consensus        20 rR~FHr   25 (35)
T signal           20 KRRFHW   25 (35)
T 143              24 RRGFHR   29 (39)
T 144              20 KRSFHR   25 (35)
T 145              20 QREFHR   25 (35)
T 147              20 RREFHR   25 (35)
T 135              29 RSGIHG   34 (44)
T 141              34 RRGFRL   39 (49)
T 142              19 RRRFHG   24 (34)
T 149              19 RREFHR   24 (34)
T 150              19 RRHFHR   24 (34)
Confidence            689997


No 2  
>Q9N2I8_21_Mito_ref_sig5_130
Probab=4.95  E-value=3.5  Score=9.01  Aligned_cols=4  Identities=50%  Similarity=0.941  Sum_probs=2.2

Q 5_Mustela         3 RFHR    6 (6)
Q 2_Macaca          3 RFHW    6 (6)
Q 11_Cavia          3 RFHR    6 (6)
Q Consensus         3 rfh~    6 (6)
                      ||-|
T Consensus        10 rFRw   13 (26)
T signal           10 RFRG   13 (26)
T 69               10 RFRW   13 (34)
T 78               10 RFRS   13 (34)
T 5                10 LFRW   13 (29)
T 34               10 RFRW   13 (41)
T 60               10 RFRP   13 (36)
T 25               10 RFRW   13 (34)
T 88               10 RFRW   13 (34)
T 46               10 LFRW   13 (25)
Confidence            6654


No 3  
>P07257_16_IM_ref_sig5_130
Probab=4.92  E-value=3.5  Score=8.51  Aligned_cols=4  Identities=50%  Similarity=0.924  Sum_probs=2.0

Q 5_Mustela         2 RRFH    5 (6)
Q 2_Macaca          2 RRFH    5 (6)
Q 11_Cavia          2 RRFH    5 (6)
Q Consensus         2 rrfh    5 (6)
                      ||++
T Consensus        15 R~~s   18 (21)
T signal           15 RRLT   18 (21)
T 20               12 RRYT   15 (18)
T 34               12 RRYT   15 (18)
T 14               13 RHYS   16 (19)
T 15               14 RKFS   17 (20)
T 23               16 RRLS   19 (22)
T 19               14 RHLS   17 (20)
T 40               16 RHYH   19 (22)
T 42               14 RKLS   17 (20)
Confidence            5553


No 4  
>P12686_37_Mito_ref_sig5_130
Probab=4.87  E-value=3.6  Score=9.84  Aligned_cols=3  Identities=100%  Similarity=1.818  Sum_probs=1.9

Q 5_Mustela         2 RRF    4 (6)
Q 2_Macaca          2 RRF    4 (6)
Q 11_Cavia          2 RRF    4 (6)
Q Consensus         2 rrf    4 (6)
                      |||
T Consensus        35 rrf   37 (42)
T signal           35 RRF   37 (42)
Confidence            566


No 5  
>Q95108_59_Mito_ref_sig5_130
Probab=4.85  E-value=3.6  Score=10.35  Aligned_cols=3  Identities=100%  Similarity=1.818  Sum_probs=1.6

Q 5_Mustela         2 RRF    4 (6)
Q 2_Macaca          2 RRF    4 (6)
Q 11_Cavia          2 RRF    4 (6)
Q Consensus         2 rrf    4 (6)
                      |||
T Consensus         8 RRf   10 (64)
T signal            8 RRF   10 (64)
T 82                8 RRL   10 (65)
T 80                8 RRF   10 (62)
T 79                8 RRF   10 (66)
T 58                8 RRL   10 (47)
T 77                8 KRV   10 (67)
T 78                8 RRL   10 (70)
T 76                7 RPF    9 (68)
Confidence            555


No 6  
>P20069_32_MM_ref_sig5_130
Probab=4.59  E-value=3.8  Score=9.46  Aligned_cols=3  Identities=100%  Similarity=1.818  Sum_probs=1.6

Q 5_Mustela         2 RRF    4 (6)
Q 2_Macaca          2 RRF    4 (6)
Q 11_Cavia          2 RRF    4 (6)
Q Consensus         2 rrf    4 (6)
                      |+|
T Consensus        30 RqF   32 (37)
T signal           30 RRF   32 (37)
T 143              30 RQF   32 (37)
T 58               31 RRF   33 (37)
T 118              34 RQF   36 (40)
T 119              32 RRF   34 (38)
T 123              31 RQF   33 (37)
T 127              32 RGF   34 (38)
T 129              32 RQF   34 (38)
T 134              31 RQF   33 (37)
T 121              21 RRF   23 (27)
Confidence            555


No 7  
>P43265_42_IM_ref_sig5_130
Probab=4.26  E-value=4.2  Score=9.82  Aligned_cols=3  Identities=100%  Similarity=1.818  Sum_probs=1.7

Q 5_Mustela         2 RRF    4 (6)
Q 2_Macaca          2 RRF    4 (6)
Q 11_Cavia          2 RRF    4 (6)
Q Consensus         2 rrf    4 (6)
                      |||
T Consensus        15 rrf   17 (47)
T signal           15 RRF   17 (47)
Confidence            565


No 8  
>P22068_44_Mito_IM_ref_sig5_130
Probab=4.22  E-value=4.3  Score=9.87  Aligned_cols=3  Identities=100%  Similarity=1.818  Sum_probs=1.7

Q 5_Mustela         2 RRF    4 (6)
Q 2_Macaca          2 RRF    4 (6)
Q 11_Cavia          2 RRF    4 (6)
Q Consensus         2 rrf    4 (6)
                      |||
T Consensus        11 rrf   13 (49)
T signal           11 RRF   13 (49)
Confidence            555


No 9  
>P25711_33_IM_ref_sig5_130
Probab=4.02  E-value=4.5  Score=9.33  Aligned_cols=3  Identities=33%  Similarity=0.335  Sum_probs=1.8

Q 5_Mustela         2 RRF    4 (6)
Q 2_Macaca          2 RRF    4 (6)
Q 11_Cavia          2 RRF    4 (6)
Q Consensus         2 rrf    4 (6)
                      |||
T Consensus        30 RR~   32 (38)
T signal           30 QRR   32 (38)
T 153              28 RRA   30 (36)
T 150              29 RRY   31 (36)
T 151              29 RRY   31 (36)
T 152              28 RRF   30 (35)
T 146              30 RRH   32 (35)
T 149              32 RRF   34 (38)
T 147              28 RRF   30 (34)
T 148              28 RRF   30 (34)
Confidence            565


No 10 
>Q40089_21_Mito_IM_ref_sig5_130
Probab=3.31  E-value=5.7  Score=8.40  Aligned_cols=3  Identities=67%  Similarity=1.198  Sum_probs=1.3

Q 5_Mustela         2 RRF    4 (6)
Q 2_Macaca          2 RRF    4 (6)
Q 11_Cavia          2 RRF    4 (6)
Q Consensus         2 rrf    4 (6)
                      |+|
T Consensus        19 R~f   21 (26)
T signal           19 RPF   21 (26)
T 44               19 RPF   21 (26)
T 43               19 RSF   21 (26)
T 40               17 RRF   19 (23)
T 41               17 RRF   19 (23)
T 3                19 RSY   21 (24)
T 5                19 RTY   21 (24)
T 10               19 RTY   21 (24)
T 11               19 RGY   21 (24)
Confidence            444


No 11 
>P39112_41_MM_ref_sig5_130
Probab=3.04  E-value=6.4  Score=9.28  Aligned_cols=4  Identities=75%  Similarity=1.456  Sum_probs=1.8

Q 5_Mustela         2 RRFH    5 (6)
Q 2_Macaca          2 RRFH    5 (6)
Q 11_Cavia          2 RRFH    5 (6)
Q Consensus         2 rrfh    5 (6)
                      |-||
T Consensus        14 rsfh   17 (46)
T signal           14 RSFH   17 (46)
Confidence            3454


No 12 
>Q9YHT2_38_IM_ref_sig5_130
Probab=2.86  E-value=6.9  Score=9.09  Aligned_cols=2  Identities=50%  Similarity=0.617  Sum_probs=0.7

Q 5_Mustela         2 RR    3 (6)
Q 2_Macaca          2 RR    3 (6)
Q 11_Cavia          2 RR    3 (6)
Q Consensus         2 rr    3 (6)
                      |+
T Consensus        12 r~   13 (43)
T signal           12 RG   13 (43)
T 89               10 RG   11 (32)
T 118              10 RR   11 (32)
T 132              10 RR   11 (32)
T 85                9 SK   10 (32)
T 99                8 RR    9 (30)
T 101               8 RL    9 (30)
T 120               8 RG    9 (32)
T 125               8 LR    9 (30)
T 83                5 LF    6 (31)
Confidence            33


No 13 
>Q04467_39_Mito_ref_sig5_130
Probab=2.47  E-value=8.2  Score=5.03  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.2

Q 5_Mustela         3 R    3 (6)
Q 2_Macaca          3 R    3 (6)
Q 11_Cavia          3 R    3 (6)
Q Consensus         3 r    3 (6)
                      |
T Consensus        43 ~   43 (44)
T signal           43 R   43 (44)
T 92_Chlorella     41 -   40 (40)
T 86_Trichoplax    41 -   40 (40)
T 76_Bombyx        41 -   40 (40)
T 69_Meleagris     41 -   40 (40)
T 65_Latimeria     41 -   40 (40)
T 38_Sarcophilus   41 -   40 (40)
Confidence            2


No 14 
>P00366_57_MM_ref_sig5_130
Probab=2.39  E-value=8.5  Score=6.68  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.2

Q 5_Mustela         2 R    2 (6)
Q 2_Macaca          2 R    2 (6)
Q 11_Cavia          2 R    2 (6)
Q Consensus         2 r    2 (6)
                      |
T Consensus        51 ~   51 (62)
T signal           51 R   51 (62)
T 114_Galdieria    44 -   43 (43)
T 110_Naegleria    44 -   43 (43)
T 104_Ichthyopht   44 -   43 (43)
T 102_Guillardia   44 -   43 (43)
T 93_Amphimedon    44 -   43 (43)
T 92_Schistosoma   44 -   43 (43)
T 89_Culex         44 -   43 (43)
T 69_Gallus        44 -   43 (43)
T 24_Saimiri       44 -   43 (43)
Confidence            2


No 15 
>Q02486_26_Mito_Nu_ref_sig5_130
Probab=2.17  E-value=9.6  Score=7.84  Aligned_cols=4  Identities=75%  Similarity=1.456  Sum_probs=2.1

Q 5_Mustela         2 RRFH    5 (6)
Q 2_Macaca          2 RRFH    5 (6)
Q 11_Cavia          2 RRFH    5 (6)
Q Consensus         2 rrfh    5 (6)
                      ++||
T Consensus         9 ~~~~   12 (31)
T signal            9 RSFH   12 (31)
T cl|CABBABABA|1    9 GRIS   12 (31)
T cl|FABBABABA|1    9 NVNG   12 (31)
T 2                 9 ADKP   12 (31)
Confidence            4555


No 16 
>Q27607_9_Mito_ref_sig5_130
Probab=2.11  E-value=9.9  Score=6.93  Aligned_cols=3  Identities=67%  Similarity=1.608  Sum_probs=1.9

Q 5_Mustela         3 RFH    5 (6)
Q 2_Macaca          3 RFH    5 (6)
Q 11_Cavia          3 RFH    5 (6)
Q Consensus         3 rfh    5 (6)
                      .||
T Consensus         2 qfh    4 (14)
T signal            2 QFH    4 (14)
Confidence            477


No 17 
>P32799_9_IM_ref_sig5_130
Probab=2.02  E-value=10  Score=6.76  Aligned_cols=2  Identities=50%  Similarity=0.484  Sum_probs=0.8

Q 5_Mustela         2 RR    3 (6)
Q 2_Macaca          2 RR    3 (6)
Q 11_Cavia          2 RR    3 (6)
Q Consensus         2 rr    3 (6)
                      ||
T Consensus         8 R~    9 (14)
T signal            8 RY    9 (14)
T 25                8 RR    9 (12)
T 27                8 RR    9 (12)
T 33                8 RS    9 (14)
T 34                8 K-    8 (13)
T 35                8 R-    8 (13)
T 40                8 R-    8 (13)
T 24                8 R-    8 (13)
T 43                8 R-    8 (13)
Confidence            33


No 18 
>P19234_31_IM_ref_sig5_130
Probab=1.89  E-value=11  Score=8.10  Aligned_cols=3  Identities=33%  Similarity=0.921  Sum_probs=1.2

Q 5_Mustela         3 RFH    5 (6)
Q 2_Macaca          3 RFH    5 (6)
Q 11_Cavia          3 RFH    5 (6)
Q Consensus         3 rfh    5 (6)
                      -+|
T Consensus        23 ~LH   25 (36)
T signal           23 NLH   25 (36)
T 55               23 CLH   25 (36)
T 99               18 NLH   20 (31)
T 2                19 YLH   21 (32)
T 13               19 GLH   21 (32)
T 25               19 ALH   21 (32)
T 102              24 YLH   26 (36)
T 104              18 YLH   20 (31)
T 1                16 SLH   18 (28)
T 33               17 YLH   19 (29)
Confidence            344


No 19 
>P37298_31_IM_ref_sig5_130
Probab=1.86  E-value=11  Score=8.19  Aligned_cols=2  Identities=50%  Similarity=1.364  Sum_probs=0.9

Q 5_Mustela         4 FH    5 (6)
Q 2_Macaca          4 FH    5 (6)
Q 11_Cavia          4 FH    5 (6)
Q Consensus         4 fh    5 (6)
                      ||
T Consensus        24 fh   25 (36)
T signal           24 FQ   25 (36)
T 12               13 FH   14 (25)
T 15               15 FH   16 (27)
T 11               13 LH   14 (25)
T 16               18 FQ   19 (30)
Confidence            44


No 20 
>O35854_27_Mito_ref_sig5_130
Probab=1.82  E-value=12  Score=5.53  Aligned_cols=1  Identities=0%  Similarity=-0.329  Sum_probs=0.3

Q 5_Mustela         3 R    3 (6)
Q 2_Macaca          3 R    3 (6)
Q 11_Cavia          3 R    3 (6)
Q Consensus         3 r    3 (6)
                      |
T Consensus        25 ~   25 (32)
T signal           25 G   25 (32)
T 55_Tetrapisisp   25 N   25 (32)
T 51_Micromonas    25 K   25 (32)
T 50_Laccaria      25 I   25 (32)
T 47_Acanthamoeb   25 S   25 (32)
T 46_Dictyosteli   25 S   25 (32)
T 44_Xenopus       25 W   25 (32)
T 42_Alligator     25 I   25 (32)
T 41_Capra         25 R   25 (32)
T 38_Saimiri       25 V   25 (32)
Confidence            3


Done!
