Query         5_Mustela
Match_columns 6
No_of_seqs    1 out of 20
Neff          1.0 
Searched_HMMs 436
Date          Wed Sep  6 16:09:43 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_78A84_78Q84_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P92507_25_IM_ref_sig5_130        0.4      60    0.14    6.0   0.0    2    3-4      14-15  (30)
  2 Q8K2C6_36_Mito_ref_sig5_130      0.4      63    0.14    3.4   0.0    1    2-2      41-41  (41)
  3 P36516_59_Mito_ref_sig5_130      0.4      65    0.15    6.8   0.0    1    3-3       7-7   (64)
  4 P21642_33_Mito_ref_sig5_130      0.4      65    0.15    6.2   0.0    1    5-5      15-15  (38)
  5 Q02253_32_Mito_ref_sig5_130      0.4      66    0.15    6.1   0.0    3    3-5      14-16  (37)
  6 Q05046_32_Mito_ref_sig5_130      0.4      70    0.16    3.6   0.0    1    2-2      37-37  (37)
  7 P26267_25_MM_ref_sig5_130        0.4      71    0.16    5.8   0.0    1    4-4       8-8   (30)
  8 P38077_33_Mito_IM_ref_sig5_130   0.3      90    0.21    5.8   0.0    2    3-4       4-5   (38)
  9 P36531_14_Mito_ref_sig5_130      0.3      95    0.22    3.8   0.0    1    3-3       3-3   (19)
 10 P11325_9_MM_ref_sig5_130         0.3      98    0.23    4.5   0.0    1    2-2      13-13  (14)
 11 P68209_42_Mito_ref_sig5_130      0.3      98    0.23    5.9   0.0    1    4-4       8-8   (47)
 12 Q9FLX7_11_IM_ref_sig5_130        0.3      99    0.23    4.6   0.0    1    4-4       6-6   (16)
 13 P12695_28_MM_ref_sig5_130        0.3   1E+02    0.23    5.4   0.0    1    3-3       6-6   (33)
 14 P49364_30_Mito_ref_sig5_130      0.3   1E+02    0.23    5.5   0.0    3    1-3      21-23  (35)
 15 P28834_11_Mito_ref_sig5_130      0.3   1E+02    0.24    4.6   0.0    4    2-5       8-11  (16)
 16 P26969_86_Mito_ref_sig5_130      0.2 1.1E+02    0.24    6.6   0.0    4    2-5      14-17  (91)
 17 P99028_13_IM_ref_sig5_130        0.2 1.1E+02    0.24    4.7   0.0    3    1-3       5-7   (18)
 18 Q04728_8_MM_ref_sig5_130         0.2 1.1E+02    0.25    4.3   0.0    2    3-4       2-3   (13)
 19 P05165_52_MM_ref_sig5_130        0.2 1.1E+02    0.25    2.7   0.0    1    6-6      57-57  (57)
 20 P07926_68_MitoM_ref_sig5_130     0.2 1.1E+02    0.25    6.2   0.0    4    3-6      68-71  (73)

No 1  
>P92507_25_IM_ref_sig5_130
Probab=0.43  E-value=60  Score=5.99  Aligned_cols=2  Identities=100%  Similarity=1.448  Sum_probs=0.8

Q 5_Mustela         3 RI    4 (6)
Q Consensus         3 ri    4 (6)
                      ||
T Consensus        14 ri   15 (30)
T signal           14 RI   15 (30)
Confidence            33


No 2  
>Q8K2C6_36_Mito_ref_sig5_130
Probab=0.41  E-value=63  Score=3.35  Aligned_cols=1  Identities=0%  Similarity=-0.130  Sum_probs=0.0

Q 5_Mustela         2 K    2 (6)
Q Consensus         2 k    2 (6)
                      .
T Consensus        41 ~   41 (41)
T signal           41 A   41 (41)
T 107_Tribolium    41 G   41 (41)
T 92_Nasonia       41 F   41 (41)
T 132_Schizophyl   41 G   41 (41)
T 145_Zymoseptor   41 R   41 (41)
T 112_Nannochlor   41 W   41 (41)
T 102_Metaseiulu   41 S   41 (41)
T 155_Leishmania   41 S   41 (41)
Confidence            0


No 3  
>P36516_59_Mito_ref_sig5_130
Probab=0.40  E-value=65  Score=6.82  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.3

Q 5_Mustela         3 R    3 (6)
Q Consensus         3 r    3 (6)
                      |
T Consensus         7 r    7 (64)
T signal            7 R    7 (64)
Confidence            2


No 4  
>P21642_33_Mito_ref_sig5_130
Probab=0.40  E-value=65  Score=6.17  Aligned_cols=1  Identities=0%  Similarity=-0.894  Sum_probs=0.3

Q 5_Mustela         5 I    5 (6)
Q Consensus         5 i    5 (6)
                      |
T Consensus        15 ~   15 (38)
T signal           15 E   15 (38)
T cl|CABBABABA|1   15 I   15 (38)
Confidence            3


No 5  
>Q02253_32_Mito_ref_sig5_130
Probab=0.39  E-value=66  Score=6.14  Aligned_cols=3  Identities=67%  Similarity=1.276  Sum_probs=1.2

Q 5_Mustela         3 RII    5 (6)
Q Consensus         3 rii    5 (6)
                      ||+
T Consensus        14 riL   16 (37)
T signal           14 RIL   16 (37)
T 23               14 RIL   16 (37)
T 135              13 RIL   15 (35)
T 138              14 RIL   16 (37)
T 134              11 QML   13 (33)
Confidence            443


No 6  
>Q05046_32_Mito_ref_sig5_130
Probab=0.38  E-value=70  Score=3.60  Aligned_cols=1  Identities=0%  Similarity=-0.562  Sum_probs=0.0

Q 5_Mustela         2 K    2 (6)
Q Consensus         2 k    2 (6)
                      -
T Consensus        37 ~   37 (37)
T signal           37 V   37 (37)
T 119_Torulaspor   37 -   36 (36)
T 56_Guillardia    37 -   36 (36)
T 51_Chondrus      37 -   36 (36)
T 120_Trichoplax   37 -   36 (36)
T 34_Ostreococcu   37 -   36 (36)
Confidence            0


No 7  
>P26267_25_MM_ref_sig5_130
Probab=0.37  E-value=71  Score=5.80  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.4

Q 5_Mustela         4 I    4 (6)
Q Consensus         4 i    4 (6)
                      |
T Consensus         8 i    8 (30)
T signal            8 I    8 (30)
T 14                8 I    8 (33)
T 9                 8 V    8 (33)
Confidence            3


No 8  
>P38077_33_Mito_IM_ref_sig5_130
Probab=0.29  E-value=90  Score=5.76  Aligned_cols=2  Identities=100%  Similarity=1.448  Sum_probs=0.8

Q 5_Mustela         3 RI    4 (6)
Q Consensus         3 ri    4 (6)
                      ||
T Consensus         4 ri    5 (38)
T signal            4 RI    5 (38)
Confidence            34


No 9  
>P36531_14_Mito_ref_sig5_130
Probab=0.28  E-value=95  Score=3.83  Aligned_cols=1  Identities=0%  Similarity=0.899  Sum_probs=0.2

Q 5_Mustela         3 R    3 (6)
Q Consensus         3 r    3 (6)
                      |
T Consensus         3 r    3 (19)
T signal            3 K    3 (19)
T cl|DABBABABA|1    3 S    3 (18)
T cl|GABBABABA|1    3 N    3 (18)
T cl|KABBABABA|1    3 T    3 (18)
T 9_Kazachstania    3 S    3 (18)
T 5_Vanderwaltoz    3 R    3 (18)
T 3_Naumovozyma     3 N    3 (18)
T 18_Scheffersom    3 R    3 (18)
T 16_Meyerozyma     3 R    3 (18)
T 14_Komagataell    3 K    3 (18)
Confidence            2


No 10 
>P11325_9_MM_ref_sig5_130
Probab=0.27  E-value=98  Score=4.49  Aligned_cols=1  Identities=100%  Similarity=1.066  Sum_probs=0.3

Q 5_Mustela         2 K    2 (6)
Q Consensus         2 k    2 (6)
                      |
T Consensus        13 k   13 (14)
T signal           13 K   13 (14)
Confidence            3


No 11 
>P68209_42_Mito_ref_sig5_130
Probab=0.27  E-value=98  Score=5.88  Aligned_cols=1  Identities=0%  Similarity=0.932  Sum_probs=0.3

Q 5_Mustela         4 I    4 (6)
Q Consensus         4 i    4 (6)
                      +
T Consensus         8 l    8 (47)
T signal            8 L    8 (47)
Confidence            3


No 12 
>Q9FLX7_11_IM_ref_sig5_130
Probab=0.27  E-value=99  Score=4.63  Aligned_cols=1  Identities=100%  Similarity=1.331  Sum_probs=0.3

Q 5_Mustela         4 I...    4 (6)
Q Consensus         4 i...    4 (6)
                      |   
T Consensus         6 i...    6 (16)
T signal            6 I...    6 (16)
T 12                6 I...    6 (16)
T 11                6 I...    6 (16)
T 9                 6 L...    6 (16)
T 17                6 Ilsg    9 (24)
Confidence            2   


No 13 
>P12695_28_MM_ref_sig5_130
Probab=0.26  E-value=1e+02  Score=5.42  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.3

Q 5_Mustela         3 R    3 (6)
Q Consensus         3 r    3 (6)
                      |
T Consensus         6 R    6 (33)
T signal            6 R    6 (33)
T 9                 6 R    6 (32)
T 61                6 R    6 (36)
T 72                6 R    6 (34)
T 81                6 R    6 (34)
T 38                6 R    6 (30)
T 84                6 R    6 (30)
T 87                6 R    6 (37)
Confidence            2


No 14 
>P49364_30_Mito_ref_sig5_130
Probab=0.26  E-value=1e+02  Score=5.52  Aligned_cols=3  Identities=67%  Similarity=1.176  Sum_probs=0.0

Q 5_Mustela         1 DKR    3 (6)
Q Consensus         1 dkr    3 (6)
                      ||.
T Consensus        21 dkk   23 (35)
T signal           21 DKK   23 (35)
T 163              21 DKK   23 (35)
T 164              20 DKK   22 (34)
T 159              21 DKK   23 (35)


No 15 
>P28834_11_Mito_ref_sig5_130
Probab=0.26  E-value=1e+02  Score=4.58  Aligned_cols=4  Identities=50%  Similarity=0.841  Sum_probs=0.0

Q 5_Mustela         2 KRII    5 (6)
Q Consensus         2 krii    5 (6)
                      ||-+
T Consensus         8 krtl   11 (16)
T signal            8 KRTL   11 (16)


No 16 
>P26969_86_Mito_ref_sig5_130
Probab=0.25  E-value=1.1e+02  Score=6.59  Aligned_cols=4  Identities=50%  Similarity=1.124  Sum_probs=0.0

Q 5_Mustela         2 KRII    5 (6)
Q Consensus         2 krii    5 (6)
                      ||++
T Consensus        14 kRLv   17 (91)
T signal           14 KRLL   17 (91)
T 198              14 KRLL   17 (64)
T 196              14 RRLV   17 (92)
T 193              14 KRLV   17 (93)
T 195              14 KRLV   17 (83)
T 194              14 RRLV   17 (77)
T 191              14 KRLV   17 (69)
T 188              14 KRLV   17 (73)
T 189              15 RRLV   18 (79)
T 185              14 RRLL   17 (43)


No 17 
>P99028_13_IM_ref_sig5_130
Probab=0.25  E-value=1.1e+02  Score=4.66  Aligned_cols=3  Identities=67%  Similarity=1.177  Sum_probs=0.0

Q 5_Mustela         1 DKR    3 (6)
Q Consensus         1 dkr    3 (6)
                      |++
T Consensus         5 de~    7 (18)
T signal            5 DER    7 (18)
T 23                5 DEQ    7 (18)
T 42                5 DER    7 (18)
T 3                 5 DEQ    7 (18)
T 14                5 DER    7 (18)
T 24                5 DKL    7 (18)
T 59                5 EEK    7 (17)
T 62                5 EEQ    7 (18)


No 18 
>Q04728_8_MM_ref_sig5_130
Probab=0.25  E-value=1.1e+02  Score=4.28  Aligned_cols=2  Identities=100%  Similarity=1.448  Sum_probs=0.0

Q 5_Mustela         3 RI    4 (6)
Q Consensus         3 ri    4 (6)
                      +|
T Consensus         2 ki    3 (13)
T signal            2 RI    3 (13)
T 90                2 KI    3 (13)
T 88                2 KI    3 (13)


No 19 
>P05165_52_MM_ref_sig5_130
Probab=0.24  E-value=1.1e+02  Score=2.67  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.0

Q 5_Mustela         6 D    6 (6)
Q Consensus         6 d    6 (6)
                      |
T Consensus        57 ~   57 (57)
T signal           57 D   57 (57)
T 91_Xenopus       55 -   54 (54)
T 88_Taeniopygia   55 -   54 (54)
T 68_Strongyloce   55 -   54 (54)
T 59_Acanthamoeb   55 -   54 (54)
T 52_Trichoplax    55 -   54 (54)
T 42_Condylura     55 -   54 (54)
T 6_Columba        55 -   54 (54)
T 1_Mustela        55 -   54 (54)


No 20 
>P07926_68_MitoM_ref_sig5_130
Probab=0.24  E-value=1.1e+02  Score=6.17  Aligned_cols=4  Identities=75%  Similarity=0.800  Sum_probs=0.0

Q 5_Mustela         3 RIID    6 (6)
Q Consensus         3 riid    6 (6)
                      |=||
T Consensus        68 RDID   71 (73)
T signal           68 RDID   71 (73)
T 10               77 RDID   80 (82)
T 14               56 ----   55 (55)
T 22               38 ----   37 (37)
T 9                62 CDID   65 (67)
T 5                69 RDID   72 (74)
T 6                63 RDID   66 (68)
T 2                79 RDID   82 (84)
T 3                79 RDVE   82 (84)
T 53               35 KDID   38 (40)


Done!
