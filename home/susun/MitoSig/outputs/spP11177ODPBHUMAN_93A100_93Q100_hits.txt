Query         5_Mustela
Match_columns 7
No_of_seqs    1 out of 20
Neff          1.0 
Searched_HMMs 436
Date          Wed Sep  6 16:10:06 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_93A100_93Q100_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 Q01992_33_MM_ref_sig5_130        0.9      27   0.062    7.5   0.0    1    3-3      38-38  (38)
  2 P20069_32_MM_ref_sig5_130        0.9      27   0.062    7.4   0.0    1    4-4       5-5   (37)
  3 P14408_41_Mito_Cy_ref_sig5_130   0.9      28   0.063    6.0   0.0    1    3-3      46-46  (46)
  4 Q02253_32_Mito_ref_sig5_130      0.8      32   0.073    7.3   0.0    1    4-4       5-5   (37)
  5 P35571_42_Mito_ref_sig5_130      0.7      35    0.08    5.2   0.0    1    3-3      47-47  (47)
  6 Q25423_17_IM_ref_sig5_130        0.6      40   0.091    6.3   0.0    1    3-3      22-22  (22)
  7 Q49AM1_35_Mito_MM_Nu_ref_sig5_   0.6      42   0.097    4.7   0.0    1    3-3      40-40  (40)
  8 P05626_35_Mito_IM_ref_sig5_130   0.6      43   0.099    7.0   0.0    1    5-5      32-32  (40)
  9 P36516_59_Mito_ref_sig5_130      0.6      45     0.1    7.6   0.0    1    3-3      10-10  (64)
 10 P05165_52_MM_ref_sig5_130        0.6      46     0.1    3.7   0.0    1    1-1      11-11  (57)
 11 P35914_27_MM_Pe_ref_sig5_130     0.5      52    0.12    6.4   0.0    1    5-5      14-14  (32)
 12 P30084_27_MM_ref_sig5_130        0.5      52    0.12    6.4   0.0    1    5-5      30-30  (32)
 13 P36528_19_Mito_ref_sig5_130      0.5      54    0.12    6.0   0.0    1    2-2      11-11  (24)
 14 P09671_24_MM_ref_sig5_130        0.5      55    0.13    6.0   0.0    1    5-5      18-18  (29)
 15 P17783_27_MM_ref_sig5_130        0.5      57    0.13    6.3   0.0    1    3-3      32-32  (32)
 16 P14604_29_MM_ref_sig5_130        0.4      59    0.13    6.3   0.0    1    5-5      30-30  (34)
 17 P28350_44_Mito_Cy_ref_sig5_130   0.4      60    0.14    6.8   0.0    1    3-3      49-49  (49)
 18 P05166_28_MM_ref_sig5_130        0.4      60    0.14    6.2   0.0    1    3-3      33-33  (33)
 19 P12787_37_IM_ref_sig5_130        0.4      62    0.14    6.5   0.0    1    5-5       3-3   (42)
 20 P30099_34_MitoM_ref_sig5_130     0.4      62    0.14    4.8   0.0    1    1-1       1-1   (39)

No 1  
>Q01992_33_MM_ref_sig5_130
Probab=0.89  E-value=27  Score=7.53  Aligned_cols=1  Identities=0%  Similarity=-1.193  Sum_probs=0.0

Q 5_Mustela         3 A    3 (7)
Q Consensus         3 a    3 (7)
                      -
T Consensus        38 W   38 (38)
T signal           38 W   38 (38)
T 180              38 W   38 (38)
T 130              37 W   37 (37)
T 102              36 W   36 (36)
T 165              36 W   36 (36)
T 168              36 W   36 (36)
T 182              36 W   36 (36)
T 186              36 W   36 (36)
T 164              36 W   36 (36)
T 169              36 W   36 (36)
Confidence            0


No 2  
>P20069_32_MM_ref_sig5_130
Probab=0.89  E-value=27  Score=7.44  Aligned_cols=1  Identities=100%  Similarity=1.132  Sum_probs=0.2

Q 5_Mustela         4 V    4 (7)
Q Consensus         4 v    4 (7)
                      |
T Consensus         5 v    5 (37)
T signal            5 V    5 (37)
T 143               5 V    5 (37)
T 58                5 V    5 (37)
T 118               5 V    5 (40)
T 119               5 T    5 (38)
T 123               5 A    5 (37)
T 127               5 Q    5 (38)
T 129               5 V    5 (38)
T 134               5 V    5 (37)
T 121               1 -    0 (27)
Confidence            2


No 3  
>P14408_41_Mito_Cy_ref_sig5_130
Probab=0.87  E-value=28  Score=6.03  Aligned_cols=1  Identities=0%  Similarity=0.368  Sum_probs=0.0

Q 5_Mustela         3 A    3 (7)
Q Consensus         3 a    3 (7)
                      .
T Consensus        46 ~   46 (46)
T signal           46 S   46 (46)
T 132_Malassezia   26 -   25 (25)
T 76_Musca         26 -   25 (25)
T 49_Anas          26 -   25 (25)
T cl|CABBABABA|1   26 -   25 (25)
Confidence            0


No 4  
>Q02253_32_Mito_ref_sig5_130
Probab=0.77  E-value=32  Score=7.30  Aligned_cols=1  Identities=100%  Similarity=1.132  Sum_probs=0.2

Q 5_Mustela         4 V    4 (7)
Q Consensus         4 v    4 (7)
                      |
T Consensus         5 V    5 (37)
T signal            5 V    5 (37)
T 23                5 V    5 (37)
T 135               4 V    4 (35)
T 138               5 V    5 (37)
T 134               2 V    2 (33)
Confidence            2


No 5  
>P35571_42_Mito_ref_sig5_130
Probab=0.71  E-value=35  Score=5.15  Aligned_cols=1  Identities=0%  Similarity=0.368  Sum_probs=0.0

Q 5_Mustela         3 A    3 (7)
Q Consensus         3 a    3 (7)
                      .
T Consensus        47 ~   47 (47)
T signal           47 S   47 (47)
T 85_Nematostell   47 -   46 (46)
T 97_Amphimedon    47 -   46 (46)
T 64_Oreochromis   47 -   46 (46)
T 74_Pongo         47 -   46 (46)
T 158_Laccaria     47 -   46 (46)
T 73_Ciona         47 -   46 (46)
T 81_Drosophila    47 -   46 (46)
T 112_Aplysia      47 -   46 (46)
T 58_Columba       47 -   46 (46)
Confidence            0


No 6  
>Q25423_17_IM_ref_sig5_130
Probab=0.63  E-value=40  Score=6.31  Aligned_cols=1  Identities=0%  Similarity=0.169  Sum_probs=0.0

Q 5_Mustela         3 A    3 (7)
Q Consensus         3 a    3 (7)
                      .
T Consensus        22 g   22 (22)
T signal           22 G   22 (22)
T cl|CABBABABA|1   25 G   25 (25)
Confidence            0


No 7  
>Q49AM1_35_Mito_MM_Nu_ref_sig5_130
Probab=0.59  E-value=42  Score=4.67  Aligned_cols=1  Identities=0%  Similarity=-0.064  Sum_probs=0.0

Q 5_Mustela         3 A    3 (7)
Q Consensus         3 a    3 (7)
                      .
T Consensus        40 ~   40 (40)
T signal           40 Q   40 (40)
T 69_Xiphophorus   40 A   40 (40)
T 50_Columba       40 R   40 (40)
T 48_Anas          40 S   40 (40)
T 45_Echinops      40 E   40 (40)
T 40_Cavia         40 A   40 (40)
T 38_Cricetulus    40 C   40 (40)
T 30_Dasypus       40 F   40 (40)
T cl|PIBBABABA|9   40 -   39 (39)
Confidence            0


No 8  
>P05626_35_Mito_IM_ref_sig5_130
Probab=0.58  E-value=43  Score=6.99  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.3

Q 5_Mustela         5 G    5 (7)
Q Consensus         5 g    5 (7)
                      |
T Consensus        32 g   32 (40)
T signal           32 G   32 (40)
T 47               28 N   28 (34)
T 43               26 G   26 (32)
T 45               26 G   26 (32)
T 48               26 G   26 (32)
T 49               26 G   26 (32)
Confidence            2


No 9  
>P36516_59_Mito_ref_sig5_130
Probab=0.57  E-value=45  Score=7.59  Aligned_cols=1  Identities=100%  Similarity=0.800  Sum_probs=0.3

Q 5_Mustela         3 A    3 (7)
Q Consensus         3 a    3 (7)
                      |
T Consensus        10 a   10 (64)
T signal           10 A   10 (64)
Confidence            3


No 10 
>P05165_52_MM_ref_sig5_130
Probab=0.55  E-value=46  Score=3.66  Aligned_cols=1  Identities=0%  Similarity=-1.459  Sum_probs=0.0

Q 5_Mustela         1 G    1 (7)
Q Consensus         1 g    1 (7)
                      .
T Consensus        11 ~   11 (57)
T signal           11 L   11 (57)
T 91_Xenopus       11 Q   11 (54)
T 88_Taeniopygia   11 S   11 (54)
T 68_Strongyloce   11 S   11 (54)
T 59_Acanthamoeb   11 A   11 (54)
T 52_Trichoplax    11 L   11 (54)
T 42_Condylura     11 P   11 (54)
T 6_Columba        11 D   11 (54)
T 1_Mustela        11 L   11 (54)
Confidence            0


No 11 
>P35914_27_MM_Pe_ref_sig5_130
Probab=0.49  E-value=52  Score=6.42  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.2

Q 5_Mustela         5 G    5 (7)
Q Consensus         5 g    5 (7)
                      |
T Consensus        14 g   14 (32)
T signal           14 G   14 (32)
T 49               15 G   15 (33)
T 68               14 G   14 (32)
T 51               14 -   13 (26)
T 52               13 -   12 (25)
T 75               14 G   14 (32)
T 76               14 G   14 (32)
T 77               14 G   14 (32)
T 78               14 G   14 (32)
T 59               14 -   13 (29)
Confidence            2


No 12 
>P30084_27_MM_ref_sig5_130
Probab=0.49  E-value=52  Score=6.37  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.4

Q 5_Mustela         5 G    5 (7)
Q Consensus         5 g    5 (7)
                      |
T Consensus        30 G   30 (32)
T signal           30 G   30 (32)
T 128              30 G   30 (32)
T 131              30 S   30 (32)
T 140              30 G   30 (32)
T 136              30 G   30 (31)
T 135              27 G   27 (29)
T 120              30 G   30 (32)
Confidence            3


No 13 
>P36528_19_Mito_ref_sig5_130
Probab=0.48  E-value=54  Score=6.05  Aligned_cols=1  Identities=0%  Similarity=0.932  Sum_probs=0.2

Q 5_Mustela         2 I    2 (7)
Q Consensus         2 i    2 (7)
                      +
T Consensus        11 l   11 (24)
T signal           11 L   11 (24)
Confidence            2


No 14 
>P09671_24_MM_ref_sig5_130
Probab=0.47  E-value=55  Score=6.00  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.2

Q 5_Mustela         5 G..    5 (7)
Q Consensus         5 g..    5 (7)
                      |  
T Consensus        18 g..   18 (29)
T signal           18 G..   18 (29)
T 128              20 Swk   22 (33)
T 134              20 G..   20 (31)
T 138              20 T..   20 (31)
T 124              19 A..   19 (30)
T 145              18 G..   18 (29)
T 149              20 A..   20 (31)
T 156              18 G..   18 (29)
T 113              19 A..   19 (30)
T 114              18 G..   18 (29)
Confidence            2  


No 15 
>P17783_27_MM_ref_sig5_130
Probab=0.45  E-value=57  Score=6.31  Aligned_cols=1  Identities=0%  Similarity=0.036  Sum_probs=0.0

Q 5_Mustela         3 A    3 (7)
Q Consensus         3 a    3 (7)
                      .
T Consensus        32 v   32 (32)
T signal           32 V   32 (32)
T 120              35 K   35 (35)
T 121              27 V   27 (27)
T 123              27 V   27 (27)
T 118              27 A   27 (27)
T 122              30 V   30 (30)
T 125              31 V   31 (31)
Confidence            0


No 16 
>P14604_29_MM_ref_sig5_130
Probab=0.44  E-value=59  Score=6.29  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.4

Q 5_Mustela         5 G    5 (7)
Q Consensus         5 g    5 (7)
                      |
T Consensus        30 G   30 (34)
T signal           30 G   30 (34)
T 122              30 G   30 (34)
T 133              30 G   30 (34)
T 142              30 S   30 (34)
T 145              30 G   30 (34)
T 146              30 G   30 (34)
T 152              27 G   27 (31)
T 139              30 G   30 (34)
T 129              30 G   30 (34)
T 120              30 G   30 (34)
Confidence            3


No 17 
>P28350_44_Mito_Cy_ref_sig5_130
Probab=0.43  E-value=60  Score=6.83  Aligned_cols=1  Identities=0%  Similarity=-0.064  Sum_probs=0.0

Q 5_Mustela         3 A    3 (7)
Q Consensus         3 a    3 (7)
                      -
T Consensus        49 q   49 (49)
T signal           49 Q   49 (49)
Confidence            0


No 18 
>P05166_28_MM_ref_sig5_130
Probab=0.43  E-value=60  Score=6.25  Aligned_cols=1  Identities=0%  Similarity=0.202  Sum_probs=0.0

Q 5_Mustela         3 A    3 (7)
Q Consensus         3 a    3 (7)
                      .
T Consensus        33 v   33 (33)
T signal           33 T   33 (33)
T 86               33 -   32 (32)
T 49               34 A   34 (34)
T 62               36 V   36 (36)
T 64               33 V   33 (33)
T 67               35 V   35 (35)
T 70               39 V   39 (39)
T 74               39 V   39 (39)
T 89               33 V   33 (33)
T 65               38 -   37 (37)
Confidence            0


No 19 
>P12787_37_IM_ref_sig5_130
Probab=0.42  E-value=62  Score=6.52  Aligned_cols=1  Identities=0%  Similarity=0.169  Sum_probs=0.2

Q 5_Mustela         5 G    5 (7)
Q Consensus         5 g    5 (7)
                      |
T Consensus         3 g    3 (42)
T signal            3 A    3 (42)
T 122               3 G    3 (46)
T 99                3 G    3 (44)
T 100               3 G    3 (43)
T 101               3 A    3 (41)
T 103               3 A    3 (48)
T 108               1 -    0 (36)
T 129               3 G    3 (46)
T 130               3 G    3 (39)
T 78                1 -    0 (29)
Confidence            2


No 20 
>P30099_34_MitoM_ref_sig5_130
Probab=0.42  E-value=62  Score=4.76  Aligned_cols=1  Identities=0%  Similarity=-1.161  Sum_probs=0.0

Q 5_Mustela         1 G    1 (7)
Q Consensus         1 g    1 (7)
                      -
T Consensus         1 ~    1 (39)
T signal            1 M    1 (39)
T 46_Amphimedon     1 -    0 (28)
T 44_Anolis         1 -    0 (28)
T 42_Monodelphis    1 -    0 (28)
T 39_Sarcophilus    1 -    0 (28)
T 35_Mustela        1 -    0 (28)
T 7_Heterocephal    1 -    0 (28)
T cl|DABBABABA|1    1 -    0 (28)
T cl|RABBABABA|1    1 -    0 (28)
T cl|VABBABABA|2    1 -    0 (28)
Confidence            0


Done!
