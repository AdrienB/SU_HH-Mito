Query         5_Mustela
Match_columns 7
No_of_seqs    1 out of 20
Neff          1.0 
Searched_HMMs 436
Date          Wed Sep  6 16:10:14 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_98A105_98Q105_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P26360_45_Mito_IM_ref_sig5_130  32.5    0.21 0.00047   14.1   0.0    6    2-7       2-7   (50)
  2 Q07021_73_MM_Nu_Cm_Cy_ref_sig5  16.0     0.7  0.0016   13.3   0.0    6    2-7      15-20  (78)
  3 Q02253_32_Mito_ref_sig5_130     12.6       1  0.0023   11.5   0.0    6    2-7       7-12  (37)
  4 P30038_24_MM_ref_sig5_130        6.1     2.7  0.0061    9.8   0.0    4    4-7      20-23  (29)
  5 P23965_28_MM_ref_sig5_130        5.9     2.8  0.0064    9.9   0.0    1    5-5       6-6   (33)
  6 Q29551_39_Mito_ref_sig5_130      5.7     2.9  0.0066   10.3   0.0    1    4-4       2-2   (44)
  7 Q9HCC0_22_MM_ref_sig5_130        5.4     3.1  0.0071    9.6   0.0    2    5-6      19-20  (27)
  8 P31023_31_MM_ref_sig5_130        4.6     3.8  0.0088    9.8   0.0    4    2-5       2-5   (36)
  9 P34942_23_MM_ref_sig5_130        4.4       4  0.0092    5.7   0.0    1    3-3       1-1   (28)
 10 Q6CE48_34_IM_ref_sig5_130        4.4     4.1  0.0093    9.9   0.0    3    4-6       2-4   (39)
 11 P55809_39_MM_ref_sig5_130        4.3     4.1  0.0095    9.9   0.0    1    4-4       2-2   (44)
 12 Q96E29_68_Mito_ref_sig5_130      4.3     4.2  0.0096    6.6   0.0    1    3-3       1-1   (73)
 13 P41563_27_Mito_ref_sig5_130      4.2     4.2  0.0097    9.2   0.0    3    3-5       1-3   (32)
 14 P22778_45_Mito_IM_ref_sig5_130   4.2     4.3  0.0099   10.2   0.0    4    2-5       2-5   (50)
 15 P17694_33_IM_ref_sig5_130        4.1     4.4    0.01    5.5   0.0    1    3-3       1-1   (38)
 16 P22068_44_Mito_IM_ref_sig5_130   4.1     4.4    0.01   10.2   0.0    6    2-7       6-11  (49)
 17 P11024_43_IM_ref_sig5_130        3.8     4.8   0.011    5.4   0.0    1    1-1       1-1   (48)
 18 P11177_30_MM_ref_sig5_130        3.6     5.2   0.012    9.3   0.0    1    3-3       4-4   (35)
 19 P41367_25_MM_ref_sig5_130        3.4     5.6   0.013    5.3   0.0    1    3-3       1-1   (30)
 20 P08503_25_MM_ref_sig5_130        3.4     5.6   0.013    5.3   0.0    1    3-3       1-1   (30)

No 1  
>P26360_45_Mito_IM_ref_sig5_130
Probab=32.46  E-value=0.21  Score=14.05  Aligned_cols=6  Identities=83%  Similarity=1.016  Sum_probs=3.7

Q 5_Mustela         2 AM.AGLR    7 (7)
Q Consensus         2 am.aglr    7 (7)
                      || |.||
T Consensus         2 Am.aalR    7 (50)
T signal            2 AM.AALR    7 (50)
T 181               2 AM.AALR    7 (45)
T 183               2 AM.AALR    7 (48)
T 170               2 AM.AALR    7 (51)
T 171               2 AM.AALR    7 (49)
T 182               2 AM.AALR    7 (50)
T 174               2 AM.AVFR    7 (47)
T 178               2 AMaAALR    8 (44)
T 177               2 AMaAALR    8 (50)
T 168               2 AS.RSAR    7 (52)
Confidence            56 6665


No 2  
>Q07021_73_MM_Nu_Cm_Cy_ref_sig5_130
Probab=15.97  E-value=0.7  Score=13.27  Aligned_cols=6  Identities=67%  Similarity=1.132  Sum_probs=3.0

Q 5_Mustela         2 AMAGLR    7 (7)
Q Consensus         2 amaglr    7 (7)
                      |.+|||
T Consensus        15 A~~~lr   20 (78)
T signal           15 SVAGLR   20 (78)
T 52                8 AAAALR   13 (78)
T 36                1 ------    0 (31)
T 26               11 AVR-VS   15 (85)
T 48               12 AAR-LS   16 (80)
T 30                9 ATR---   11 (68)
T 19                1 ------    0 (49)
T 16                1 ------    0 (49)
T 21                1 ------    0 (45)
T 13                1 ------    0 (47)
Confidence            445554


No 3  
>Q02253_32_Mito_ref_sig5_130
Probab=12.59  E-value=1  Score=11.55  Aligned_cols=6  Identities=50%  Similarity=0.617  Sum_probs=2.6

Q 5_Mustela         2 AMAGLR    7 (7)
Q Consensus         2 amaglr    7 (7)
                      |.|++|
T Consensus         7 AAA~~R   12 (37)
T signal            7 AAAAVR   12 (37)
T 23                7 AAAAMR   12 (37)
T 135               6 AAAVVR   11 (35)
T 138               7 AAAAVR   12 (37)
T 134               4 AAAGLR    9 (33)
Confidence            344444


No 4  
>P30038_24_MM_ref_sig5_130
Probab=6.09  E-value=2.7  Score=9.80  Aligned_cols=4  Identities=100%  Similarity=1.473  Sum_probs=1.8

Q 5_Mustela         4 AGLR    7 (7)
Q Consensus         4 aglr    7 (7)
                      ||||
T Consensus        20 agL~   23 (29)
T signal           20 AGLR   23 (29)
T 77               20 AGLC   23 (29)
T 102              19 --LC   20 (26)
T 126              20 AGLC   23 (29)
T 127              21 TGLQ   24 (30)
T 128              20 AGLP   23 (29)
T 129              20 ARLW   23 (29)
T 141              18 AGLR   21 (27)
T 145              19 SGLR   22 (28)
T 121              20 AGLR   23 (29)
Confidence            4443


No 5  
>P23965_28_MM_ref_sig5_130
Probab=5.88  E-value=2.8  Score=9.95  Aligned_cols=1  Identities=0%  Similarity=0.169  Sum_probs=0.4

Q 5_Mustela         5 G    5 (7)
Q Consensus         5 g    5 (7)
                      |
T Consensus         6 g    6 (33)
T signal            6 A    6 (33)
T 94                6 G    6 (43)
T 93                6 R    6 (46)
T 72                6 G    6 (49)
T 79                6 G    6 (46)
T 63                6 A    6 (34)
T 78                6 G    6 (37)
T 39                6 G    6 (44)
Confidence            3


No 6  
>Q29551_39_Mito_ref_sig5_130
Probab=5.73  E-value=2.9  Score=10.31  Aligned_cols=1  Identities=100%  Similarity=0.800  Sum_probs=0.4

Q 5_Mustela         4 A    4 (7)
Q Consensus         4 a    4 (7)
                      |
T Consensus         2 A    2 (44)
T signal            2 A    2 (44)
T 113               2 A    2 (44)
T 82                2 A    2 (44)
T 3                 2 A    2 (44)
T 102               2 A    2 (44)
T 98                2 A    2 (44)
T 108               1 -    0 (40)
T 79                1 -    0 (27)
T 112               1 -    0 (29)
T 95                2 A    2 (51)
Confidence            3


No 7  
>Q9HCC0_22_MM_ref_sig5_130
Probab=5.42  E-value=3.1  Score=9.55  Aligned_cols=2  Identities=50%  Similarity=0.717  Sum_probs=0.7

Q 5_Mustela         5 GL    6 (7)
Q Consensus         5 gl    6 (7)
                      |+
T Consensus        19 ~~   20 (27)
T signal           19 GP   20 (27)
T 6                19 TL   20 (27)
T 23               19 RS   20 (27)
T 30               19 GL   20 (27)
T 36               19 GL   20 (27)
T 44               19 GL   20 (27)
T 50               19 RS   20 (27)
T 57               19 RP   20 (27)
T 63               19 RQ   20 (27)
T 74               19 GL   20 (27)
Confidence            33


No 8  
>P31023_31_MM_ref_sig5_130
Probab=4.58  E-value=3.8  Score=9.82  Aligned_cols=4  Identities=75%  Similarity=0.792  Sum_probs=2.0

Q 5_Mustela         2 AMAG    5 (7)
Q Consensus         2 amag    5 (7)
                      |||.
T Consensus         2 amas    5 (36)
T signal            2 AMAN    5 (36)
T 194               2 GLAN    5 (38)
T 185               2 AMA-    4 (37)
T 186               2 AMAS    5 (46)
T 187               2 AIGS    5 (35)
T 188               2 AMAS    5 (45)
T 190               2 AMAS    5 (45)
T 193               2 AMAN    5 (35)
T 189               2 AMAS    5 (42)
T 191               2 AMAS    5 (41)
Confidence            4554


No 9  
>P34942_23_MM_ref_sig5_130
Probab=4.42  E-value=4  Score=5.67  Aligned_cols=1  Identities=100%  Similarity=1.431  Sum_probs=0.2

Q 5_Mustela         3 M    3 (7)
Q Consensus         3 m    3 (7)
                      |
T Consensus         1 M    1 (28)
T signal            1 M    1 (28)
T 78_Musca          1 M    1 (39)
T 74_Strongyloce    1 M    1 (39)
T 62_Alligator      1 M    1 (39)
T 60_Xenopus        1 M    1 (39)
T 56_Anas           1 M    1 (39)
T 52_Meleagris      1 M    1 (39)
T 40_Saimiri        1 M    1 (39)
Confidence            2


No 10 
>Q6CE48_34_IM_ref_sig5_130
Probab=4.39  E-value=4.1  Score=9.93  Aligned_cols=3  Identities=33%  Similarity=0.888  Sum_probs=1.2

Q 5_Mustela         4 AGL    6 (7)
Q Consensus         4 agl    6 (7)
                      -|+
T Consensus         2 rgf    4 (39)
T signal            2 RGF    4 (39)
Confidence            343


No 11 
>P55809_39_MM_ref_sig5_130
Probab=4.32  E-value=4.1  Score=9.94  Aligned_cols=1  Identities=100%  Similarity=0.800  Sum_probs=0.3

Q 5_Mustela         4 A    4 (7)
Q Consensus         4 a    4 (7)
                      |
T Consensus         2 A    2 (44)
T signal            2 A    2 (44)
T 109               2 A    2 (44)
T 123               2 A    2 (44)
T 120               2 Q    2 (44)
T 5                 2 A    2 (44)
T 100               2 A    2 (44)
T 97                2 A    2 (44)
T 105               1 -    0 (40)
T 112               1 -    0 (29)
T 79                1 -    0 (27)
Confidence            3


No 12 
>Q96E29_68_Mito_ref_sig5_130
Probab=4.27  E-value=4.2  Score=6.58  Aligned_cols=1  Identities=100%  Similarity=1.431  Sum_probs=0.2

Q 5_Mustela         3 M    3 (7)
Q Consensus         3 m    3 (7)
                      |
T Consensus         1 M    1 (73)
T signal            1 M    1 (73)
T 93_Musca          1 M    1 (72)
T 107_Ciona         1 M    1 (72)
T 58_Taeniopygia    1 M    1 (72)
T 112_Loa           1 M    1 (72)
T 77_Takifugu       1 M    1 (72)
T 85_Acyrthosiph    1 M    1 (72)
Confidence            2


No 13 
>P41563_27_Mito_ref_sig5_130
Probab=4.23  E-value=4.2  Score=9.25  Aligned_cols=3  Identities=100%  Similarity=1.475  Sum_probs=1.2

Q 5_Mustela         3 MAG    5 (7)
Q Consensus         3 mag    5 (7)
                      |||
T Consensus         1 mAg    3 (32)
T signal            1 MAG    3 (32)
T 20                1 MAG    3 (30)
T 42                1 AAG    3 (32)
T 45                1 MAT    3 (31)
T 46                1 MA-    2 (30)
T 61                1 MAG    3 (31)
T 76                1 AAG    3 (30)
T 84                1 MAG    3 (31)
T 94                1 MAG    3 (31)
T 114               1 MAA    3 (31)
Confidence            343


No 14 
>P22778_45_Mito_IM_ref_sig5_130
Probab=4.16  E-value=4.3  Score=10.16  Aligned_cols=4  Identities=75%  Similarity=1.157  Sum_probs=1.9

Q 5_Mustela         2 AMAG    5 (7)
Q Consensus         2 amag    5 (7)
                      ||+|
T Consensus         2 A~ag    5 (50)
T signal            2 AMTG    5 (50)
T 194               2 ALCG    5 (53)
T 196               2 AMAG    5 (51)
T 197               2 AMAG    5 (45)
T 185               2 AMAG    5 (51)
T 191               2 AFAN    5 (49)
T 192               2 ALCN    5 (47)
T 195               2 ALYG    5 (47)
T 189               2 ALAN    5 (43)
T 193               1 --SS    2 (50)
Confidence            4544


No 15 
>P17694_33_IM_ref_sig5_130
Probab=4.13  E-value=4.4  Score=5.50  Aligned_cols=1  Identities=100%  Similarity=1.431  Sum_probs=0.2

Q 5_Mustela         3 M    3 (7)
Q Consensus         3 m    3 (7)
                      |
T Consensus         1 M    1 (38)
T signal            1 M    1 (38)
T 203_Boea          1 M    1 (37)
T 189_Naegleria     1 M    1 (37)
T 167_Loa           1 M    1 (37)
T 125_Talaromyce    1 M    1 (37)
T 84_Andalucia      1 M    1 (37)
Confidence            1


No 16 
>P22068_44_Mito_IM_ref_sig5_130
Probab=4.08  E-value=4.4  Score=10.21  Aligned_cols=6  Identities=50%  Similarity=1.132  Sum_probs=2.7

Q 5_Mustela         2 AMAGLR    7 (7)
Q Consensus         2 amaglr    7 (7)
                      |..|+|
T Consensus         6 alsgir   11 (49)
T signal            6 ALSGIR   11 (49)
Confidence            344543


No 17 
>P11024_43_IM_ref_sig5_130
Probab=3.80  E-value=4.8  Score=5.37  Aligned_cols=1  Identities=0%  Similarity=-0.230  Sum_probs=0.0

Q 5_Mustela         1 A    1 (7)
Q Consensus         1 a    1 (7)
                      -
T Consensus         1 M    1 (48)
T signal            1 M    1 (48)
T 144_Moniliopht    1 M    1 (47)
T 141_Cryptospor    1 M    1 (47)
T 108_Ichthyopht    1 M    1 (47)
T 99_Aspergillus    1 M    1 (47)
T 97_Volvox         1 M    1 (47)
T 96_Schizophyll    1 M    1 (47)
Confidence            0


No 18 
>P11177_30_MM_ref_sig5_130
Probab=3.61  E-value=5.2  Score=9.30  Aligned_cols=1  Identities=0%  Similarity=0.534  Sum_probs=0.3

Q 5_Mustela         3 M    3 (7)
Q Consensus         3 m    3 (7)
                      +
T Consensus         4 l    4 (35)
T signal            4 V    4 (35)
T 143               8 L    8 (39)
T 144               4 L    4 (35)
T 145               4 M    4 (35)
T 147               4 L    4 (35)
T 135               4 A    4 (44)
T 141               4 A    4 (49)
T 142               3 L    3 (34)
T 149               3 L    3 (34)
T 150               3 L    3 (34)
Confidence            2


No 19 
>P41367_25_MM_ref_sig5_130
Probab=3.40  E-value=5.6  Score=5.30  Aligned_cols=1  Identities=100%  Similarity=1.431  Sum_probs=0.2

Q 5_Mustela         3 M    3 (7)
Q Consensus         3 m    3 (7)
                      |
T Consensus         1 M    1 (30)
T signal            1 M    1 (30)
T 102_Strongyloc    1 M    1 (28)
T 100_Leishmania    1 M    1 (28)
T 89_Caenorhabdi    1 M    1 (28)
T 88_Haplochromi    1 M    1 (28)
T 79_Cavia          1 M    1 (28)
T 76_Tribolium      1 M    1 (28)
T 71_Pediculus      1 M    1 (28)
T 64_Sarcophilus    1 T    1 (28)
T 41_Ficedula       1 M    1 (28)
Confidence            1


No 20 
>P08503_25_MM_ref_sig5_130
Probab=3.36  E-value=5.6  Score=5.34  Aligned_cols=1  Identities=100%  Similarity=1.431  Sum_probs=0.2

Q 5_Mustela         3 M    3 (7)
Q Consensus         3 m    3 (7)
                      |
T Consensus         1 M    1 (30)
T signal            1 M    1 (30)
T 96_Nematostell    1 M    1 (28)
T 51_Xenopus        1 M    1 (28)
T 47_Ornithorhyn    1 M    1 (28)
T 46_Geospiza       1 M    1 (28)
Confidence            1


Done!
