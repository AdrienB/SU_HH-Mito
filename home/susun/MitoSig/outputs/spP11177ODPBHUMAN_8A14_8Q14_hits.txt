Query         5_Mustela
Match_columns 6
No_of_seqs    7 out of 20
Neff          1.6 
Searched_HMMs 436
Date          Wed Sep  6 16:07:57 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_8A14_8Q14_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P13183_24_IM_ref_sig5_130        2.8       7   0.016    8.3   0.0    1    4-4      17-17  (29)
  2 P05630_22_Mito_IM_ref_sig5_130   2.7     7.3   0.017    8.2   0.0    3    1-3       8-10  (27)
  3 P23786_25_IM_ref_sig5_130        2.7     7.3   0.017    8.4   0.0    3    2-4      23-25  (30)
  4 P09440_34_Mito_ref_sig5_130      2.6     7.7   0.018    8.7   0.0    2    4-5      14-15  (39)
  5 Q01205_68_Mito_ref_sig5_130      2.5     8.2   0.019    9.5   0.0    1    2-2      29-29  (73)
  6 P07806_47_Cy_Mito_ref_sig5_130   2.3     8.7    0.02    9.0   0.0    1    2-2      23-23  (52)
  7 P54898_44_Mito_ref_sig5_130      2.3     8.8    0.02    8.9   0.0    3    2-4      35-37  (49)
  8 P0CS90_23_MM_Nu_ref_sig5_130     2.3     9.1   0.021    7.8   0.0    2    3-4      16-17  (28)
  9 P09624_21_MM_ref_sig5_130        2.2     9.3   0.021    7.7   0.0    3    2-4      12-14  (26)
 10 P07926_68_MitoM_ref_sig5_130     2.2     9.4   0.021    9.3   0.0    1    3-3      24-24  (73)
 11 Q02337_46_MM_ref_sig5_130        2.2     9.4   0.022    6.5   0.0    1    1-1       1-1   (51)
 12 P18886_25_IM_ref_sig5_130        2.2     9.6   0.022    8.1   0.0    3    2-4      23-25  (30)
 13 P10817_9_IM_ref_sig5_130         2.2     9.6   0.022    6.9   0.0    3    2-4       3-5   (14)
 14 P13184_23_IM_ref_sig5_130        2.1     9.7   0.022    7.9   0.0    1    2-2      22-22  (28)
 15 P25285_22_Mito_ref_sig5_130      2.1      10   0.023    7.8   0.0    3    2-4      18-20  (27)
 16 P42847_59_Mito_ref_sig5_130      2.0      10   0.024    9.1   0.0    3    2-4      17-19  (64)
 17 P36517_14_Mito_ref_sig5_130      2.0      10   0.024    7.3   0.0    3    3-5      12-14  (19)
 18 Q91YT0_20_IM_ref_sig5_130        2.0      11   0.025    7.6   0.0    3    2-4       5-7   (25)
 19 P12063_32_MM_ref_sig5_130        1.9      11   0.025    8.2   0.0    1    2-2      31-31  (37)
 20 P49411_43_Mito_ref_sig5_130      1.7      13   0.029    8.5   0.0    1    3-3      30-30  (48)

No 1  
>P13183_24_IM_ref_sig5_130
Probab=2.80  E-value=7  Score=8.28  Aligned_cols=1  Identities=0%  Similarity=0.932  Sum_probs=0.3

Q 5_Mustela         4 L    4 (6)
Q 2_Macaca          4 L    4 (6)
Q 15_Ictidomys      4 L    4 (6)
Q 11_Cavia          4 L    4 (6)
Q 17_Tupaia         4 F    4 (6)
Q 19_Trichechus     4 L    4 (6)
Q 16_Equus          4 L    4 (6)
Q Consensus         4 l    4 (6)
                      +
T Consensus        17 i   17 (29)
T signal           17 I   17 (29)
T 11               17 F   17 (29)
T 12               17 I   17 (29)
T 16               17 I   17 (29)
T 10               17 I   17 (28)
T 49               16 I   16 (28)
T 50               16 I   16 (28)
T 56               16 I   16 (28)
T 58               16 I   16 (28)
T 1                16 A   16 (24)
Confidence            2


No 2  
>P05630_22_Mito_IM_ref_sig5_130
Probab=2.73  E-value=7.3  Score=8.21  Aligned_cols=3  Identities=100%  Similarity=1.885  Sum_probs=1.2

Q 5_Mustela         1 RRP    3 (6)
Q 2_Macaca          1 RRP    3 (6)
Q 15_Ictidomys      1 RRP    3 (6)
Q 11_Cavia          1 QRP    3 (6)
Q 17_Tupaia         1 RRP    3 (6)
Q 19_Trichechus     1 RRP    3 (6)
Q 16_Equus          1 RRS    3 (6)
Q Consensus         1 rRp    3 (6)
                      |||
T Consensus         8 rr~   10 (27)
T signal            8 RRP   10 (27)
T 133               8 RRP   10 (27)
T 148               8 RHP   10 (27)
T 167               8 RRS   10 (27)
T 155               8 RRP   10 (27)
T 147               8 RRP   10 (27)
T 140               8 RRL   10 (26)
T 142               8 RRC   10 (32)
T 121               8 LAL   10 (34)
T 134               8 ARP   10 (29)
Confidence            344


No 3  
>P23786_25_IM_ref_sig5_130
Probab=2.72  E-value=7.3  Score=8.35  Aligned_cols=3  Identities=100%  Similarity=1.807  Sum_probs=1.7

Q 5_Mustela         2 RPL    4 (6)
Q 2_Macaca          2 RPL    4 (6)
Q 15_Ictidomys      2 RPL    4 (6)
Q 11_Cavia          2 RPL    4 (6)
Q 17_Tupaia         2 RPF    4 (6)
Q 19_Trichechus     2 RPL    4 (6)
Q 16_Equus          2 RSL    4 (6)
Q Consensus         2 Rpl    4 (6)
                      |||
T Consensus        23 RpL   25 (30)
T signal           23 RPL   25 (30)
T 101              23 RPL   25 (30)
T 104              23 RPL   25 (30)
T 108              23 RPL   25 (30)
T 109              24 RSL   26 (31)
T 117              23 RSL   25 (30)
T 119              23 RPL   25 (30)
T 59               28 RSL   30 (35)
Confidence            565


No 4  
>P09440_34_Mito_ref_sig5_130
Probab=2.60  E-value=7.7  Score=8.72  Aligned_cols=2  Identities=0%  Similarity=0.617  Sum_probs=0.7

Q 5_Mustela         4 LE    5 (6)
Q 2_Macaca          4 LR    5 (6)
Q 15_Ictidomys      4 LQ    5 (6)
Q 11_Cavia          4 LR    5 (6)
Q 17_Tupaia         4 FQ    5 (6)
Q 19_Trichechus     4 LR    5 (6)
Q 16_Equus          4 LE    5 (6)
Q Consensus         4 l~    5 (6)
                      ++
T Consensus        14 fq   15 (39)
T signal           14 FQ   15 (39)
Confidence            33


No 5  
>Q01205_68_Mito_ref_sig5_130
Probab=2.48  E-value=8.2  Score=9.52  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.3

Q 5_Mustela         2 R    2 (6)
Q 2_Macaca          2 R    2 (6)
Q 15_Ictidomys      2 R    2 (6)
Q 11_Cavia          2 R    2 (6)
Q 17_Tupaia         2 R    2 (6)
Q 19_Trichechus     2 R    2 (6)
Q 16_Equus          2 R    2 (6)
Q Consensus         2 R    2 (6)
                      |
T Consensus        29 r   29 (73)
T signal           29 R   29 (73)
T 126              28 R   28 (72)
T 144              30 R   30 (74)
T 148              18 R   18 (62)
T 128              29 R   29 (72)
T 129              29 R   29 (72)
T 131              29 R   29 (72)
T 142              25 C   25 (70)
T 141              25 R   25 (69)
Confidence            3


No 6  
>P07806_47_Cy_Mito_ref_sig5_130
Probab=2.35  E-value=8.7  Score=9.04  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.4

Q 5_Mustela         2 R    2 (6)
Q 2_Macaca          2 R    2 (6)
Q 15_Ictidomys      2 R    2 (6)
Q 11_Cavia          2 R    2 (6)
Q 17_Tupaia         2 R    2 (6)
Q 19_Trichechus     2 R    2 (6)
Q 16_Equus          2 R    2 (6)
Q Consensus         2 R    2 (6)
                      |
T Consensus        23 r   23 (52)
T signal           23 R   23 (52)
T 176               1 -    0 (29)
Confidence            3


No 7  
>P54898_44_Mito_ref_sig5_130
Probab=2.32  E-value=8.8  Score=8.91  Aligned_cols=3  Identities=33%  Similarity=0.811  Sum_probs=1.2

Q 5_Mustela         2 RPL    4 (6)
Q 2_Macaca          2 RPL    4 (6)
Q 15_Ictidomys      2 RPL    4 (6)
Q 11_Cavia          2 RPL    4 (6)
Q 17_Tupaia         2 RPF    4 (6)
Q 19_Trichechus     2 RPL    4 (6)
Q 16_Equus          2 RSL    4 (6)
Q Consensus         2 Rpl    4 (6)
                      ||+
T Consensus        35 R~~   37 (49)
T signal           35 RQI   37 (49)
T 154              37 RPF   39 (51)
T 151              36 RPF   38 (50)
T 104              35 RSI   37 (43)
T 137              42 RSF   44 (49)
Confidence            443


No 8  
>P0CS90_23_MM_Nu_ref_sig5_130
Probab=2.25  E-value=9.1  Score=7.81  Aligned_cols=2  Identities=0%  Similarity=0.401  Sum_probs=0.8

Q 5_Mustela         3 PL    4 (6)
Q 2_Macaca          3 PL    4 (6)
Q 15_Ictidomys      3 PL    4 (6)
Q 11_Cavia          3 PL    4 (6)
Q 17_Tupaia         3 PF    4 (6)
Q 19_Trichechus     3 PL    4 (6)
Q 16_Equus          3 SL    4 (6)
Q Consensus         3 pl    4 (6)
                      |+
T Consensus        16 ~~   17 (28)
T signal           16 SF   17 (28)
T 55               17 AL   18 (29)
T 65               17 VV   18 (29)
T 54               17 -P   17 (28)
T 57               14 PI   15 (26)
T 59               14 PL   15 (26)
T 62               13 -N   13 (24)
T 56               12 PL   13 (24)
T 60               11 NV   12 (23)
Confidence            33


No 9  
>P09624_21_MM_ref_sig5_130
Probab=2.22  E-value=9.3  Score=7.74  Aligned_cols=3  Identities=33%  Similarity=0.778  Sum_probs=1.2

Q 5_Mustela         2 RPL.    4 (6)
Q 2_Macaca          2 RPL.    4 (6)
Q 15_Ictidomys      2 RPL.    4 (6)
Q 11_Cavia          2 RPL.    4 (6)
Q 17_Tupaia         2 RPF.    4 (6)
Q 19_Trichechus     2 RPL.    4 (6)
Q 16_Equus          2 RSL.    4 (6)
Q Consensus         2 Rpl.    4 (6)
                      |.| 
T Consensus        12 Rsf.   14 (26)
T signal           12 RAF.   14 (26)
T 188               8 RSF.   10 (22)
T 183              12 RSL.   14 (26)
T 186              12 RSF.   14 (25)
T 191              12 RRFl   15 (27)
T 187               9 RSF.   11 (21)
Confidence            333 


No 10 
>P07926_68_MitoM_ref_sig5_130
Probab=2.21  E-value=9.4  Score=9.35  Aligned_cols=1  Identities=0%  Similarity=0.136  Sum_probs=0.4

Q 5_Mustela         3 P.....    3 (6)
Q 2_Macaca          3 P.....    3 (6)
Q 15_Ictidomys      3 P.....    3 (6)
Q 11_Cavia          3 P.....    3 (6)
Q 17_Tupaia         3 P.....    3 (6)
Q 19_Trichechus     3 P.....    3 (6)
Q 16_Equus          3 S.....    3 (6)
Q Consensus         3 p.....    3 (6)
                      |     
T Consensus        24 P.....   24 (73)
T signal           24 S.....   24 (73)
T 10               24 P.....   24 (82)
T 14               24 P.....   24 (55)
T 22               24 P.....   24 (37)
T 9                24 P.....   24 (67)
T 5                27 P.....   27 (74)
T 6                27 P.....   27 (68)
T 2                21 Salrpl   26 (84)
T 3                26 P.....   26 (84)
T 53                8 P.....    8 (40)
Confidence            3     


No 11 
>Q02337_46_MM_ref_sig5_130
Probab=2.19  E-value=9.4  Score=6.54  Aligned_cols=1  Identities=0%  Similarity=-0.562  Sum_probs=0.0

Q 5_Mustela         1 R    1 (6)
Q 2_Macaca          1 R    1 (6)
Q 15_Ictidomys      1 R    1 (6)
Q 11_Cavia          1 Q    1 (6)
Q 17_Tupaia         1 R    1 (6)
Q 19_Trichechus     1 R    1 (6)
Q 16_Equus          1 R    1 (6)
Q Consensus         1 r    1 (6)
                      =
T Consensus         1 M    1 (51)
T signal            1 M    1 (51)
T 76_Pediculus      1 M    1 (50)
T 74_Branchiosto    1 M    1 (50)
T 69_Papio          1 V    1 (50)
T 67_Maylandia      1 M    1 (50)
T 60_Sus            1 M    1 (50)
T 72_Ciona          1 M    1 (50)
T 59_Anolis         1 I    1 (50)
T 51_Canis          1 M    1 (50)
T 38_Nomascus       1 M    1 (50)
Confidence            0


No 12 
>P18886_25_IM_ref_sig5_130
Probab=2.17  E-value=9.6  Score=8.05  Aligned_cols=3  Identities=100%  Similarity=1.807  Sum_probs=1.6

Q 5_Mustela         2 RPL    4 (6)
Q 2_Macaca          2 RPL    4 (6)
Q 15_Ictidomys      2 RPL    4 (6)
Q 11_Cavia          2 RPL    4 (6)
Q 17_Tupaia         2 RPF    4 (6)
Q 19_Trichechus     2 RPL    4 (6)
Q 16_Equus          2 RSL    4 (6)
Q Consensus         2 Rpl    4 (6)
                      |||
T Consensus        23 R~L   25 (30)
T signal           23 RPL   25 (30)
T 84               23 RTL   25 (30)
T 87               23 RPL   25 (30)
T 102              24 RSL   26 (31)
T 114              23 RPL   25 (30)
T 92               23 RLL   25 (30)
T 94               23 RSL   25 (30)
T 48               28 RSL   30 (35)
Confidence            555


No 13 
>P10817_9_IM_ref_sig5_130
Probab=2.17  E-value=9.6  Score=6.87  Aligned_cols=3  Identities=33%  Similarity=0.545  Sum_probs=1.1

Q 5_Mustela         2 RPL    4 (6)
Q 2_Macaca          2 RPL    4 (6)
Q 15_Ictidomys      2 RPL    4 (6)
Q 11_Cavia          2 RPL    4 (6)
Q 17_Tupaia         2 RPF    4 (6)
Q 19_Trichechus     2 RPL    4 (6)
Q 16_Equus          2 RSL    4 (6)
Q Consensus         2 Rpl    4 (6)
                      |.|
T Consensus         3 r~L    5 (14)
T signal            3 KVL    5 (14)
T 14                3 QLL    5 (14)
T 18                3 RSL    5 (14)
T 22                3 KSL    5 (14)
T 23                3 RLL    5 (14)
T 46                3 RAL    5 (14)
T 4                 3 RTL    5 (14)
Confidence            333


No 14 
>P13184_23_IM_ref_sig5_130
Probab=2.14  E-value=9.7  Score=7.86  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.3

Q 5_Mustela         2 R    2 (6)
Q 2_Macaca          2 R    2 (6)
Q 15_Ictidomys      2 R    2 (6)
Q 11_Cavia          2 R    2 (6)
Q 17_Tupaia         2 R    2 (6)
Q 19_Trichechus     2 R    2 (6)
Q 16_Equus          2 R    2 (6)
Q Consensus         2 R    2 (6)
                      |
T Consensus        22 R   22 (28)
T signal           22 R   22 (28)
T 63               22 R   22 (28)
T 28               22 R   22 (28)
T 15               22 R   22 (28)
T 25               22 R   22 (28)
T 16               22 R   22 (28)
T 19               22 R   22 (28)
T 12               22 R   22 (28)
T 14               22 S   22 (28)
Confidence            3


No 15 
>P25285_22_Mito_ref_sig5_130
Probab=2.05  E-value=10  Score=7.75  Aligned_cols=3  Identities=67%  Similarity=1.010  Sum_probs=1.6

Q 5_Mustela         2 RPL    4 (6)
Q 2_Macaca          2 RPL    4 (6)
Q 15_Ictidomys      2 RPL    4 (6)
Q 11_Cavia          2 RPL    4 (6)
Q 17_Tupaia         2 RPF    4 (6)
Q 19_Trichechus     2 RPL    4 (6)
Q 16_Equus          2 RSL    4 (6)
Q Consensus         2 Rpl    4 (6)
                      |||
T Consensus        18 R~l   20 (27)
T signal           18 RSL   20 (27)
T 107              18 RSL   20 (27)
T 127              18 RSF   20 (27)
T 128              18 RGL   20 (27)
T 134              18 RPL   20 (27)
T 138              18 RSL   20 (27)
T 145              18 RPF   20 (27)
T 146              18 RLY   20 (27)
T 147              18 RPL   20 (27)
T 152              18 RQL   20 (27)
Confidence            555


No 16 
>P42847_59_Mito_ref_sig5_130
Probab=2.04  E-value=10  Score=9.12  Aligned_cols=3  Identities=67%  Similarity=1.674  Sum_probs=1.3

Q 5_Mustela         2 RPL    4 (6)
Q 2_Macaca          2 RPL    4 (6)
Q 15_Ictidomys      2 RPL    4 (6)
Q 11_Cavia          2 RPL    4 (6)
Q 17_Tupaia         2 RPF    4 (6)
Q 19_Trichechus     2 RPL    4 (6)
Q 16_Equus          2 RSL    4 (6)
Q Consensus         2 Rpl    4 (6)
                      ||+
T Consensus        17 rpi   19 (64)
T signal           17 RPI   19 (64)
Confidence            444


No 17 
>P36517_14_Mito_ref_sig5_130
Probab=2.02  E-value=10  Score=7.28  Aligned_cols=3  Identities=67%  Similarity=1.331  Sum_probs=1.5

Q 5_Mustela         3 PLE    5 (6)
Q 2_Macaca          3 PLR    5 (6)
Q 15_Ictidomys      3 PLQ    5 (6)
Q 11_Cavia          3 PLR    5 (6)
Q 17_Tupaia         3 PFQ    5 (6)
Q 19_Trichechus     3 PLR    5 (6)
Q 16_Equus          3 SLE    5 (6)
Q Consensus         3 pl~    5 (6)
                      ||+
T Consensus        12 plr   14 (19)
T signal           12 PLR   14 (19)
Confidence            554


No 18 
>Q91YT0_20_IM_ref_sig5_130
Probab=1.95  E-value=11  Score=7.64  Aligned_cols=3  Identities=33%  Similarity=0.623  Sum_probs=1.3

Q 5_Mustela         2 RPL    4 (6)
Q 2_Macaca          2 RPL    4 (6)
Q 15_Ictidomys      2 RPL    4 (6)
Q 11_Cavia          2 RPL    4 (6)
Q 17_Tupaia         2 RPF    4 (6)
Q 19_Trichechus     2 RPL    4 (6)
Q 16_Equus          2 RSL    4 (6)
Q Consensus         2 Rpl    4 (6)
                      |||
T Consensus         5 R~l    7 (25)
T signal            5 RHF    7 (25)
T 132               5 RPL    7 (29)
T 93                5 RRL    7 (25)
Confidence            443


No 19 
>P12063_32_MM_ref_sig5_130
Probab=1.94  E-value=11  Score=8.24  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.3

Q 5_Mustela         2 R    2 (6)
Q 2_Macaca          2 R    2 (6)
Q 15_Ictidomys      2 R    2 (6)
Q 11_Cavia          2 R    2 (6)
Q 17_Tupaia         2 R    2 (6)
Q 19_Trichechus     2 R    2 (6)
Q 16_Equus          2 R    2 (6)
Q Consensus         2 R    2 (6)
                      |
T Consensus        31 r   31 (37)
T signal           31 R   31 (37)
T 193              31 R   31 (37)
Confidence            3


No 20 
>P49411_43_Mito_ref_sig5_130
Probab=1.73  E-value=13  Score=8.46  Aligned_cols=1  Identities=0%  Similarity=-0.762  Sum_probs=0.4

Q 5_Mustela         3 P    3 (6)
Q 2_Macaca          3 P    3 (6)
Q 15_Ictidomys      3 P    3 (6)
Q 11_Cavia          3 P    3 (6)
Q 17_Tupaia         3 P    3 (6)
Q 19_Trichechus     3 P    3 (6)
Q 16_Equus          3 S    3 (6)
Q Consensus         3 p    3 (6)
                      |
T Consensus        30 p   30 (48)
T signal           30 L   30 (48)
T 163              30 P   30 (48)
T 166              25 P   25 (43)
T 167              30 P   30 (48)
T 170              27 P   27 (45)
T 171              30 P   30 (48)
T 177              30 L   30 (48)
T 157              44 P   44 (62)
Confidence            3


Done!
