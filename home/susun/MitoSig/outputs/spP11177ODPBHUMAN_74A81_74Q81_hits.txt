Query         5_Mustela
Match_columns 7
No_of_seqs    1 out of 20
Neff          1.0 
Searched_HMMs 436
Date          Wed Sep  6 16:09:38 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_74A81_74Q81_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P33198_8_Mito_ref_sig5_130       8.7     1.7  0.0038    8.9   0.0    6    2-7       7-12  (13)
  2 P07806_47_Cy_Mito_ref_sig5_130   0.5      53    0.12    7.1   0.0    1    3-3      52-52  (52)
  3 Q6SZW1_27_Cy_Mito_ref_sig5_130   0.5      57    0.13    3.3   0.0    1    3-3      32-32  (32)
  4 P42028_36_Mito_ref_sig5_130      0.4      57    0.13    6.6   0.0    2    2-3      39-40  (41)
  5 P53219_38_Mito_ref_sig5_130      0.4      59    0.13    6.7   0.0    2    3-4      13-14  (43)
  6 P34942_23_MM_ref_sig5_130        0.4      59    0.14    3.1   0.0    1    3-3      28-28  (28)
  7 P09622_35_MM_ref_sig5_130        0.4      60    0.14    3.0   0.0    1    3-3      40-40  (40)
  8 Q02372_28_IM_ref_sig5_130        0.4      64    0.15    2.9   0.0    1    3-3      33-33  (33)
  9 P40360_13_Mito_ref_sig5_130      0.4      64    0.15    5.4   0.0    3    2-4      11-13  (18)
 10 P38071_9_MM_ref_sig5_130         0.4      66    0.15    5.1   0.0    1    6-6       6-6   (14)
 11 P09624_21_MM_ref_sig5_130        0.4      68    0.16    5.7   0.0    1    6-6      11-11  (26)
 12 P10507_20_MM_ref_sig5_130        0.4      75    0.17    5.7   0.0    1    3-3      25-25  (25)
 13 Q9HCC0_22_MM_ref_sig5_130        0.3      75    0.17    5.8   0.0    2    4-5      25-26  (27)
 14 P01098_23_Mito_ref_sig5_130      0.3      76    0.17    5.8   0.0    3    3-5      23-25  (28)
 15 Q9UJ68_23_Mito_Cy_Nu_ref_sig5_   0.3      78    0.18    5.7   0.0    2    4-5      24-25  (28)
 16 P83484_51_Mito_IM_ref_sig5_130   0.3      82    0.19    5.1   0.0    1    4-4      42-42  (56)
 17 P10355_8_IM_ref_sig5_130         0.3      85    0.19    4.7   0.0    1    6-6       6-6   (13)
 18 P36516_59_Mito_ref_sig5_130      0.3      86     0.2    6.7   0.0    1    3-3      64-64  (64)
 19 P07919_13_IM_ref_sig5_130        0.3      87     0.2    5.0   0.0    2    4-5      14-15  (18)
 20 A4FUC0_29_Mito_ref_sig5_130      0.3      87     0.2    2.4   0.0    1    3-3      34-34  (34)

No 1  
>P33198_8_Mito_ref_sig5_130
Probab=8.68  E-value=1.7  Score=8.95  Aligned_cols=6  Identities=50%  Similarity=1.099  Sum_probs=3.8

Q 5_Mustela         2 KYGDKR    7 (7)
Q Consensus         2 kygdkr    7 (7)
                      .|.|.|
T Consensus         7 hyadqr   12 (13)
T signal            7 HYADQR   12 (13)
Confidence            477765


No 2  
>P07806_47_Cy_Mito_ref_sig5_130
Probab=0.48  E-value=53  Score=7.09  Aligned_cols=1  Identities=0%  Similarity=-0.462  Sum_probs=0.0

Q 5_Mustela         3 Y    3 (7)
Q Consensus         3 y    3 (7)
                      -
T Consensus        52 N   52 (52)
T signal           52 N   52 (52)
T 176              29 N   29 (29)
Confidence            0


No 3  
>Q6SZW1_27_Cy_Mito_ref_sig5_130
Probab=0.45  E-value=57  Score=3.25  Aligned_cols=1  Identities=0%  Similarity=-1.327  Sum_probs=0.0

Q 5_Mustela         3 Y    3 (7)
Q Consensus         3 y    3 (7)
                      =
T Consensus        32 ~   32 (32)
T signal           32 G   32 (32)
T 93_Amphimedon    32 I   32 (32)
T 87_Branchiosto   32 V   32 (32)
T 83_Aplysia       32 N   32 (32)
T 81_Aedes         32 G   32 (32)
T 61_Chrysemys     32 L   32 (32)
T 57_Ficedula      32 I   32 (32)
T 52_Falco         32 V   32 (32)
Confidence            0


No 4  
>P42028_36_Mito_ref_sig5_130
Probab=0.45  E-value=57  Score=6.57  Aligned_cols=2  Identities=100%  Similarity=1.829  Sum_probs=0.7

Q 5_Mustela         2 KY    3 (7)
Q Consensus         2 ky    3 (7)
                      ||
T Consensus        39 KY   40 (41)
T signal           39 KY   40 (41)
T 142              35 KY   36 (37)
T 152              37 KY   38 (39)
T 172              37 KY   38 (39)
T 180              37 KY   38 (39)
T 162              37 KY   38 (39)
T 113              33 KY   34 (35)
T 147              34 KY   35 (36)
T 140              33 KY   34 (35)
T 144              34 KY   35 (36)
Confidence            33


No 5  
>P53219_38_Mito_ref_sig5_130
Probab=0.44  E-value=59  Score=6.69  Aligned_cols=2  Identities=100%  Similarity=2.394  Sum_probs=1.0

Q 5_Mustela         3 YG    4 (7)
Q Consensus         3 yg    4 (7)
                      ||
T Consensus        13 yg   14 (43)
T signal           13 YG   14 (43)
Confidence            44


No 6  
>P34942_23_MM_ref_sig5_130
Probab=0.44  E-value=59  Score=3.13  Aligned_cols=1  Identities=0%  Similarity=-1.028  Sum_probs=0.0

Q 5_Mustela         3 Y    3 (7)
Q Consensus         3 y    3 (7)
                      -
T Consensus        28 ~   28 (28)
T signal           28 P   28 (28)
T 78_Musca         40 -   39 (39)
T 74_Strongyloce   40 -   39 (39)
T 62_Alligator     40 -   39 (39)
T 60_Xenopus       40 -   39 (39)
T 56_Anas          40 -   39 (39)
T 52_Meleagris     40 -   39 (39)
T 40_Saimiri       40 -   39 (39)
Confidence            0


No 7  
>P09622_35_MM_ref_sig5_130
Probab=0.43  E-value=60  Score=2.97  Aligned_cols=1  Identities=0%  Similarity=-0.231  Sum_probs=0.0

Q 5_Mustela         3 Y    3 (7)
Q Consensus         3 y    3 (7)
                      .
T Consensus        40 ~   40 (40)
T signal           40 I   40 (40)
T 181_Paramecium   39 -   38 (38)
T 142_Physcomitr   39 -   38 (38)
T 97_Anopheles     39 -   38 (38)
T 92_Brugia        39 -   38 (38)
T 77_Tursiops      39 -   38 (38)
T 74_Ciona         39 -   38 (38)
T 62_Meleagris     39 -   38 (38)
T 17_Ceratotheri   39 -   38 (38)
Confidence            0


No 8  
>Q02372_28_IM_ref_sig5_130
Probab=0.41  E-value=64  Score=2.85  Aligned_cols=1  Identities=0%  Similarity=-0.629  Sum_probs=0.0

Q 5_Mustela         3 Y    3 (7)
Q Consensus         3 y    3 (7)
                      =
T Consensus        33 ~   33 (33)
T signal           33 T   33 (33)
T 89_Brugia        32 -   31 (31)
T 78_Nasonia       32 -   31 (31)
T 75_Bombyx        32 -   31 (31)
T 74_Branchiosto   32 -   31 (31)
T 72_Tursiops      32 -   31 (31)
T 62_Anas          32 -   31 (31)
T 57_Falco         32 -   31 (31)
T 53_Oryzias       32 -   31 (31)
T 29_Capra         32 -   31 (31)
Confidence            0


No 9  
>P40360_13_Mito_ref_sig5_130
Probab=0.41  E-value=64  Score=5.38  Aligned_cols=3  Identities=67%  Similarity=1.232  Sum_probs=1.4

Q 5_Mustela         2 KYG    4 (7)
Q Consensus         2 kyg    4 (7)
                      ||-
T Consensus        11 ky~   13 (18)
T signal           11 KYD   13 (18)
T 54               11 RYE   13 (18)
T 53               11 KYD   13 (18)
T 52               11 GYQ   13 (17)
T 50               11 KYE   13 (17)
T 51               11 KVE   13 (18)
T 49               12 KLN   14 (19)
Confidence            453


No 10 
>P38071_9_MM_ref_sig5_130
Probab=0.40  E-value=66  Score=5.12  Aligned_cols=1  Identities=100%  Similarity=1.066  Sum_probs=0.3

Q 5_Mustela         6 K    6 (7)
Q Consensus         6 k    6 (7)
                      |
T Consensus         6 K    6 (14)
T signal            6 K    6 (14)
T 173               6 K    6 (12)
T 174               6 K    6 (12)
Confidence            3


No 11 
>P09624_21_MM_ref_sig5_130
Probab=0.38  E-value=68  Score=5.72  Aligned_cols=1  Identities=100%  Similarity=1.066  Sum_probs=0.3

Q 5_Mustela         6 K    6 (7)
Q Consensus         6 k    6 (7)
                      |
T Consensus        11 k   11 (26)
T signal           11 K   11 (26)
T 188               7 T    7 (22)
T 183              11 K   11 (26)
T 186              11 R   11 (25)
T 191              11 S   11 (27)
T 187               8 K    8 (21)
Confidence            3


No 12 
>P10507_20_MM_ref_sig5_130
Probab=0.35  E-value=75  Score=5.71  Aligned_cols=1  Identities=0%  Similarity=-1.327  Sum_probs=0.0

Q 5_Mustela         3 Y    3 (7)
Q Consensus         3 y    3 (7)
                      -
T Consensus        25 ~   25 (25)
T signal           25 G   25 (25)
T 191              19 L   19 (19)
Confidence            0


No 13 
>Q9HCC0_22_MM_ref_sig5_130
Probab=0.35  E-value=75  Score=5.76  Aligned_cols=2  Identities=100%  Similarity=1.880  Sum_probs=0.9

Q 5_Mustela         4 GD    5 (7)
Q Consensus         4 gd    5 (7)
                      ||
T Consensus        25 GD   26 (27)
T signal           25 GD   26 (27)
T 6                25 GA   26 (27)
T 23               25 GD   26 (27)
T 30               25 QD   26 (27)
T 36               25 GD   26 (27)
T 44               25 GD   26 (27)
T 50               25 GD   26 (27)
T 57               25 GD   26 (27)
T 63               25 GD   26 (27)
T 74               25 GD   26 (27)
Confidence            44


No 14 
>P01098_23_Mito_ref_sig5_130
Probab=0.35  E-value=76  Score=5.82  Aligned_cols=3  Identities=67%  Similarity=1.431  Sum_probs=1.2

Q 5_Mustela         3 YGD    5 (7)
Q Consensus         3 ygd    5 (7)
                      |.|
T Consensus        23 ysd   25 (28)
T signal           23 YSD   25 (28)
Confidence            443


No 15 
>Q9UJ68_23_Mito_Cy_Nu_ref_sig5_130
Probab=0.34  E-value=78  Score=5.73  Aligned_cols=2  Identities=50%  Similarity=1.464  Sum_probs=1.0

Q 5_Mustela         4 GD    5 (7)
Q Consensus         4 gd    5 (7)
                      ||
T Consensus        24 GD   25 (28)
T signal           24 GN   25 (28)
T 102              22 GD   23 (26)
T 105              22 GD   23 (26)
T 110              22 GD   23 (26)
T 111              22 GD   23 (26)
T 108              22 GD   23 (26)
T 113              22 GD   23 (26)
T 76               22 GD   23 (26)
Confidence            55


No 16 
>P83484_51_Mito_IM_ref_sig5_130
Probab=0.32  E-value=82  Score=5.12  Aligned_cols=1  Identities=0%  Similarity=0.136  Sum_probs=0.2

Q 5_Mustela         4 G    4 (7)
Q Consensus         4 g    4 (7)
                      |
T Consensus        42 ~   42 (56)
T signal           42 S   42 (56)
T 34_Volvox        39 -   38 (38)
T 134_Guillardia   39 -   38 (38)
T 152_Trichinell   39 -   38 (38)
T 159_Ixodes       39 -   38 (38)
T 41_Galdieria     39 -   38 (38)
T 112_Tribolium    39 -   38 (38)
T 117_Ceratitis    39 -   38 (38)
T 111_Musca        39 -   38 (38)
T 39_Selaginella   39 -   38 (38)
Confidence            2


No 17 
>P10355_8_IM_ref_sig5_130
Probab=0.31  E-value=85  Score=4.73  Aligned_cols=1  Identities=100%  Similarity=1.066  Sum_probs=0.4

Q 5_Mustela         6 K    6 (7)
Q Consensus         6 k    6 (7)
                      |
T Consensus         6 k    6 (13)
T signal            6 K    6 (13)
Confidence            3


No 18 
>P36516_59_Mito_ref_sig5_130
Probab=0.31  E-value=86  Score=6.66  Aligned_cols=1  Identities=0%  Similarity=1.696  Sum_probs=0.0

Q 5_Mustela         3 Y    3 (7)
Q Consensus         3 y    3 (7)
                      |
T Consensus        64 f   64 (64)
T signal           64 F   64 (64)
Confidence            0


No 19 
>P07919_13_IM_ref_sig5_130
Probab=0.30  E-value=87  Score=5.01  Aligned_cols=2  Identities=100%  Similarity=1.880  Sum_probs=0.8

Q 5_Mustela         4 GD    5 (7)
Q Consensus         4 gd    5 (7)
                      ||
T Consensus        14 gD   15 (18)
T signal           14 GD   15 (18)
T 41               14 GD   15 (18)
T 13               14 RD   15 (18)
T 23               14 GD   15 (18)
T 58               13 ED   14 (17)
T 61               14 GD   15 (18)
T 18               14 GE   15 (18)
T 22               14 GD   15 (18)
T 59               14 GD   15 (18)
T 79               11 GE   12 (15)
Confidence            44


No 20 
>A4FUC0_29_Mito_ref_sig5_130
Probab=0.30  E-value=87  Score=2.43  Aligned_cols=1  Identities=0%  Similarity=-1.327  Sum_probs=0.0

Q 5_Mustela         3 Y    3 (7)
Q Consensus         3 y    3 (7)
                      .
T Consensus        34 ~   34 (34)
T signal           34 G   34 (34)
T 83_Ceratitis     34 A   34 (34)
T 70_Xiphophorus   34 N   34 (34)
T 62_Anolis        34 G   34 (34)
T 61_Geospiza      34 E   34 (34)
T 60_Anas          34 V   34 (34)
T 59_Zonotrichia   34 K   34 (34)
T 57_Columba       34 E   34 (34)
T 55_Falco         34 A   34 (34)
T 43_Dasypus       34 P   34 (34)
Confidence            0


Done!
