Query         5_Mustela
Match_columns 7
No_of_seqs    3 out of 20
Neff          1.2 
Searched_HMMs 436
Date          Wed Sep  6 16:08:39 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_36A43_36Q43_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P36516_59_Mito_ref_sig5_130      7.8     1.9  0.0044   11.7   0.0    6    2-7       8-13  (64)
  2 P10817_9_IM_ref_sig5_130         6.2     2.6   0.006    8.5   0.0    6    2-7       4-9   (14)
  3 P07471_12_IM_ref_sig5_130        4.1     4.4    0.01    8.4   0.0    6    2-7       7-12  (17)
  4 P40360_13_Mito_ref_sig5_130      0.6      39   0.089    6.0   0.0    3    4-6       7-9   (18)
  5 P53193_10_MM_ref_sig5_130        0.6      39   0.089    5.8   0.0    3    3-5       5-7   (15)
  6 P36523_28_Mito_ref_sig5_130      0.6      41   0.094    6.8   0.0    3    2-4      18-20  (33)
  7 P08425_17_MM_ref_sig5_130        0.6      43   0.099    6.2   0.0    3    3-5       3-5   (22)
  8 P39697_29_Mito_ref_sig5_130      0.5      47    0.11    6.7   0.0    3    4-6      12-14  (34)
  9 P36517_14_Mito_ref_sig5_130      0.5      49    0.11    5.9   0.0    3    4-6       8-10  (19)
 10 P17694_33_IM_ref_sig5_130        0.5      50    0.11    3.3   0.0    1    3-3      38-38  (38)
 11 P05626_35_Mito_IM_ref_sig5_130   0.5      51    0.12    6.8   0.0    1    6-6      23-23  (40)
 12 Q54DF1_17_Mito_IM_ref_sig5_130   0.5      51    0.12    6.0   0.0    2    4-5      16-17  (22)
 13 Q04728_8_MM_ref_sig5_130         0.5      51    0.12    5.3   0.0    3    4-6       9-11  (13)
 14 P23965_28_MM_ref_sig5_130        0.5      52    0.12    6.4   0.0    1    6-6       6-6   (33)
 15 P26269_27_MM_ref_sig5_130        0.5      53    0.12    6.4   0.0    1    4-4       4-4   (32)
 16 A4FUC0_29_Mito_ref_sig5_130      0.5      53    0.12    3.0   0.0    1    1-1       1-1   (34)
 17 P35171_23_IM_ref_sig5_130        0.5      56    0.13    6.1   0.0    1    3-3       8-8   (28)
 18 Q874C1_38_IM_ref_sig5_130        0.4      58    0.13    6.7   0.0    1    6-6      37-37  (43)
 19 P31334_19_Mito_ref_sig5_130      0.4      58    0.13    6.0   0.0    2    5-6       6-7   (24)
 20 P13184_23_IM_ref_sig5_130        0.4      60    0.14    6.0   0.0    1    3-3       8-8   (28)

No 1  
>P36516_59_Mito_ref_sig5_130
Probab=7.80  E-value=1.9  Score=11.71  Aligned_cols=6  Identities=67%  Similarity=0.933  Sum_probs=3.5

Q 5_Mustela         2 AINQGM    7 (7)
Q 12_Sorex          2 ALNQGM    7 (7)
Q 9_Ailuropoda      2 AINQGM    7 (7)
Q Consensus         2 ainqgm    7 (7)
                      ||..||
T Consensus         8 aiaagm   13 (64)
T signal            8 AIAAGM   13 (64)
Confidence            555665


No 2  
>P10817_9_IM_ref_sig5_130
Probab=6.20  E-value=2.6  Score=8.54  Aligned_cols=6  Identities=17%  Similarity=0.556  Sum_probs=3.6

Q 5_Mustela         2 AINQGM    7 (7)
Q 12_Sorex          2 ALNQGM    7 (7)
Q 9_Ailuropoda      2 AINQGM    7 (7)
Q Consensus         2 ainqgm    7 (7)
                      .++.|+
T Consensus         4 ~LsRgl    9 (14)
T signal            4 VLSRSM    9 (14)
T 14                4 LLNRGL    9 (14)
T 18                4 SLSRGL    9 (14)
T 22                4 SLSRGL    9 (14)
T 23                4 LLGRGL    9 (14)
T 46                4 ALNRGF    9 (14)
T 4                 4 TLSRGL    9 (14)
Confidence            466675


No 3  
>P07471_12_IM_ref_sig5_130
Probab=4.08  E-value=4.4  Score=8.35  Aligned_cols=6  Identities=17%  Similarity=0.872  Sum_probs=3.2

Q 5_Mustela         2 AINQGM    7 (7)
Q 12_Sorex          2 ALNQGM    7 (7)
Q 9_Ailuropoda      2 AINQGM    7 (7)
Q Consensus         2 ainqgm    7 (7)
                      +++.|+
T Consensus         7 ~LsRgl   12 (17)
T signal            7 SLSRGL   12 (17)
T 8                 7 LLNRGL   12 (17)
T 15                7 VLSRSM   12 (17)
T 17                7 LLGRGL   12 (17)
T 18                7 ALSRGL   12 (17)
T 42                7 ALNRGF   12 (17)
T 3                 7 TLSRGL   12 (17)
Confidence            356664


No 4  
>P40360_13_Mito_ref_sig5_130
Probab=0.64  E-value=39  Score=5.96  Aligned_cols=3  Identities=0%  Similarity=0.014  Sum_probs=1.1

Q 5_Mustela         4 N.QG    6 (7)
Q 12_Sorex          4 N.QG    6 (7)
Q 9_Ailuropoda      4 N.QG    6 (7)
Q Consensus         4 n.qg    6 (7)
                      + +|
T Consensus         7 ~.~~    9 (18)
T signal            7 A.HE    9 (18)
T 54                7 S.NG    9 (18)
T 53                7 N.RG    9 (18)
T 52                7 G.QR    9 (17)
T 50                7 K.HG    9 (17)
T 51                7 S.GG    9 (18)
T 49                7 SgAR   10 (19)
Confidence            3 33


No 5  
>P53193_10_MM_ref_sig5_130
Probab=0.64  E-value=39  Score=5.80  Aligned_cols=3  Identities=33%  Similarity=0.368  Sum_probs=1.5

Q 5_Mustela         3 INQ    5 (7)
Q 12_Sorex          3 LNQ    5 (7)
Q 9_Ailuropoda      3 INQ    5 (7)
Q Consensus         3 inq    5 (7)
                      +||
T Consensus         5 v~q    7 (15)
T signal            5 LVQ    7 (15)
T 63                5 VRQ    7 (15)
T 62                1 VNQ    3 (11)
Confidence            455


No 6  
>P36523_28_Mito_ref_sig5_130
Probab=0.61  E-value=41  Score=6.80  Aligned_cols=3  Identities=67%  Similarity=0.999  Sum_probs=1.3

Q 5_Mustela         2 AIN    4 (7)
Q 12_Sorex          2 ALN    4 (7)
Q 9_Ailuropoda      2 AIN    4 (7)
Q Consensus         2 ain    4 (7)
                      |+|
T Consensus        18 aln   20 (33)
T signal           18 ALN   20 (33)
Confidence            444


No 7  
>P08425_17_MM_ref_sig5_130
Probab=0.58  E-value=43  Score=6.19  Aligned_cols=3  Identities=33%  Similarity=0.900  Sum_probs=1.2

Q 5_Mustela         3 INQ    5 (7)
Q 12_Sorex          3 LNQ    5 (7)
Q 9_Ailuropoda      3 INQ    5 (7)
Q Consensus         3 inq    5 (7)
                      +|.
T Consensus         3 lnr    5 (22)
T signal            3 LNR    5 (22)
Confidence            343


No 8  
>P39697_29_Mito_ref_sig5_130
Probab=0.55  E-value=47  Score=6.70  Aligned_cols=3  Identities=67%  Similarity=1.121  Sum_probs=1.3

Q 5_Mustela         4 NQG    6 (7)
Q 12_Sorex          4 NQG    6 (7)
Q 9_Ailuropoda      4 NQG    6 (7)
Q Consensus         4 nqg    6 (7)
                      .||
T Consensus        12 kqg   14 (34)
T signal           12 KQG   14 (34)
T 4                12 KQG   14 (33)
Confidence            344


No 9  
>P36517_14_Mito_ref_sig5_130
Probab=0.52  E-value=49  Score=5.85  Aligned_cols=3  Identities=67%  Similarity=1.132  Sum_probs=1.4

Q 5_Mustela         4 NQG    6 (7)
Q 12_Sorex          4 NQG    6 (7)
Q 9_Ailuropoda      4 NQG    6 (7)
Q Consensus         4 nqg    6 (7)
                      .||
T Consensus         8 sqg   10 (19)
T signal            8 SQG   10 (19)
Confidence            355


No 10 
>P17694_33_IM_ref_sig5_130
Probab=0.51  E-value=50  Score=3.29  Aligned_cols=1  Identities=0%  Similarity=-0.629  Sum_probs=0.0

Q 5_Mustela         3 I    3 (7)
Q 12_Sorex          3 L    3 (7)
Q 9_Ailuropoda      3 I    3 (7)
Q Consensus         3 i    3 (7)
                      -
T Consensus        38 ~   38 (38)
T signal           38 Q   38 (38)
T 203_Boea         38 -   37 (37)
T 189_Naegleria    38 -   37 (37)
T 167_Loa          38 -   37 (37)
T 125_Talaromyce   38 -   37 (37)
T 84_Andalucia     38 -   37 (37)
Confidence            0


No 11 
>P05626_35_Mito_IM_ref_sig5_130
Probab=0.51  E-value=51  Score=6.78  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.3

Q 5_Mustela         6 G    6 (7)
Q 12_Sorex          6 G    6 (7)
Q 9_Ailuropoda      6 G    6 (7)
Q Consensus         6 g    6 (7)
                      |
T Consensus        23 G   23 (40)
T signal           23 G   23 (40)
T 47               19 G   19 (34)
T 43               19 G   19 (32)
T 45               19 G   19 (32)
T 48               19 G   19 (32)
T 49               19 A   19 (32)
Confidence            3


No 12 
>Q54DF1_17_Mito_IM_ref_sig5_130
Probab=0.50  E-value=51  Score=5.99  Aligned_cols=2  Identities=100%  Similarity=1.082  Sum_probs=0.9

Q 5_Mustela         4 NQ    5 (7)
Q 12_Sorex          4 NQ    5 (7)
Q 9_Ailuropoda      4 NQ    5 (7)
Q Consensus         4 nq    5 (7)
                      ||
T Consensus        16 nq   17 (22)
T signal           16 NQ   17 (22)
Confidence            44


No 13 
>Q04728_8_MM_ref_sig5_130
Probab=0.50  E-value=51  Score=5.28  Aligned_cols=3  Identities=0%  Similarity=0.291  Sum_probs=1.2

Q 5_Mustela         4 NQG    6 (7)
Q 12_Sorex          4 NQG    6 (7)
Q 9_Ailuropoda      4 NQG    6 (7)
Q Consensus         4 nqg    6 (7)
                      .||
T Consensus         9 q~~   11 (13)
T signal            9 QRS   11 (13)
T 90                9 QQG   11 (13)
T 88                9 QHG   11 (13)
Confidence            343


No 14 
>P23965_28_MM_ref_sig5_130
Probab=0.49  E-value=52  Score=6.43  Aligned_cols=1  Identities=0%  Similarity=0.169  Sum_probs=0.2

Q 5_Mustela         6 G    6 (7)
Q 12_Sorex          6 G    6 (7)
Q 9_Ailuropoda      6 G    6 (7)
Q Consensus         6 g    6 (7)
                      |
T Consensus         6 g    6 (33)
T signal            6 A    6 (33)
T 94                6 G    6 (43)
T 93                6 R    6 (46)
T 72                6 G    6 (49)
T 79                6 G    6 (46)
T 63                6 A    6 (34)
T 78                6 G    6 (37)
T 39                6 G    6 (44)
Confidence            2


No 15 
>P26269_27_MM_ref_sig5_130
Probab=0.49  E-value=53  Score=6.44  Aligned_cols=1  Identities=100%  Similarity=1.265  Sum_probs=0.3

Q 5_Mustela         4 N    4 (7)
Q 12_Sorex          4 N    4 (7)
Q 9_Ailuropoda      4 N    4 (7)
Q Consensus         4 n    4 (7)
                      |
T Consensus         4 n    4 (32)
T signal            4 N    4 (32)
Confidence            2


No 16 
>A4FUC0_29_Mito_ref_sig5_130
Probab=0.48  E-value=53  Score=2.96  Aligned_cols=1  Identities=0%  Similarity=-0.994  Sum_probs=0.0

Q 5_Mustela         1 D    1 (7)
Q 12_Sorex          1 D    1 (7)
Q 9_Ailuropoda      1 E    1 (7)
Q Consensus         1 d    1 (7)
                      =
T Consensus         1 M    1 (34)
T signal            1 M    1 (34)
T 83_Ceratitis      1 M    1 (34)
T 70_Xiphophorus    1 M    1 (34)
T 62_Anolis         1 M    1 (34)
T 61_Geospiza       1 W    1 (34)
T 60_Anas           1 C    1 (34)
T 59_Zonotrichia    1 R    1 (34)
T 57_Columba        1 P    1 (34)
T 55_Falco          1 W    1 (34)
T 43_Dasypus        1 M    1 (34)
Confidence            0


No 17 
>P35171_23_IM_ref_sig5_130
Probab=0.46  E-value=56  Score=6.08  Aligned_cols=1  Identities=0%  Similarity=0.932  Sum_probs=0.2

Q 5_Mustela         3 I    3 (7)
Q 12_Sorex          3 L    3 (7)
Q 9_Ailuropoda      3 I    3 (7)
Q Consensus         3 i    3 (7)
                      +
T Consensus         8 l    8 (28)
T signal            8 L    8 (28)
T 73                8 L    8 (28)
T 29                8 I    8 (28)
T 18                8 L    8 (28)
T 21                8 F    8 (28)
T 20                8 L    8 (28)
T 3                 8 L    8 (28)
T 22                4 I    4 (27)
T 71                5 L    5 (24)
Confidence            2


No 18 
>Q874C1_38_IM_ref_sig5_130
Probab=0.44  E-value=58  Score=6.70  Aligned_cols=1  Identities=0%  Similarity=0.136  Sum_probs=0.2

Q 5_Mustela         6 G    6 (7)
Q 12_Sorex          6 G    6 (7)
Q 9_Ailuropoda      6 G    6 (7)
Q Consensus         6 g    6 (7)
                      |
T Consensus        37 g   37 (43)
T signal           37 S   37 (43)
T 176              37 G   37 (43)
T 175              36 G   36 (42)
T 177              37 G   37 (43)
T 173              39 G   39 (45)
T 170              33 S   33 (39)
Confidence            2


No 19 
>P31334_19_Mito_ref_sig5_130
Probab=0.44  E-value=58  Score=5.95  Aligned_cols=2  Identities=100%  Similarity=1.548  Sum_probs=1.1

Q 5_Mustela         5 QG    6 (7)
Q 12_Sorex          5 QG    6 (7)
Q 9_Ailuropoda      5 QG    6 (7)
Q Consensus         5 qg    6 (7)
                      ||
T Consensus         6 qg    7 (24)
T signal            6 QG    7 (24)
Confidence            55


No 20 
>P13184_23_IM_ref_sig5_130
Probab=0.43  E-value=60  Score=6.04  Aligned_cols=1  Identities=0%  Similarity=0.932  Sum_probs=0.2

Q 5_Mustela         3 I    3 (7)
Q 12_Sorex          3 L    3 (7)
Q 9_Ailuropoda      3 I    3 (7)
Q Consensus         3 i    3 (7)
                      +
T Consensus         8 l    8 (28)
T signal            8 L    8 (28)
T 63                8 L    8 (28)
T 28                8 L    8 (28)
T 15                8 L    8 (28)
T 25                8 L    8 (28)
T 16                8 L    8 (28)
T 19                8 V    8 (28)
T 12                8 L    8 (28)
T 14                8 F    8 (28)
Confidence            2


Done!
