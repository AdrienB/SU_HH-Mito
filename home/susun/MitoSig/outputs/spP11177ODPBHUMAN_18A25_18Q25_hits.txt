Query         5_Mustela
Match_columns 7
No_of_seqs    3 out of 20
Neff          1.2 
Searched_HMMs 436
Date          Wed Sep  6 16:08:13 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_18A25_18Q25_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P11177_30_MM_ref_sig5_130       94.7 2.5E-05 5.8E-08   23.8   0.0    7    1-7      19-25  (35)
  2 Q9H300_52_IM_Nu_ref_sig5_130    15.4    0.74  0.0017   12.7   0.0    6    1-6      39-44  (57)
  3 Q93170_22_Mito_ref_sig5_130      6.4     2.5  0.0058    9.8   0.0    6    1-6      13-18  (27)
  4 P22354_18_Mito_ref_sig5_130      4.9     3.5   0.008    9.2   0.0    4    3-6       9-12  (23)
  5 P12063_32_MM_ref_sig5_130        4.4     4.1  0.0093    9.8   0.0    1    3-3      35-35  (37)
  6 Q9N2I8_21_Mito_ref_sig5_130      4.2     4.3  0.0098    9.1   0.0    4    4-7      10-13  (26)
  7 Q99757_59_Mito_ref_sig5_130      4.2     4.3  0.0098   10.5   0.0    3    3-5       8-10  (64)
  8 Q9YHT1_44_IM_ref_sig5_130        3.9     4.6   0.011    9.9   0.0    4    3-6      32-35  (49)
  9 P32445_17_Mito_ref_sig5_130      3.9     4.7   0.011    8.7   0.0    4    3-6       8-11  (22)
 10 Q64591_34_Mito_ref_sig5_130      3.9     4.8   0.011    9.6   0.0    3    3-5      22-24  (39)
 11 Q95108_59_Mito_ref_sig5_130      3.8     4.9   0.011   10.3   0.0    3    3-5       8-10  (64)
 12 P07257_16_IM_ref_sig5_130        3.3     5.7   0.013    8.3   0.0    4    3-6      15-18  (21)
 13 Q0QF01_42_IM_ref_sig5_130        3.3     5.8   0.013    9.6   0.0    4    3-6      31-34  (47)
 14 P20069_32_MM_ref_sig5_130        3.2       6   0.014    9.3   0.0    3    3-5      30-32  (37)
 15 P12686_37_Mito_ref_sig5_130      3.2       6   0.014    9.5   0.0    3    3-5      35-37  (42)
 16 P22068_44_Mito_IM_ref_sig5_130   2.8     7.2   0.016    9.6   0.0    3    3-5      11-13  (49)
 17 P25711_33_IM_ref_sig5_130        2.6     7.5   0.017    9.0   0.0    3    3-5      30-32  (38)
 18 P04182_35_MM_ref_sig5_130        2.4     8.6    0.02    8.8   0.0    4    3-6      16-19  (40)
 19 Q40089_21_Mito_IM_ref_sig5_130   2.3     8.9    0.02    8.2   0.0    4    2-5      18-21  (26)
 20 P39112_41_MM_ref_sig5_130        2.2     9.3   0.021    9.1   0.0    3    4-6      15-17  (46)

No 1  
>P11177_30_MM_ref_sig5_130
Probab=94.72  E-value=2.5e-05  Score=23.76  Aligned_cols=7  Identities=71%  Similarity=1.308  Sum_probs=6.1

Q 5_Mustela         1 LRRRFHR    7 (7)
Q 2_Macaca          1 LKRRFHW    7 (7)
Q 11_Cavia          1 LKRRFHR    7 (7)
Q Consensus         1 lkrrfh~    7 (7)
                      +||+|||
T Consensus        19 lrR~FHr   25 (35)
T signal           19 LKRRFHW   25 (35)
T 143              23 PRRGFHR   29 (39)
T 144              19 LKRSFHR   25 (35)
T 145              19 LQREFHR   25 (35)
T 147              19 LRREFHR   25 (35)
T 135              28 LRSGIHG   34 (44)
T 141              33 QRRGFRL   39 (49)
T 142              18 GRRRFHG   24 (34)
T 149              18 QRREFHR   24 (34)
T 150              18 RRRHFHR   24 (34)
Confidence            6899997


No 2  
>Q9H300_52_IM_Nu_ref_sig5_130
Probab=15.40  E-value=0.74  Score=12.71  Aligned_cols=6  Identities=67%  Similarity=1.143  Sum_probs=3.5

Q 5_Mustela         1 LRRRFH    6 (7)
Q 2_Macaca          1 LKRRFH    6 (7)
Q 11_Cavia          1 LKRRFH    6 (7)
Q Consensus         1 lkrrfh    6 (7)
                      |-|||+
T Consensus        39 lgrRFn   44 (57)
T signal           39 LGRRFN   44 (57)
T 109              37 LGRRLN   42 (55)
T 119              39 SGRRFN   44 (57)
T 120              37 LGRRFN   42 (55)
T 129              35 LGRRFD   40 (53)
T 130              39 FGRRFT   44 (57)
T 136              39 FPCRFN   44 (57)
T 117              38 LSPRFN   43 (56)
T 116              14 SLPRFH   19 (32)
Confidence            346774


No 3  
>Q93170_22_Mito_ref_sig5_130
Probab=6.39  E-value=2.5  Score=9.83  Aligned_cols=6  Identities=50%  Similarity=0.816  Sum_probs=3.1

Q 5_Mustela         1 LRRRFH    6 (7)
Q 2_Macaca          1 LKRRFH    6 (7)
Q 11_Cavia          1 LKRRFH    6 (7)
Q Consensus         1 lkrrfh    6 (7)
                      |||-|-
T Consensus        13 lkrifc   18 (27)
T signal           13 LKRIFC   18 (27)
Confidence            456553


No 4  
>P22354_18_Mito_ref_sig5_130
Probab=4.93  E-value=3.5  Score=9.16  Aligned_cols=4  Identities=75%  Similarity=1.456  Sum_probs=2.5

Q 5_Mustela         3 RRFH    6 (7)
Q 2_Macaca          3 RRFH    6 (7)
Q 11_Cavia          3 RRFH    6 (7)
Q Consensus         3 rrfh    6 (7)
                      |-||
T Consensus         9 rsfh   12 (23)
T signal            9 RSFH   12 (23)
Confidence            5576


No 5  
>P12063_32_MM_ref_sig5_130
Probab=4.37  E-value=4.1  Score=9.82  Aligned_cols=1  Identities=100%  Similarity=1.564  Sum_probs=0.3

Q 5_Mustela         3 R    3 (7)
Q 2_Macaca          3 R    3 (7)
Q 11_Cavia          3 R    3 (7)
Q Consensus         3 r    3 (7)
                      |
T Consensus        35 r   35 (37)
T signal           35 R   35 (37)
T 193              35 R   35 (37)
Confidence            2


No 6  
>Q9N2I8_21_Mito_ref_sig5_130
Probab=4.21  E-value=4.3  Score=9.10  Aligned_cols=4  Identities=50%  Similarity=0.941  Sum_probs=2.1

Q 5_Mustela         4 RFHR    7 (7)
Q 2_Macaca          4 RFHW    7 (7)
Q 11_Cavia          4 RFHR    7 (7)
Q Consensus         4 rfh~    7 (7)
                      ||.|
T Consensus        10 rFRw   13 (26)
T signal           10 RFRG   13 (26)
T 69               10 RFRW   13 (34)
T 78               10 RFRS   13 (34)
T 5                10 LFRW   13 (29)
T 34               10 RFRW   13 (41)
T 60               10 RFRP   13 (36)
T 25               10 RFRW   13 (34)
T 88               10 RFRW   13 (34)
T 46               10 LFRW   13 (25)
Confidence            5654


No 7  
>Q99757_59_Mito_ref_sig5_130
Probab=4.21  E-value=4.3  Score=10.53  Aligned_cols=3  Identities=100%  Similarity=1.818  Sum_probs=1.6

Q 5_Mustela         3 RRF    5 (7)
Q 2_Macaca          3 RRF    5 (7)
Q 11_Cavia          3 RRF    5 (7)
Q Consensus         3 rrf    5 (7)
                      |||
T Consensus         8 RRf   10 (64)
T signal            8 RRF   10 (64)
T 80                8 RRF   10 (62)
T 82                8 RRC   10 (64)
T 81                8 RRL   10 (65)
T 88                8 RRF   10 (64)
T 78                8 RRF   10 (66)
T 61                8 RRL   10 (47)
T 65                8 RRL   10 (43)
T 77                8 RRL   10 (70)
T 76                7 RPF    9 (68)
Confidence            555


No 8  
>Q9YHT1_44_IM_ref_sig5_130
Probab=3.94  E-value=4.6  Score=9.89  Aligned_cols=4  Identities=75%  Similarity=1.497  Sum_probs=2.1

Q 5_Mustela         3 RRFH    6 (7)
Q 2_Macaca          3 RRFH    6 (7)
Q 11_Cavia          3 RRFH    6 (7)
Q Consensus         3 rrfh    6 (7)
                      |-||
T Consensus        32 R~FH   35 (49)
T signal           32 RNFH   35 (49)
T 127              26 RDFH   29 (43)
T 141              30 RGFD   33 (47)
T 160              32 RGFH   35 (49)
T 126              30 RDFH   33 (47)
T 129              29 RQLH   32 (45)
T 133              31 RNFH   34 (47)
T 140              34 RNFH   37 (51)
T 163              31 RNFH   34 (48)
T 176              27 RNFY   30 (44)
Confidence            4455


No 9  
>P32445_17_Mito_ref_sig5_130
Probab=3.91  E-value=4.7  Score=8.68  Aligned_cols=4  Identities=75%  Similarity=1.206  Sum_probs=1.9

Q 5_Mustela         3 RRFH    6 (7)
Q 2_Macaca          3 RRFH    6 (7)
Q 11_Cavia          3 RRFH    6 (7)
Q Consensus         3 rrfh    6 (7)
                      |-||
T Consensus         8 R~fh   11 (22)
T signal            8 RFFH   11 (22)
T 7                 8 RSLH   11 (22)
T 8                 8 RFFS   11 (22)
T 12                8 RGFH   11 (22)
T 15                8 RSFH   11 (22)
T 16                7 RSFH   10 (21)
T 17                7 RSLH   10 (21)
T 11                4 RQFH    7 (18)
T 14                4 RQFH    7 (18)
T 5                 8 RAFS   11 (22)
Confidence            3455


No 10 
>Q64591_34_Mito_ref_sig5_130
Probab=3.85  E-value=4.8  Score=9.57  Aligned_cols=3  Identities=67%  Similarity=1.464  Sum_probs=1.6

Q 5_Mustela         3 RRF    5 (7)
Q 2_Macaca          3 RRF    5 (7)
Q 11_Cavia          3 RRF    5 (7)
Q Consensus         3 rrf    5 (7)
                      |||
T Consensus        22 rRF   24 (39)
T signal           22 QRF   24 (39)
T 76               22 RRF   24 (39)
T 78               22 RRF   24 (39)
T 52               16 RRF   18 (32)
T 55               20 HKF   22 (36)
T 80               22 RRF   24 (38)
T 31               11 ARF   13 (28)
T 34                9 -EF   10 (25)
T 46               11 RRF   13 (28)
T 85               12 GRF   14 (29)
Confidence            455


No 11 
>Q95108_59_Mito_ref_sig5_130
Probab=3.79  E-value=4.9  Score=10.34  Aligned_cols=3  Identities=100%  Similarity=1.818  Sum_probs=1.5

Q 5_Mustela         3 RRF    5 (7)
Q 2_Macaca          3 RRF    5 (7)
Q 11_Cavia          3 RRF    5 (7)
Q Consensus         3 rrf    5 (7)
                      |||
T Consensus         8 RRf   10 (64)
T signal            8 RRF   10 (64)
T 82                8 RRL   10 (65)
T 80                8 RRF   10 (62)
T 79                8 RRF   10 (66)
T 58                8 RRL   10 (47)
T 77                8 KRV   10 (67)
T 78                8 RRL   10 (70)
T 76                7 RPF    9 (68)
Confidence            454


No 12 
>P07257_16_IM_ref_sig5_130
Probab=3.32  E-value=5.7  Score=8.28  Aligned_cols=4  Identities=50%  Similarity=0.924  Sum_probs=1.9

Q 5_Mustela         3 RRFH    6 (7)
Q 2_Macaca          3 RRFH    6 (7)
Q 11_Cavia          3 RRFH    6 (7)
Q Consensus         3 rrfh    6 (7)
                      ||++
T Consensus        15 R~~s   18 (21)
T signal           15 RRLT   18 (21)
T 20               12 RRYT   15 (18)
T 34               12 RRYT   15 (18)
T 14               13 RHYS   16 (19)
T 15               14 RKFS   17 (20)
T 23               16 RRLS   19 (22)
T 19               14 RHLS   17 (20)
T 40               16 RHYH   19 (22)
T 42               14 RKLS   17 (20)
Confidence            4543


No 13 
>Q0QF01_42_IM_ref_sig5_130
Probab=3.26  E-value=5.8  Score=9.58  Aligned_cols=4  Identities=75%  Similarity=1.456  Sum_probs=2.0

Q 5_Mustela         3 RRFH    6 (7)
Q 2_Macaca          3 RRFH    6 (7)
Q 11_Cavia          3 RRFH    6 (7)
Q Consensus         3 rrfh    6 (7)
                      |-||
T Consensus        31 R~FH   34 (47)
T signal           31 RSFH   34 (47)
T 124              31 RDFH   34 (47)
T 126              29 RKLH   32 (43)
T 136              34 RNFH   37 (50)
T 137              31 RNFH   34 (46)
T 144              32 RNLH   35 (48)
T 145              31 RNFH   34 (47)
T 171              28 CGFH   31 (44)
T 154              14 RKFH   17 (30)
Confidence            4455


No 14 
>P20069_32_MM_ref_sig5_130
Probab=3.20  E-value=6  Score=9.27  Aligned_cols=3  Identities=100%  Similarity=1.818  Sum_probs=1.5

Q 5_Mustela         3 RRF    5 (7)
Q 2_Macaca          3 RRF    5 (7)
Q 11_Cavia          3 RRF    5 (7)
Q Consensus         3 rrf    5 (7)
                      |+|
T Consensus        30 RqF   32 (37)
T signal           30 RRF   32 (37)
T 143              30 RQF   32 (37)
T 58               31 RRF   33 (37)
T 118              34 RQF   36 (40)
T 119              32 RRF   34 (38)
T 123              31 RQF   33 (37)
T 127              32 RGF   34 (38)
T 129              32 RQF   34 (38)
T 134              31 RQF   33 (37)
T 121              21 RRF   23 (27)
Confidence            455


No 15 
>P12686_37_Mito_ref_sig5_130
Probab=3.18  E-value=6  Score=9.54  Aligned_cols=3  Identities=100%  Similarity=1.818  Sum_probs=1.8

Q 5_Mustela         3 RRF    5 (7)
Q 2_Macaca          3 RRF    5 (7)
Q 11_Cavia          3 RRF    5 (7)
Q Consensus         3 rrf    5 (7)
                      |||
T Consensus        35 rrf   37 (42)
T signal           35 RRF   37 (42)
Confidence            566


No 16 
>P22068_44_Mito_IM_ref_sig5_130
Probab=2.76  E-value=7.2  Score=9.58  Aligned_cols=3  Identities=100%  Similarity=1.818  Sum_probs=1.6

Q 5_Mustela         3 RRF    5 (7)
Q 2_Macaca          3 RRF    5 (7)
Q 11_Cavia          3 RRF    5 (7)
Q Consensus         3 rrf    5 (7)
                      |||
T Consensus        11 rrf   13 (49)
T signal           11 RRF   13 (49)
Confidence            455


No 17 
>P25711_33_IM_ref_sig5_130
Probab=2.65  E-value=7.5  Score=9.05  Aligned_cols=3  Identities=33%  Similarity=0.335  Sum_probs=1.6

Q 5_Mustela         3 RRF    5 (7)
Q 2_Macaca          3 RRF    5 (7)
Q 11_Cavia          3 RRF    5 (7)
Q Consensus         3 rrf    5 (7)
                      |||
T Consensus        30 RR~   32 (38)
T signal           30 QRR   32 (38)
T 153              28 RRA   30 (36)
T 150              29 RRY   31 (36)
T 151              29 RRY   31 (36)
T 152              28 RRF   30 (35)
T 146              30 RRH   32 (35)
T 149              32 RRF   34 (38)
T 147              28 RRF   30 (34)
T 148              28 RRF   30 (34)
Confidence            555


No 18 
>P04182_35_MM_ref_sig5_130
Probab=2.37  E-value=8.6  Score=8.78  Aligned_cols=4  Identities=25%  Similarity=0.526  Sum_probs=1.5

Q 5_Mustela         3 RRFH    6 (7)
Q 2_Macaca          3 RRFH    6 (7)
Q 11_Cavia          3 RRFH    6 (7)
Q Consensus         3 rrfh    6 (7)
                      |-+|
T Consensus        16 rgih   19 (40)
T signal           16 RGLR   19 (40)
T 138              16 GNIK   19 (40)
T 140              16 RGVH   19 (40)
T 134              18 RSIH   21 (42)
T 141              16 RGLH   19 (40)
T 153              16 RGIH   19 (40)
T 139              16 RGAH   19 (40)
T 127              15 CGIH   18 (39)
T 128              18 RSIH   21 (42)
T 132              17 QNLP   20 (41)
Confidence            3333


No 19 
>Q40089_21_Mito_IM_ref_sig5_130
Probab=2.31  E-value=8.9  Score=8.21  Aligned_cols=4  Identities=75%  Similarity=1.290  Sum_probs=1.7

Q 5_Mustela         2 RRRF    5 (7)
Q 2_Macaca          2 KRRF    5 (7)
Q 11_Cavia          2 KRRF    5 (7)
Q Consensus         2 krrf    5 (7)
                      +|+|
T Consensus        18 rR~f   21 (26)
T signal           18 RRPF   21 (26)
T 44               18 RRPF   21 (26)
T 43               18 TRSF   21 (26)
T 40               16 TRRF   19 (23)
T 41               16 TRRF   19 (23)
T 3                18 RRSY   21 (24)
T 5                18 RRTY   21 (24)
T 10               18 RRTY   21 (24)
T 11               18 RRGY   21 (24)
Confidence            3444


No 20 
>P39112_41_MM_ref_sig5_130
Probab=2.21  E-value=9.3  Score=9.14  Aligned_cols=3  Identities=67%  Similarity=1.420  Sum_probs=1.2

Q 5_Mustela         4 RFH    6 (7)
Q 2_Macaca          4 RFH    6 (7)
Q 11_Cavia          4 RFH    6 (7)
Q Consensus         4 rfh    6 (7)
                      -||
T Consensus        15 sfh   17 (46)
T signal           15 SFH   17 (46)
Confidence            344


Done!
