Query         5_Mustela
Match_columns 6
No_of_seqs    1 out of 20
Neff          1.0 
Searched_HMMs 436
Date          Wed Sep  6 16:10:08 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_95A101_95Q101_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P36967_16_Mito_ref_sig5_130      0.6      40   0.091    6.0   0.0    1    4-4       4-4   (21)
  2 P09671_24_MM_ref_sig5_130        0.6      40   0.092    6.2   0.0    1    3-3      18-18  (29)
  3 P35914_27_MM_Pe_ref_sig5_130     0.6      41   0.093    6.5   0.0    1    3-3      14-14  (32)
  4 P22315_53_IM_ref_sig5_130        0.6      42   0.096    7.2   0.0    1    3-3       5-5   (58)
  5 P04179_24_MM_ref_sig5_130        0.5      47    0.11    6.0   0.0    1    3-3      18-18  (29)
  6 Q9N2I8_21_Mito_ref_sig5_130      0.5      50    0.12    6.0   0.0    1    4-4      22-22  (26)
  7 P20000_21_MM_ref_sig5_130        0.5      51    0.12    5.9   0.0    1    2-2      26-26  (26)
  8 P35571_42_Mito_ref_sig5_130      0.5      52    0.12    4.5   0.0    1    2-2      47-47  (47)
  9 P30099_34_MitoM_ref_sig5_130     0.5      53    0.12    4.8   0.0    1    1-1       1-1   (39)
 10 P12234_49_IM_ref_sig5_130        0.5      55    0.13    4.4   0.0    1    2-2      54-54  (54)
 11 P23965_28_MM_ref_sig5_130        0.5      57    0.13    6.1   0.0    1    2-2      33-33  (33)
 12 P30084_27_MM_ref_sig5_130        0.4      58    0.13    6.0   0.0    1    3-3      30-30  (32)
 13 Q6UPE0_34_IM_ref_sig5_130        0.4      58    0.13    3.0   0.0    1    2-2      39-39  (39)
 14 P14604_29_MM_ref_sig5_130        0.4      59    0.13    6.1   0.0    2    3-4      30-31  (34)
 15 P11884_19_MM_ref_sig5_130        0.4      59    0.14    5.7   0.0    1    2-2      24-24  (24)
 16 P09622_35_MM_ref_sig5_130        0.4      61    0.14    2.8   0.0    1    2-2      40-40  (40)
 17 P32089_13_IM_ref_sig5_130        0.4      62    0.14    5.3   0.0    1    2-2      18-18  (18)
 18 P00507_29_MM_Cm_ref_sig5_130     0.4      64    0.15    4.0   0.0    1    1-1       1-1   (34)
 19 Q25423_17_IM_ref_sig5_130        0.4      68    0.16    5.5   0.0    1    4-4      13-13  (22)
 20 P16276_27_Mito_ref_sig5_130      0.4      69    0.16    5.9   0.0    1    3-3      16-16  (32)

No 1  
>P36967_16_Mito_ref_sig5_130
Probab=0.63  E-value=40  Score=6.03  Aligned_cols=1  Identities=100%  Similarity=0.800  Sum_probs=0.3

Q 5_Mustela         4 A    4 (6)
Q Consensus         4 a    4 (6)
                      |
T Consensus         4 a    4 (21)
T signal            4 A    4 (21)
Confidence            2


No 2  
>P09671_24_MM_ref_sig5_130
Probab=0.62  E-value=40  Score=6.17  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.3

Q 5_Mustela         3 G..    3 (6)
Q Consensus         3 g..    3 (6)
                      |  
T Consensus        18 g..   18 (29)
T signal           18 G..   18 (29)
T 128              20 Swk   22 (33)
T 134              20 G..   20 (31)
T 138              20 T..   20 (31)
T 124              19 A..   19 (30)
T 145              18 G..   18 (29)
T 149              20 A..   20 (31)
T 156              18 G..   18 (29)
T 113              19 A..   19 (30)
T 114              18 G..   18 (29)
Confidence            2  


No 3  
>P35914_27_MM_Pe_ref_sig5_130
Probab=0.62  E-value=41  Score=6.49  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.3

Q 5_Mustela         3 G    3 (6)
Q Consensus         3 g    3 (6)
                      |
T Consensus        14 g   14 (32)
T signal           14 G   14 (32)
T 49               15 G   15 (33)
T 68               14 G   14 (32)
T 51               14 -   13 (26)
T 52               13 -   12 (25)
T 75               14 G   14 (32)
T 76               14 G   14 (32)
T 77               14 G   14 (32)
T 78               14 G   14 (32)
T 59               14 -   13 (29)
Confidence            2


No 4  
>P22315_53_IM_ref_sig5_130
Probab=0.60  E-value=42  Score=7.23  Aligned_cols=1  Identities=0%  Similarity=0.136  Sum_probs=0.2

Q 5_Mustela         3 G    3 (6)
Q Consensus         3 g    3 (6)
                      |
T Consensus         5 g    5 (58)
T signal            5 S    5 (58)
T 170               5 S    5 (59)
T 162               1 -    0 (32)
T 165               1 -    0 (36)
T 155               1 -    0 (40)
T 141               4 G    4 (54)
T 142               5 H    5 (44)
T 148               1 -    0 (39)
T 137               1 -    0 (41)
Confidence            2


No 5  
>P04179_24_MM_ref_sig5_130
Probab=0.54  E-value=47  Score=5.97  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.2

Q 5_Mustela         3 G.    3 (6)
Q Consensus         3 g.    3 (6)
                      | 
T Consensus        18 g.   18 (29)
T signal           18 G.   18 (29)
T 162              18 G.   18 (29)
T 127              20 Rq   21 (32)
T 133              20 G.   20 (31)
T 135              20 A.   20 (31)
T 139              20 T.   20 (31)
T 118              11 P.   11 (22)
T 123              11 A.   11 (22)
T 113              19 A.   19 (30)
T 117              18 -.   17 (26)
Confidence            2 


No 6  
>Q9N2I8_21_Mito_ref_sig5_130
Probab=0.51  E-value=50  Score=6.00  Aligned_cols=1  Identities=100%  Similarity=0.800  Sum_probs=0.2

Q 5_Mustela         4 A.......    4 (6)
Q Consensus         4 a.......    4 (6)
                      |       
T Consensus        22 A.......   22 (26)
T signal           22 A.......   22 (26)
T 69               25 Aargaa..   30 (34)
T 78               25 Aanav...   29 (34)
T 5                25 A.......   25 (29)
T 34               32 Aarsaa..   37 (41)
T 60               25 Aagaasaa   32 (36)
T 25               25 Aagaaa..   30 (34)
T 88               25 Aaggaa..   30 (34)
T 46               25 A.......   25 (25)
Confidence            2       


No 7  
>P20000_21_MM_ref_sig5_130
Probab=0.50  E-value=51  Score=5.93  Aligned_cols=1  Identities=0%  Similarity=-0.495  Sum_probs=0.0

Q 5_Mustela         2 V    2 (6)
Q Consensus         2 v    2 (6)
                      -
T Consensus        26 ~   26 (26)
T signal           26 Q   26 (26)
T 54               26 -   25 (25)
T 69               24 -   23 (23)
T 79               26 -   25 (25)
T 81               26 -   25 (25)
T 42               25 -   24 (24)
T 43               23 -   22 (22)
T 49               29 -   28 (28)
T 50               24 -   23 (23)
T 78               26 -   25 (25)
Confidence            0


No 8  
>P35571_42_Mito_ref_sig5_130
Probab=0.49  E-value=52  Score=4.53  Aligned_cols=1  Identities=0%  Similarity=-0.329  Sum_probs=0.0

Q 5_Mustela         2 V    2 (6)
Q Consensus         2 v    2 (6)
                      -
T Consensus        47 ~   47 (47)
T signal           47 S   47 (47)
T 85_Nematostell   47 -   46 (46)
T 97_Amphimedon    47 -   46 (46)
T 64_Oreochromis   47 -   46 (46)
T 74_Pongo         47 -   46 (46)
T 158_Laccaria     47 -   46 (46)
T 73_Ciona         47 -   46 (46)
T 81_Drosophila    47 -   46 (46)
T 112_Aplysia      47 -   46 (46)
T 58_Columba       47 -   46 (46)
Confidence            0


No 9  
>P30099_34_MitoM_ref_sig5_130
Probab=0.48  E-value=53  Score=4.75  Aligned_cols=1  Identities=0%  Similarity=-0.230  Sum_probs=0.0

Q 5_Mustela         1 A    1 (6)
Q Consensus         1 a    1 (6)
                      .
T Consensus         1 ~    1 (39)
T signal            1 M    1 (39)
T 46_Amphimedon     1 -    0 (28)
T 44_Anolis         1 -    0 (28)
T 42_Monodelphis    1 -    0 (28)
T 39_Sarcophilus    1 -    0 (28)
T 35_Mustela        1 -    0 (28)
T 7_Heterocephal    1 -    0 (28)
T cl|DABBABABA|1    1 -    0 (28)
T cl|RABBABABA|1    1 -    0 (28)
T cl|VABBABABA|2    1 -    0 (28)
Confidence            0


No 10 
>P12234_49_IM_ref_sig5_130
Probab=0.47  E-value=55  Score=4.38  Aligned_cols=1  Identities=0%  Similarity=-0.495  Sum_probs=0.0

Q 5_Mustela         2 V    2 (6)
Q Consensus         2 v    2 (6)
                      -
T Consensus        54 ~   54 (54)
T signal           54 Q   54 (54)
T 124_Salpingoec   42 -   41 (41)
T 115_Sorghum      42 -   41 (41)
T 97_Branchiosto   42 -   41 (41)
T 80_Trichoplax    42 -   41 (41)
T 66_Ciona         42 -   41 (41)
T 62_Geospiza      42 -   41 (41)
T 58_Anas          42 -   41 (41)
T 57_Falco         42 -   41 (41)
T 54_Ficedula      42 -   41 (41)
Confidence            0


No 11 
>P23965_28_MM_ref_sig5_130
Probab=0.45  E-value=57  Score=6.09  Aligned_cols=1  Identities=0%  Similarity=-0.662  Sum_probs=0.0

Q 5_Mustela         2 V    2 (6)
Q Consensus         2 v    2 (6)
                      =
T Consensus        33 R   33 (33)
T signal           33 R   33 (33)
T 94               43 R   43 (43)
T 93               46 R   46 (46)
T 72               49 R   49 (49)
T 79               46 R   46 (46)
T 63               34 R   34 (34)
T 78               37 R   37 (37)
T 39               44 R   44 (44)
Confidence            0


No 12 
>P30084_27_MM_ref_sig5_130
Probab=0.45  E-value=58  Score=6.02  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.3

Q 5_Mustela         3 G    3 (6)
Q Consensus         3 g    3 (6)
                      |
T Consensus        30 G   30 (32)
T signal           30 G   30 (32)
T 128              30 G   30 (32)
T 131              30 S   30 (32)
T 140              30 G   30 (32)
T 136              30 G   30 (31)
T 135              27 G   27 (29)
T 120              30 G   30 (32)
Confidence            3


No 13 
>Q6UPE0_34_IM_ref_sig5_130
Probab=0.44  E-value=58  Score=3.03  Aligned_cols=1  Identities=0%  Similarity=0.036  Sum_probs=0.0

Q 5_Mustela         2 V    2 (6)
Q Consensus         2 v    2 (6)
                      -
T Consensus        39 ~   39 (39)
T signal           39 A   39 (39)
T 99_Culex         38 -   37 (37)
T 93_Megachile     38 -   37 (37)
T 68_Ciona         38 -   37 (37)
T 66_Xiphophorus   38 -   37 (37)
T 61_Oryzias       38 -   37 (37)
Confidence            0


No 14 
>P14604_29_MM_ref_sig5_130
Probab=0.44  E-value=59  Score=6.07  Aligned_cols=2  Identities=100%  Similarity=1.498  Sum_probs=0.7

Q 5_Mustela         3 GA    4 (6)
Q Consensus         3 ga    4 (6)
                      ||
T Consensus        30 Ga   31 (34)
T signal           30 GA   31 (34)
T 122              30 GA   31 (34)
T 133              30 GA   31 (34)
T 142              30 SA   31 (34)
T 145              30 GA   31 (34)
T 146              30 GA   31 (34)
T 152              27 GT   28 (31)
T 139              30 GG   31 (34)
T 129              30 GT   31 (34)
T 120              30 GG   31 (34)
Confidence            33


No 15 
>P11884_19_MM_ref_sig5_130
Probab=0.43  E-value=59  Score=5.65  Aligned_cols=1  Identities=0%  Similarity=0.003  Sum_probs=0.0

Q 5_Mustela         2 V    2 (6)
Q Consensus         2 v    2 (6)
                      -
T Consensus        24 t   24 (24)
T signal           24 T   24 (24)
T 63               26 A   26 (26)
T 45               25 A   25 (25)
T 46               29 -   28 (28)
T 49               24 A   24 (24)
T 52               29 -   28 (28)
T 55               24 T   24 (24)
T 58               26 T   26 (26)
T 60               26 T   26 (26)
T 70               26 T   26 (26)
Confidence            0


No 16 
>P09622_35_MM_ref_sig5_130
Probab=0.42  E-value=61  Score=2.82  Aligned_cols=1  Identities=0%  Similarity=1.032  Sum_probs=0.0

Q 5_Mustela         2 V    2 (6)
Q Consensus         2 v    2 (6)
                      +
T Consensus        40 ~   40 (40)
T signal           40 I   40 (40)
T 181_Paramecium   39 -   38 (38)
T 142_Physcomitr   39 -   38 (38)
T 97_Anopheles     39 -   38 (38)
T 92_Brugia        39 -   38 (38)
T 77_Tursiops      39 -   38 (38)
T 74_Ciona         39 -   38 (38)
T 62_Meleagris     39 -   38 (38)
T 17_Ceratotheri   39 -   38 (38)
Confidence            0


No 17 
>P32089_13_IM_ref_sig5_130
Probab=0.41  E-value=62  Score=5.31  Aligned_cols=1  Identities=0%  Similarity=-1.093  Sum_probs=0.0

Q 5_Mustela         2 V    2 (6)
Q Consensus         2 v    2 (6)
                      -
T Consensus        18 g   18 (18)
T signal           18 G   18 (18)
T 136              16 G   16 (16)
T 126              18 G   18 (18)
T 127              18 K   18 (18)
Confidence            0


No 18 
>P00507_29_MM_Cm_ref_sig5_130
Probab=0.41  E-value=64  Score=4.05  Aligned_cols=1  Identities=0%  Similarity=-0.230  Sum_probs=0.0

Q 5_Mustela         1 A    1 (6)
Q Consensus         1 a    1 (6)
                      -
T Consensus         1 M    1 (34)
T signal            1 M    1 (34)
T 39_Myotis         1 M    1 (31)
T 158_Cryptococc    1 M    1 (31)
T 102_Metaseiulu    1 M    1 (31)
T 104_Pediculus     1 M    1 (31)
T 111_Neosartory    1 M    1 (31)
T 72_Nomascus       1 S    1 (31)
T 43_Sarcophilus    1 M    1 (31)
T 78_Aplysia        1 M    1 (31)
Confidence            0


No 19 
>Q25423_17_IM_ref_sig5_130
Probab=0.38  E-value=68  Score=5.46  Aligned_cols=1  Identities=100%  Similarity=0.800  Sum_probs=0.2

Q 5_Mustela         4 A    4 (6)
Q Consensus         4 a    4 (6)
                      |
T Consensus        13 a   13 (22)
T signal           13 A   13 (22)
T cl|CABBABABA|1   16 A   16 (25)
Confidence            2


No 20 
>P16276_27_Mito_ref_sig5_130
Probab=0.38  E-value=69  Score=5.86  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.2

Q 5_Mustela         3 G..    3 (6)
Q Consensus         3 g..    3 (6)
                      |  
T Consensus        16 G..   16 (32)
T signal           16 G..   16 (32)
T 50               18 G..   18 (34)
T 97               18 G..   18 (34)
T 111              18 N..   18 (34)
T 112              16 G..   16 (32)
T 113              20 G..   20 (36)
T 122              20 Gsg   22 (38)
T 115              17 G..   17 (33)
T 105              18 G..   18 (34)
T 108              18 G..   18 (34)
Confidence            2  


Done!
