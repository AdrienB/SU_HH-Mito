Query         5_Mustela
Match_columns 6
No_of_seqs    1 out of 20
Neff          1.0 
Searched_HMMs 436
Date          Wed Sep  6 16:10:04 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_92A98_92Q98_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P38447_48_Mito_ref_sig5_130      0.5      50    0.11    6.8   0.0    1    2-2      53-53  (53)
  2 P21801_20_IM_ref_sig5_130        0.5      55    0.13    5.9   0.0    3    2-4      18-20  (25)
  3 Q01992_33_MM_ref_sig5_130        0.5      55    0.13    6.4   0.0    1    2-2      38-38  (38)
  4 Q9NRK6_105_IM_ref_sig5_130       0.5      57    0.13    7.7   0.0    1    2-2      85-85  (110)
  5 P07471_12_IM_ref_sig5_130        0.4      58    0.13    5.3   0.0    3    2-4      11-13  (17)
  6 Q04467_39_Mito_ref_sig5_130      0.4      59    0.14    3.2   0.0    1    1-1       1-1   (44)
  7 P28350_44_Mito_Cy_ref_sig5_130   0.4      65    0.15    6.5   0.0    1    5-5      10-10  (49)
  8 Q9UQX0_21_MM_ref_sig5_130        0.4      66    0.15    5.7   0.0    1    4-4      19-19  (26)
  9 P35914_27_MM_Pe_ref_sig5_130     0.4      67    0.15    5.9   0.0    1    2-2      14-14  (32)
 10 P15690_23_IM_ref_sig5_130        0.4      67    0.15    5.7   0.0    1    2-2      12-12  (28)
 11 O46419_25_Mito_ref_sig5_130      0.4      71    0.16    5.7   0.0    1    2-2      23-23  (30)
 12 Q0P5I5_25_Mito_ref_sig5_130      0.4      71    0.16    5.7   0.0    1    2-2      30-30  (30)
 13 P0C2B9_31_Mito_ref_sig5_130      0.4      72    0.17    5.8   0.0    1    4-4       4-4   (36)
 14 P36967_16_Mito_ref_sig5_130      0.3      76    0.17    5.3   0.0    1    2-2       5-5   (21)
 15 P00366_57_MM_ref_sig5_130        0.3      76    0.17    4.3   0.0    1    2-2      62-62  (62)
 16 P09457_17_Mito_IM_ref_sig5_130   0.3      76    0.18    5.3   0.0    1    2-2      17-17  (22)
 17 Q49AM1_35_Mito_MM_Nu_ref_sig5_   0.3      79    0.18    3.8   0.0    1    2-2      40-40  (40)
 18 P12234_49_IM_ref_sig5_130        0.3      81    0.19    4.0   0.0    1    1-1       1-1   (54)
 19 P53875_73_Mito_ref_sig5_130      0.3      81    0.19    6.6   0.0    1    1-1       1-1   (78)
 20 P30048_62_Mito_ref_sig5_130      0.3      84    0.19    4.0   0.0    1    1-1       1-1   (67)

No 1  
>P38447_48_Mito_ref_sig5_130
Probab=0.51  E-value=50  Score=6.85  Aligned_cols=1  Identities=0%  Similarity=0.169  Sum_probs=0.0

Q 5_Mustela         2 G    2 (6)
Q Consensus         2 g    2 (6)
                      .
T Consensus        53 ~   53 (53)
T signal           53 A   53 (53)
T 102              47 -   46 (46)
T 113              48 A   48 (48)
T 111              51 -   50 (50)
T 63               42 -   41 (41)
T 93               45 -   44 (44)
T 97               52 -   51 (51)
T 103              50 -   49 (49)
T 96               39 -   38 (38)
T 101              45 -   44 (44)
Confidence            0


No 2  
>P21801_20_IM_ref_sig5_130
Probab=0.47  E-value=55  Score=5.86  Aligned_cols=3  Identities=67%  Similarity=1.276  Sum_probs=1.2

Q 5_Mustela         2 GIA    4 (6)
Q Consensus         2 gia    4 (6)
                      |.|
T Consensus        18 G~A   20 (25)
T signal           18 GMA   20 (25)
T 192              16 GLA   18 (23)
T 191              14 GVA   16 (21)
Confidence            333


No 3  
>Q01992_33_MM_ref_sig5_130
Probab=0.47  E-value=55  Score=6.39  Aligned_cols=1  Identities=0%  Similarity=-1.327  Sum_probs=0.0

Q 5_Mustela         2 G    2 (6)
Q Consensus         2 g    2 (6)
                      =
T Consensus        38 W   38 (38)
T signal           38 W   38 (38)
T 180              38 W   38 (38)
T 130              37 W   37 (37)
T 102              36 W   36 (36)
T 165              36 W   36 (36)
T 168              36 W   36 (36)
T 182              36 W   36 (36)
T 186              36 W   36 (36)
T 164              36 W   36 (36)
T 169              36 W   36 (36)
Confidence            0


No 4  
>Q9NRK6_105_IM_ref_sig5_130
Probab=0.45  E-value=57  Score=7.70  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.2

Q 5_Mustela         2 G    2 (6)
Q Consensus         2 g    2 (6)
                      |
T Consensus        85 G   85 (110)
T signal           85 G   85 (110)
T 146              65 G   65 (90)
T 153              80 G   80 (105)
T 152              82 -   81 (81)
T 147              69 G   69 (94)
T 151              21 G   21 (45)
T 135              62 G   62 (84)
T 142              55 -   54 (54)
T 141              88 -   87 (87)
T 129              67 -   66 (86)
Confidence            1


No 5  
>P07471_12_IM_ref_sig5_130
Probab=0.44  E-value=58  Score=5.31  Aligned_cols=3  Identities=67%  Similarity=1.309  Sum_probs=1.2

Q 5_Mustela         2 GIA    4 (6)
Q Consensus         2 gia    4 (6)
                      |.|
T Consensus        11 glA   13 (17)
T signal           11 GLA   13 (17)
T 8                11 GLA   13 (17)
T 15               11 SMA   13 (17)
T 17               11 GLA   13 (17)
T 18               11 GLA   13 (17)
T 42               11 GFA   13 (17)
T 3                11 GLA   13 (17)
Confidence            333


No 6  
>Q04467_39_Mito_ref_sig5_130
Probab=0.44  E-value=59  Score=3.16  Aligned_cols=1  Identities=0%  Similarity=-0.230  Sum_probs=0.0

Q 5_Mustela         1 A    1 (6)
Q Consensus         1 a    1 (6)
                      -
T Consensus         1 M    1 (44)
T signal            1 M    1 (44)
T 92_Chlorella      1 M    1 (40)
T 86_Trichoplax     1 M    1 (40)
T 76_Bombyx         1 M    1 (40)
T 69_Meleagris      1 F    1 (40)
T 65_Latimeria      1 A    1 (40)
T 38_Sarcophilus    1 M    1 (40)
Confidence            0


No 7  
>P28350_44_Mito_Cy_ref_sig5_130
Probab=0.40  E-value=65  Score=6.49  Aligned_cols=1  Identities=0%  Similarity=1.032  Sum_probs=0.2

Q 5_Mustela         5 V    5 (6)
Q Consensus         5 v    5 (6)
                      +
T Consensus        10 i   10 (49)
T signal           10 I   10 (49)
Confidence            2


No 8  
>Q9UQX0_21_MM_ref_sig5_130
Probab=0.39  E-value=66  Score=5.70  Aligned_cols=1  Identities=100%  Similarity=0.800  Sum_probs=0.3

Q 5_Mustela         4 A    4 (6)
Q Consensus         4 a    4 (6)
                      |
T Consensus        19 a   19 (26)
T signal           19 A   19 (26)
Confidence            2


No 9  
>P35914_27_MM_Pe_ref_sig5_130
Probab=0.39  E-value=67  Score=5.88  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.2

Q 5_Mustela         2 G    2 (6)
Q Consensus         2 g    2 (6)
                      |
T Consensus        14 g   14 (32)
T signal           14 G   14 (32)
T 49               15 G   15 (33)
T 68               14 G   14 (32)
T 51               14 -   13 (26)
T 52               13 -   12 (25)
T 75               14 G   14 (32)
T 76               14 G   14 (32)
T 77               14 G   14 (32)
T 78               14 G   14 (32)
T 59               14 -   13 (29)
Confidence            2


No 10 
>P15690_23_IM_ref_sig5_130
Probab=0.39  E-value=67  Score=5.69  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.2

Q 5_Mustela         2 G    2 (6)
Q Consensus         2 g    2 (6)
                      |
T Consensus        12 g   12 (28)
T signal           12 G   12 (28)
T 110              13 G   13 (29)
T 99               13 N   13 (34)
T 102              15 A   15 (38)
T 105              13 P   13 (34)
T 107              13 G   13 (32)
T 116              13 A   13 (29)
T 119              12 S   12 (28)
T 123              13 G   13 (31)
T 128              12 G   12 (28)
Confidence            2


No 11 
>O46419_25_Mito_ref_sig5_130
Probab=0.37  E-value=71  Score=5.67  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.3

Q 5_Mustela         2 G    2 (6)
Q Consensus         2 g    2 (6)
                      |
T Consensus        23 g   23 (30)
T signal           23 G   23 (30)
T 118              23 G   23 (29)
T 119              23 G   23 (29)
T 120              23 G   23 (29)
T 122              23 G   23 (29)
T 123              23 G   23 (29)
T 131              23 G   23 (29)
T 133              23 A   23 (29)
T 107              23 G   23 (30)
T 117              22 C   22 (28)
Confidence            3


No 12 
>Q0P5I5_25_Mito_ref_sig5_130
Probab=0.37  E-value=71  Score=5.65  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.0

Q 5_Mustela         2 G    2 (6)
Q Consensus         2 g    2 (6)
                      |
T Consensus        30 G   30 (30)
T signal           30 G   30 (30)
T 80               30 G   30 (30)
T 82               24 G   24 (24)
T 83               30 R   30 (30)
T 88               30 G   30 (30)
T 91               30 G   30 (30)
T 94               30 G   30 (30)
T 95               33 G   33 (33)
T 112              30 G   30 (30)
T 47               30 G   30 (30)
Confidence            0


No 13 
>P0C2B9_31_Mito_ref_sig5_130
Probab=0.36  E-value=72  Score=5.84  Aligned_cols=1  Identities=100%  Similarity=0.800  Sum_probs=0.2

Q 5_Mustela         4 A    4 (6)
Q Consensus         4 a    4 (6)
                      |
T Consensus         4 a    4 (36)
T signal            4 A    4 (36)
T 31                4 T    4 (36)
T 49                4 A    4 (36)
T 26                4 K    4 (36)
T 35                4 A    4 (36)
T 10                1 -    0 (40)
T 24                4 T    4 (42)
T 19                4 A    4 (41)
T 8                 4 A    4 (40)
T 14                4 T    4 (42)
Confidence            2


No 14 
>P36967_16_Mito_ref_sig5_130
Probab=0.35  E-value=76  Score=5.27  Aligned_cols=1  Identities=100%  Similarity=2.195  Sum_probs=0.3

Q 5_Mustela         2 G    2 (6)
Q Consensus         2 g    2 (6)
                      |
T Consensus         5 g    5 (21)
T signal            5 G    5 (21)
Confidence            3


No 15 
>P00366_57_MM_ref_sig5_130
Probab=0.35  E-value=76  Score=4.33  Aligned_cols=1  Identities=0%  Similarity=0.036  Sum_probs=0.0

Q 5_Mustela         2 G    2 (6)
Q Consensus         2 g    2 (6)
                      -
T Consensus        62 ~   62 (62)
T signal           62 D   62 (62)
T 114_Galdieria    44 -   43 (43)
T 110_Naegleria    44 -   43 (43)
T 104_Ichthyopht   44 -   43 (43)
T 102_Guillardia   44 -   43 (43)
T 93_Amphimedon    44 -   43 (43)
T 92_Schistosoma   44 -   43 (43)
T 89_Culex         44 -   43 (43)
T 69_Gallus        44 -   43 (43)
T 24_Saimiri       44 -   43 (43)
Confidence            0


No 16 
>P09457_17_Mito_IM_ref_sig5_130
Probab=0.34  E-value=76  Score=5.30  Aligned_cols=1  Identities=0%  Similarity=0.169  Sum_probs=0.4

Q 5_Mustela         2 G    2 (6)
Q Consensus         2 g    2 (6)
                      |
T Consensus        17 g   17 (22)
T signal           17 A   17 (22)
T 184              17 G   17 (18)
T 180              17 G   17 (18)
T 181              17 G   17 (18)
T 176              17 -   16 (16)
T 175              17 -   16 (16)
T 170              17 -   16 (16)
T 186              17 G   17 (18)
T 177              17 -   16 (16)
T 185              17 -   16 (16)
Confidence            3


No 17 
>Q49AM1_35_Mito_MM_Nu_ref_sig5_130
Probab=0.33  E-value=79  Score=3.82  Aligned_cols=1  Identities=0%  Similarity=-0.330  Sum_probs=0.0

Q 5_Mustela         2 G    2 (6)
Q Consensus         2 g    2 (6)
                      -
T Consensus        40 ~   40 (40)
T signal           40 Q   40 (40)
T 69_Xiphophorus   40 A   40 (40)
T 50_Columba       40 R   40 (40)
T 48_Anas          40 S   40 (40)
T 45_Echinops      40 E   40 (40)
T 40_Cavia         40 A   40 (40)
T 38_Cricetulus    40 C   40 (40)
T 30_Dasypus       40 F   40 (40)
T cl|PIBBABABA|9   40 -   39 (39)
Confidence            0


No 18 
>P12234_49_IM_ref_sig5_130
Probab=0.32  E-value=81  Score=3.97  Aligned_cols=1  Identities=0%  Similarity=-0.230  Sum_probs=0.0

Q 5_Mustela         1 A    1 (6)
Q Consensus         1 a    1 (6)
                      -
T Consensus         1 M    1 (54)
T signal            1 M    1 (54)
T 124_Salpingoec    1 M    1 (41)
T 115_Sorghum       1 M    1 (41)
T 97_Branchiosto    1 L    1 (41)
T 80_Trichoplax     1 M    1 (41)
T 66_Ciona          1 M    1 (41)
T 62_Geospiza       1 M    1 (41)
T 58_Anas           1 K    1 (41)
T 57_Falco          1 M    1 (41)
T 54_Ficedula       1 P    1 (41)
Confidence            0


No 19 
>P53875_73_Mito_ref_sig5_130
Probab=0.32  E-value=81  Score=6.63  Aligned_cols=1  Identities=0%  Similarity=-0.230  Sum_probs=0.0

Q 5_Mustela         1 A    1 (6)
Q Consensus         1 a    1 (6)
                      .
T Consensus         1 m    1 (78)
T signal            1 M    1 (78)
T 27                1 -    0 (71)
T 144               1 -    0 (71)
T 63                1 -    0 (81)
T 139               1 M    1 (77)
T 23                1 -    0 (68)
T 12                1 -    0 (67)
T 52                1 -    0 (69)
T 95                1 -    0 (70)
T 25                1 -    0 (60)
Confidence            0


No 20 
>P30048_62_Mito_ref_sig5_130
Probab=0.31  E-value=84  Score=4.04  Aligned_cols=1  Identities=0%  Similarity=-0.230  Sum_probs=0.0

Q 5_Mustela         1 A    1 (6)
Q Consensus         1 a    1 (6)
                      .
T Consensus         1 M    1 (67)
T signal            1 M    1 (67)
T 79_Drosophila     1 M    1 (67)
T 76_Meleagris      1 M    1 (67)
T 70_Strongyloce    1 M    1 (67)
T 69_Metaseiulus    1 M    1 (67)
T 63_Columba        1 Q    1 (67)
T 57_Falco          1 P    1 (67)
T 44_Camelus        1 M    1 (67)
Confidence            0


Done!
