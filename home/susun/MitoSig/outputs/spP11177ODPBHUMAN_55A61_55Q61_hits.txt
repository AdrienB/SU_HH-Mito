Query         5_Mustela
Match_columns 6
No_of_seqs    1 out of 20
Neff          1.0 
Searched_HMMs 436
Date          Wed Sep  6 16:09:08 2017
Command       hhsearch -i /export2/susun/MitoSig/datapath/spP11177ODPBHUMANcut.hhm -d /export2/susun/MitoSig/modelpath/mTP.hhm -p 0 -e 100 -seq 10 -Z 20 -B 20 -o /export2/susun/MitoSig/morfsite_lite/outputs/spP11177ODPBHUMAN_55A61_55Q61_hits.txt 

 No Hit                             Prob E-value P-value  Score    SS Cols Query HMM  Template HMM
  1 P32473_33_MM_ref_sig5_130        0.3      95    0.22    5.7   0.0    1    2-2      38-38  (38)
  2 Q99797_35_MM_ref_sig5_130        0.3      98    0.22    5.7   0.0    1    2-2      40-40  (40)
  3 P43265_42_IM_ref_sig5_130        0.3      99    0.23    5.9   0.0    1    2-2      47-47  (47)
  4 P24918_33_IM_ref_sig5_130        0.3   1E+02    0.23    5.5   0.0    1    4-4      35-35  (38)
  5 Q05046_32_Mito_ref_sig5_130      0.3   1E+02    0.23    3.2   0.0    4    2-5      34-37  (37)
  6 P12063_32_MM_ref_sig5_130        0.3   1E+02    0.24    5.5   0.0    5    2-6      12-16  (37)
  7 P10817_9_IM_ref_sig5_130         0.2 1.1E+02    0.24    4.4   0.0    6    1-6       5-10  (14)
  8 P51557_62_Mito_ref_sig5_130      0.2 1.1E+02    0.24    4.4   0.0    6    1-6      61-66  (67)
  9 P26360_45_Mito_IM_ref_sig5_130   0.2 1.1E+02    0.25    5.8   0.0    4    3-6      36-39  (50)
 10 P15538_24_MitoM_ref_sig5_130     0.2 1.1E+02    0.25    5.2   0.0    6    1-6      24-29  (29)
 11 P22142_42_IM_ref_sig5_130        0.2 1.2E+02    0.27    4.0   0.0    3    4-6      44-46  (47)
 12 P28350_44_Mito_Cy_ref_sig5_130   0.2 1.2E+02    0.27    5.7   0.0    3    2-4      46-48  (49)
 13 P25348_71_Mito_ref_sig5_130      0.2 1.2E+02    0.27    6.2   0.0    6    1-6       6-11  (76)
 14 P21839_50_MM_ref_sig5_130        0.2 1.2E+02    0.28    5.8   0.0    5    2-6      42-46  (55)
 15 P49448_53_MM_ref_sig5_130        0.2 1.2E+02    0.28    4.5   0.0    4    3-6      55-58  (58)
 16 P23004_14_IM_ref_sig5_130        0.2 1.3E+02    0.29    4.5   0.0    2    5-6      18-19  (19)
 17 Q01992_33_MM_ref_sig5_130        0.2 1.3E+02    0.29    5.3   0.0    6    1-6      16-21  (38)
 18 P22354_18_Mito_ref_sig5_130      0.2 1.3E+02     0.3    4.7   0.0    5    1-5       2-6   (23)
 19 P82919_34_Mito_ref_sig5_130      0.2 1.3E+02     0.3    5.2   0.0    2    4-5      37-38  (39)
 20 P13621_23_Mito_IM_ref_sig5_130   0.2 1.4E+02    0.31    4.8   0.0    5    1-5       9-13  (28)

No 1  
>P32473_33_MM_ref_sig5_130
Probab=0.28  E-value=95  Score=5.69  Aligned_cols=1  Identities=0%  Similarity=-0.363  Sum_probs=0.0

Q 5_Mustela         2 G    2 (6)
Q Consensus         2 g    2 (6)
                      -
T Consensus        38 t   38 (38)
T signal           38 T   38 (38)
Confidence            0


No 2  
>Q99797_35_MM_ref_sig5_130
Probab=0.27  E-value=98  Score=5.69  Aligned_cols=1  Identities=0%  Similarity=-1.327  Sum_probs=0.0

Q 5_Mustela         2 G    2 (6)
Q Consensus         2 g    2 (6)
                      =
T Consensus        40 W   40 (40)
T signal           40 W   40 (40)
T 102              40 W   40 (40)
T 164              40 W   40 (40)
T 180              40 W   40 (40)
T 183              40 W   40 (40)
T 122              39 W   39 (39)
T 158              37 W   37 (37)
T 168              40 W   40 (40)
T 174              40 W   40 (40)
T 155              41 W   41 (41)
Confidence            0


No 3  
>P43265_42_IM_ref_sig5_130
Probab=0.27  E-value=99  Score=5.87  Aligned_cols=1  Identities=0%  Similarity=0.036  Sum_probs=0.0

Q 5_Mustela         2 G    2 (6)
Q Consensus         2 g    2 (6)
                      .
T Consensus        47 d   47 (47)
T signal           47 D   47 (47)
Confidence            0


No 4  
>P24918_33_IM_ref_sig5_130
Probab=0.26  E-value=1e+02  Score=5.48  Aligned_cols=1  Identities=100%  Similarity=1.199  Sum_probs=0.3

Q 5_Mustela         4 E    4 (6)
Q Consensus         4 e    4 (6)
                      |
T Consensus        35 E   35 (38)
T signal           35 E   35 (38)
T 46               34 E   34 (37)
T 49               36 E   36 (39)
T 57               33 E   33 (36)
T 63               33 E   33 (36)
T 90               33 E   33 (36)
T 33               33 E   33 (36)
T 72               37 E   37 (40)
T 20               42 E   42 (45)
Confidence            2


No 5  
>Q05046_32_Mito_ref_sig5_130
Probab=0.26  E-value=1e+02  Score=3.19  Aligned_cols=4  Identities=25%  Similarity=0.651  Sum_probs=0.0

Q 5_Mustela         2 GEEV    5 (6)
Q Consensus         2 geev    5 (6)
                      +.++
T Consensus        34 ~~~~   37 (37)
T signal           34 AKDV   37 (37)
T 119_Torulaspor   34 VDA-   36 (36)
T 56_Guillardia    34 PKG-   36 (36)
T 51_Chondrus      34 RNL-   36 (36)
T 120_Trichoplax   34 GVE-   36 (36)
T 34_Ostreococcu   34 YAK-   36 (36)


No 6  
>P12063_32_MM_ref_sig5_130
Probab=0.25  E-value=1e+02  Score=5.54  Aligned_cols=5  Identities=40%  Similarity=0.767  Sum_probs=0.0

Q 5_Mustela         2 GEEVA    6 (6)
Q Consensus         2 geeva    6 (6)
                      |-.||
T Consensus        12 ggsia   16 (37)
T signal           12 GGSIA   16 (37)
T 193              12 GGSVA   16 (37)


No 7  
>P10817_9_IM_ref_sig5_130
Probab=0.25  E-value=1.1e+02  Score=4.37  Aligned_cols=6  Identities=33%  Similarity=0.501  Sum_probs=0.0

Q 5_Mustela         1 LGEEVA    6 (6)
Q Consensus         1 lgeeva    6 (6)
                      |+.-.|
T Consensus         5 LsRglA   10 (14)
T signal            5 LSRSMA   10 (14)
T 14                5 LNRGLA   10 (14)
T 18                5 LSRGLA   10 (14)
T 22                5 LSRGLA   10 (14)
T 23                5 LGRGLA   10 (14)
T 46                5 LNRGFA   10 (14)
T 4                 5 LSRGLA   10 (14)


No 8  
>P51557_62_Mito_ref_sig5_130
Probab=0.25  E-value=1.1e+02  Score=4.41  Aligned_cols=6  Identities=17%  Similarity=-0.197  Sum_probs=0.0

Q 5_Mustela         1 LGEEVA    6 (6)
Q Consensus         1 lgeeva    6 (6)
                      .+++.+
T Consensus        61 ~~~~~~   66 (67)
T signal           61 QLEATL   66 (67)
T 64_Maylandia     62 KNAMVA   67 (67)
T 49_Columba       62 LGSRLE   67 (67)
T 77_Phytophthor   61 CGEIF-   65 (65)
T 76_Amphimedon    61 FDATL-   65 (65)
T cl|CABBABABA|1   61 ------   60 (60)
T cl|QABBABABA|1   61 ------   60 (60)
T 44_Ictidomys     60 ------   59 (59)


No 9  
>P26360_45_Mito_IM_ref_sig5_130
Probab=0.24  E-value=1.1e+02  Score=5.82  Aligned_cols=4  Identities=50%  Similarity=0.900  Sum_probs=0.0

Q 5_Mustela         3 EEVA    6 (6)
Q Consensus         3 eeva    6 (6)
                      ||.+
T Consensus        36 ~e~~   39 (50)
T signal           36 EEIG   39 (50)
T 181              31 EEQG   34 (45)
T 183              34 EEPV   37 (48)
T 170              37 PEMA   40 (51)
T 171              35 QEIA   38 (49)
T 182              36 EEQA   39 (50)
T 174              33 QEEG   36 (47)
T 178              30 VDQS   33 (44)
T 177              36 EQSS   39 (50)
T 168              38 GDIA   41 (52)


No 10 
>P15538_24_MitoM_ref_sig5_130
Probab=0.24  E-value=1.1e+02  Score=5.19  Aligned_cols=6  Identities=50%  Similarity=0.745  Sum_probs=0.0

Q 5_Mustela         1 LGEEVA    6 (6)
Q Consensus         1 lgeeva    6 (6)
                      ||...|
T Consensus        24 lgtraa   29 (29)
T signal           24 LGTRAA   29 (29)


No 11 
>P22142_42_IM_ref_sig5_130
Probab=0.23  E-value=1.2e+02  Score=3.98  Aligned_cols=3  Identities=33%  Similarity=0.324  Sum_probs=0.0

Q 5_Mustela         4 EVA    6 (6)
Q Consensus         4 eva    6 (6)
                      |.+
T Consensus        44 ~~~   46 (47)
T signal           44 EPS   46 (47)
T 180_Trichinell   28 ---   27 (27)
T 177_Loa          28 ---   27 (27)
T 164_Beta         28 ---   27 (27)
T 135_Galdieria    28 ---   27 (27)


No 12 
>P28350_44_Mito_Cy_ref_sig5_130
Probab=0.22  E-value=1.2e+02  Score=5.68  Aligned_cols=3  Identities=67%  Similarity=1.431  Sum_probs=0.0

Q 5_Mustela         2 GEE    4 (6)
Q Consensus         2 gee    4 (6)
                      ||.
T Consensus        46 ged   48 (49)
T signal           46 GED   48 (49)


No 13 
>P25348_71_Mito_ref_sig5_130
Probab=0.22  E-value=1.2e+02  Score=6.19  Aligned_cols=6  Identities=33%  Similarity=0.872  Sum_probs=0.0

Q 5_Mustela         1 LGEEVA    6 (6)
Q Consensus         1 lgeeva    6 (6)
                      .|...|
T Consensus         6 ~~~~~~   11 (76)
T signal            6 FGKQLA   11 (76)
T 21                1 ------    0 (63)
T 19                1 ------    0 (57)
T 20                1 ------    0 (52)
T 15                1 ------    0 (44)
T 17                1 ------    0 (34)
T 12                1 ------    0 (27)
T 14                1 ------    0 (38)
T 9                 1 ------    0 (49)
T 13                1 ------    0 (36)


No 14 
>P21839_50_MM_ref_sig5_130
Probab=0.22  E-value=1.2e+02  Score=5.78  Aligned_cols=5  Identities=20%  Similarity=-0.150  Sum_probs=0.0

Q 5_Mustela         2 G...E...EVA    6 (6)
Q Consensus         2 g...e...eva    6 (6)
                      |   +   +.+
T Consensus        42 ~...~...daa   46 (55)
T signal           42 Y...G...AAA   46 (55)
T 150              40 G...E...DAT   44 (53)
T 153              42 G...K...DAA   46 (55)
T 155              42 G...R...EGA   46 (55)
T 160              42 T...G...DAA   46 (55)
T 154              42 SeggG...VVA   49 (58)
T 164              42 D...A...DAT   46 (55)
T 112              42 G...ArggDAI   49 (58)
T 148              42 G...AgggNAI   49 (58)
T 127              42 R...E...NAT   46 (55)


No 15 
>P49448_53_MM_ref_sig5_130
Probab=0.21  E-value=1.2e+02  Score=4.45  Aligned_cols=4  Identities=75%  Similarity=0.551  Sum_probs=0.0

Q 5_Mustela         3 EEVA    6 (6)
Q Consensus         3 eeva    6 (6)
                      ++.|
T Consensus        55 ~~~~   58 (58)
T signal           55 ELVA   58 (58)
T 17_Sorghum       55 RVQ-   57 (57)
T 13_Coccomyxa     55 REV-   57 (57)
T 5_Ciona          55 IMR-   57 (57)
T cl|CABBABABA|1   55 DDG-   57 (57)
T cl|DABBABABA|1   55 LDY-   57 (57)
T cl|LABBABABA|1   55 INE-   57 (57)
T cl|RABBABABA|1   55 EGL-   57 (57)
T cl|TABBABABA|1   55 KPG-   57 (57)
T cl|VABBABABA|1   55 XYY-   57 (57)


No 16 
>P23004_14_IM_ref_sig5_130
Probab=0.21  E-value=1.3e+02  Score=4.46  Aligned_cols=2  Identities=100%  Similarity=0.966  Sum_probs=0.0

Q 5_Mustela         5 VA    6 (6)
Q Consensus         5 va    6 (6)
                      ||
T Consensus        18 vA   19 (19)
T signal           18 VA   19 (19)
T 83               18 VA   19 (19)
T 38               15 VA   16 (16)
T 44               15 TA   16 (16)
T 45               16 VA   17 (17)
T 46               15 VA   16 (16)
T 39               17 VA   18 (18)
T 47               16 VA   17 (17)
T 51               17 VA   18 (18)
T 40               19 AA   20 (20)


No 17 
>Q01992_33_MM_ref_sig5_130
Probab=0.21  E-value=1.3e+02  Score=5.29  Aligned_cols=6  Identities=17%  Similarity=-0.036  Sum_probs=0.0

Q 5_Mustela         1 LGEEVA    6 (6)
Q Consensus         1 lgeeva    6 (6)
                      ||.-+|
T Consensus        16 lgAgaa   21 (38)
T signal           16 RRAAAA   21 (38)
T 180               8 LGARAA   13 (38)
T 130               9 LGAGAA   14 (37)
T 102               6 LGAGVG   11 (36)
T 165               6 LGAGLA   11 (36)
T 168               6 LTARAS   11 (36)
T 182               6 LGAGVA   11 (36)
T 186               6 LGAGVA   11 (36)
T 164               6 LAARAA   11 (36)
T 169               6 LGAGAA   11 (36)


No 18 
>P22354_18_Mito_ref_sig5_130
Probab=0.20  E-value=1.3e+02  Score=4.69  Aligned_cols=5  Identities=40%  Similarity=0.826  Sum_probs=0.0

Q 5_Mustela         1 LGEEV    5 (6)
Q Consensus         1 lgeev    5 (6)
                      +|.-|
T Consensus         2 igrgv    6 (23)
T signal            2 IGRGV    6 (23)


No 19 
>P82919_34_Mito_ref_sig5_130
Probab=0.20  E-value=1.3e+02  Score=5.23  Aligned_cols=2  Identities=100%  Similarity=1.165  Sum_probs=0.0

Q 5_Mustela         4 EV    5 (6)
Q Consensus         4 ev    5 (6)
                      ||
T Consensus        37 EV   38 (39)
T signal           37 EV   38 (39)
T 45               37 EV   38 (39)
T 49               37 EV   38 (39)
T 52               36 EV   37 (38)
T 55               37 EV   38 (39)
T 64               37 EV   38 (39)
T 32               35 QV   36 (37)
T 33               33 EV   34 (35)
T 40               33 QV   34 (35)
T 39               34 QV   35 (36)


No 20 
>P13621_23_Mito_IM_ref_sig5_130
Probab=0.19  E-value=1.4e+02  Score=4.83  Aligned_cols=5  Identities=40%  Similarity=0.747  Sum_probs=0.0

Q 5_Mustela         1 LGEEV    5 (6)
Q Consensus         1 lgeev    5 (6)
                      |+..|
T Consensus         9 l~~qV   13 (28)
T signal            9 LSQQV   13 (28)
T 148               9 MIQQV   13 (28)
T 131               5 LGQQV    9 (24)
T 123               5 LGQKV    9 (24)
T 137               2 FSVKV    6 (21)
T 130               5 LPGKL    9 (24)
T 144               5 LSHKV    9 (24)
T 134               5 FALKV    9 (24)


Done!
