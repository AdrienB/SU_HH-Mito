import re, os, pickle, datetime
from multiprocessing import Process
from django.core.files.base import ContentFile
from django.shortcuts import render, redirect
from django.contrib.sessions.models import Session
from django.http import HttpResponse, StreamingHttpResponse
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from django.utils.safestring import mark_safe
from wsgiref.util import FileWrapper
from morfeus_app.forms import FirstForm, SecondForm
from morfsite.custom_methods import get_number_of_tasks, queue_the_task,\
generate_tag, ask_status, clear_record, start_session, get_session_dict,\
end_session, schedule_end_session
##
from morfsite.config import MAX_TASKS_QUEUED, SOME_PARAMETER
from morfsite.settings import BASE_DIR

TOOL_NAME = '<span class="tool-name">HH-MitoSig</span>'

@csrf_exempt
def index(request):
    return redirect('search/')

@csrf_exempt
def search(request):
    
    # Process an AJAX request (e.g., if checking of input is finished)
    if request.is_ajax():
        
        # Get the session key from the request
        # As the session key is stored at the client side, it needs a validity (injection safety) check before it can be used to fetch data from MySQL
        session_key = request.POST.get('session_key', '')
        session = None
        if re.search(r'^[0-9a-f]+$', session_key):
            session = get_session_dict(session_key)
        if not session:
            return HttpResponse('Connection timeout')
        
        # Get the job tag
        tag = session.get('tag', None)
        
        # Make a request to MySQL to check if the checking is finished
        status = ask_status(tag)
        if status == None:
            return HttpResponse('.RUNNING') #The checking is still running
        elif status == '': #The checking was successful
            
            # Get the user-defined parameters
            user_email = session.get('user_email', None)
            is_email = int(bool(user_email)) #0 if the user does not provide e-mail, 1 - otherwise
            if session.get('sent', None):
                clear_record(tag, 'prioritized')
                return HttpResponse('.SUCCESS {} {}'.format(tag, is_email))
            submit_time = session.get('submit_time', None)
            job_name = session.get('job_name', None)
            
            # Pickle the paraameters and register them with the main worker
            args = pickle.dumps( user_email, job_name, submit_time, tag)
            queue_the_task('heavy', args, tag)
            
            # Clean up
            end_session(session_key)
            clear_record(tag, 'prioritized')
            
            # Report success to the client and send relevant parameters to restore the state of the webpage and generate correct messages for the user
            return HttpResponse('.SUCCESS {} {}'.format(tag, is_email))
        
        else: #The checking completed with an error
            
            # Clean up
            end_session(session_key)
            clear_record(tag, 'prioritized')
            
            # Report the error to the client
            return HttpResponse(status)
    
    # Generating empty forms (in their default state)
    first_form = FirstForm()
    second_form = SecondForm()

    # Process GET request
    # This is usually the initial request to the server, so the default clean page is returned
    if not request.POST:
        dict_ = {'tool': mark_safe(TOOL_NAME),
                 'first_form': first_form,
                 'second_form': second_form}
        return render(request, 'morfeus_app/index.html', dict_)
    
    # Below is the processing of non-GET and non-AJAX requests, which should thus be POST
    # Such a request comes usually when the user clicks the submit button
    
    # Getting the working mode based on which exactly form was submitted
    first_form = FirstForm()
    second_form = SecondForm()
    mode = request.POST['form_type'].split('_')[0].upper()+'_MODE'
    input_file = None
    text_input1 = False
    text_input2 = False
    form_error = ''       # Establish the varibale for the error status

    # Check for server overload
    if get_number_of_tasks('heavy') >= MAX_TASKS_QUEUED:
        message = 'The server is overloaded now. Please try again later'
        messages.info(request, message)
        dict_ = {'tool': mark_safe(TOOL_NAME),
                 'mode': mode,
                 'first_form': first_form,
                 'second_form': second_form}
        return render(request, 'morfeus_app/index.html', dict_)
    

    # Process submission of main forms
    if mode == 'FIRST_MODE':
        first_form = FirstForm(data = request.POST, files = request.FILES)
        if first_form.is_valid(): #Check if the form passed the field validators
            input_fasta_text = first_form.cleaned_data['input_text1'] or ''
            if input_fasta_text:
                text_input1 = True
                input_file = ContentFile(input_fasta_text.encode()) #Save the content from the input text field as a virtual file
            user_email = first_form.cleaned_data['user_email'] or ''
            job_name = first_form.cleaned_data['job_name'] or ''
        else:
            for key in first_form.errors:
                form_error = '{} {}\n'.format(key, first_form.errors[key])
    elif mode == 'SECOND_MODE':
        second_form = SecondForm(data = request.POST, files = request.FILES)
        if second_form.is_valid():
            input_fasta_text = second_form.cleaned_data['input_text2'] or ''
            if input_fasta_text:
                text_input2 = True
                input_file = ContentFile(input_fasta_text.encode()) #Save the content from the input text field as a virtual file
            if 'second_form_file' in request.FILES:
                if text_input2:
                    form_error =\
                    'Select ONLY one input method: either text field or file'
                    ##
                input_file = request.FILES['second_form_file']
            user_email = second_form.cleaned_data['user_email'] or ''
            job_name = second_form.cleaned_data['job_name'] or ''
        else:
            for key in second_form.errors:
                form_error = '{} {}\n'.format(key, second_form.errors[key])

    # Make error meassages from the field validators tidier
    if form_error:
        form_error = re.sub(r'<.*?>', '', form_error)
        form_error = form_error.replace('input_text1', 'The input text:')
        form_error = form_error.replace('input_text2', 'The input text:')
        form_error = form_error.replace('second_form_file', 'The input file:')
        form_error = form_error.replace('user_email', 'E-mail:')
        messages.info(request, form_error)
        dict_ = {'tool': mark_safe(TOOL_NAME),
                 'mode': mode,
                 'first_form': first_form,
                 'second_form': second_form}
        return render(request, 'morfeus_app/index.html', dict_)
    
    try:
        
        # Generate auxiliary session information
        submit_time  = str(datetime.datetime.utcnow())[: -7] + ' UTC'
        tag = generate_tag(user_email)
        
        # Pickle the parameters for the cheking and register them with the checking worker
        args = pickle.dumps(mode, input_fasta_text, input_file, tag)
        queue_the_task('prioritized', args, tag)
        
        # Generate new session and fill it with relevant parameters
        session = {}
        session['mode'] = mode
        session['user_email'] = user_email
        session['job_name'] = job_name
        session['tag'] = tag
        session['submit_time'] = submit_time
        session['sent'] = False
        # Generate session key (the tag will be for now invidible for the user) and put the session in MySQL
        session_key = generate_tag('session_tag')
        start_session(session_key, pickle.dumps(session))
        
        # Start a process that will ensure the cleaning up of the database in case the client is disconnected (DoS security)
        Process(target = schedule_end_session, args = (session_key, 30)).start()
        
        # Return the 'Please wait...' page
        dict_ = {'tool': mark_safe(TOOL_NAME),
                 'mode': mode,
                 'session_key': session_key, #Presence of the session key will be the flag to the corresponding JavaScript at the client side
                 'first_form': first_form,
                 'second_form': second_form}
        return render(request, 'morfeus_app/index.html', dict_)
    
    except Exception as e: #Can occur if the MySQL server is down
        messages.info(request, str("Server internal error"))
        dict_ = {'tool': mark_safe(TOOL_NAME),
                 'mode': mode,
                 'first_form': first_form,
                 'second_form': second_form}
        return render(request, 'morfeus_app/index.html', dict_)

def guide(request):
    dict_ = {'inner_link': request.GET.get('l', '')}
    return render(request, 'morfeus_app/guide.html', dict_)

def about(request):
    dict_ = {'tool': mark_safe(TOOL_NAME)}
    return render(request, 'morfeus_app/about.html', dict_)

def contact(request):
    return render(request, 'morfeus_app/contact.html')

# For requests to expired or never existent result pages
def output_not_found(request):
    dict_ = {'output': True}
    return render(request, 'morfeus_app/not_found.html', dict_)

# For request to absent downloadable files
def file_not_found(request):
    return render(request, 'morfeus_app/not_found.html')

# For requests to other non-existent pages
def custom_404(request):
    return render(request, 'morfeus_app/404.html')
