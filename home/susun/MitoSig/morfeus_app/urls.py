from django.conf.urls import url
from morfeus_app import views

urlpatterns = [
    url(r'^$', views.index),
    url(r'^search/$', views.search),
    url(r'^guide/$', views.guide),
    url(r'^about/$', views.about),
    url(r'^contact/$', views.contact),
    url(r'^file_not_found/$', views.file_not_found),
    url(r'^output_not_found/$', views.output_not_found)
]

handler404 = 'custom_404'

