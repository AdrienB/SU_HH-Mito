from django import forms
from django.db import models
from django.core.validators import validate_email
from django.core.exceptions import ValidationError

# Custom validators
def custom_validate_email(email):
    """Function for additional e-mail validation"""
    #if not email.endswith('biochem.mpg.de'):
        #raise ValidationError('Only internal MPIB e-mails are allowed')
        
def custom_validator_job_name(job_name):
    """Function for additional job name validation"""
    if '"' in job_name:
        raise ValidationError('Double quotes are not allowed in job names')

def custom_validate_file(file_):
    """Function for additional uploaded file validation"""
    if file_.size > 1 * 1024 * 1024:
        raise ValidationError('The uploaded file is too large')


class FirstForm(forms.Form):
    form_type = forms.CharField(initial = 'first_form')
    input_text1 = forms.CharField(widget = forms.Textarea)
    user_email = forms.EmailField(validators = [validate_email,\
    custom_validate_email], required = False)
    ##
    job_name = forms.CharField(max_length = 50, required = False)

class SecondForm(forms.Form):
    form_type = forms.CharField(initial = 'second_form')
    input_text2 = forms.CharField(widget = forms.Textarea)
    second_form_file = forms.FileField(validators = [custom_validate_file], required = False) #By default, required = True
    user_email = forms.EmailField(validators = [validate_email,\
    custom_validate_email], required = False)
    ##
    job_name = forms.CharField(max_length = 50, required = False)
