function adjustContentMargin() {
    var headerHeight=Math.round($("#header").outerHeight());
    $(".content").css("margin-top", headerHeight+1);
}
$(document).ready(function () {
    adjustContentMargin();
});
$(window).resize( function() {
    adjustContentMargin();
});
