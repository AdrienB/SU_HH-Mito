from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from morfsite import settings

admin.autodiscover()

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^', include('morfeus_app.urls', namespace="morfeus_app")),
] +  static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)
                                                                       
