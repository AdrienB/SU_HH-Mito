#Server parameters
MAX_TASKS_QUEUED = 5
SOME_PARAMETER = 3

#Paths
URL_ROOT = '/mitosig'
SERVER_URL = 'http://montblanc.biochem.mpg.de' + URL_ROOT
ADMIN_EMAIL = 'susun@biochem.mpg.de'
PYTHON_EXE = '/home/prytuliak/env/bin/python'
HHSUITE_ROOT = '/home/prytuliak/applications/hh-suite'

#MySQL parameters
MYSQL_HOST = 'matterhorn'
MYSQL_USER = 'root'
MYSQL_PASSWORD = 'n0d4rks1d3'
MYSQL_DB = 'mitosig_backend_db'

#Mail settings
MAIL_SENDER_EMAIL = 'susun@biochem.mpg.de'
MAIL_SERVER = 'mail.biochem.mpg.de'
MAIL_PORT = 587
