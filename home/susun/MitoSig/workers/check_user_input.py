from __future__ import absolute_import
import sys, os, shutil, zipfile, time, pickle, re, traceback
from getpass import getpass
from multiprocessing import Process
from mysql.connector import errors as MySQLErrors
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + '/'
sys.path.append(BASE_DIR)
from morfsite.custom_methods import prepare_input_fasta, connect_status_database, clear_record,\
ask_status, send_mail_wrapped
from morfsite.config import ADMIN_EMAIL
#MAIL_PASSWORD = getpass("Enter the mailbox password: ")

if (len(sys.argv) != 2) or (not re.search('^[0-9]+$', sys.argv[1])):
    sys.stderr.write('Usage: python {} NUM_OF_PROCESSES'.format(sys.argv[0]))
    sys.exit(1)

def write_status(tag, status, log_message = '', success = False):
    """
    Method to write result status into the database
    """
    log_file = BASE_DIR + 'logs/{}.log.txt'.format(tag)
    input_dir = BASE_DIR + 'inputs/' + tag
    with open(log_file, 'a') as log:
        log.write((log_message if log_message else status) + '\n')
    mysql_conn = connect_status_database()
    cmd = "UPDATE prioritized_queue SET status='{}' WHERE tag='{}'"
    cmd = cmd.format(status, tag)
    mysql_conn.cursor().execute(cmd)
    mysql_conn.commit()
    mysql_conn.close()
    if not success:
        shutil.rmtree(input_dir)
    curtime = time.strftime('%Y-%m-%d %H:%M:%S')
    print('{}: Completed checking for the task with tag {}. Status: \'{}\''.\
    format(curtime, tag, status))
    ##
    
    # Clean up in case of lost connection
    time.sleep(30)
    if ask_status(tag) != 'Connection timeout':
        clear_record(tag, 'prioritized')
        if success:
            shutil.rmtree(input_dir)
            shutil.rmtree(BASE_DIR + 'outputs/' + tag)


def check_firstform_input(tag, input_dir, text_input = False):
    """
    Method for checking user input submitted through the first form
    """
    
    # Generating additional folders
    input_fasta_dir = input_dir + 'input_fasta/'
    os.mkdir(input_fasta_dir)
    
    # Copying input file
    input_fasta = input_dir + 'input.fasta'
    if text_input:
        with open(input_fasta, 'wb+') as ofile:
                ofile.write(text_input)
    else:
        sample_file = BASE_DIR + 'downloads/TRG_LysEnd_APsAcLL_3.fasta'
        shutil.copy(sample_file, input_fasta)
    
    # Extracting and checking for validity the input protein set
    try:
        real_name = 'input field' if text_input else 'FASTA file'
        prepare_input_fasta(input_fasta, '/dev/null', output_dir =\
        input_fasta_dir, prots_min = 1, real_name = real_name)
        ##
    except UnicodeDecodeError as e:
        message = "The input file is not FASTA"
        write_status(tag, message)
        return False
    except IOError as e:
        write_status(tag, str(e))
        return False
    except Exception as e:
        message = ''.join(traceback.format_exception(*sys.exc_info()))
        log_file = BASE_DIR + 'logs/{}.log.txt'.format(tag)
        send_mail_wrapped(log_file, MAIL_PASSWORD, ADMIN_EMAIL, message,
                          'Server Error')
        write_status(tag, 'Server internal error', message)
        return False
    
    # Success
    return True


def check_secondform_input(tag, input_file, input_dir, text_input = False):
    """
    Method for checking user input submitted through proteome search form
    """

    # Generating additional folders
    input_fasta_dir = input_dir + 'input_fasta/'
    os.mkdir(input_fasta_dir)

    # Copying input file
    input_fasta = input_dir + 'input.fasta'
    if input_file:
        if input_file._size > 1 * 1024 * 1024:
            message = "The input protein set file is too large. "
            message += "Max. allowed size is 1 MB"
            write_status(tag, message)
            return False
        with open(input_fasta, 'wb+') as ofile:
            for chunk in input_file.chunks():
                ofile.write(chunk)

    elif text_input:
        with open(input_fasta, 'wb+') as ofile:
                ofile.write(text_input)
    else:
        sample_file = BASE_DIR + 'downloads/TRG_LysEnd_APsAcLL_3.fasta'
        shutil.copy(sample_file, input_fasta)

    

    # Extracting and checking for validity the input protein set
    try:
        real_name = 'input field' if text_input else 'FASTA file'
        prepare_input_fasta(input_fasta, '/dev/null', output_dir =\
        input_fasta_dir, real_name = real_name)
        ##


    except UnicodeDecodeError as e:
        message = "The input file is not FASTA"
        write_status(tag, message)
        return False
    except IOError as e:
        write_status(tag, str(e))
        return False
        message = ''.join(traceback.format_exception(*sys.exc_info()))
        log_file = BASE_DIR + 'logs/{}.log.txt'.format(tag)
        send_mail_wrapped(log_file, MAIL_PASSWORD, ADMIN_EMAIL, message,
                          'Server Error')
        write_status(tag, 'Server internal error', message)
        return False

    # Success
    return True


def check_user_input(args):
    """
    Method for validity checking of input files provided by the user.
    
    It also will place and name the files in the manner that they can be read
    by the main script
    """

    # Preparing for start
    starttime = time.strftime('%Y-%m-%d %H:%M:%S')
    mode, text_input, second_form_file, startsite, stopsite, tag =\
    pickle.loads(args)
    ##
    log_file = BASE_DIR + 'logs/{}.log.txt'.format(tag)
    print('{}: Received for checking the task with tag {}'.format(starttime, tag))
    with open(log_file, 'a') as log:
        log.write('Input check started\n')
    
    # Generating input folder
    input_dir = '{}inputs/{}/'.format(BASE_DIR, tag)
    try:
        os.mkdir(input_dir)
    except:
        write_status(tag, 'Duplicate request')
        return
    
    # Directing to the corresponding processing method
    if mode == 'FIRST_MODE':
        if not check_firstform_input(tag, input_dir, input_text):
            return
    elif mode == 'SECOND_MODE':
        if not check_secondform_input(tag, second_form_file, input_dir, input_text):
            return
    else:
        write_status(tag, 'Server internal error', 'Error: Unknown mode')
        return
    
    # Generating waiting HTML in case of successful check
    template = os.path.join(BASE_DIR,
                            'morfeus_app/templates/morfeus_app/queued.html')
    output_html_dir = BASE_DIR + 'outputs/{}/'.format(tag)
    os.mkdir(output_html_dir)
    target = output_html_dir + 'results.html'
    shutil.copy(template, target)
    
    #print(os.listdir(input_dir))##/
    
    # Report success
    write_status(tag, '', 'Input check successfully completed', success = True)
    


def main(i):
    curtime = time.strftime('%Y-%m-%d %H:%M:%S')
    print('{}: Worker {} is ready'.format(curtime, i))
    #mysql_conn = connect_status_database()
    while True:
        result = None
        try:
            #if not mysql_conn.is_connected():
            mysql_conn = connect_status_database()
            cmd = "SELECT args FROM prioritized_queue " +\
            "WHERE worker={} AND status IS NULL"
            ##
            cmd = cmd.format(i)
            cursor = mysql_conn.cursor()
            cursor.execute(cmd)
            result = cursor.fetchall()
            mysql_conn.commit()
            if not result:
                cmd = "SELECT tag INTO @tag{} FROM prioritized_queue " +\
                "WHERE worker=-1 ORDER BY submitted LIMIT 1 FOR UPDATE"
                ##
                cmd = cmd.format(i)
                mysql_conn.cursor().execute(cmd)
                cmd =\
                "UPDATE prioritized_queue SET worker={i} WHERE tag=@tag{i}"
                ##
                cmd = cmd.format(i = i)
                mysql_conn.cursor().execute(cmd)
                mysql_conn.commit()
                cmd = "SELECT args FROM prioritized_queue " +\
                "WHERE worker={} AND status IS NULL"
                ##
                cmd = cmd.format(i)
                cursor = mysql_conn.cursor()
                cursor.execute(cmd)
                result = cursor.fetchall()
            mysql_conn.close()
        except KeyboardInterrupt:
            sys.exit()
        if result:
            args = result[0][0]
            check_user_input(args)
        time.sleep(1)

for i in range(int(sys.argv[1])):
    p = Process(target = main, args = (i, ))
    p.start()
